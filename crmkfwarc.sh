#!/bin/bash
##
## create coolrevo firm archive file.
## Please execute this script as root.
##

FIRMTOPDIR=/firminstruct
ARCFILEDIR=${FIRMTOPDIR}/fwarchive
WGFIRMARCHIVE=${ARCFILEDIR}/smc6a_fwup.tgz
WGFIRMVERSION=${ARCFILEDIR}/smc6afirmversion


CREATEFIRMVERSION=${FIRMTOPDIR}/mtd/coolrevo/etc/smc6afirmversion

TRGSRVSSHKEY=/home/rmtods/rmtodssite7tv_private.pem
TRGSRVSSH="rmtods@camfwup.treasure-tv.jp"
SSHOPT="-oServerAliveInterval=15 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oPreferredAuthentications=publickey -oIdentitiesOnly=yes"
TRGSRVDIR=/var/www/vhosts/camfwup.treasure-tv.jp/firm_smc6a

FIRMVER=""
CRONLY=0
UPTOSRV=0
KVS=0
HELP=0
for i in $*
do
  case "${i}" in
    "-cronly")
      CRONLY=1 ;;
    "-ups")
      UPTOSRV=1 ;;
    "-kvs")
      KVS=1 ;;
    -h*)
      HELP=1 ;;
    *)
      FIRMVER="${i}" ;;
  esac
done

if [ "${FIRMVER}" = "" -o ${HELP} -eq 1 ]; then
  echo "usage: `basename $0` [-cronly|-ups] <newfirm version>"
  echo "  -cronly: coolrevo/ folders only (without /) archives."
  echo "  -ups   : transfer to wgfwup server."
  echo "  -kvs   : contains kvs modules and libraries."
  echo " newfirm version :input newfirmware version."
  exit 1
fi

CURDIR=`pwd`
cd ${FIRMTOPDIR}

echo ${FIRMVER} > ${CREATEFIRMVERSION}
echo "create firmware archive." >> ${CREATEFIRMVERSION}

PREEXEC_FILE=/mtd/coolrevo/etc/pre_exec.sh
PREEXEC_TMP_FILE=/tmp/pre_exec_sh_tmp
if [ -f ${PREEXEC_TMP_FILE} ]; then
  rm -f ${PREEXEC_TMP_FILE}
fi
COOLREVO_TMP_ARCS=./mtd_coolrevo_tmp.tgz
if [ -f ${COOLREVO_TMP_ARCS} ]; then
  rm -f ${COOLREVO_TMP_ARCS}
fi
TMPARC_STAT=1

if [ -e ./mtd/coolrevo/ -a -d ./mtd/coolrevo/ ]; then
  if [ ${CRONLY} -eq 1 ]; then
    TRGLISTS="\
  ./mtd/coolrevo/. \
  "
  else
    TRGLISTS="\
  ./etc/init.d/S98coolrevo \
  ./mtd/coolrevo/. \
  "
  fi


  tar zcvf ${COOLREVO_TMP_ARCS} ./mtd/coolrevo/.
  TMPARC_STAT=$?
  if [ ${TMPARC_STAT} -eq 0 ]; then
    ## delete not nosessary files
    if [ -d ./mtd/coolrevo/share/doc ]; then
      rm -rf ./mtd/coolrevo/share/doc
    fi
    if [ -d ./mtd/coolrevo/share/man ]; then
      rm -rf ./mtd/coolrevo/share/man
    fi
    if [ -f ./mtd/coolrevo/bin/ssh-keyscan ]; then
      rm -rf ./mtd/coolrevo/bin/ssh-keyscan
    fi
    for i in ./mtd/coolrevo/lib/lib*.a
    do
      rm -f ${i}
    done
    if [ ${KVS} -eq 0 ]; then
      if [ -f ./mtd/coolrevo/usr/sbin/udpts2kvs ]; then
        rm -f ./mtd/coolrevo/usr/sbin/udpts2kvs
      fi
      if [ -f ./mtd/coolrevo/usr/sbin/udpts2kvsrtc ]; then
        rm -f ./mtd/coolrevo/usr/sbin/udpts2kvsrtc
      fi
      if [ -f ./mtd/coolrevo/bin/ffmpeg_kvs ]; then
        rm -f ./mtd/coolrevo/bin/ffmpeg_kvs
      fi
      if [ -d ./mtd/coolrevo/etc/kvs ]; then
        rm -rf ./mtd/coolrevo/etc/kvs
      fi
      if [ -f ./mtd/coolrevo/lib/libkvsWebrtcSignalingClient.so ]; then
        rm -f ./mtd/coolrevo/lib/libkvsWebrtcSignalingClient.so
      fi
      if [ -f ./mtd/coolrevo/lib/libkvsWebrtcClient.so ]; then
        rm -f ./mtd/coolrevo/lib/libkvsWebrtcClient.so
      fi
      if [ -f ./mtd/coolrevo/lib/libwebsockets.so ]; then
        rm -f ./mtd/coolrevo/lib/libwebsockets.so
      fi
      if [ -f ./mtd/coolrevo/lib/libwebsockets.so.18 ]; then
        rm -f ./mtd/coolrevo/lib/libwebsockets.so.18
      fi
      if [ -f ./mtd/coolrevo/lib/libcproducer.so ]; then
        rm -f ./mtd/coolrevo/lib/libcproducer.so
      fi
      if [ -f ./mtd/coolrevo/lib/libssl.so.1.1 ]; then
        rm -f ./mtd/coolrevo/lib/libssl.so.1.1
      fi
      if [ -f ./mtd/coolrevo/lib/libcrypto.so.1.1 ]; then
        rm -f ./mtd/coolrevo/lib/libcrypto.so.1.1
      fi

      if [ -f ${PREEXEC_FILE} ]; then
        cp -fp ${PREEXEC_FILE} ${PREEXEC_TMP_FILE}
        cat ${PREEXEC_FILE} |\
        awk '{
          print $0;
          if (index($0,"TOPDIR=")==1){
            printf "if [ -f ${TOPDIR}/usr/sbin/udpts2kvs ]; then\n  rm -f ${TOPDIR}/usr/sbin/udpts2kvs\nfi\n";
            printf "if [ -f ${TOPDIR}/usr/sbin/udpts2kvsrtc ]; then\n  rm -f ${TOPDIR}/usr/sbin/udpts2kvsrtc\nfi\n";
            printf "if [ -f ${TOPDIR}/bin/ffmpeg_kvs ]; then\n  rm -f ${TOPDIR}/bin/ffmpeg_kvs\nfi\n";
            printf "if [ -d ${TOPDIR}/etc/kvs ]; then\n  rm -rf ${TOPDIR}/etc/kvs\nfi\n";
            printf "if [ -f ${TOPDIR}/lib/libkvsWebrtcSignalingClient.so ]; then\n  rm -f ${TOPDIR}/lib/libkvsWebrtcSignalingClient.so\nfi\n";
            printf "if [ -f ${TOPDIR}/lib/libkvsWebrtcClient.so ]; then\n  rm -f ${TOPDIR}/lib/libkvsWebrtcClient.so\nfi\n";
            printf "if [ -f ${TOPDIR}/lib/libwebsockets.so ]; then\n  rm -f ${TOPDIR}/lib/libwebsockets.so\nfi\n";
            printf "if [ -f ${TOPDIR}/lib/libwebsockets.so.18 ]; then\n  rm -f ${TOPDIR}/lib/libwebsockets.so.18\nfi\n";
            printf "if [ -f ${TOPDIR}/lib/libcproducer.so ]; then\n  rm -f ${TOPDIR}/lib/libcproducer.so\nfi\n";
            printf "if [ -f ${TOPDIR}/lib/libssl.so.1.1 ]; then\n  rm -f ${TOPDIR}/lib/libssl.so.1.1\nfi\n";
            printf "if [ -f ${TOPDIR}/lib/libcrypto.so.1.1 ]; then\n  rm -f ${TOPDIR}/lib/libcrypto.so.1.1\nfi\n";
          }
        }' > ${PREEXEC_FILE}.new
        if [ $? -eq 0 ]; then
          mv -f ${PREEXEC_FILE}.new ${PREEXEC_FILE}
        else
          if [ -f ${PREEXEC_TMP_FILE} ]; then
            rm -f ${PREEXEC_TMP_FILE}
          fi
        fi
      fi
    fi
  else
    if [ -f ${COOLREVO_TMP_ARCS} ]; then
      rm -f ${COOLREVO_TMP_ARCS}
    fi
  fi

else
  echo "./mtd/coolrevo/ not exists."
fi

TMPWGFIRMVERSION=${WGFIRMVERSION}
TMPWGFIRMARCHIVE=${WGFIRMARCHIVE}
#if [ ${NOCACHEDIR} -eq 1 ]; then
#  TMPWGFIRMVERSION=${NCDWGFIRMVERSION}
#  TMPWGFIRMARCHIVE=${NCDWGFIRMARCHIVE}
#fi
tar cvf - ${TRGLISTS} | gzip -c > ${TMPWGFIRMARCHIVE}
STAT=$?
if [ ${STAT} -ne 0 ]; then
  echo "firmware archive failed. STAT=${STAT}"
  if [ ${TMPARC_STAT} -eq 0 -a -f ${COOLREVO_TMP_ARCS} ]; then
    tar zxvf ${COOLREVO_TMP_ARCS} ./
  fi
  if [ -f ${PREEXEC_TMP_FILE} ]; then
    rm -f ${PREEXEC_TMP_FILE}
  fi
  exit ${STAT}
fi

if [ ${TMPARC_STAT} -eq 0 -a -f ${COOLREVO_TMP_ARCS} ]; then
  tar zxvf ${COOLREVO_TMP_ARCS} ./
fi

if [ -f ${PREEXEC_TMP_FILE} ]; then
  rm -f ${PREEXEC_TMP_FILE}
fi

MD5=`md5sum ${TMPWGFIRMARCHIVE} | awk '{print $1}'`
echo ${FIRMVER} > ${TMPWGFIRMVERSION}
echo ${MD5} >> ${TMPWGFIRMVERSION}

echo "${TMPWGFIRMVERSION}"
cat ${TMPWGFIRMVERSION}
ls -aClF ${TMPWGFIRMARCHIVE}

if [ ${UPTOSRV} -eq 1 ]; then
  #ssh -i ${TRGSRVSSHKEY} ${SSHOPT} ${TRGSRVSSH} "test -d ${TRGSRVDIR}/${FIRMVER}"
  ssh ${SSHOPT} ${TRGSRVSSH} "test -d ${TRGSRVDIR}/${FIRMVER}"
  stat=$?
  if [ ${stat} -ne 0 ]; then
    #ssh -i ${TRGSRVSSHKEY} ${SSHOPT} ${TRGSRVSSH} "mkdir ${TRGSRVDIR}/${FIRMVER}"
    ssh ${SSHOPT} ${TRGSRVSSH} "mkdir ${TRGSRVDIR}/${FIRMVER}"
    stat2=$?
    if [ ${stat2} -ne 0 ]; then
      echo "cannot create dir:${TRGSRVDIR}/${FIRMVER}"
      exit 10
    fi
  fi
  #scp -i ${TRGSRVSSHKEY} ${SSHOPT} ${TMPWGFIRMVERSION} ${TRGSRVSSH}:${TRGSRVDIR}/${FIRMVER}/.
  scp ${SSHOPT} ${TMPWGFIRMVERSION} ${TRGSRVSSH}:${TRGSRVDIR}/${FIRMVER}/.
  stat3=$?
  if [ ${stat3} -ne 0 ]; then
    echo "cannot copy ${TMPWGFIRMVERSION}"
    exit 11
  fi
  #scp -i ${TRGSRVSSHKEY} ${SSHOPT} ${TMPWGFIRMARCHIVE} ${TRGSRVSSH}:${TRGSRVDIR}/${FIRMVER}/.
  scp ${SSHOPT} ${TMPWGFIRMARCHIVE} ${TRGSRVSSH}:${TRGSRVDIR}/${FIRMVER}/.
  stat4=$?
  if [ ${stat4} -ne 0 ]; then
    echo "cannot copy ${TMPWGFIRMARCHIVE}"
    exit 12
  fi
fi

exit 0
