#!/bin/sh

unset LANG
unset LC
unset LC_ALL
unset LANGUAGE

PATH=/usr/local/bin:/usr/bin:/usr/sbin:/bin:/sbin
ACCOUNT=tunnel
KEY_REPO=/home/${ACCOUNT}/.ssh
AUTHORIZED_KEYS=${KEY_REPO}/authorized_keys
CSSERVERS="109 113"

ADDR_SELF=`ip -f inet -4 -o addr show eth1|sed -e 's/^.*\?inet \([0-9]\+\.\)\{3\}\([0-9]\+\)\/[0-9]\+ brd \(.*\)$/\2/g'`

CMD=$1
UNIQUE_KEY=$2
HW_ADDR=$3


if [ ! -d "${KEY_REPO}" ]; then
  mkdir -p "${KEY_REPO}"
  chown ${ACCOUNT}:${ACCOUNT} "${KEY_REPO}"
  chmod 700 "${KEY_REPO}"
fi
if [ ! -f "${AUTHORIZED_KEYS}" ]; then
  touch "${AUTHORIZED_KEYS}"
  chown ${ACCOUNT}:${ACCOUNT} "${AUTHORIZED_KEYS}"
  chmod 600 "${AUTHORIZED_KEYS}"
fi

if [ $# -ne 3 -o $1 = "" -o $2 = "" -o $3 = "" ]; then
  echo "Usage: $0 [generate|expire] UNIQUE_KEY HW_ADDR"
  exit 1
fi


merge()
{
  if [ -f "${KEY_REPO}/${UNIQUE_KEY}" ]; then
    echo "[error] same named key already exists" 2>&1
    exit 1
  fi

  while read line
  do
    echo $line >> "${KEY_REPO}/${UNIQUE_KEY}"
  done
  chown ${ACCOUNT}:${ACCOUNT} ${KEY_REPO}/${UNIQUE_KEY}
  chmod 600 ${KEY_REPO}/${UNIQUE_KEY}

  pubkey=`/usr/bin/ssh-keygen -y -f ${KEY_REPO}/${UNIQUE_KEY}`
  echo "${pubkey} ${HW_ADDR}" > ${KEY_REPO}/${UNIQUE_KEY}.pub
  chown ${ACCOUNT}:${ACCOUNT} ${KEY_REPO}/${UNIQUE_KEY}.pub
  chmod 644 ${KEY_REPO}/${UNIQUE_KEY}.pub
  cat ${KEY_REPO}/${UNIQUE_KEY}.pub >> ${AUTHORIZED_KEYS}
}

purge()
{
  if [ -f "${KEY_REPO}/${UNIQUE_KEY}" ]; then
    rm -f ${KEY_REPO}/${UNIQUE_KEY}
    rm -f ${KEY_REPO}/${UNIQUE_KEY}.pub
    sed -i "/${HW_ADDR}/d" ${AUTHORIZED_KEYS}
  fi
}

rgenerate()
{
  retval=
  clone_src=$1
  for ip in ${CSSERVERS}; do
    if [ "${ip}" != "${ADDR_SELF}" ]; then
      sibling_host="172.29.10.${ip}"
      cat ${clone_src} | ssh -p22 -l rmtods \
      -o LogLevel=quiet \
      -o PreferredAuthentications=publickey \
      -o IdentitiesOnly=yes \
      -i /data/www/pki/cameracs_rmt.key \
      ${sibling_host} /usr/local/bin/tunnelkey merge ${UNIQUE_KEY} ${HW_ADDR} -
      if [ $? != 0 ]; then
        retval="${sibling_host} merge error. \n${retval}"
      fi
    fi
  done
  if [ -n "${retval}" ]; then
    echo "${retval}" 2>&1
    exit 2
  fi
}

rexpire()
{
  retval=
  for ip in ${CSSERVERS}; do
    if [ "${ip}" != "${ADDR_SELF}" ]; then
      sibling_host="172.29.10.${ip}"
      ssh -p22 -l rmtods \
      -o LogLevel=quiet \
      -o PreferredAuthentications=publickey \
      -o IdentitiesOnly=yes \
      -i /data/www/pki/cameracs_rmt.key \
      ${sibling_host} /usr/local/bin/tunnelkey purge ${UNIQUE_KEY} ${HW_ADDR}
      if [ $? != 0 ]; then
        retval="${sibling_host} purge error. \n${retval}"
      fi
    fi
  done
  if [ -n "${retval}" ]; then
    echo "${retval}" 2>&1
    exit 2
  fi
}


generate()
{
  if [ -f "${KEY_REPO}/${UNIQUE_KEY}" ]; then
    echo "[error] same named key already exists" 2>&1
    exit 1
  fi

  ssh-keygen -t rsa -b 2048 -f "${KEY_REPO}/${UNIQUE_KEY}" -q -N "" -C "${HW_ADDR}"
  if [ $? -ne 0 ]; then
    echo "[error] generate ssh-key failed" 2>&1
    exit 1
  fi

  if [ -f "${KEY_REPO}/${UNIQUE_KEY}.pub" ]; then
    cat "${KEY_REPO}/${UNIQUE_KEY}.pub" >> "${AUTHORIZED_KEYS}"
    if [ $? -ne 0 ]; then
      echo "[error] adding ssh-pubkey failed" 2>&1
      exit 1
    fi
    chown ${ACCOUNT}:${ACCOUNT} ${KEY_REPO}/${UNIQUE_KEY}*
    chmod 600 ${KEY_REPO}/${UNIQUE_KEY}
    chmod 644 ${KEY_REPO}/${UNIQUE_KEY}.pub

    rgenerate ${KEY_REPO}/${UNIQUE_KEY}
  fi

  cat ${KEY_REPO}/${UNIQUE_KEY}
}

expire()
{
  if [ -f "${KEY_REPO}/${UNIQUE_KEY}" ]; then
    rm -f ${KEY_REPO}/${UNIQUE_KEY}
    rm -f ${KEY_REPO}/${UNIQUE_KEY}.pub
    sed -i "/${HW_ADDR}/d" ${AUTHORIZED_KEYS}

    rexpire ${KEY_REPO}/${UNIQUE_KEY}
  fi
}


case "$1" in
  generate|gen|add|new)
    generate $2 $3 $4
  ;;
  expire|del)
    expire $2 $3 $4
  ;;
  merge)
    merge $2 $3 $4
  ;;
  purge)
    purge $2 $3 $4
  ;;
  *)
    echo "Usage: $0 [generate|expire] UNIQUE_KEY HW_ADDR"
  ;;
esac

exit 0
