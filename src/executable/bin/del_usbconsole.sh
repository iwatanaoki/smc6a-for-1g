#!/bin/sh

# USAGE
#   del_usbconsole.sh
#
#   kill usbconsole, if the USB serial device
#   has disconnected.
#   called from udev rule.
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   1.0.31

unset LANG
unset LC
unset LC_ALL

kill -15 $(ps|grep usbconsole|grep -v grep|grep -v del|awk '{print $1}')

exit 0
