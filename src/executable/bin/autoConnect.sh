#!/bin/sh

# USAGE
#   autoConnect.sh
#
#   status change script for auto startup connect mode .
#
# ARGUMENTS
#   ON  -> auto connect mode ON(ffmpeg relay RTMP connect 
#               to smcrtsp.treasure-tv.jp via stunnel)
#   OFF -> auto connect mode OFF
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   3.0.47

export TZ=JST-9
TOPDIR=/mtd/coolrevo
export PATH=${PATH}:${TOPDIR}/sbin
export PATH=${PATH}:${TOPDIR}/bin
export PATH=${PATH}:${TOPDIR}/usr/sbin
export PATH=${PATH}:${TOPDIR}/usr/bin
export LD_LIBRARY_PATH=${TOPDIR}/lib:${TOPDIR}/usr/lib:${TOPDIR}/lib/expect5.45.3:/lib:/usr/lib:${LD_LIBRARY_PATH}

TOPDIR=/mtd/coolrevo
TMPDIR=${TOPDIR}/tmp
STATFILE=${TMPDIR}/autoConnect.stat

# loglevel define
LOG_EMERG=0
LOG_ALERT=1
LOG_CRIT=2
LOG_ERR=3
LOG_WARNING=4
LOG_NOTICE=5
LOG_INFO=6
LOG_DEBUG=7
LOG_XDEBUG=8
LOG_NONE=9

app_log=/mtd/coolrevo/var/log/application.log
outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  level=$1
  msg=$2
  me=`basename $0`
  curlevel=`getshm loglevel | grep -e '^[0-9]*$'`
  if [ $? -ne 0 ]; then
    curlevel=6
  fi
  if [ ${curlevel} -ge ${level} ]; then
    echo "${logdate} [$me] [$level] $msg" >> ${app_log}
  fi
}

outlog ${LOG_DEBUG} "autoConnect.sh start."

## Initial directory creation
if [ ! -d ${TMPDIR} ]; then
  mkdir -p ${TMPDIR}
fi

if [ $# -ne 1 ]; then
  outlog ${LOG_ERR} "autoConnect.sh non argument."
  exit 1
fi

MODE="$1"

if [ "${MODE}" = "ON" ]; then
  echo "${MODE}" > ${STATFILE}
  outlog ${LOG_INFO} "autoConnect.sh ${MODE}."
elif [ "${MODE}" = "OFF" ]; then
  if [ -e ${STATFILE} ]; then
    rm -f ${STATFILE}
  fi
  outlog ${LOG_INFO} "autoConnect.sh ${MODE}."
else
  outlog ${LOG_ERR} "autoConnect.sh abnormal argument(${MODE})."
  exit 2
fi

exit 0



