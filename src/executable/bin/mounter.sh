#!/bin/sh

# USAGE
#   udevmounter.sh DEVNAME
#
#   Format device specified with argument,
#
# ARGUMENTS
#   DEVNAME ... set by udevd
#
# OPTION
#   this script has no option.
#
# STATUS CODES
#     0 ... exit with no error
#   200 ... exit with error when formatting media as luks encrypted
#
# BUNDLED WITH
#   2.0.02

unset LANG
unset LC
unset LC_ALL

TOPDIR=/wondergate
CRTOPDIR=/wondergate/cr
KNTOPDIR=/wondergate/kn
PATH=/usr/bin:/usr/sbin:/bin:/sbin
PATH=/usr/local/bin:/usr/local/sbin:${PATH}
PATH=${TOPDIR}/bin:${TOPDIR}/sbin:${PATH}
PATH=${TOPDIR}/usr/bin:${TOPDIR}/usr/sbin:${PATH}
PATH=${CRTOPDIR}/usr/bin:${CRTOPDIR}/usr/sbin:${PATH}
export PATH

LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib
LD_LIBRARY_PATH=${TOPDIR}/lib:${CRTOPDIR}/lib:${KNTOPDIR}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

TZ=JST-9
export TZ


chroot=/wondergate
smbmount_root=${chroot}/samba
hwaddr=`cat /sys/class/net/eth1/address|sed -e 's/\://g'`
memtype=0
app_log=${TOPDIR}/var/log/application.log
isstarting=${CRTOPDIR}/var/tmp/isstarting_mounter.flg


##
## logging
##
logoutput()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}

myname="`basename $0`"

logoutput "start ${myname}"

if [ -e ${isstarting} ]; then
  logoutput "${myname} is already starting, this proccess is abort."
  exit 1
fi
#touch ${isstarting}

# define cloudlogin
cloudlogin=`getshm cloudlogin`
if [ "${cloudlogin}" = "shm cannot open" ]; then
  cloudlogin=`grep cloudlogin ${CRTOPDIR}/etc/shm.ini|sed -e 's/cloudlogin.*= \([a-z0-9]\+\)/\1/gI'`
  if [ -z "${cloudlogin}" ]; then
    cloudlogin=
    logoutput "cloudlogin could not defined."
  fi
fi
logoutput "set cloudlogin=${cloudlogin}"

# define memtype
shm_memtype=`getshm memtype`
if [ "${shm_memtype}" = "shm cannot open" ]; then
  shm_memtype=`grep ^memtype ${CRTOPDIR}/etc/shm.ini|sed -e 's/memtype.*= \([0-9]\+\)/\1/'`
  if [ "${shm_memtype}" -lt 0 -o "${shm_memtype}" -gt 3 ]; then
    shm_memtype=0
    logoutput "memtype could not defined."
  fi
fi
logoutput "shm_memtype=${shm_memtype}"

logoutput "ACTION=${ACTION}"
logoutput "DEVNAME=${DEVNAME}"
logoutput "DEVTYPE=${DEVTYPE}"
logoutput "DEVPATH=${DEVPATH}"
logoutput "SUBSYSTEM=${SUBSYSTEM}"
logoutput "SEQNUM=${SEQNUM}"
logoutput "UDEVD_EVENT=${UDEVD_EVENT}"
stat=0
conmemtype=-1
GREPSTR=`echo "${DEVPATH}" | awk -F/ '{printf "%s: \\\[%s\\\]", $11,$13}'`
logoutput "GREPSTR=${GREPSTR}"
STORAGETYPE=`dmesg | grep "${GREPSTR}" | tail -1`
logoutput "STORAGETYPE=${STORAGETYPE}"
echo ${STORAGETYPE} | grep "removable"
if [ $? -eq 0 ]; then
  logoutput "type is USB memory."
  MOUNTPOINT=/mnt/usbmem
  OTHERMOUNTPOINT=/mnt/hdd
  conmemtype=2
else
  logoutput "type is USB HDD."
  MOUNTPOINT=/mnt/hdd
  OTHERMOUNTPOINT=/mnt/usbmem
  conmemtype=1
fi

ischange=0
case ${shm_memtype} in
  0) logoutput "top priority storage(${shm_memtype}) is Internal." ;;
  1) logoutput "top priority storage(${shm_memtype}) is HDD." ;;
  2) logoutput "top priority storage(${shm_memtype}) is USB memory." ;;
  *) logoutput "top priority storage(${shm_memtype}) setting is invalid." ;;
esac
if [ ${shm_memtype} -eq ${conmemtype} ]; then
  logoutput "change storage."
  ischange=1
else
  logoutput "not change storage."
  ischange=0
fi

logoutput "MOUNTPOINT=${MOUNTPOINT}"

exit 1

if [ "${ACTION}" = "add" -o "${ACTION}" = "change" ]; then
  ismount=0
  if [ ${ischange} -ne 1 ]; then
    logoutput "not change storage, exit"
    logoutput "end mounter.sh."
    if [ -e ${isstarting} ]; then
      rm -f ${isstarting}
    fi
    exit 0
  fi
  if [ ${shm_memtype} -eq 1 ]; then
    ## HDD
    hddparthitioncheck.sh ${DEVNAME}
    stat=$?
    if [ ${stat} -eq 0 ]; then
      ## try remount 
      hddmount.sh ${DEVNAME}
    else
      ## create partion and mkfs and mount.
      hddpartitioning.sh ${DEVNAME}
    fi
  elif [ ${shm_memtype} -eq 2 ]; then
    ## usbmem
    ## try remount 
    usbmemmount.sh ${DEVNAME}
    stat=$?
    if [ ${stat} -ne 0 ]; then
      ## create partion and mkfs and mount.
      usbmemmkfs.sh ${DEVNAME}
    fi
  fi
  if [ ! -d ${MOUNTPOINT} ]; then
    mkdir ${MOUNTPOINT}
  fi
  mount | grep "${DEVNAME}"
  stat=$?
  if [ ${stat} -eq 0 ]; then
    logoutput "${DEVNAME} is already mounted, trying force unmount."
    umount -l ${DEVNAME}
    if [ $? -ne 0 ]; then
      logoutput "${DEVNAME} force umount failed."
    fi
  else
    logoutput "${DEVNAME} is not mounted."
  fi
  mountsuccess=0
  cnt=0
  while :
  do
    logoutput "${DEVNAME} to ${MOUNTPOINT} trying(cnt:${cnt}) mount..."
    mount -t ext4 ${DEVNAME} ${MOUNTPOINT}
    stat=$?
    if [ ${stat} -ne 0 ]; then
      logoutput "mount -t ext4 ${DEVNAME} ${MOUNTPOINT} is failed.(stat=${stat})"
    else
       logoutput "mount -t ext4 ${DEVNAME} ${MOUNTPOINT} is success.(stat=${stat})"
       mountsuccess=1
       break;
    fi
    if [ ${cnt} -gt 3 ]; then
      break;
    fi
    cnt=`expr ${cnt} + 1`
    sleep 1
  done
  if [ ${mountsuccess} -ne 1 ]; then
    logoutput "mount retry failed. create filesystem."
    mkfs.ext2 -t ext4 ${DEVNAME}
    stat=$?
    if [ ${stat} -ne 0 ]; then
      logoutput "mkfs.ext2 -t ext4 ${DEVNAME} is failed.(stat=${stat})"
      logoutput "end mounter.sh."
      if [ -e ${isstarting} ]; then
        rm -f ${isstarting}
      fi
      exit ${stat}
    else
      logoutput "mkfs.ext2 -F -t ext4 ${DEVNAME} is success."
      mount -t ext4 ${DEVNAME} ${MOUNTPOINT}
      stat=$?
      if [ ${stat} -ne 0 ]; then
        logoutput "mount -t ext4 ${DEVNAME} ${MOUNTPOINT} is failed.(stat=${stat})"
        logoutput "end mounter.sh."
        if [ -e ${isstarting} ]; then
          rm -f ${isstarting}
        fi
        exit ${stat}
      else
        logoutput "mount -t ext4 ${DEVNAME} ${MOUNTPOINT} is success."
        ismount=1
      fi
    fi
  else
    logoutput "mount -t ext4 ${DEVNAME} ${MOUNTPOINT} is success."
    ismount=1
  fi
  if [ ${ismount} -eq 1 ]; then
    umount -l ${OTHERMOUNTPOINT}
    stat=$?
    if [ ${stat} -ne 0 ]; then
      logoutput "umount -l ${OTHERMOUNTPOINT} is failed.(stat=${stat}), force umount."
      umount -f ${OTHERMOUNTPOINT}
      stat=$?
      if [ ${stat} -ne 0 ]; then
        logoutput "umount -f ${OTHERMOUNTPOINT} is failed.(stat=${stat})"
      else
        logoutput "umount -f ${OTHERMOUNTPOINT} is success."
      fi
    else
      logoutput "umount -l ${OTHERMOUNTPOINT} is success."
    fi
  fi
  if [ ${ismount} -eq 1 ]; then
    if [ ! -e ${MOUNTPOINT}/samba ]; then
      mkdir -p ${MOUNTPOINT}/samba
      chmod -R 755 ${MOUNTPOINT}/samba
      #mkdir -p ${MOUNTPOINT}/samba/${cloudlogin}
      #chown ${cloudlogin}:${cloudlogin} ${MOUNTPOINT}/samba/${cloudlogin}
    fi
    if [ ! -e ${MOUNTPOINT}/hls ]; then
      mkdir ${MOUNTPOINT}/hls
      chmod -R 755 ${MOUNTPOINT}/hls
    fi
    if [ ! -e ${MOUNTPOINT}/transfer ]; then
      mkdir ${MOUNTPOINT}/transfer
      chmod -R 755 ${MOUNTPOINT}/transfer
    fi
    rm /wondergate/samba
    rm /wondergate/hls
    rm /wondergate/transfer
    ln -s ${MOUNTPOINT}/samba /wondergate/samba
    ln -s ${MOUNTPOINT}/hls /wondergate/hls
    ln -s ${MOUNTPOINT}/transfer /wondergate/transfer
  fi
elif [ "${ACTION}" = "remove" ]; then
  isumount=0
  umount -l ${MOUNTPOINT}
  stat=$?
  if [ ${stat} -ne 0 ]; then
    logoutput "umount -l ${MOUNTPOINT} is failed.(stat=${stat}), force umount."
    umount -f ${MOUNTPOINT}
    stat=$?
    if [ ${stat} -ne 0 ]; then
      logoutput "umount -f ${MOUNTPOINT} is failed.(stat=${stat})"
    else
      logoutput "umount -f ${MOUNTPOINT} is success."
      isumount=1
    fi
  else
    logoutput "umount -l ${MOUNTPOINT} is success."
    isumount=1
  fi
  if [ ${isumount} -eq 1 ]; then
    if [ ${ischange} -ne 1 ]; then
      logoutput "not change storage, exit"
      logoutput "end mounter.sh."
      if [ -e ${isstarting} ]; then
        rm -f ${isstarting}
      fi
      exit 0
    fi
    rm /wondergate/samba
    rm /wondergate/hls
    rm /wondergate/transfer
    ln -s /wondergate/internal/samba /wondergate/samba
    if [ ! -d /wondergate/internal/samba/${cloudlogin} ]; then
      mkdir -p /wondergate/internal/samba
      chmod -R 755 ${MOUNTPOINT}/samba
      #mkdir -p /wondergate/internal/samba/${cloudlogin}
      #chown ${cloudlogin}:${cloudlogin} ${MOUNTPOINT}/samba/${cloudlogin}
    fi
    ln -s /wondergate/internal/hls /wondergate/hls
    ln -s /wondergate/internal/transfer /wondergate/transfer
  fi
fi

logoutput "end mounter.sh."
if [ -e ${isstarting} ]; then
  rm -f ${isstarting}
fi
exit ${stat}
