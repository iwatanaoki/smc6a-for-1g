#!/bin/sh

# USAGE
#   saveSDcard.sh
#
#   status change script for save SDCard mode .
#
# ARGUMENTS
#   ON  -> save SD Card mode ON(start ffmpeg recording process by aboxd)
#   OFF -> save SD card mode OFF(stop ffmpeg recording process by aboxd)
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   3.0.46

export TZ=JST-9
TOPDIR=/mtd/coolrevo
export PATH=${PATH}:${TOPDIR}/sbin
export PATH=${PATH}:${TOPDIR}/bin
export PATH=${PATH}:${TOPDIR}/usr/sbin
export PATH=${PATH}:${TOPDIR}/usr/bin
export LD_LIBRARY_PATH=${TOPDIR}/lib:${TOPDIR}/usr/lib:${TOPDIR}/lib/expect5.45.3:/lib:/usr/lib:${LD_LIBRARY_PATH}

TOPDIR=/mtd/coolrevo
TMPDIR=${TOPDIR}/tmp
STATFILE=${TMPDIR}/is_save_sdcard.stat

app_log=/mtd/coolrevo/var/log/application.log
outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}

outlog "saveSDcard.sh start."

## Initial directory creation
if [ ! -d ${TMPDIR} ]; then
  mkdir -p ${TMPDIR}
fi

if [ $# -ne 1 ]; then
  outlog "saveSDcard.sh non argument."
  exit 1
fi

MODE="$1"

if [ "${MODE}" = "ON" ]; then
  echo "${MODE}" > ${STATFILE}
  outlog "saveSDcard.sh ${MODE}."
elif [ "${MODE}" = "OFF" ]; then
  if [ -e ${STATFILE} ]; then
    rm -f ${STATFILE}
  fi
  outlog "saveSDcard.sh ${MODE}."
else
  outlog "saveSDcard.sh abnormal argument(${MODE})."
  exit 2
fi

exit 0



