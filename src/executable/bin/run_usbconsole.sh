#!/bin/sh

# USAGE
#   run_usbconsole.sh
#
#   usbconsole launcher,
#   called from udev rule.
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   1.0.31

unset LANG
unset LC
unset LC_ALL

/root/target/usr/bin/usbconsole &

exit 0
     
