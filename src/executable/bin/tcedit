#!/bin/sh

# USAGE
#   tcedit [COMMAND] [[RULE]]
#
#   add or remove crontab rules with arguments.
#
# COMMANDS
#   add      ... add or change one traffic control
#   del      ... remove all traffic controls
#
# ARGUMENTS (add)
#   RULE     ... crontab rule string(IT MUST BE ESCAPED!!!)
#
# EXAMPLE
#   tcedit add "* * * * * /path/to/command"
#   tcedit del
#
# STATUS CODES
#   0 ... exit with no error
#   1 ... exit with invalid command option error
#
# BUNDLED WITH
#   1.0.31

unset LANG
unset LC
unset LC_ALL

TOPDIR=/wondergate
CRTOPDIR=/wondergate/cr
KNTOPDIR=/wondergate/kn
PATH=/usr/bin:/usr/sbin:/bin:/sbin
PATH=/usr/local/bin:/usr/local/sbin:${PATH}
PATH=${TOPDIR}/bin:${TOPDIR}/sbin:${PATH}
PATH=${TOPDIR}/usr/bin:${TOPDIR}/usr/sbin:${PATH}
PATH=${CRTOPDIR}/usr/bin:${CRTOPDIR}/usr/sbin:${PATH}
export PATH

LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib
LD_LIBRARY_PATH=${TOPDIR}/lib:${CRTOPDIR}/lib:${KNTOPDIR}/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

TZ=JST-9
export TZ


header=`echo -e "SHELL=/bin/sh
PATH=/usr/bin:/usr/sbin:/bin:/sbin
MAILTO=
"`
defaultcmd=`echo -e "* * * * * run-parts /wondergate/etc/cron.minutely
0 * * * * run-parts /wondergate/etc/cron.hourly
05 4 * * * run-parts /wondergate/etc/cron.daily
10 5 * * 0 run-parts /wondergate/etc/cron.weekly
30 6 1 * * run-parts /wondergate/etc/cron.monthly
"`

addjob() {
  body=`(crontab -u root -l | grep -Ev "respawn|run-parts|SHELL|PATH|MAILTO"; echo "$1") | sort -k2n, -k7gr | uniq`
  (crontab -u root -r; echo "${header}"; echo "${defaultcmd}"; echo "${body}") | crontab -
}

delele() {
  (crontab -u root -r; echo "${header}"; echo "${defaultcmd}") | crontab -
}

showhelp() {
  echo -e "Usage: tcedit {add|del} [RULE]\n\n"
}

case "$1" in
  add)
    addjob "$2"
  ;;
  del)
    delele
  ;;
  *)
    showhelp
  ;;
esac

exit 0
