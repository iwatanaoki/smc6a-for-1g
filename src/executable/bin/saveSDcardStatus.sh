#!/bin/sh

# USAGE
#   saveSDcardStatus.sh
#
#   get status for save SDcard mode.
#
# ARGUMENTS
#   none
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   3.0.46

export TZ=JST-9
TOPDIR=/mtd/coolrevo
export PATH=${PATH}:${TOPDIR}/sbin
export PATH=${PATH}:${TOPDIR}/bin
export PATH=${PATH}:${TOPDIR}/usr/sbin
export PATH=${PATH}:${TOPDIR}/usr/bin
export LD_LIBRARY_PATH=${TOPDIR}/lib:${TOPDIR}/usr/lib:${TOPDIR}/lib/expect5.45.3:/lib:/usr/lib:${LD_LIBRARY_PATH}

TOPDIR=/mtd/coolrevo
TMPDIR=${TOPDIR}/tmp
STATFILE=${TMPDIR}/is_save_sdcard.stat

app_log=/mtd/coolrevo/var/log/application.log
outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}

outlog "saveSDcardStatus.sh start."

if [ -e ${STATFILE} ]; then
  MODE=`cat ${STATFILE}`
  if [ "${MODE}" = "ON" ]; then
    echo "ON"
  else
    echo "OFF"
    MODE="OFF"
  fi
else
   echo "OFF"
   MODE="OFF"
fi

outlog "saveSDcardStatus.sh end MODE=${MODE}."

exit 0



