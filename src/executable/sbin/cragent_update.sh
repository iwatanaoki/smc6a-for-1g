#!/bin/sh

# USAGE
#   cragent_update.sh
#
#   execute by initial execution script
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   4.0.3

export TZ=JST-9
TOPDIR=/mtd/coolrevo
export PATH=${PATH}:${TOPDIR}/sbin
export PATH=${PATH}:${TOPDIR}/bin
export PATH=${PATH}:${TOPDIR}/usr/sbin
export PATH=${PATH}:${TOPDIR}/usr/bin
export LD_LIBRARY_PATH=${TOPDIR}/lib:${TOPDIR}/usr/lib:${TOPDIR}/lib/expect5.45.3:/lib:/usr/lib:${LD_LIBRARY_PATH}

# loglevel define
LOG_EMERG=0
LOG_ALERT=1
LOG_CRIT=2
LOG_ERR=3
LOG_WARNING=4
LOG_NOTICE=5
LOG_INFO=6
LOG_DEBUG=7
LOG_XDEBUG=8
LOG_NONE=9

app_log=/mtd/coolrevo/var/log/application.log
outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  level=$1
  msg=$2
  me=`basename $0`
  curlevel=`getshm loglevel | grep -e '^[0-9]*$'`
  if [ $? -ne 0 ]; then
    curlevel=6
  fi
  if [ ${curlevel} -ge ${level} ]; then
    echo "${logdate} [$me] [$level] $msg" >> ${app_log}
  fi
}

downloader()
{
  DOWNURL=$1
  OUTFILE=$2
  STAT=0
  if [ -e /mtd/coolrevo/bin/curl ]; then
    outlog ${LOG_DEBUG} "curl --fail --connect-timeout 3 -4 --insecure ${DOWNURL} -o ${OUTFILE}"
    STAT=0
    cnt=0
    while [ ${cnt} -le 30 ]
    do 
      cnt=`expr ${cnt} + 1`
      curl --fail --connect-timeout 3 -4 --insecure ${DOWNURL} -o ${OUTFILE}
      STAT=$?
      if [ ${STAT} -eq 6 ]; then
        outlog ${LOG_WARNING} "curl --fail --connect-timeout 3 -4 --insecure ${DOWNURL} -o ${OUTFILE}: cannot resolve, retry.${cnt}"
        sleep 10
        continue;
      else
        break;
      fi
    done
  else
    outlog ${LOG_DEBUG} "wget -T 3 -t 1 --no-check-certificate ${DOWNURL} -O ${OUTFILE}"
    wget -T 3 -t 1 --no-check-certificate ${DOWNURL} -O ${OUTFILE}
    STAT=$?
  fi
  return ${STAT}
}

ABOXDCONF=/mtd/coolrevo/etc/aboxd.conf

DEFNTPSERVER=ntp.jst.mfeed.ad.jp
NTPSERVER=${DEFNTPSERVER}
DEFFWUPURL=https://camfwup.treasure-tv.jp/smc6a
FWUPURL=""

if [ -f ${ABOXDCONF} ]; then
  NTPSERVER=`grep -e '^ntpserver.*=' ${ABOXDCONF} | sed 's/^ntpserver.*=[ |\t]*\(.*\)$/\1/'`
  FWUPURL=`grep -e '^fwupurl.*=' ${ABOXDCONF} | sed 's/^fwupurl.*=[ |\t]*\(.*\)$/\1/'`
fi
if [ "${NTPSERVER}" = "" ]; then
  NTPSERVER=${DEFNTPSERVER}
fi
#ntpdate ${NTPSERVER}

if [ "${FWUPURL}" = "" ]; then
  FWUPURL=${DEFFWUPURL}
fi

kill_cragent() {
  pid=`pgrep -f aboxd`
  if [ "${pid}" != "" ]; then
    killall -TERM aboxd
  fi
  pid=`pgrep -f stunnel`
  if [ "${pid}" != "" ]; then
    killall -KILL stunnel
  fi
  pid=`pgrep -f ffmpeg`
  if [ "${pid}" != "" ]; then
    killall -KILL ffmpeg
  fi
  pid=`pgrep -f poorntpd`
  if [ "${pid}" != "" ]; then
    killall -KILL poorntpd
  fi
  pid=`pgrep -f ssh`
  if [ "${pid}" != "" ]; then
    killall -KILL ssh
  fi
  pid=`pgrep -f sshd`
  if [ "${pid}" != "" ]; then
    killall -KILL sshd
  fi
}


#SMCPFWUPDIR=/mtd/coolrevo/fwup
SMCPFWUPDIR=/tmp
SMCPFWUPFILENAME=smc6a_fwup.tgz
SMCPFWUPFILE=${SMCPFWUPDIR}/${SMCPFWUPFILENAME}
SMCPFIRMVERSIONFILENAME=smc6afirmversion
SMCPFIRMVERSION=${SMCPFWUPDIR}/${SMCPFIRMVERSIONFILENAME}
NOWSMCPFIRMVERSION=/mtd/coolrevo/etc/${SMCPFIRMVERSIONFILENAME}
#TOPSMCPFIRMVERSION=/${SMCPFIRMVERSIONFILENAME}
SMCPFIRMCLEARFILENAME=smc6aclear.xml
SMCPFIRMCLEAR=${SMCPFWUPDIR}/${SMCPFIRMCLEARFILENAME}
MACADDR=`ifconfig eth0 | grep HWaddr | sed -e 's/^.*HWaddr[ |\t]*\(.*\)$/\1/' -e 's/[ |\t]*//g' | awk '{print toupper($1)}'`
MACPATH=`echo ${MACADDR} | sed 's/[:]//g'`

## Firmware download check
FIRMDOWNLOAD=0
if [ "${FWUPURL}" != "" -a "${MACPATH}" != "" ]; then
  outlog ${LOG_DEBUG} "fwupurl(${FWUPURL}) is found in aboxd.conf, and macpath(${MACPATH}) seting found."
  if [ ! -f ${SMCPFIRMVERSION} -o ! -f ${SMCPFWUPFILE} ]; then
    outlog ${LOG_WARNING} "smc6afirmversion file or smc6a_fwup.tgz is not found, try access to ${FWUPURL}."
    ## try access to fwupserver.
    downloader ${FWUPURL}/${MACPATH}/${SMCPFIRMVERSIONFILENAME} ${SMCPFIRMVERSION}
    stat=$?
    if [ ${stat} -eq 0 ]; then
      if [ -f ${NOWSMCPFIRMVERSION} ]; then
        NEWVER=`head -1 ${SMCPFIRMVERSION}`
        NOWVER=`head -1 ${NOWSMCPFIRMVERSION}`
        if [ "${NEWVER}" != "${NOWVER}" ]; then
          outlog ${LOG_INFO} "different found between now firmeware version and new firmware version."
          FIRMDOWNLOAD=1
        else
          outlog ${LOG_WARNING} "no diffrent now firmeware version and new firmware version. smc6a_fwup.tgz download skip."
          FIRMDOWNLOAD=0
        fi
      else
        FIRMDOWNLOAD=1
      fi
      if [ ${FIRMDOWNLOAD} -eq 1 ]; then
        outlog ${LOG_DEBUG} "try download smc6a_fwup.tgz."
        downloader ${FWUPURL}/${MACPATH}/${SMCPFWUPFILENAME} ${SMCPFWUPFILE}
        if [ $? -eq 0 ]; then
          outlog ${LOG_INFO} "download smc6a_fwup.tgz success."
          downloader ${FWUPURL}/${MACPATH}/${SMCPFIRMCLEARFILENAME} ${SMCPFIRMCLEAR}
          if [ $? -eq 0 ]; then
            outlog ${LOG_DEBUG} "clear file found."
          else
            outlog ${LOG_DEBUG} "clear file not found."
          fi
        else
          outlog ${LOG_WARNING} "download smc6a_fwup.tgz failed."
        fi
      fi
    else
      outlog ${LOG_WARNING} "download smc6afirmversion failed(stat:${stat})."
    fi
  fi
fi


## Firmware update check
FIRMUP=0
if [ -f ${SMCPFIRMVERSION} -a -f ${SMCPFWUPFILE} ]; then
  outlog "new firmeware file, ${SMCPFIRMVERSION} and ${SMCPFWUPFILE} is found."
  if [ -f ${NOWSMCPFIRMVERSION} ]; then
    NEWVER=`head -1 ${SMCPFIRMVERSION}`
    NOWVER=`head -1 ${NOWSMCPFIRMVERSION}`
    if [ "${NEWVER}" != "${NOWVER}" ]; then
      outlog ${LOG_INFO} "different found between now firmeware version and new firmware version."
      FIRMUP=1
    fi
  else
    FIRMUP=1
  fi
fi

if [ ${FIRMUP} -eq 1 ]; then
  FILEMD5=`md5sum ${SMCPFWUPFILE} | awk '{print $1}'`
  VERMD5=`tail -1 ${SMCPFIRMVERSION}`
  if [ "${FILEMD5}" = "" -o "${VERMD5}" = "" ]; then
    outlog "md5 value invalid, firmup skip."
  elif [ "${FILEMD5}" = "${VERMD5}" ]; then
    outlog "new firmware found. firmware updating."
    if [ -e ${SMCPFIRMCLEAR} ]; then
      outlog ${LOG_INFO} "clear file found, First delete unnecessary files and directories."
      find /mtd/coolrevo/ -maxdepth 1 \( -type d -a ! -name 'etc' -a ! -name 'fwup'\) -exec rm -rf {} \;
      find /mtd/coolrevo/etc/ \( ! -name 'rc.local' -a ! -name 'smc6afirmversion' -a ! -name 'etc/' \) -exec rm -rf {} \;
    fi
    kill_cragent;
    gzip -dc ${SMCPFWUPFILE} | (cd /; tar xvf - )
    STAT=$?
    if [ ${STAT} -ne 0 ]; then
      outlog ${LOG_WARNING} "new firmware extract failed, skip reboot."
      rm -f ${SMCPFWUPFILE}
      rm -f ${SMCPFIRMVERSION}
      if [ -e ${SMCPFIRMCLEAR} ]; then
        rm -f ${SMCPFIRMCLEAR}
      fi
      sync;
    else
      sleep 3
      rm -f ${SMCPFWUPFILE}
      if [ -f ${NOWSMCPFIRMVERSION} ]; then
        mv -f ${NOWSMCPFIRMVERSION} ${NOWSMCPFIRMVERSION}.old
      fi
      mv -f ${SMCPFIRMVERSION} ${NOWSMCPFIRMVERSION}
      ## HUNT F/W のアップデートチェックで無限ループに陥るため下記取りやめ
      #cp -f ${NOWSMCPFIRMVERSION} ${TOPSMCPFIRMVERSION}
      if [ -e ${SMCPFIRMCLEAR} ]; then
        rm -f ${SMCPFIRMCLEAR}
      fi
      outlog ${LOG_INFO} "firmware updated, reboot."
      sync;
      reboot;
    fi
  else
    outlog ${LOG_WARNING} "md5sum value diffeent, new firmware is broken.. remove abnormal firmware file."
    rm -f ${SMCPFWUPFILE}
    rm -f ${SMCPFIRMVERSION}
    if [ -e ${SMCPFIRMCLEAR} ]; then
      rm -f ${SMCPFIRMCLEAR}
    fi
    sync;
  fi
fi

sumstat=`expr ${stat} + ${STAT}`
exit ${sumstat}
