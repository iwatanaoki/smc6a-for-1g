#!/bin/sh

# USAGE
#   loguploader.sh [LOGFILE[...]]
#
#   used with logrotate, postrotate/endscript
#
# ARGUMENTS
#   $1, $2, ... $n   logfile name (it is NOT renamed)
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   2.0.32

unset LANG
unset LC
unset LC_ALL

## no action
exit 0

PATH=/usr/bin:/usr/sbin:/bin:/sbin
PATH=/usr/local/bin:/usr/local/sbin:${PATH}
PATH=/wondergate/bin:/wondergate/sbin:${PATH}
PATH=/wondergate/usr/bin:/wondergate/usr/sbin:${PATH}
export PATH

LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib
LD_LIBRARY_PATH=/usr/share/bluetooth/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/home/hybroad/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/wondergate/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/wondergate/usr/lib:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# Agentbox version string
version_str=`aboxd -v 2>&1|cut -d ' ' -f3|tr -d '\n'`
# Agentbox firmware version string
firmver_str=`cat /wondergate/etc//wgfirmversion |head -1`
# Agentbox hardware address string
hwaddr_str=`cat /sys/class/net/eth1/address|sed -e's/\://g' |tr '[:lower:]' '[:upper:]'|tr -d '\n'`
# Agentbox uniquekey string
uniquekey=`cat /sys/class/net/eth1/address|tr -d '\n'|openssl sha256 2>/dev/null|sed -e 's/^.*= \(.*\)/\1/'`
# logging
app_log=/wondergate/var/log/application.log
# daemon program image
program=/wondergate/cr/usr/sbin/aboxd
# PID file
pidfile=/wondergate/var/run/aboxd.pid
# SHMID file
sidfile=/var/run/aboxd.shm
# SHM cache
shmfile=/wondergate/cr/etc/shm.ini
# curl program image
curl=/wondergate/bin/curl
# preferred fqdn
fqdn=`cat /wondergate/cr/etc/aboxd.conf|egrep -v "^#" | egrep "^server" |sed -e 's/.*= http\(s\):\/\///'`
# user-agent name
agent_name="Aboxd/${version_str} (Linux; armv7l-linux-gnueabi 3.10.0; ${hwaddr_str}) Wondergate01/${firmver_str} Loguploader/1.0 libcurl/7.43.0"

##
## logging
##
logger()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}

suffix=`date +%Y%m%d%H%M -D %s -d $(($(date +%s) - 86400))`

response=`${curl} --insecure \
         --resolve ${fqdn}:443:202.32.197.24 \
         --header "x-smc-aboxaddr: ${hwaddr_str}" \
         --user-agent "${agent_name}" \
         --form "actas=Wondergate" \
         --form "uniquekey=${uniquekey}" \
         --form "file=@${1}.1; filename=${1}.${suffix}" \
         https://${fqdn}/api/wondergates/logupload`

if [ ! -z "${response}" ]; then
  if [ $? -ne 0 ]; then
    logger "helper cannot launched."
  fi
fi

exit 0
