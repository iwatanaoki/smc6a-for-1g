#!/bin/sh

# USAGE
#   directmessage.sh [SUBJECT] [BODY]
#
#   send e-mail message using server API
#
# ARGUMENTS
#   SUBJECT    ... email subject
#   BODY       ... email body
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   1.0.32

unset LANG
unset LC
unset LC_ALL

PATH=/usr/bin:/usr/sbin:/bin:/sbin
PATH=/usr/local/bin:/usr/local/sbin:${PATH}
PATH=/root/target/bin:/root/target/sbin:${PATH}
PATH=/root/target/usr/bin:/root/target/usr/sbin:${PATH}
export PATH

LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib
LD_LIBRARY_PATH=/usr/share/bluetooth/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/home/hybroad/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/usr/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/usr/lib/expect5.45.3:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# Agentbox version string
version_str=`aboxd -v 2>&1|cut -d ' ' -f3|tr -d '\n'`
# Agentbox firmware version string
firmver_str=`cat /home/hybroad/version.txt|tr -d '\n'`
# Agentbox hardware address string
hwaddr_str=`cat /sys/class/net/eth0/address|sed -e's/\://g' |tr '[:lower:]' '[:upper:]'|tr -d '\n'`
# Agentbox uniquekey string
uniquekey=`cat /sys/class/net/eth0/address|tr -d '\n'|openssl sha256 2>/dev/null|sed -e 's/^.*= \(.*\)/\1/'`
# logging
app_log=/root/target/var/log/application.log
# daemon program image
program=/root/target/usr/sbin/aboxd
# SHMID file
shmfile=/root/target/root/shm.ini
# counter cache file
counterfile=/tmp/.ddead
# curl program image
curl=/root/target/usr/bin/curl
# preferred fqdn
fqdn=`cat /root/target/etc/aboxd.conf|grep server|sed -e 's/.*= http\(s\):\/\///'`
# helper server
helper_server=202.32.197.26
# helper port
helper_port=
# user-agent name
agent_name="Aboxd/${version_str} (Linux; armv7l-linux-gnueabi 3.10.0; ${hwaddr_str}) Agentbox/${firmver_str} DirectMessenger/1.0 libcurl/7.43.0"

# helper must be run?
# 0: no need to run helper
# 1: reason for aboxd maybe dead
# 2: reason for cache not updated
mustrun=0

# daemon dead counter
dead_counter=0


##
## logging
##
logger()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}

##
## if in process DEACTIVATE, launch hlper will be cancelled
##
[ -e /tmp/.deactivated ] && exit 0

##
## get remotehelp in configuration
##
remotehelp=`getshm remotehelp`
if [ "${remotehelp}" = "shm cannot open" ]; then
  remotehelp=`grep remotehelp ${shmfile} | sed -e 's/^.*\?=[[:space:]]*\?//'`
fi
if [ "${remotehelp}" -ne 0 -a "${remotehelp}" -ne 1 -a "${remotehelp}" -ne 2 ]; then
  remotehelp=2
fi

##
## if counter file exists, store value
##
if [ -f "${counterfile}" ]; then
  dead_counter=`cat ${counterfile}`
fi

##
## test aboxd is alive
##
alive=`pgrep -f "^${program}"`
if [ -z "${alive}" ]; then
  new_num=`expr ${dead_counter} + 1`
  if [ "${new_num}" -gt 5 ]; then
    mustrun=1
  fi
  echo ${new_num} > "${counterfile}"
else
 [ -f "${counterfile}" ] && rm -f "${counterfile}"
fi

##
## check if shm.ini remains unchanged over 30 minutes.
##
curtime=`date +%s`
shmstat=`stat -c '%Z' ${shmfile}`
diff=`expr ${curtime} - ${shmstat}`
if [ "${diff}" -gt 1800 ]; then
  mustrun=2
fi


##
## helper is already launched?
##
launched=`pgrep -f "^ssh -l stunnel -p9876 -oSendEnv=SMC_ABOXADDR"`

if [ "${remotehelp}" -eq 1 -o "${mustrun}" -ne 0 ]; then

  ##
  ## test if helper is already launched
  ##
  if [ -z "${launched}" ]; then

    case "${mustrun}" in
      1)
        logger "aboxd maybe dead. Remote SSH helper must be launched."
        ;;
      2)
        logger "shm.ini is not changed over 30 minutes. Remote SSH helper must be launched."
        ;;
    esac

    response=`${curl} --insecure \
             --resolve ${fqdn}:443:202.32.197.24 \
             --header "x-smc-aboxaddr: ${hwaddr_str}" \
             --user-agent "${agent_name}" \
             --form "actas=Agentbox" \
             --form "uniquekey=${uniquekey}" \
             https://${fqdn}/api/agentboxes/helperport`

    if [ -n "${response}" ]; then

      helper_port=`echo -e "${response}" | sed -e 's/^port=//g'`
      if [ ${helper_port} -gt 32768 -a ${helper_port} -lt 33270 ]; then

        # launch remote access helper
        export SMC_ABOXADDR="${hwaddr_str}"
        export SMC_ABOXFWRD="${helper_port}"
        ssh -l stunnel -p9876 -oSendEnv=SMC_ABOXADDR -oSendEnv=SMC_ABOXFWRD -fgnT \
            -oLogLevel=quiet \
            -oServerAliveInterval=15 \
            -oStrictHostKeyChecking=no \
            -oUserKnownHostsFile=/dev/null \
            -oPreferredAuthentications=publickey \
            -oIdentitiesOnly=yes \
            -i/root/target/etc/bundle.pem \
            -R ${helper_port}:127.0.0.1:22 ${helper_server} \
            "while :; do sleep 5; done"
        code=$?
        if [ ${code} -eq 0 ]; then
          npid=`pgrep -f "^ssh -l stunnel -p9876 -oSendEnv=SMC_ABOXADDR"`
          logger "Remote SSH helper is launched[pid:${npid}]."
        else
          logger "SSH command exit status is ${code}. helper cannot launched."
        fi
      else
        logger "port cannot reserve(${helper_port}). helper aborted."
      fi
    else
      logger "API does not respond. helper aborted."
    fi

  fi

else

  if [ -n "${launched}" ]; then
    logger "remote helper[pid=${launched}] will be shutdown."
    kill "${launched}"
  fi

fi
