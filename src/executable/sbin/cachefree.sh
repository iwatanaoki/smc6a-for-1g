#!/bin/sh

# USAGE
#   aboxhbcheck.sh
#
#   If shm.ini is not timestamp updated,
#   execute aboxd -k or send SIGKILL to aboxd.
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   1.0.39

unset LANG
unset LC
unset LC_ALL

PATH=/usr/bin:/usr/sbin:/bin:/sbin
PATH=/usr/local/bin:/usr/local/sbin:${PATH}
PATH=/wondergate/bin:/wondergate/sbin:${PATH}
PATH=/wondergate/usr/bin:/wondergate/usr/sbin:${PATH}
export PATH

LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib
LD_LIBRARY_PATH=/usr/share/bluetooth/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/wondergate/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/wondergate/usr/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/wondergate/usr/lib/expect5.45.3:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# logging
app_log=/wondergate/var/log/application.log
# target program image
program=aboxd
programpath=/wondergate/usr/sbin/${program}
# shm.ini file
shmini=shm.ini
shminipath=/wondergate/etc/${shmini}

##
## logging
##
outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}


##
## page cache free
##
echo 1 > /proc/sys/vm/drop_caches

outlog "page cache free."

exit 0
