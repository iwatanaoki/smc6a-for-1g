#!/bin/sh

# USAGE
#   reloadstunnelcf.sh
#
#   stunnel.conf force reload script.
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   2.0.1

unset LANG
unset LC
unset LC_ALL

PATH=/usr/bin:/usr/sbin:/bin:/sbin
PATH=/usr/local/bin:/usr/local/sbin:${PATH}
PATH=/mtd/coolrevo/bin:/mtd/coolrevo/sbin:${PATH}
PATH=/mtd/coolrevo/usr/bin:/mtd/coolrevo/usr/sbin:${PATH}
export PATH

LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib
LD_LIBRARY_PATH=/usr/share/bluetooth/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/mtd/coolrevo/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/mtd/coolrevo/usr/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/mtd/coolrevo/usr/lib/expect5.45.3:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# logging
app_log=/mtd/coolrevo/var/log/application.log

##
## logging
##
outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}


##
## reload stunnel conf
##
killall -HUP stunnel

outlog "force reloaded stunnel conf."

exit 0
