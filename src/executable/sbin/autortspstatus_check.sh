#!/bin/sh

# USAGE
#   autortspstatus_check.sh
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   4.0.20 1g

unset LANG
unset LC
unset LC_ALL

export TZ=JST-9
TOPDIR=/mtd/coolrevo
export PATH=${PATH}:${TOPDIR}/sbin
export PATH=${PATH}:${TOPDIR}/bin
export PATH=${PATH}:${TOPDIR}/usr/sbin
export PATH=${PATH}:${TOPDIR}/usr/bin
export LD_LIBRARY_PATH=${TOPDIR}/lib:${TOPDIR}/usr/lib:${TOPDIR}/lib/expect5.45.3:/lib:/usr/lib:${LD_LIBRARY_PATH}

mycmdname=`basename $0`

# system(chrooted) root
chroot=/mtd/coolrevo

# [ROOT]/etc
cretc_dir=${chroot}/etc


# loop interval
interval=180 # main loop interval 180s

# other paramters
oneofsev=5   # the parameter is determine do run the one of several times

# logging
applog=${chroot}/var/log/application.log

# result html
rtspstat_ramdisk=${chroot}/ramdisk1
#resultfile=${chroot}/var/log/autoRTSPstatus.html
resultfile=${rtspstat_ramdisk}/autoRTSPstatus.html
result_tmp=${resultfile}.tmp

#band_block=${chroot}/var/log/result_bandwitdh.txt
band_block=${rtspstat_ramdisk}/result_bandwitdh.txt

# loglevel define
LOG_EMERG=0
LOG_ALERT=1
LOG_CRIT=2
LOG_ERR=3
LOG_WARNING=4
LOG_NOTICE=5
LOG_INFO=6
LOG_DEBUG=7
LOG_XDEBUG=8
LOG_NONE=9

outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  level=$1
  msg=$2
  me=`basename $0`
  curlevel=`getshm loglevel | grep -e '^[0-9]*$'`
  if [ $? -ne 0 ]; then
    curlevel=6
  fi
  if [ ${curlevel} -ge ${level} ]; then
    echo "${logdate} [$me] [$level] $msg" >> ${applog}
  fi
}

if [ -f ${band_block} ]; then
  rm -f ${band_block}
fi

forhuntlink=/www/autoRTSPstatus.html

autoCon=0    # result by autoConnectStatus.sh, ON:1, OFF:0
activate=0   # activate status, activated:1, not activated:0
prev_activate=0

# interrupt
interrupt=0

# pause/wake
paused=0

#PID
PIDFILE=${chroot}/var/run/`basename $mycmdname .sh`.pid


#
# display STATUS into STDOUT and exit
# @param $1 exit status (0-255)
#
abort() {
  echo $1
  outlog ${LOG_ERR} "abort:$1"
  exit $1
}

#
# signal handler for quit
#
sig_catch() {
  outlog ${LOG_NOTICE} "Interrupt SIGNAL received"
  interrupt=1
  if [ -f "${PIDFILE}" ]; then
    rm ${PIDFILE}
  fi
  outlog ${LOG_NOTICE} "agent will be shutdown."
  exit 30
}

#
# signal handler for waiting
#
sig_pause() {
  outlog ${LOG_NOTICE} "Pause SIGNAL received"
  paused=1
}

#
# signal handler for end waiting
#
sig_wake() {
  outlog ${LOG_NOTICE} "Resume SIGNAL received"
  paused=0
}

trap sig_catch 2 3 15
trap sig_pause 10
## SMC-6A ではうまく機能しない
#trap sig_wake 18


# write PID file
if [ -f "${PIDFILE}" ]; then
  echo "may be already running."
  exit 1
else
  echo $$ > ${PIDFILE}
  outlog ${LOG_INFO} "agent launched[pid:$$]."
fi

cnt=0
while :
do

  ##
  ## header
  ##
  echo -n "\
<!DOCTYPE html>
<html lang=\"ja\">
  <head>
    <meta charset=\"utf-8\">
    <meta http-equiv=\"cache-control\" content=\"max-age=0\"/>
    <meta http-equiv=\"cache-control\" content=\"no-cache\"/>
    <meta http-equiv=\"expires\" content=\"0\"/>
    <meta http-equiv=\"expires\" content=\"Tue, 01 Jan 1980 1:00:00 GMT\"/>
    <meta http-equiv=\"pragma\" content=\"no-cache\"/>
    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
    <meta http-equiv=\"refresh\" content=\"120; URL=\">

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=0.5, minimum-scale=0.5, maximum-scale=1.0, user-scalable=1\">
    <style type=\"text/css\">
      table {
        border-collapse: collapse;
        border-spacing: 0;
        table-layout: fixed;
        width: 90%;
      }
      table tr {
        border-bottom: solid 1px #eee;
      }
      table th {
        font-size: 14px;
        padding: 7px 0;
        border-right:solid 1px #ddd;
        border-left:solid 1px #ddd;
        background-color:#dddddd;
      }
      table th.foot {
        font-size: 14px;
        padding: 7px 0;
        border-right:solid 1px #ddd;
        border-left:solid 1px #ddd;
        background-color:white;
      }
      table td {
        font-size: 14px;
        padding: 7px 0;
        border-right:solid 1px #ddd;
        border-left:solid 1px #ddd;
      }
      table td.sep {
        font-size: 14px;
        padding: 7px 0;
        border-right:solid 1px #ddd;
        border-left:solid 1px #ddd;
        width: 10px;
      }
    </style>
    <title>auto RTSP status</title>
  </head>
  <body>
    <center>
      <table>
        <tr>" > ${result_tmp}
  
  ##
  ## autoConnectStatus check
  ##
  statstr=`autoConnectStatus.sh`
  echo -n "
         <th>自律接続モード</th><td class=\"sep\">:</td>" >> ${result_tmp}
  if [ "${statstr}" = "ON" ]; then
    echo -n "<td><font color=\"green\">" >> ${result_tmp}
    autoCon=1
  else
    echo -n "<td><font color=\"red\">" >> ${result_tmp}
    autoCon=0
  fi
  echo "${statstr}</font></td>
        </tr>" >> ${result_tmp}

  ##
  ## activ_status check
  ##
  echo -n "
        <tr>
          <th>アクティベート</th><td>:</td>" >> ${result_tmp}
  stat=`getshm activ_status`
  if [ "${stat}" = "" ]; then
    echo -n "<td><font color=\"red\">NG(failed)</font></td>" >> ${result_tmp}
    activate=0
  else
    if [ ${stat} -eq 1 ]; then
      echo -n "<td><font color=\"green\">activate</font></td>" >> ${result_tmp}
      activate=1
    else
      echo -n "<td><font color=\"red\">not activate</font></td>" >> ${result_tmp}
      activate=0
    fi
  fi
  echo "
        </tr>" >> ${result_tmp}

  ##
  ## key exists check
  ##
  echo -n "
        <tr>
          <th>Keyファイル</th><td class=\"sep\">:</td>" >> ${result_tmp}
  ls ${cretc_dir}/*.key > /dev/null 2>&1
  stat=$?
  if [ ${stat} -eq 1 ]; then
    echo -n "<td><font color=\"orange\">key not exists</font></td>" >> ${result_tmp}
  else
    echo "<td>" >> ${result_tmp}
    for i in `ls ${cretc_dir}/*.key`
    do
      dt=`stat -c "%y" ${i} | sed 's/\..*$//'`
      echo "  ${dt} ${i}<br>" >> ${result_tmp}
    done
    echo "          </td>" >> ${result_tmp}
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  ##
  ## server access check
  ##
  if [ ${autoCon} -eq 1 ]; then
    serverurl=`getshm server`
    if [ "${serverurl}" = "" ]; then
      echo -n "
        <tr>
          <th>サーバアクセス</th><td class=\"sep\">:</td>" >> ${result_tmp}
      echo "<td><font color=\"red\">not found server url</font></td>" >> ${result_tmp}
    else
      echo -n "
        <tr>
          <th>サーバ(${serverurl})アクセス</th><td class=\"sep\">:</td>" >> ${result_tmp}
      curl --fail --connect-timeout 3 -4 --insecure --head ${serverurl} > /dev/null 2>&1
      stat=$?
      if [ ${stat} -eq 0 -o ${stat} -eq 22 ]; then
        echo "<td><font color=\"green\">OK</font></td>" >> ${result_tmp}
      else
        echo "<td><font color=\"red\">NG</font></td>" >> ${result_tmp}
      fi
    fi
    echo "
        </tr>" >> ${result_tmp}
  fi
  
  ##
  ## DNS check
  ##
  echo -n "
        <tr>
          <th>名前解決</th><td class=\"sep\">:</td>" >> ${result_tmp}
  server=`echo ${serverurl} | sed -e 's/^http.:\/\///' -e 's/\/.*$//'`
  if [ "${server}" = "" ]; then
    echo "<td><font color=\"red\">failed</font></td>" >> ${result_tmp}
  else
    #nslookup ${server} > /dev/null 2>&1
    #ping -c 1 -W 1 -w 1 ${server} > /dev/null 2>&1
    cr_nslookup ${server} > /dev/null 2>&1
    stat=$?
    if [ ${stat} -eq 0 ]; then
      echo "<td><font color=\"green\">OK</font></td>" >> ${result_tmp}
    else
      echo "<td><font color=\"red\">NG</font></td>" >> ${result_tmp}
    fi
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  ##
  ## ping check1
  ##
  if [ "${server}" = "" ]; then
    echo -n "
        <tr>
          <th>ping</th><td class=\"sep\">:</td>"
    echo "<td><font color=\"red\">not found server</font></td>" >> ${result_tmp}
  else
    echo -n "
        <tr>
          <th>ping(${server})</th><td class=\"sep\">:</td>" >> ${result_tmp}
    ping -c 1 -W 1 -w 1 ${server} > /dev/null 2>&1
    stat=$?
    if [ ${stat} -eq 0 ]; then
      echo "<td><font color=\"green\">OK</font></td>" >> ${result_tmp}
    else
      echo "<td><font color=\"red\">NG</font></td>" >> ${result_tmp}
    fi
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  ##
  ## ping check2
  ##
  server="8.8.8.8"
  echo -n "
        <tr>
          <th>ping(${server})</th><td class=\"sep\">:</td>" >> ${result_tmp}
  ping -c 1 -W 1 -w 1 ${server} > /dev/null 2>&1
  stat=$?
  if [ ${stat} -eq 0 ]; then
    echo "<td><font color=\"green\">OK</font></td>" >> ${result_tmp}
  else
    echo "<td><font color=\"red\">NG</font></td>" >> ${result_tmp}
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  ##
  ## ffmpeg alive check
  ##
  echo -n "
        <tr>
          <th>RTSPリレー</th><td class=\"sep\">:</td>" >> ${result_tmp}
  rtspstat=0
  pgrep -f "ffmpeg" > /dev/null 2>&1
  stat=$?
  if [ ${stat} -eq 0 ]; then
    elapsed=`/mtd/coolrevo/bin/ps -xo etime,args | egrep "ffmpeg -y .*" |\
      grep -v egrep | awk '{print $1}' | sed 's/^.*\([0-9][0-9]\):[0-9][0-9]$/\1/'`
    if [ "${elapsed}" = "" ]; then
      echo "<td><font color=\"red\">NG</font></td>" >> ${result_tmp}
      rtspstat=1
    else
      if [ ${elapsed} -gt 1 ]; then
        echo "<td><font color=\"green\">OK</font></td>" >> ${result_tmp}
      else
        echo "<td><font color=\"red\">NG</font></td>" >> ${result_tmp}
        rtspstat=1
      fi
    fi
  else
    echo "<td><font color=\"red\">NG</font></td>" >> ${result_tmp}
    rtspstat=1
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  ##
  ## bandwidth check
  ##
  if [ ${autoCon} -eq 1 -a ${rtspstat} -ne 0 ]; then
    tunnelurl=`getshm tunnelurl`
    if [ "${tunnelurl}" = "" ]; then
      echo -n "
        <tr>
          <th>通信帯域</th><td class=\"sep\">:</td>" >> ${result_tmp}
      echo "<td><font color=\"red\">failed</font></td>" >> ${result_tmp}
    else
      if [ \( ${activate} -eq 0 -a ${prev_activate} -ne ${activate} \) -o \
        ${cnt} -eq 0 -o `expr ${cnt} % ${oneofsev}` -eq 0 ]; then
        rate=`curl --fail --connect-timeout 5 -4 \
          --insecure ${tunnelurl}/check/test.dmp -o /dev/null \
          -w '%{speed_download}' 2>/dev/null | \
            awk '{n=($1*8)/1024/1024;printf "%.2fMbps", n}'`
        echo -n "
        <tr>
          <th>通信帯域</th><td class=\"sep\">:</td>" >> ${result_tmp}
        echo "<td>${rate}</td>" >> ${result_tmp}
        echo -n "
        <tr>
          <th>通信帯域</th><td class=\"sep\">:</td>" > ${band_block}
        echo "<td>${rate}</td>" >> ${band_block}
      else
        if [ -f ${band_block} ]; then
          cat ${band_block} >> ${result_tmp}
        fi
      fi
    fi
  else
    rate=`(ifconfig eth0 | grep "TX bytes" |\
      sed 's/^.* TX bytes:\([0-9]*\).*/\1/';sleep 1;ifconfig eth0 |\
        grep "TX bytes" | sed 's/^.* TX bytes:\([0-9]*\).*/\1/') |\
          awk 'BEGIN{p=0;n=0}{if(NR==1){p=$1}else{n=$1} }\
          END{diff=n-p;rate=(diff*8)/1024/1024;printf "%.2fMbps",rate}'`
    echo -n "
        <tr>
          <th>現通信状況</th><td class=\"sep\">:</td>" >> ${result_tmp}
    echo "<td>${rate}</td>" >> ${result_tmp}
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  ##
  ## ssh tunnel proxy(for web) check
  ##
  echo -n "
        <tr>
          <th>ssh tunnel(web)</th><td class=\"sep\">:</td>" >> ${result_tmp}
  pgrep -f "^ssh .*:80 .*" > /dev/null 2>&1
  stat=$?
  if [ ${stat} -eq 0 ]; then
    echo "<td><font color=\"green\">connected</font></td>" >> ${result_tmp}
  else
    echo "<td><font color=\"red\">not connected</font></td>" >> ${result_tmp}
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  ##
  ## ssh tunnel proxy(for console) check
  ##
  echo -n "
        <tr>
          <th>ssh tunnel(console)</th><td class=\"sep\">:</td>" >> ${result_tmp}
  pgrep -f "^ssh .*:10022 .*" > /dev/null 2>&1
  stat=$?
  if [ ${stat} -eq 0 ]; then
    echo "<td><font color=\"green\">connected</font></td>" >> ${result_tmp}
  else
    echo "<td><font color=\"orange\">not connected</font></td>" >> ${result_tmp}
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  ##
  ## sshd check
  ##
  echo -n "
        <tr>
          <th>sshd</th><td class=\"sep\">:</td>" >> ${result_tmp}
  pgrep -f "sshd .*" > /dev/null 2>&1
  stat=$?
  if [ ${stat} -eq 0 ]; then
    echo "<td><font color=\"green\">running</font></td>" >> ${result_tmp}
  else
    echo "<td><font color=\"red\">not running</font></td>" >> ${result_tmp}
  fi
  echo "
        </tr>" >> ${result_tmp}

  ##
  ## CR Agent ver
  ##
  echo -n "
        <tr>
          <th>CR Agent versoin</th><td class=\"sep\">:</td>" >> ${result_tmp}
  crver=`head -1 /mtd/coolrevo/etc/smc6afirmversion`
  stat=$?
  if [ ${stat} -eq 0 ]; then
    echo "<td>${crver}</td>" >> ${result_tmp}
  else
    echo "<td><font color=\"red\">cannot get ver</font></td>" >> ${result_tmp}
  fi
  echo "
        </tr>" >> ${result_tmp}
  
  checkeddate=`date '+%Y-%m-%d %H:%M:%S'`
  echo "
        <tr>
          <th colspan=\"3\" align=\"right\" class=\"foot\">更新日時：${checkeddate}</th>
        </tr>
      </table>
    </center>
  </body>
</html>" >> ${result_tmp}

  mv -f ${result_tmp} ${resultfile}

  if [ -f ${resultfile} ]; then
    if [ ! -L ${forhuntlink} ]; then
      ln -s ${resultfile} ${forhuntlink}
    else
      nowreakpath=`realpath ${forhuntlink}`
      if [ "${nowreakpath}" != "${resultfile}" ]; then
        outlog ${LOG_WARNING} "${forhuntlink} link to is not ${resultfile}, relink try"
        rm -f ${forhuntlink}
        ln -s ${resultfile} ${forhuntlink}
        outlog ${LOG_INFO} "${forhuntlink} linked to ${resultfile}"
      fi
    fi
  fi

  prev_activate=${activate}
  cnt=`expr ${cnt} + 1`
  if [ ${cnt} -gt 10000 ]; then
    cnt=0
  fi
  sleep ${interval}
done

if [ -f "${PIDFILE}" ]; then
  rm ${PIDFILE}
fi
outlog ${LOG_INFO} "will be shutdown."

exit 0
