#!/bin/sh

# USAGE
#   respawnd.sh
#
#   If aboxd was terminated or dead,
#   respawn daemon.
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   4.1.4 1g

unset LANG
unset LC
unset LC_ALL

TOPDIR=/mtd/coolrevo
export PATH=${PATH}:${TOPDIR}/sbin
export PATH=${PATH}:${TOPDIR}/bin
export PATH=${PATH}:${TOPDIR}/usr/sbin
export PATH=${PATH}:${TOPDIR}/usr/bin
export LD_LIBRARY_PATH=${TOPDIR}/lib:${TOPDIR}/usr/lib:${TOPDIR}/lib/expect5.45.3:/lib:/usr/lib:${LD_LIBRARY_PATH}

# logging
app_log=/mtd/coolrevo/var/log/application.log
# daemon program image
program=/mtd/coolrevo/usr/sbin/aboxd
# bad path daemon program image
badpath=/mnt/sdcard/target/usr/sbin/aboxd
# PID file
pidfile=/mtd/coolrevo/var/run/aboxd.pid
# SHMID file
shmfile=/mtd/coolrevo/var/run/aboxd.shm
# transfer pid file
transfer_pid=/mtd/coolrevo/var/run/transfer.pid
# ntpd pid file
ntpd_pid=/mtd/coolrevo/var/run/poorntpd.pid
# current pid placeholder
curpid=0
# current shm placeholder
curshm=-1
# heartbeat file
heartbeat=/mtd/coolrevo/tmp/aboxd_heartbeat.txt
# DNS force change IP
forcednsip="8.8.8.8"
checkfqdn1="smc-cloud.jp"
checkfqdn2="smcrtsp.treasure-tv.jp"

# loglevel define
LOG_EMERG=0
LOG_ALERT=1
LOG_CRIT=2
LOG_ERR=3
LOG_WARNING=4
LOG_NOTICE=5
LOG_INFO=6
LOG_DEBUG=7
LOG_XDEBUG=8
LOG_NONE=9

app_log=/mtd/coolrevo/var/log/application.log
outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  level=$1
  msg=$2
  me=`basename $0`
  curlevel=`getshm loglevel | grep -e '^[0-9]*$'`
  if [ $? -ne 0 ]; then
    curlevel=6
  fi
  if [ ${curlevel} -ge ${level} ]; then
    echo "${logdate} [$me] [$level] $msg" >> ${app_log}
  fi
}

##
## aboxd heartbeat freeze check.
## 
nowtime=`date '+%s'`
if [ -e ${heartbeat} ]; then
  heartbeattime=`stat -c '%Z' ${heartbeat}`
  difftime=`expr ${nowtime} - ${heartbeattime}`
  if [ ${difftime} -gt 600 ]; then
    outlog ${LOG_ERR} "${heartbeat} is very old, force remove, and force restart aboxd."
    rm -f ${heartbeat}
    killall -KILL aboxd
    sleep 3
    outlog ${LOG_INFO} 'respawn aboxd.'
    # respawn agentbox daemon
    rm /mtd/coolrevo/var/run/aboxd.pid
    # delete shmid file if exists
    if [ -f "${shmfile}" ]; then
      curshm=`cat "${shmfile}"`
      if [ -n "${curshm}" -a "${curshm}" != "-1" ]; then
        outlog ${LOG_DEBUG} "delete curshm=${curshm}"
        ipcrm -m ${curshm}
        curshm=""
      fi
      rm "${shmfile}"
    fi
    /mtd/coolrevo/usr/sbin/aboxd -c /mtd/coolrevo/etc/aboxd.conf
  fi
fi


##
## if .launch file doesnt exist, respawn will be cancelled
##
[ -e /tmp/.launch ] || exit 0

##
## if in process DEACTIVATE, respawn will be cancelled
##
[ -e /tmp/.deactivated ] && exit 0

##
## test ntpd is alive
##
if [ -f ${ntpd_pid} ]; then
  ntpd_ps=`cat ${ntpd_pid}`
  ntpd_alive=`pgrep poorntpd`
  if [ -z ${ntpd_alive} ]; then
    rm ${ntpd_pid}
  else
    if [ ${ntpd_ps} -ne ${ntpd_alive} ]; then
      rm ${ntpd_pid}
    fi
  fi
fi



##
## check stunnel is alive
##
pgrep -f '/mtd/coolrevo/bin/stunnel' > /dev/null
if [ $? -ne 0 ]; then
    outlog ${LOG_WARNING} 'stunnel maybe down. respawn daemon.'
    /mtd/coolrevo/bin/stunnel /mtd/coolrevo/etc/stunnel/stunnel.conf > /dev/null 2>&1 &
fi

##
## check fwupdate_check is alive
##
pgrep -f ".*fwupdate_check" > /dev/null
if [ $? -ne 0 ]; then
  outlog ${LOG_WARNING} "fwupdate_check is not running,start."
  if [ -f /mtd/coolrevo/var/run/fwupdate_check.pid ]; then
    rm /mtd/coolrevo/var/run/fwupdate_check.pid
  fi
  nohup /mtd/coolrevo/usr/sbin/fwupdate_check > /dev/null 2>&1 &
fi

###
### check autortspstatus_check.sh is alive
###
pgrep -f ".*autortspstatus_check.sh" > /dev/null
if [ $? -ne 0 ]; then
  if [ -f /mtd/coolrevo/var/run/autortspstatus_check.pid ]; then
    rm -f /mtd/coolrevo/var/run/autortspstatus_check.pid
  fi
  outlog ${LOG_WARNING} "autortspstatus_check.sh is not running,start."
  nohup /mtd/coolrevo/usr/sbin/autortspstatus_check.sh > /dev/null 2>&1 &
fi

##
## test aboxd runs with bad path
##
run_at_badpath=`pgrep -f "^${badpath}"`
if [ ! -z "${run_at_badpath}" ]; then
  outlog ${LOG_ERR} 'aboxd runs bad path. shutdown and respawn from correct path.'
  kill ${run_at_badpath}
fi

##
## test aboxd is alive
##

# process alival check
alive=`pgrep -f "^${program}" | wc -l`
# pid file is not zero
[ -f "${pidfile}" ] && curpid=`cat "${pidfile}"`
# shm id is not zero,
[ -f "${shmfile}" ] && curshm=`cat "${shmfile}"`

if [ "${alive}" -ne 1 -o "${curshm}" -eq -1 ]; then

  outlog ${LOG_ERR} "alive=${alive}!=1 or curshm=${curshm}==-1,  shutdown aboxd."
  # shutdown bad processes
  killall -TERM aboxd
  if [ -f ${transfer_pid} ]; then
    killall -KILL transfer
  fi

  # wait processes really killed
  sleep 5

  ## shutdown ssh tunnel session
  for i in `pgrep -f "^ssh .*p9876 .*"`
  do
    kill -TERM "${i}"
  done

  # delete transfer pid file if exists
  if [ -f ${transfer_pid} ]; then
    rm ${transfer_pid}
  fi

  # delete pid file if exists
  if [ -f "${pidfile}" ]; then
    rm "${pidfile}"
  fi

  # delete shmid file if exists
  if [ -f "${shmfile}" ]; then
    rm "${shmfile}"
  fi

  outlog ${LOG_INFO} 'respawn aboxd.'
  # respawn agentbox daemon
  rm /mtd/coolrevo/var/run/aboxd.pid
  # delete shmid file if exists
  if [ -f "${shmfile}" ]; then
    curshm=`cat "${shmfile}"`
  fi
  if [ -n "${curshm}" -a "${curshm}" != "-1" ]; then
    outlog ${LOG_DEBUG} "delete curshm=${curshm}"
    ipcrm -m ${curshm}
    curshm=""
  fi
  /mtd/coolrevo/usr/sbin/aboxd -c /mtd/coolrevo/etc/aboxd.conf

fi

##
## check status dns stable
##
forcechangedns=0
/mtd/coolrevo/bin/timeout -s TERM 3 ping -c 1 -W 1 -w 1 ${checkfqdn1} > /dev/null 2>&1
stat1=$?
/mtd/coolrevo/bin/timeout -s TERM 3 ping -c 1 -W 1 -w 1 ${checkfqdn2} > /dev/null 2>&1
stat2=$?
/mtd/coolrevo/bin/timeout -s TERM 3 nslookup ${checkfqdn1} > /dev/null 2>&1
statdns=$?
if [ \( ${stat1} -ne 0 -a ${stat2} -ne 0 \) -o ${statdns} -ne 0 ]; then
  outlog ${LOG_WARNING} "${checkfqdn1} and ${checkfqdn2} access failed or dns query failed."
  if [ ${statdns} -eq 0 ]; then
    /mtd/coolrevo/bin/timeout -s TERM 5 nslookup ${checkfqdn2} | grep Address | sed 's/^Address[ ]*[0-9]*:[ ]*//' | grep -e '[0-9]' > /dev/null 2>&1
    statdnsv6=$?
    if [ ${statdnsv6} -eq 0 ]; then
      outlog ${LOG_WARNING} "${checkfqdn2} is nslookup IPv6 response found."
      forcechangedns=1
    fi
  else
    outlog ${LOG_WARNING} "${checkfqdn2} is nslookup check failed."
    forcechangedns=1
  fi
fi

#dhcp=`wget -q -O - http://127.0.0.1/GetNetwork.cgi | sed 's/^Proto=\([0-9]\).*$/\1/'`

if [ ${forcechangedns} -eq 1 ]; then
  /mtd/coolrevo/bin/timeout -s TERM 3 ping -c 1 -W 1 -w 1 ${forcednsip} > /dev/null 2>&1
  stat=$?
  if [ ${stat} -eq 0 ]; then
    outlog ${LOG_INFO} "${forcednsip} is access success."
    #nowdnsip=`wget -q -O - http://127.0.0.1/GetNetwork.cgi | sed 's/^.*Dns=\([0-9\.]*\).*$/\1/'`
    resolvconf=/etc/resolv.conf
    if [ -f ${resolvconf} ]; then
      nowdnsip=`/mtd/coolrevo/bin/head -1 ${resolvconf} | sed 's/^nameserver[ ]*\([0-9|\.]*\)$/\1/'`
      if [ "${forcednsip}" != "${nowdnsip}" ]; then
        outlog ${LOG_WARNING} "DNS unstable!, force change DNS IP to ${forcednsip}"
        #wget -q -O - http://127.0.0.1/SetNetwork.cgi?Dns=${forcednsip}
        export forcednsip
        cat ${resolvconf} | awk 'BEGIN{forcednsip=ENVIRON["forcednsip"];}{if (NR==1){printf "nameserver %s\n", forcednsip;}else{print $0;}}' > ${resolvconf}.tmp
        stat2=$?
        if [ ${stat2} -ne 0 ]; then
          outlog ${LOG_ERR} "${forcednsip} is force change failed."
          exit ${stat2}
        fi
        if [ -f ${resolvconf}.tmp ]; then
          mv -f ${resolvconf}.tmp ${resolvconf}
        fi
      else
        outlog ${LOG_WARNING} "${forcednsip} is already set DNS1."
      fi
    else
      outlog ${LOG_ERR} "${resolvconf} is not found."
    fi
  fi
fi

exit 0
