#!/bin/sh

# USAGE
#   factorydefault.sh
#
#   once deactivated, clean up user-related
#   files and system halt.
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   1.0.31

unset LANG
unset LC
unset LC_ALL

PATH=/usr/bin:/usr/sbin:/bin:/sbin
PATH=/usr/local/bin:/usr/local/sbin:${PATH}
PATH=/root/target/bin:/root/target/sbin:${PATH}
PATH=/root/target/usr/bin:/root/target/usr/sbin:${PATH}
export PATH

LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib
LD_LIBRARY_PATH=/usr/share/bluetooth/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/home/hybroad/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/usr/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/usr/lib/expect5.45.3:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# output path
output=/tmp/progress.dump

# DO NOT respawn aboxd
touch /tmp/.deactivated


echo -e '!!!THE SYSTEM WILL BE RESTORED FACTORY DEFAULTS!!!\n\n' > ${output}


# kill aboxd
echo 'shutdown agentbox daemon...' >> ${output}
aboxd -k
sleep 1
echo -e 'done.\n\n' >> ${output}

# disable all traffic control
tcctl stop

# kill crond
killall crond

#kill (poor)ntpd
ntpd=`pgrep -f poorntpd`
if [ $? -eq 0 ]; then
  echo 'shutdown poor ntp service...' >> ${output}
  poorntpd -k
fi
ntpd=`pgrep -f ntpd`
if [ $? -eq 0 ]; then
  echo 'shutdown ntp service...' >> ${output}
  kill ${ntpd}
fi
sleep 1
echo -e 'done.\n\n' >> ${output}


# kill transfer
echo 'shutdown transfer agent...' >> ${output}
if [ -f /var/run/transfer.pid ]; then
  kill -SIGKILL $(cat /var/run/transfer.pid)
fi
[ $? -eq 0 ] && echo -e 'done.\n\n' >> ${output}
sleep 1


# delete samba users
echo 'delete samba users...' >> ${output}
aboxpasswd deactivate
[ $? -eq 0 ] && echo -e 'done.\n\n' >> ${output}
sleep 1


# shutdown samba daemon
echo 'shutdown samba...' >> ${output}
if [ -f /var/run/smbd.pid ]; then
  smbstatus -p|grep -E '^([0-9]+).*?'|awk '{system("smbcontrol "$1" close-share "$2)}'
  sleep 3
  smbcontrol smbd shutdown
fi
[ $? -eq 0 ] && echo -e 'done.\n\n' >> ${output}
sleep 1


# shutdown processes using dir/file in mount point
echo 'shutdown sftp process...' >> ${output}
while :
do
  opened_pids=`lsof|grep var/samba|grep -vE '(lsof|grep|aboxd)'|awk '{print $2}'|uniq`
  if [ -z "${opened_pids}" ]; then
    break;
  fi
  kill -SIGINT "${opened_pids}"
done
[ $? -eq 0 ] && echo -e 'done.\n\n' >> ${output}
sleep 1


# delete transfer.conf.sh
echo 'delete transfer agent config if exists...' >> ${output}
transfer_conf=/mnt/sdcard/target/etc/transfer.conf.sh
[ -f "${transfer_conf}" ] && rm -f "${transfer_conf}"
[ $? -eq 0 ] && echo -e 'done.\n\n' >> ${output}
sleep 1


# delete LUKS crypt keys
echo 'delete LUKS crypt key if exists...' >> ${output}
rm -Rf /root/target/etc/luks/*
[ $? -eq 0 ] && echo -e 'done.\n\n' >> ${output}
sleep 1


# delete shm cache file
echo 'delete shared memory cache if exists...' >> ${output}
shm_ini=/root/target/root/shm.ini
[ -f "${shm_ini}" ] && rm -f "${shm_ini}"
[ $? -eq 0 ] && echo -e 'done.\n\n' >> ${output}
sleep 1


# set for dhcp configuration
echo 'delete static IP address configuration if exists...' >> ${output}
yx_cfg_ini=/root/yx_config_network.ini
[ -f "${yx_cfg_ini}" ] && rm -f "${yx_cfg_ini}"
[ $? -eq 0 ] && echo -e 'done.\n\n' >> ${output}
sleep 1


echo 'delete other configuration file if exists...' >> ${output}
other_file=/root/target/root/activated.key
[ -f "${other_file}" ] && rm -f "${other_file}"
other_file=/root/target/root/tunnel.key
[ -f "${other_file}" ] && rm -f "${other_file}"
other_file=/root/target/etc/samba/smb.conf.d/share.conf
[ -f "${other_file}" ] && rm -f "${other_file}"
/root/target/usr/bin/tcedit del

# clean up /var/run
echo -e '\ndeactivate agentbox has successfully finished.' >> ${output}

sync

# finally, system halt
echo -e '\n\nSYSTEM HALTED.' >> ${output}
powerled 0
halt
