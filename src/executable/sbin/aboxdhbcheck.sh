#!/bin/sh

# USAGE
#   aboxhbcheck.sh
#
#   If shm.ini is not timestamp updated,
#   execute aboxd -k or send SIGKILL to aboxd.
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   4.0.1

unset LANG
unset LC
unset LC_ALL

TOPDIR=/mtd/coolrevo
export PATH=${PATH}:${TOPDIR}/sbin
export PATH=${PATH}:${TOPDIR}/bin
export PATH=${PATH}:${TOPDIR}/usr/sbin
export PATH=${PATH}:${TOPDIR}/usr/bin
export LD_LIBRARY_PATH=${TOPDIR}/lib:${TOPDIR}/usr/lib:${TOPDIR}/lib/expect5.45.3:/lib:/usr/lib:${LD_LIBRARY_PATH}

# logging
app_log=/mtd/coolrevo/var/log/application.log

# target program image
program=aboxd
programpath=${TOPDIR}/usr/sbin/${program}
# shm.ini file
shmini=shm.ini
shminipath=${TOPDIR}/etc/${shmini}
# freeze threshold[sec]
freezethr=900

##
## logging
##
outlog()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}


##
## get interval value in shm.ini
##
if [ ! -e "${shminipath}" ]; then
  outlog "${shminipath} is not found."
  exit 1
fi

tmpinterval=`sed 's/[ |\t]*//g' ${shminipath} | awk '
BEGIN{
  flg=0;
  ptn="interval=";
}
{
  if ($0 == "[runtime]") {
    flg=1;
    next;
  }
  if (flg == 0) {
    next;
  }
  else {
    pos=index($0,ptn);
    if (pos > 0) {
      ptnlen=length(ptn);
      len=length($0);
      diflen=len-ptnlen;
      interval=int(substr($0,ptnlen+1,diflen));
      printf "%d\n", interval;
    }
  }
}'`
if [ "${tmpinterval}" != "" ]; then
  freezethr=`expr ${tmpinterval} + 180`
fi


##
## 
##
ps_num=`ps | grep -v grep | grep -c "\<${program}\>"`
if [ "${ps_num}" -eq 0 ]; then
  outlog "${program} is not runninng."
  exit 2
fi

nowtime=`date '+%s'`
modtime=`stat -c "%Y" ${shminipath}`
difftime=`expr ${nowtime} - ${modtime}`
if [ ${difftime} -gt ${freezethr} -a ${difftime} -lt 1500000000 ]; then
  outlog "shm.ini was no update while ${freezethr} sec, execute aboxd -k."
  aboxd -k
  sleep 3
  ps_num=`ps | grep -v grep | grep -c "\<${program}\>"`
  if [ "${ps_num}" -ne 0 ]; then
    outlog "aboxd -k was not effective, it will try sending SIGKILL to aboxd."
    killall -KILL ${program}
  fi
fi

exit 0
