#!/bin/sh

# USAGE
#   thrombusfree.sh
#
#   If transfer is sleeped and hodged,
#   send SIGNAL and restarts.
#
# ARGUMENTS
#   this script requires no option.
#
# OPTION
#   this script has no option.
#
# BUNDLED WITH
#   1.0.34
#
# REVISION
#   0.4

unset LANG
unset LC
unset LC_ALL

PATH=/usr/bin:/usr/sbin:/bin:/sbin
PATH=/usr/local/bin:/usr/local/sbin:${PATH}
PATH=/root/target/bin:/root/target/sbin:${PATH}
PATH=/root/target/usr/bin:/root/target/usr/sbin:${PATH}
export PATH

LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib
LD_LIBRARY_PATH=/usr/share/bluetooth/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/home/hybroad/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/usr/lib:${LD_LIBRARY_PATH}
LD_LIBRARY_PATH=/root/target/usr/lib/expect5.45.3:${LD_LIBRARY_PATH}
export LD_LIBRARY_PATH

# logging
app_log=/root/target/var/log/application.log
# target program image
program=/root/target/usr/sbin/transfer
# PID file
pidfile=/var/run/transfer.pid
# counter cache file prefix
counterfile=/tmp/.thrombus
# target program log
prog_log=/root/target/var/log/transfer.log

##
## logging
##
logger()
{
  logdate=`date '+%Y-%m-%d %H:%M:%S'`
  msg=$1
  me=`basename $0`
  echo "${logdate} [$me] $msg" >> ${app_log}
}

##
## cleanup
##
clean_exit()
{
  removals=`ls ${counterfile}.*`
  if [ -n "${removals}" ]; then
    for r in ${removals}
    do
      logger "thrombus cache file ${r} will be removed."
      rm -f ${r}
    done
  fi
  exit 0
}


##
## if pid file does not exist, action will be cancelled
##
[ -e "${pidfile}" ] || exit 1

##
## if .launch file does not exist, action will be cancelled
##
[ -e /tmp/.launch ] || exit 2

##
## if in process DEACTIVATE, action will be cancelled
##
[ -e /tmp/.deactivated ] && exit 0

##
## check if transfer.log remains unchanged over 15 minutes.
##
curtime=`date +%s`
logstat=`stat -c '%Z' ${prog_log}`
diff=`expr ${curtime} - ${logstat}`
if [ "${diff}" -gt 900 ]; then
  logger "${prog_log} remains unchanged over 15 minutes. restart new process."
  killall -KILL transfer
  sleep 1
  [ -f "${pidfile}" ] && rm -f "${pidfile}"
  exit 0
fi

##
## if transfer runs only 1 process, action will be cancelled
## (if process does not run, transfer will respawn by aboxd)
##
ps_num=`ps | grep "${program}" | grep -v grep | wc -l`
if [ "${ps_num}" -eq 0 -o "${ps_num}" -eq 1 ]; then
  clean_exit
fi

##
## fix transfer deepsleep
##

# master process pid
master_pid=`cat ${pidfile}`
# child process(es) pid
pids=`ps | grep "${program}" | grep -v ${master_pid} | grep -v grep | awk '{print $1}'`

for pid in ${pids}
do

  # child-process deadlock counter
  deadlock_counter=0

  ppid=`cat /proc/${pid}/stat | awk '{print $4}'`
  # ppid equals master process?
  if [ "${ppid}" -eq "${master_pid}" ]; then

    # if counter file exists, get the value
    if [ -f "${counterfile}.${pid}" ]; then
      deadlock_counter=`cat ${counterfile}.${pid}`
    fi

    # store value in cache file
    new_counter=`expr ${deadlock_counter} + 1`
    logger "process doubts deadlock, store thrombus cache ${new_counter} in ${counterfile}.${pid}"
    echo ${new_counter} > "${counterfile}.${pid}"

    if [ "${new_counter}" -gt 5 ]; then
      # process maybe deadlocked. will be killed
      logger "process[pid:${pid}] maybe locked. send signal SIGKILL"
      killall -KILL transfer
      sleep 1
      [ -f "${pidfile}" ] && rm -f "${pidfile}"
    fi

  else

    # process complexed, will be killed
    logger "process[pid:${pid}] doubts complexed[master_pid:${master_pid}]. shutdown and restart new process."
    killall -KILL transfer
    sleep 1
    [ -f "${pidfile}" ] && rm -f "${pidfile}"

  fi

done

exit 0

