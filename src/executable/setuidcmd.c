#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>

#define XARGC(x) (3 + (x - 1))


const char* command_path = "/usr/local/bin/tunnelkey.sh";


int main(int argc, char *argv[]) {

  struct passwd *pw;
  int xargc = XARGC(argc);
  char *xarg[XARGC(argc)];
  int i;

  pw = getpwuid(geteuid());
  if (!(pw && pw->pw_name && pw->pw_name[0])) {
    perror("getpwuid");
    exit(EXIT_FAILURE);
  }
  if (-1 == initgroups(pw->pw_name, pw->pw_gid)) {
    perror("initgropuos");
    exit(EXIT_FAILURE);
  }
  if (-1 == setgid(pw->pw_gid)) {
    perror("setgid");
    exit(EXIT_FAILURE);
  }
  if (-1 == setuid(pw->pw_uid)) {
    perror("setuid");
    exit(EXIT_FAILURE);
  }

  xarg[0] = "-c";
  strcpy(xarg[1], command_path);
  for (i = 2; i < xargc; i++) {
    if (i == (xargc - 1)) {
      xarg[i] = NULL;
    } else {
      xarg[i] = argv[i - 1];
    }
  }

  if (-1 == execv("/bin/sh", xarg)) {
    perror("execv");
    exit(EXIT_FAILURE);
  }
  exit(EXIT_SUCCESS);
}