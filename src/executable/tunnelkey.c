#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <grp.h>

int main(int argc, char *argv[]) {

  struct passwd *pw;
  pw = getpwuid(geteuid());

  if (!(pw && pw->pw_name && pw->pw_name[0])) {
    perror("getpwuid");
    exit(EXIT_FAILURE);
  }
  if (-1 == initgroups(pw->pw_name, pw->pw_gid)) {
    perror("initgropuos");
    exit(EXIT_FAILURE);
  }
  if (-1 == setgid(pw->pw_gid)) {
    perror("setgid");
    exit(EXIT_FAILURE);
  }
  if (-1 == setuid(pw->pw_uid)) {
    perror("setuid");
    exit(EXIT_FAILURE);
  }
  char *xarg[6];
  xarg[0] = "-c";
  xarg[1] = "/usr/local/bin/tunnelkey.sh";
  xarg[2] = argv[1];
  xarg[3] = argv[2];
  xarg[4] = argv[3];
  xarg[5] = NULL;

  if (-1 == execv("/bin/sh", xarg)) {
    perror("execv");
    exit(EXIT_FAILURE);
  }
  exit(EXIT_SUCCESS);
}