#!/bin/sh

##
## not nessesary files delete.
##

TOPDIR=/mtd/coolrevo
if [ -d ${TOPDIR}/share/doc ]; then
  rm -rf ${TOPDIR}/share/doc
fi
if [ -d ${TOPDIR}/share/man ]; then
  rm -rf ${TOPDIR}/share/man
fi

if [ -f ${TOPDIR}/bin/ssh-keyscan ]; then
  rm -f ${TOPDIR}/bin/ssh-keyscan
fi

for i in ${TOPDIR}/lib/lib*.a
do
  rm -f ${i}
done

exit
