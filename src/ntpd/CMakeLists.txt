add_executable(poorntpd
  ../aboxd/fetch.c
  poorntpd.c
)
set(INCLUDE_DIR "../include")
target_include_directories(poorntpd PUBLIC ${INCLUDE_DIR})

link_directories(
  ${agentbox_SOURCE_DIR}/build/i586-pc-linux-gnu/usr/lib
  ${agentbox_SOURCE_DIR}/build/i586-pc-linux-gnu/lib
  ${agentbox_TOOLCHAIN_ROOT}/lib
)
target_link_libraries(poorntpd ini)

target_compile_options(aboxd PRIVATE -s -O2 -Wall -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Wnested-externs -Wbad-function-cast -Wold-style-definition -Wdeclaration-after-statement)
