### poorntpd ###################################################

NTPD_SRCDIR=$(ABSDIR)/ntpd
ifeq "$(strip $(NTPD_SRCDIR))" ""
  NTPD_SRCDIR=.
endif
NTPD_SRCS=$(notdir $(wildcard $(NTPD_SRCDIR)/*.c))
NTPD_OBJS=$(notdir $(NTPD_SRCS:.c=.o))
NTPD_TGTS=poorntpd
NTPD_LIBS=-lm -L$(SMC3APLUSDIR)/libini -lini -lcrypto -lssl -lcurl

NTPD_DEPS=fetch.o

################################################################

$(NTPD_OBJS):
	cd $(NTPD_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c $(@F:.o=.c) -o $@

$(NTPD_TGTS): $(NTPD_OBJS) ntpd-depnds
	cd $(NTPD_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LINKER_FLAGS) $(LDFLAGS) $(NTPD_OBJS) $(NTPD_DEPS) -o $@ $(NTPD_LIBS)
ifeq ($(BUILD_TYPE),Release)
	cd $(NTPD_SRCDIR); \
	$(STRIP) $@
endif

ntpd: $(NTPD_TGTS)

ntpd-depnds:
	cd $(NTPD_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c ../aboxd/fetch.c -o ./fetch.o;

ntpd-install: ntpd install-dir
	cp $(NTPD_SRCDIR)/$(NTPD_TGTS) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(NTPD_TGTS)

ntpd-uninstall: ntpd
	$(RM) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(NTPD_TGTS)

ntpd-clean:
	cd $(NTPD_SRCDIR); \
	$(RM) $(NTPD_TGTS) *.o

ntpd-dry:
	@cd $(NTPD_SRCDIR); \
	echo "NTPD_SRCS='$(NTPD_SRCS)'"; \
	echo "NTPD_OBJS='$(NTPD_OBJS)'"; \
	echo "NTPD_TGTS='$(NTPD_TGTS)'"; \
	echo "NTPD_LIBS='$(NTPD_LIBS)'"
