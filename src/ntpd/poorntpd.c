/**
 * @file    poorntpd.c
 * @brief   HTTPプロトコルで時刻設定するコマンドユーティリティ
 * @author  cool-revo inc.
 */
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <curl/curl.h>
#include "fetch.h"

#define DEF_NTPD_LOGFILE "/mtd/coolrevo/var/log/application.log"

#define logMessage(a,c){ntpdLogMessage(a,__FILE__,__LINE__,c);}
#define logMessage2(a,fmt, ...) {ntpdLogMessage2(a,__FILE__,__LINE__,fmt,__VA_ARGS__);}

#define DATE_LENGTH         64
#define BUFF_LENGTH_SHORT   64
#define BUFF_LENGTH        512
#define BUFF_LENGTH_LONG  2048
#define FNAME_LENGTH       256

#define DEF_LOG_ERR   1
#define DEF_LOG_WRN   2
#define DEF_LOG_INF   3
#define DEF_LOG_DTL   4
#define DEF_LOG_DBG   5

/** シグナル受信用割込フラグ */
static int interrupted = 0;


/* プロトタイプ */
void sigcatch(int);
int fetch_date(char*, const char*, double*);
void ntpdLogMessage(int, char*, int, char*);
void ntpdLogMessage2(int, char*, int, char*, ...);
static char* getnowdate(char*);

int g_logLevel = DEF_LOG_INF;
char g_mycmd[FNAME_LENGTH];


/**
 * シグナル処理のコールバック
 *
 * @param[in] sig シグナル番号
 */
void sigcatch(int sig)
{
  if ((SIGINT == sig) || (SIGTERM == sig)) {
    interrupted = 1;
  }
}


/**
 * リモートコンテンツを取得する
 *
 * @param[in,out] dest 取得したコンテンツをコピーする
 * @param[in] url コンテンツのあるリモートURI
 * @param[in,out] totaltime コンテンツ取得に要した時間
 * @return curl_easy_performの戻り値
 */
int fetch_date(char* dest, const char* url, double* totaltime) {
  CURL* curl;
  MEMFILE* mf = NULL;
  int result = 0;
  int resp_code = 0;
  struct curl_slist* resolver = NULL;

  /* resolv.confが壊れても管理サーバの名前解決を可能にする */
  resolver = curl_slist_append(NULL, "smc-cloud.jp:443:202.32.197.24");

  fprintf(stderr, "in fetch_date.\n");
  mf = memfopen();
  curl = curl_easy_init();
  curl_easy_setopt(curl, CURLOPT_URL, url);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
  curl_easy_setopt(curl, CURLOPT_SSL_VERIFYSTATUS, 0);
  curl_easy_setopt(curl, CURLOPT_WRITEDATA, mf);
  curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, memfwrite);
  curl_easy_setopt(curl, CURLOPT_RESOLVE, resolver);
  fprintf(stderr, "  befere curl_easy_perform.\n");
  result = curl_easy_perform(curl);
  fprintf(stderr, "  after curl_easy_perform result:%d.\n", result);
  if (0 == result) {
    if (mf->size >= 32) {
      strncpy(dest, memfstrdup(mf), 32);
    }
    else if (mf->size > 0) {
      strcpy(dest, memfstrdup(mf));
    }
    fprintf(stderr, "  after strcpy.\n");
    curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, totaltime);
  }
  curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &resp_code);
  curl_easy_cleanup(curl);
  memfclose(mf);
  if (result != 0 && resp_code == 200) {
    result = 0;
  }
  else if (resp_code != 200) {
    logMessage2(DEF_LOG_ERR,
              "fetch_date failed.result::%d,resp_code:%d",
              result, resp_code);
    result = resp_code;
  }
  logMessage2(DEF_LOG_DBG,
            "fetch_date result::%d,resp_code:%d",
            result, resp_code);
  return result;
}

static char* getnowdate(char* i_datestr) {
  time_t l_now = 0;
  struct tm* l_strtim = (struct tm*)NULL;

  if (i_datestr == (char*)NULL) {
    return((char*)NULL);
  }

  time(&l_now);
  l_strtim = localtime(&l_now);

  sprintf(i_datestr, "%04d-%02d-%02d %02d:%02d:%02d",
    l_strtim->tm_year + 1900,
    l_strtim->tm_mon + 1,
    l_strtim->tm_mday,
    l_strtim->tm_hour,
    l_strtim->tm_min,
    l_strtim->tm_sec
  );
  return(i_datestr);
}


void ntpdLogMessage(int i_logLevel, char *i_FILE, int i_LINE, char *i_errmsg) {
  char l_buff2[4096];
  char l_msg[4096];
  char l_nowdatestr[DATE_LENGTH];
  FILE *l_fp = (FILE*)NULL;
  char l_mode[8];

  if (i_logLevel > g_logLevel) {
    return;
  }
  if (i_errmsg == (char*)NULL || (i_errmsg != (char*)NULL && *i_errmsg == '\0')) {
    return;
  }

  switch (i_logLevel) {
    case DEF_LOG_ERR:
      strcpy(l_mode, "ERR");
      break;
    case DEF_LOG_WRN:
      strcpy(l_mode, "WRN");
      break;
    case DEF_LOG_INF:
      strcpy(l_mode, "INF");
      break;
    case DEF_LOG_DTL:
      strcpy(l_mode, "DTL");
      break;
    case DEF_LOG_DBG:
      strcpy(l_mode, "DBG");
      break;
  }

  strncpy(l_buff2, i_errmsg, sizeof(l_buff2)-1);
  l_buff2[sizeof(l_buff2)-1] = '\0';

  sprintf(l_msg, "%s :%s::%s:%s:%d:%s", 
    getnowdate(l_nowdatestr), l_mode, g_mycmd, i_FILE, i_LINE, l_buff2);
  l_msg[2048-1] = '\0';

  l_fp = fopen(DEF_NTPD_LOGFILE, "a+");
  if (l_fp != (FILE*)NULL) {
    fprintf(l_fp, "%s\n", l_msg);
    fflush(l_fp);
    fclose(l_fp);
    l_fp = (FILE*)NULL;
  }
  else {
    fprintf(stdout, "%s\n", l_msg);
    fflush(stdout);
  }

  return;
}

void ntpdLogMessage2(int i_logLevel, char *i_FILE, int i_LINE, char *i_fmt, ...) {
  va_list arg;
  char l_buff[8192];
  char l_buff2[4096];
  char l_msg[8192];
  char l_nowdatestr[DATE_LENGTH];
  FILE *l_fp = (FILE*)NULL;
  char l_mode[8];

  if (i_logLevel > g_logLevel) {
    return;
  }

  switch (i_logLevel) {
    case DEF_LOG_ERR:
      strcpy(l_mode, "ERR");
      break;
    case DEF_LOG_WRN:
      strcpy(l_mode, "WRN");
      break;
    case DEF_LOG_INF:
      strcpy(l_mode, "INF");
      break;
    case DEF_LOG_DTL:
      strcpy(l_mode, "DTL");
      break;
    case DEF_LOG_DBG:
      strcpy(l_mode, "DBG");
      break;
  }

  va_start(arg, i_fmt);
  vsprintf(l_buff, i_fmt, arg);
  va_end(arg);

  strncpy(l_buff2, l_buff, sizeof(l_buff2)-1);
  l_buff2[sizeof(l_buff2)-1] = '\0';

  sprintf(l_msg, "%s :%s::%s:%s:%d:%s", 
    getnowdate(l_nowdatestr), l_mode, g_mycmd, i_FILE, i_LINE, l_buff2);
  l_msg[2048-1] = '\0';

  l_fp = fopen(DEF_NTPD_LOGFILE, "a+");
  if (l_fp != (FILE*)NULL) {
    fprintf(l_fp, "%s\n", l_msg);
    fflush(l_fp);
    fclose(l_fp);
    l_fp = (FILE*)NULL;
  }
  else {
    fprintf(stdout, "%s\n", l_msg);
    fflush(stdout);
  }

  return;
}


/**
 * メイン処理
 *
 * @param[in] argc コマンドラインオプション数
 * @param[in] argv コマンドラインオプションを表す配列
 * @return int 終了ステータス
 * @sa <aboxd.h> プログラム終了ステータス定義
 */
int main(int argc, char** argv)
{
  /** コマンドラインオプションのコード。
   * command -h とした場合'h' のコードが代入される。
   * command --help とした場合struct longopt 構造体の第4メンバが代入される。
   */
  int opt;
  /** @var flag daemon動作のフラグ */
  int flag = 1;
  /** フラグがFLAG_DAEMONIZE(=0)の時はカレントディレクトリを "/" (ルート) にchrootする */
  int nochdir = 1;
  /** フラグがFLAG_DAEMONIZE(=0)の時は標準入力・標準出力・標準エラーを閉じる */
  int noclose = 0;
  /** フラグが1の時は時間補正後、すぐに終了する */
  int quit_if_sync = 0;
  /** PIDを格納 */
  pid_t pid;
  /** PIDファイルディスクリプタ */
  FILE* pid_fp;
  /** PIDファイルパス */
  const char* pid_path = "/mtd/coolrevo/var/run/poorntpd.pid";
  /** このプログラム名 */
  const char* appname = "poorntpd";
  /** 入出力バッファ */
  char* buffer;
  /** 時刻を取得するhttpサーバ */
  const char* server = "https://smc-cloud.jp/api/ntp/microepoch";
  /** レスポンスから変換した時刻を格納する */
  double capture = 0;
  /** レスポンス取得に要した時間を格納する */
  double elapsed = 0;
  /** 新時間を格納するプレースホルダ */
  struct timeval when;
  /** 直前の実行時刻 */
  time_t prev_exec = 0;
  /**
   * @struct option longopt
   * @brief コマンドライン引数のテーブル
   * @par 構造体メンバ
   *      - <b>第1メンバ</b> char  オプション名 (--help の場合 "help"）
   *      - <b>第2メンバ</b> char  必須属性とオプション値必要の有無
   *      - <b>第3メンバ</b> *void 未使用のため、NULLを代入
   *      - <b>第4メンバ</b> int   変数optに代入されるコード
   * @par 必須属性
   *      - <b>no_argument</b> 値を必要としない
   *      - <b>required_argument</b> 値を必要とする
   *      - <b>optional_argument</b> オプション
   */
  const struct option longopt[] = {
    {"front"   , no_argument      , NULL, 'f'},
    {"help"    , no_argument      , NULL, 'h'},
    {"kill"    , no_argument      , NULL, 'k'},
    {"quit"    , no_argument      , NULL, 'q'},
    {"server"  , optional_argument, NULL, 's'},
    {"version" , no_argument      , NULL, 'v'},
    {0         , 0                , 0   ,  0 }
  };

  int l_rtn = 0;

  /* シグナルの処理 */
  signal(SIGPIPE, SIG_IGN);
  signal(SIGINT,  sigcatch);
  signal(SIGTERM, sigcatch);

  /* 起動時のオプションをパースする */
  while ((opt = getopt_long(argc, argv, "fkqs:v", longopt, NULL)) != -1) {
    switch (opt) {
      case 'f':
        flag = 0;
        break;
      case 'k':
        flag = 2;
        break;
      case 'q':
        quit_if_sync = 1;
        break;
      case 's':
        server = optarg;
        break;
      case 'v':
        fputs("poorntpd Version 1.0.27", stdout);
        return 0;
      default:
        fputs("Usage: poorntpd [-s http://your.ntp.server] [-f]\n"
              "       poorntpd -k\n\n"
              "Options: \n"
              " -f --front   : run in frontend, no daemon.\n"
              " -h --help    : show this help.\n"
              " -k --kill    : kill daemon running specified PID.\n"
              " -q --quit    : quit immediately if synchronize.\n"
              " -s --server  : specify http time server.\n"
              " -v --version : show version.\n\n"
              "(c) 2017 cool-revo inc.", stdout);
        return 0;
    }
  }

  /* オプション --killが指定された場合はdaemonにシグナルを送る */
  if (2 == flag) {
    if (NULL != (pid_fp = fopen(pid_path, "r"))) {
      fscanf(pid_fp, "%d\n", &pid);
      fclose(pid_fp);
      if (0 == kill(pid, SIGTERM)) {
        fprintf(stderr, "%s will be shutdown.\n", appname);
      }
    } else {
      fprintf(stderr, "%s not started.\n", appname);
      return -1;
    }
    return 0;
  }

  /* 重複起動チェック */
  if (NULL != (pid_fp = fopen(pid_path, "r"))) {
    fclose(pid_fp);
    fputs("poorntpd maybe already started.\n", stderr);
    return -1;
  }

  /* daemon起動 */
  if (flag == 1) {
    if (-1 == daemon(nochdir, noclose)) {
      fprintf(stderr, "%s: failed to launch daemon.\n", appname);
      return -1;
    }
  }

  /* PIDを取得し、*.pidを作成 */
  pid = getpid();
  if (NULL == (pid_fp = fopen(pid_path, "w"))) {
    fprintf(stderr, "%s: failed to create pidfile.\n", appname);
    return -1;
  }
  fprintf(pid_fp, "%d\n", pid);
  fclose(pid_fp);

/* daemon化時のループ処理 */

  while (1) {
    /* SIGTERM||SIGINTを受信したら終了 */
    if (0 != interrupted) {
      break;
    }
    /* 1時間毎に実行 */
    if ((0 == prev_exec) || ((time(NULL) - prev_exec) > 3600)) {
      elapsed = 0;
      buffer = (char *) malloc(sizeof(char) * 32);
      if (NULL != buffer) {
        memset(buffer, 0, sizeof(char) * 32);
        fprintf(stderr, "%s::before fetch_date.\n", appname);
        l_rtn = fetch_date(buffer, server, &elapsed);
        fprintf(stderr, "%s::after fetch_date.\n", appname);
        if (0 == l_rtn) {
          fprintf(stderr, "%s::fetch_date is success.\n", appname);
          sscanf(buffer, "%lf", &capture);
          if (buffer[0] == '\0' || capture <= 0.0) {
            logMessage2(DEF_LOG_ERR,
              "buffer:%s by fetch_date is wrong.", buffer);
          }
          else {
            logMessage2(DEF_LOG_DBG,
              "buffer:%s by fetch_date success.", buffer);
            when.tv_sec  = (long int) floor(capture);
            when.tv_usec = (long int) ceil((capture - floor(capture) + (elapsed * 127)) * 1000);
            settimeofday(&when, 0);
          }
          prev_exec = time(NULL);
        }
        else {
          fprintf(stderr, "%s::fetch_date is failed:%d.\n", appname, l_rtn);
          logMessage2(DEF_LOG_ERR, "fetch_date is failed:%d.", l_rtn);
        }
        free(buffer);
        if (404 == l_rtn) {
          fprintf(stderr, "%s::fetch_date is http code::%d, break.\n", appname, l_rtn);
          logMessage2(DEF_LOG_ERR, "fetch_date is http code:%d, break", l_rtn);
          break;
        }
      }
    }
    /* quit_if_syncが1の時は時間補正後終了 */
    if (1 == quit_if_sync) {
      raise(SIGTERM);
      break;
    }
    sleep(5);
  }

/* プロセス終了の処理 */
  if (-1 == remove(pid_path)) {
    perror("remove() removal PID file error.\n");
  }
  return 0;
}
