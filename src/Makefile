# @build 2016-11-09 16:24:22
#
# make DISTFOR=SMC3APLUS (or without DISTFOR) ... SMC-3A Plus distribution build
# make DISTFOR=X86                       ... debugging x86 Linux build
#
# Other Defines
#   ROOTFSDIR   ... Path for base of rootfs.img
#
# Example:
#   make ROOTFSDIR=/home/user/A312_rootfs aboxd
#   make DISTFOR=X86 ROOTFSDIR=/home/user/A312_x68_64/rootfs all
#

OSTYPE=$(shell uname -s)

SRCDIR=.
ABSDIR=$(shell pwd)
SMC3APLUSDIR=$(shell pwd)
BUILDDIR=$(ABSDIR)/build
DISTDIR=/mtd/coolrevo
#INSTALL_DIR=$(ABSDIR)/dist
#INSTALL_DIR=$(DISTDIR)/usr/sbin
INSTALL_DIR=

ifndef ROOTFSDIR
  ROOTFSDIR=/home/developer/A312/dist/image/rootfs
endif
INSTALL_PREFIX=
CROSS_COMPILE=

ifndef DISTFOR
  DISTFOR=SMC3APLUS
endif
ifeq ($(DISTFOR),SMC3APLUS)
  ifneq ($(OSTYPE),Linux)
    $(error DISTFOR=A312 cannot build under Darwin or Other environment.)
  endif
  #export PATH:=/opt/hisi-linux/x86-arm/arm-hisiv200-linux/bin:$(PATH)
  #export PATH:=/opt/hisi-linux/x86-arm/arm-hisiv500-linux/bin:$(PATH)
  export PATH:=/opt/toolchain/rsdk-6.5.0-5281-EL-4.9-u1.0-m32fut-191025p1/bin:/opt/toolchain/rsdk-6.5.0-5281-EL-4.9-u1.0-m32fut-191025p1/mips-linux/bin:$(PATH)

  #CROSS_COMPILE:=arm-hisiv200-linux-gnueabi-
  #CROSS_COMPILE:=arm-histbv310-linux-
  #CROSS_COMPILE:=arm-hisiv500-linux-uclibcgnueabi-
  CROSS_COMPILE=mips-linux-uclibc-
endif

ifeq ($(DISTFOR),X86)
  ifeq ($(OSTYPE),Linux)
  endif
  ifeq ($(OSTYPE),Darwin)
    export PATH:=/usr/local/gcc-4.8.1-for-linux32/bin:$(PATH)
    CROSS_COMPILE:=i586-pc-linux-
  endif
endif

ifndef BUILD_TYPE
  BUILD_TYPE=Release
endif

### path config ################################################

CC=$(CROSS_COMPILE)gcc
CXX=$(CROSS_COMPILE)g++
AR=$(CROSS_COMPILE)ar
RANLIB=$(CROSS_COMPILE)ranlib
OBJCOPY=$(CROSS_COMPILE)objcopy
OBJDUMP=$(CROSS_COMPILE)opjdump
NM=$(CROSS_COMPILE)gcc-nm
LD=$(CROSS_COMPILE)ld
STRIP=$(CROSS_COMPILE)strip --strip-unneeded
RM=rm -f
MKDIR=mkdir -p

#CPPFLAGS=-I$(ABSDIR)/include -I$(DISTDIR)/usr/include \
#         -I$(ROOTFSDIR)$(DISTDIR)/usr/include \
#         -I$(ROOTFSDIR)/usr/include
CPPFLAGS=-I$(SMC3APLUSDIR)/include \
         -I$(SDK_TOOLCHAINDIR)/include \
         -I$(SDK_TOOLCHAINDIR)/usr/include \
         -I$(SDK_TOOLCHAINDIR)/target/include \
         -I$(SDK_TOOLCHAINDIR)/target/usr/include \
         -I$(SDK_DEVDIR)/include \
         -I$(DISTDIR)/include \
         -I$(DISTDIR)/usr/include
CFLAGS=-D_SVID_SOURCE -D_GNU_SOURCE -D_BSD_SOURCE -DMIPS_REALTEK -std=c99 \
       -Wall -Wstrict-prototypes -Wstrict-aliasing \
       -Wmissing-prototypes -Wmissing-declarations \
       -Wnested-externs -Wbad-function-cast \
       -Wold-style-definition -Wdeclaration-after-statement
CXXFLAGS=$(CFLAGS)
#LDFLAGS=-L$(DISTDIR)/usr/lib -L$(DISTDIR)/lib \
#        -L$(ROOTFSDIR)$(DISTDIR)/usr/lib -L$(ROOTFSDIR)$(DISTDIR)/lib \
#        -L$(ROOTFSDIR)/usr/lib -L$(ROOTFSDIR)/lib
LDFLAGS=-L$(SDK_TOOLCHAINDIR)/lib \
        -L$(SDK_TOOLCHAINDIR)/usr/lib \
        -L$(SDK_DEVDIR)/lib \
        -L$(DISTDIR)/lib
#LIBS=-lini -lcrypto -lcurl -ljansson
LIBS=$(SMC3APLUSDIR)/libini/libini.a -lcrypto -lcurl -ljansson -lpthread
#LINKER_FLAGS=-Wl,-no-as-needed -Wl,-rpath-link,$(DISTDIR)/usr/lib:$(DISTDIR)/lib:$(ROOTFSDIR)$(DISTDIR)/usr/lib:$(ROOTFSDIR)$(DISTDIR)/lib:$(ROOTFSDIR)/usr/lib:$(ROOTFSDIR)/lib
LINKER_FLAGS=-Wl,-no-as-needed -Wl,-rpath-link,$(DISTDIR)/usr/lib:$(DISTDIR)/lib:$(SDK_TOOLCHAINDIR)/lib:$(SDK_TOOLCHAINDIR)/usr/lib:$(SDK_DEVDIR)/lib
DOCUMENTOR:=doxygen

ifeq ($(DISTFOR),X86)
  ifeq ($(OSTYPE),Darwin)
    LDFLAGS=-L$(ROOTFSDIR)$(DISTDIR)/usr/lib -L$(ROOTFSDIR)$(DISTDIR)/lib \
            -L$(ROOTFSDIR)/usr/lib -L$(ROOTFSDIR)/lib
    LINKER_FLAGS=-Wl,-no-as-needed -Wl,-rpath-link,$(ROOTFSDIR)$(DISTDIR)/usr/lib:$(ROOTFSDIR)$(DISTDIR)/lib:$(ROOTFSDIR)/usr/lib:$(ROOTFSDIR)/lib
  endif
endif

ifeq ($(BUILD_TYPE),Release)
  CFLAGS+= -O2
  CXXFLAGS=$(CFLAGS)
  LINKER_FLAGS+= -s
else
  CFLAGS+= -g3 -O0 -D_DEBUG
  CXXFLAGS=$(CFLAGS)
endif

### includes ###################################################

include aboxd/Makefile.mk
include cgi/Makefile.mk
include getshm/Makefile.mk
include libini/Makefile.mk
include ntpd/Makefile.mk
include cr_nslookup/Makefile.mk
include nohup/Makefile.mk
#include sendevent/Makefile.mk

### begin rules ################################################

help:
	@echo
	@echo "Agentobox Makefile rules:"
	@echo "  all: (default)"
	@echo "     Make all Programs."
	@echo "  aboxd:"
	@echo "     Build aboxd daemon program."
	@echo "  cgi:"
	@echo "     Build cgi, distribute web interface files."
	@echo "  getshm:"
	@echo "     Build getshm command-line program."
	@echo "  libini:"
	@echo "     Build libini.a static library and place Libs/Includes."
	@echo "  ntpd:"
	@echo "     Build poorntpd daemon program."
	@echo "  powerled:"
	@echo "     Build A312 front panel LED control program."
	@echo "  watchcat:"
	@echo "     Build process watch and respawn program."
	@echo
	@echo "  Each targets has sub-targets"
	@echo "   *-install:"
	@echo "   *-uninstall:"
	@echo "   *-clean:"
	@echo "   *-dry:"
	@echo
	@echo "  archive-tgz:"
	@echo "     Copy all programs, then create target.tgz archived file."
	@echo "  doc:"
	@echo "     Output Doxygen documents."
	@echo "  printenv:"
	@echo "     Display compile-time environment variables."
	@echo "  help:"
	@echo "     Display this message."

printenv:
	@echo "PATH='$(PATH)'"
	@echo "DISTFOR='$(DISTFOR)'"
	@echo "OSTYPE='$(OSTYPE)'"
	@echo "CROSS_COMPILE='$(CROSS_COMPILE)'"
	@echo "CC='$(CC)'"
	@echo "CXX='$(CXX)'"
	@echo "CPPFLAGS='$(CPPFLAGS)'"
	@echo "CFLAGS='$(CFLAGS)'"
	@echo "CXXFLAGS='$(CXXFLAGS)'"
	@echo "LDFLAGS='$(LDFLAGS)'"
	@echo "LINKER_FLAGS='$(LINKER_FLAGS)'"
	@echo "LIBS='$(LIBS)'"
	@echo "INSTALL_DIR='$(INSTALL_DIR)'"

install-dir:
	$(MKDIR) $(INSTALL_DIR)$(DISTDIR)
	$(MKDIR) $(INSTALL_DIR)$(DISTDIR)/{etc,usr,var}
	$(MKDIR) $(INSTALL_DIR)$(DISTDIR)/etc/{httpd,samba,udev}
	$(MKDIR) $(INSTALL_DIR)$(DISTDIR)/usr/{bin,include,lib,libexec,sbin,share}
	$(MKDIR) $(INSTALL_DIR)$(DISTDIR)/var/{empty,lib,lock,run,samba,spool,tmp}

.c.o:
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c $<

.cpp.o:
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LDFLAGS) -c $<

default: all

all: depend aboxd getshm ntpd cr_nslookup nohup sendevent

depend: libini

clean: libini-clean aboxd-clean cgi-clean getshm-clean ntpd-clean cr_nslookup-clean nohup-clean sendevent-clean

doc:
	$(DOCUMENTOR)

doc-clean:
	$(RM) -R documentation

.PHONY: clean
