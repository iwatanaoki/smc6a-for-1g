/**
 * @file    aboxd.c
 * @brief   SMC-3A Plus メインスレッド
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_ABOXD_C
#define AGENTBOX_ABOXD_C
#endif // AGENTBOX_ABOXD_C

#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <getopt.h>
#include <signal.h>
#include <time.h>
#include <sys/stat.h>
#include "abox_macro.h"
#include "aboxd.h"
#include "shm.h"
#include "ipaddr.h"
#include "json.h"
#include "auth.h"
#include "logger.h"
#include "sshtunnel.h"
#include "ntp.h"
#include "ipc.h"
#include "rtsp.h"
#include "storage.h"
#include "samba.h"
#include "base64utl.h"


/** 共有メモリ構造体を指すポインタ */
ABOX_SHM* shm_ptr;

/** 共有メモリIDを格納するプレースホルダ */
int shm_id;

/** 共有メモリIDを出力するファイルパス */
const char* shm_file;

/** メッセージキューを作成する共通キー */
int msq_id;

/** シグナル受信用割込フラグ */
static int interrupted = 0;

const char g_sig_str[38][20] = {
  "NONE",
  "SIGHUP",
  "SIGINT",
  "SIGQUIT",
  "SIGILL",
  "SIGTRAP",
  "SIGABRT",
  "SIGBUS",
  "SIGFPE",
  "SIGKILL",
  "SIGUSR1",
  "SIGSEGV",
  "SIGUSR2",
  "SIGPIPE",
  "SIGALRM",
  "SIGTERM",
  "SIGSTKFLT",
  "SIGCHLD",
  "SIGCONT",
  "SIGSTOP",
  "SIGTSTP",
  "SIGTTIN",
  "SIGTTOU",
  "SIGURG",
  "SIGXCPU",
  "SIGXFSZ",
  "SIGVTALRM",
  "SIGPROF",
  "SIGWINCH",
  "SIGIO",
  "SIGPWR",
  "SIGSYS",
  "NONE",
  "NONE",
  "NONE",
  "NONE",
  "NONE",
  ""
};

int g_isSetSD = false;  // SDカードの接続状態(false:未接続、true:接続中）
int g_prev_isSetSD = false;  // SDカードの直前接続状態(false:未接続、true:接続中）

int g_isSaveSDcard = false;  // SDカード保存機能有効状態（false:無効、true:有効)
int g_prev_isSaveSDcard = false;

int g_isDisconnectRtspServer = false;  // rtspサーバ接続可否フラグ（false:接続可、true:不可）
int g_prev_isDisconnectRtspServer = false;

int g_autoConnect = false;      // 自立接続モード
int g_prev_autoConnect = false; // 自立接続モード(直前状態の保存）

int g_getallcount = 0;          // getall コール成功時に 1セット
int g_getallfailcount = 0;      // getall コール失敗時に カウントアップ


/**
 * シグナル処理のコールバック
 *
 * @param[in] sig シグナル番号
 */
void sigcatch(int sig)
{
  fprintf(stderr, "%s: signal(%d):%s catched.\n", ABOXD_PROG_NAME, sig, g_sig_str[sig]);
  fflush(stderr);
  logger_write(LOG_DEBUG, "%s: signal(%d):%s catched. %s", ABOXD_PROG_NAME, sig, g_sig_str[sig], HERE);
  switch (sig) {
    case SIGINT:
    case SIGQUIT:
    case SIGTERM:
      interrupted = 1;
      break;
    case SIGSEGV:
    case SIGBUS:
      exit(2);
      break;
    case SIGCONT:
      interrupted = 0;
      break;
    case SIGHUP:
      logger_close();
      logger_open();
      writecount = 1;
      logger_write(LOG_DEBUG, "%s: aboxd.log close and open. %s", ABOXD_PROG_NAME, HERE);
      break;
    default:
      break;
  }
}


/**
 * .confファイルのパース用コールバック
 *
 * libini の関数 ini_parse(const char*, ini_handler, void*) のコールバックとして使用する
 *
 * @param[in,out] user 値をコピーする対象へのポインタ。対象に応じて内部でキャストする
 * @param[in] section .confのセクション部(ブラケット[]で囲まれた箇所)の名称
 * @param[in] name    .confのパラメータ名
 * @param[in] value   .confのパラメータの値
 * @retval 0 正常終了(パースエラーなく成功)
 * @sa <libini.h> ini_parse
 */
int parse_handler(void* user, const char* section, const char* name, const char* value)
{
  config_t* pcfg;
  unsigned int conv;

  if (NULL == user) {
    return(0);
  }
  if (NULL == section || (NULL != section && '\0' == *section)) {
    return(0);
  }
  if (NULL == name) {
    return(0);
  }
  if (NULL == value) {
    return(0);
  }
  pcfg = (config_t *) user;
  if (0 == strcmp(section, "aboxd")) {
    if (0 < strlen(value)) {
      if (0 == strcmp(name, "interval")) {
        conv = strtoul(value, (char **) NULL, 10);
        if (conv < ABOXD_MIN_LOOP_INTERVAL) {
          conv = ABOXD_MIN_LOOP_INTERVAL;
        }
        pcfg->interval = conv;
      } else if (0 == strcmp(name, "pidfile")) {
        pcfg->pidfile = strdup(value);
      } else if (0 == strcmp(name, "shmfile")) {
        pcfg->shmfile = strdup(value);
        shm_file = strdup(value);
      } else if (0 == strcmp(name, "logfile")) {
        pcfg->logfile  = strdup(value);
      } else if (0 == strcmp(name, "cacert")) {
        pcfg->cacert = strdup(value);
      } else if (0 == strcmp(name, "server")) {
        pcfg->server = strdup(value);
      } else if (0 == strcmp(name, "csurl")) {
        pcfg->csurl = strdup(value);
      } else if (0 == strcmp(name, "tunnelurl")) {
        pcfg->tunnelurl = strdup(value);
      }
    }
  }
  return 0;
}

/**
 * カメラ F/W バージョン情報をクラウド管理画面に送信する
 *
 */
void send_version(void)
{
  int abox_id;
  int api_result;
  char* data;
  char* response;
  char* cmdstr;
  size_t buffer_len;
  char *ver_ptr = (char*)NULL;
  static char s_prev_ver[BUF_128];
  static int  s_count = 0;
  static int  s_prev_failed = -1;


  if (s_count == 0) {
    memset(s_prev_ver, 0, sizeof(s_prev_ver));
    s_count = 1;
  }

  if (g_getallcount == 0) {
    logger_write(LOG_NOTICE,
      "getall success not yet. %s", HERE);
    return;
  }

  buffer_len = sizeof(char) * BUF_1024;
  data     = (char *) malloc(buffer_len);
  response = (char *) malloc(buffer_len);
  cmdstr   = (char *) malloc(buffer_len);
  if (NULL == data || NULL == response || NULL == cmdstr) {
    logger_write(LOG_ERR,
      "data or response or cmdstr malloc failed. %s", HERE);
    if (NULL != data) {
      free(data);
    }
    if (NULL != response) {
      free(response);
    }
    if (NULL != cmdstr) {
      free(cmdstr);
    }
    return;
  }

  logger_write(LOG_DEBUG,
    "send firmware version. %s", HERE);
  ver_ptr = get_abox_version();
  if (shm_ptr->version[0] == '\0') {
    if (ver_ptr != NULL) {
      strcpy(shm_ptr->version, ver_ptr);
    }
    else {
      logger_write(LOG_ALERT,
        "cannot get firmware version. %s", HERE);
      free(cmdstr);
      free(response);
      free(data);
      return;
    }
  }
  else {
    if (ver_ptr != NULL) {
      if (strcmp(shm_ptr->version, ver_ptr) != 0) {
        strcpy(shm_ptr->version, ver_ptr);
      }
    }
    else {
      logger_write(LOG_ALERT,
        "cannot get firmware version. %s", HERE);
      free(cmdstr);
      free(response);
      free(data);
      return;
    }
  }

  if (strcmp(s_prev_ver, shm_ptr->version) == 0) {
    logger_write(LOG_DEBUG,
      "not diffrent prev set version string. %s", HERE);
    if (s_prev_failed == 0) {
      free(cmdstr);
      free(response);
      free(data);
      return;
    }
    else {
      logger_write(LOG_ALERT,
        "not diffrent prev set version string, but previous send version failed.%s", HERE);
    }
  }
  else {
    strcpy(s_prev_ver, shm_ptr->version);
  }

  if (shm_ptr->id <= 0) {
    logger_write(LOG_ALERT,
      "not get camera_id yet. %s", HERE);
    free(cmdstr);
    free(response);
    free(data);
    return;
  }

  abox_id = shm_ptr->id;
  sprintf(data, "id=%d&version=%s", abox_id, shm_ptr->version);
  api_result = get_api_response(response, buffer_len,
    AGENTBOX_CFG_API_CAMERA_HEALTH, data
  );
  if (0 == api_result) {
    logger_write(LOG_DEBUG,
      "get_api_response() success:%s, data:%s. %s",
      AGENTBOX_CFG_API_CAMERA_HEALTH, data, HERE);
    s_prev_failed = 0;
  }
  else {
    logger_write(LOG_ALERT,
      "get_api_response() failed(%d):%s, data:%s. %s",
      api_result, AGENTBOX_CFG_API_CAMERA_HEALTH, data, HERE);
    s_prev_failed = 1;
  }
  free(cmdstr);
  free(response);
  free(data);

  return;
}



/**
 * メイン処理
 *
 * @param[in] argc コマンドラインオプション数
 * @param[in] argv コマンドラインオプションを表す配列
 * @return int 終了ステータス
 * @sa <aboxd.h> プログラム終了ステータス定義
 */
int main(int argc, char** argv)
{
  /* コマンドラインオプションのコード
   * command -h とした場合'h' のコードが代入される
   * command --help とした場合option構造体 longoptの第4メンバが代入される */
  int opt;
  /* daemon動作のフラグ */
  int flag = ABOXD_FLAG_DAEMONIZE;
  /* フラグがFLAG_DAEMONIZE(=0)の時はカレントディレクトリを "/" (ルート) にchrootする */
  int nochdir = 1;
  /* フラグがFLAG_DAEMONIZE(=0)の時は標準入力・標準出力・標準エラーを閉じる */
  int noclose = 0;
  /* 終了コードのプレースホルダ */
  int exitcode = ABOXD_EXIT_NOERR;
  /* アクティベーション状態 */
  int activ = -1;
  /* アクティベーション失敗カウンタ */
  int fail_counter = 0;
  /* 設定ファイル(*.conf)パス */
  const char* conf_file_path = ABOXD_DEFAULT_CFG_PATH;
  /* 入出力バッファ */
  char* buffer;
  /* 入出力バッファ長 */
  size_t buffer_len;
  /* 起動時間 */
//  const time_t launched = time(NULL);
  /* ループ開始時刻 */
  time_t seq = 0;
  /* サーバとの最終同期時間 */
  time_t lastsync = 0;
  /* イベントループ */
  time_t evloop_world  = 0; /* 最大周期 ABOXD_EVT_TRIGGER_WORLD */
  time_t evloop_long   = 0; /* 大周期 ABOXD_EVT_TRIGGER_LONG */
  time_t evloop_short  = 0; /* 中周期 ABOXD_EVT_TRIGGER_MIDDLE */
  time_t evloop_middle = 0; /* 小周期 ABOXD_EVT_TRIGGER_SHORT */
  time_t evloop_micro  = 0; /* 最小周期 ABOXD_EVT_TRIGGER_MICRO */
  time_t evloop_nano   = 0; /* 極小周期 ABOXD_EVT_TRIGGER_NANO */
  // activate 処理エラーで (ABOXD_ACTIV_FAIL_MAX+2 <= fail_counter)
  // 10分以上継続したかどうかの判定用の差分チェック用 time_t 変数
  time_t prev_fail_count_time = 0;
  /* デフォルトの起動オプション */
  config_t* cfg;
  /* PIDを格納 */
  pid_t pid;
  /* PIDファイルディスクリプタ */
  FILE* pid_fp;
  /* コマンドライン引数のテーブル
   * 構造体メンバ
   *   第1メンバ char  オプション名 (--help の場合 "help"）
   *   第2メンバ char  必須属性とオプション値必要の有無
   *   第3メンバ *void 未使用のため、NULLを代入
   *   第4メンバ int   変数optに代入されるコード
   * 必須属性
   *   no_argument       値を必要としない
   *   required_argument 値を必要とする
   *   optional_argument オプション */
  const struct option longopt[] = {
    {"config"  , required_argument, NULL, 'c'},
    {"front"   , no_argument      , NULL, 'f'},
    {"help"    , no_argument      , NULL, 'h'},
    {"kill"    , no_argument      , NULL, 'k'},
    {"version" , no_argument      , NULL, 'v'},
    {0         , 0                , 0   ,  0 }
  };
  /* json2shm戻り値 */
  int res_json2shm = 0;

  int l_kind = 1; // 1: web access via ssh tunnel, 2: ssh shell access.

  int l_ssh_web_status = -1; // 0: SSH web tunnel OK.

  /* autoConnect.stat 用 */
  struct stat st;
  FILE *l_fp = (FILE*)NULL;
  char *l_cp = (char*)NULL;
  char l_buf[BUF_64];
  time_t l_prev_seq = 0;
  time_t l_prev_seq_micro = 0;
  FILE *l_hbfp = (FILE*)NULL;
  int l_rtn = 0;
  FILE *l_sdfp = (FILE*)NULL;
  FILE *l_sdthrfp = (FILE*)NULL;

  /* デフォルト起動オプションの生成 */
  cfg = (config_t *) malloc(sizeof(config_t));
  if (NULL == cfg) {
    fprintf(stderr, "%s: cfg malloc failed.\n", ABOXD_PROG_NAME);
    return ABOXD_EXIT_ERR_MALLOC;
  }
  cfg->interval = ABOXD_MIN_LOOP_INTERVAL;
  cfg->pidfile  = ABOXD_DEFAULT_PID_PATH;
  cfg->shmfile  = ABOXD_DEFAULT_SHM_PATH;
  cfg->logfile  = ABOXD_DEFAULT_LOG_PATH;
  cfg->cacert   = ABOXD_DEFAULT_CACERT_PATH;
  cfg->server   = ABOXD_DEFAULT_SERVER_URI;
  cfg->csurl      = ABOXD_DEFAULT_CS_URI;
  cfg->tunnelurl  = ABOXD_DEFAULT_TUNNEL_URI;

  /* シグナルの処理 */
  //signal(SIGPIPE, SIG_IGN);
  signal(SIGPIPE, sigcatch);
  signal(SIGQUIT, sigcatch);
  signal(SIGINT,  sigcatch);
  signal(SIGBUS,  sigcatch);
  signal(SIGSEGV, sigcatch);
  signal(SIGTERM, sigcatch);
  signal(SIGUSR1, sigcatch);
  signal(SIGUSR2, sigcatch);
  signal(SIGCONT, sigcatch);
  signal(SIGHUP,  sigcatch);

  signal(SIGILL,  sigcatch);
  signal(SIGTRAP, sigcatch);
  signal(SIGABRT, sigcatch);
  signal(SIGFPE,  sigcatch);
  signal(SIGALRM, sigcatch);
#ifndef MIPS_REALTEK
  signal(SIGSTKFLT, sigcatch);
#endif
  signal(SIGTSTP, sigcatch);
  signal(SIGPROF, sigcatch);
  signal(SIGPWR,  sigcatch);
  signal(SIGSYS,  sigcatch);

  /* 起動時のオプションをパースする */
  while (-1 != (opt = getopt_long(argc, argv, "c:fkv", longopt, NULL))) {
    switch (opt) {
      case 'c':
        conf_file_path = optarg;
        break;
      case 'f':
        flag = ABOXD_FLAG_NODAEMONIZE;
        break;
      case 'k':
        flag = ABOXD_FLAG_KILL;
        break;
      case 'v':
        fprintf(stderr, "%s Version %s", ABOXD_PROG_NAME, PROJ_VERSION_STRING);
        return ABOXD_EXIT_NOERR;
      default:
        printf("Usage: %s [-c /path/to/config] [-f]\n"
               "       %s [-c /path/to/config] -k\n\n"
               "Options: \n"
               " -c --config  : specify .conf file path.\n"
               " -f --front   : run in frontend, no daemon.\n"
               " -h --help    : show this help.\n"
               " -k --kill    : kill daemon running specified PID.\n"
               " -v --version : show version.\n\n"
               "(c) 2017 cool-revo inc.", ABOXD_PROG_NAME, ABOXD_PROG_NAME);
        return ABOXD_EXIT_NOERR;
    }
  }

/* daemon、コマンド実行共通 */

  /* オプションパース後、引数が必須のオプション値が空の場合はエラーを表示して終了 */
  if ((NULL == conf_file_path) || !strlen(conf_file_path)) {
    /* -c[--config] */
    fprintf(stderr, "%s: Option -c[--config] is required 1 paramter.\n",
      ABOXD_PROG_NAME);
    return ABOXD_EXIT_ERR_EMPTY_ARGS;
  }

  /* confの読み込み */
  if (0 > ini_parse(conf_file_path, parse_handler, cfg)) {
    fprintf(stderr, "%s: failed to read config. continue with defaults.\n",
      ABOXD_PROG_NAME);
  }
  /* confパース後、pidfile・shmfileファイルパスが空の場合はエラーを表示して終了 */
  if ((NULL == cfg->pidfile) || !strlen(cfg->pidfile)) {
    fprintf(stderr, "%s: pidfile must be valid path.\n", ABOXD_PROG_NAME);
    return ABOXD_EXIT_ERR_EMPTY_PIDFILE_PATH;
  }
  if ((NULL == cfg->shmfile) || !strlen(cfg->shmfile)) {
    fprintf(stderr, "%s: shmfile must be valid path.\n", ABOXD_PROG_NAME);
    return ABOXD_EXIT_ERR_EMPTY_SHMFILE_PATH;
  }

  /* オプション -k[--kill] が指定された場合はdaemonにシグナルを送る */
  if (flag == ABOXD_FLAG_KILL) {
    if (NULL != (pid_fp = fopen(cfg->pidfile, "r"))) {
      fscanf(pid_fp, "%d\n", &pid);
      fclose(pid_fp);
      if (0 == kill(pid, SIGTERM)) {
        fprintf(stderr, "%s: will be shutdown.\n", ABOXD_PROG_NAME);
      }
    } else {
      fprintf(stderr, "%s: not started.\n", ABOXD_PROG_NAME);
      return ABOXD_EXIT_DAEMON_NOT_STARTED;
    }
    return ABOXD_EXIT_NOERR;
  }

  /* 重複起動チェック */
  if (NULL != (pid_fp = fopen(cfg->pidfile, "r"))) {
    fclose(pid_fp);
    fprintf(stderr, "%s: maybe already started.\n", ABOXD_PROG_NAME);
    return ABOXD_EXIT_DAEMON_ALREADY_STARTED;
  }
  /* 共有メモリのアタッチ */
  if (0 != create_shm()) {
    fprintf(stderr, "%s: failed to create shm.\n", ABOXD_PROG_NAME);
    return ABOXD_EXIT_ERR_SHM_OPEN;
  }

  /* configurationの共有メモリへの書き込みと掃除 */
  shm_ptr->interval = cfg->interval;
  strcpy(shm_ptr->pidfile, cfg->pidfile);
  strcpy(shm_ptr->shmfile, cfg->shmfile);
  strcpy(shm_ptr->logfile, cfg->logfile);
  strcpy(shm_ptr->cacert,  cfg->cacert);
  strcpy(shm_ptr->server,  cfg->server);
  strcpy(shm_ptr->csurl,      cfg->csurl);
  strcpy(shm_ptr->tunnelurl,  cfg->tunnelurl);
  free(cfg);

  /* daemon起動 */
  if (flag == ABOXD_FLAG_DAEMONIZE) {
    if (-1 == daemon(nochdir, noclose)) {
      fprintf(stderr, "%s: failed to launch daemon.\n", ABOXD_PROG_NAME);
      return ABOXD_EXIT_ERR_DAEMONIZE;
    }
  }

  /* PIDを取得し、*.pidを作成 */
  pid = getpid();
  if (NULL == (pid_fp = fopen(cfg->pidfile, "w"))) {
    fprintf(stderr, "%s: failed to create pidfile.\n", ABOXD_PROG_NAME);
    return ABOXD_EXIT_ERR_PID_OPEN;
  }
  fprintf(pid_fp, "%d\n", pid);
  fclose(pid_fp);

  /* メッセージキューのオープン */
  if (0 != msqueue_open()) {
    fprintf(stderr, "%s: failed to create sysv message queue.\n", ABOXD_PROG_NAME);
    return ABOXD_EXIT_ERR_MSQ_OPEN;
  }

  /* ロギングの開始 */
  logger_write(LOG_INFO, "%s %s daemon started.[PID:%d]",
    ABOXD_PROG_NAME, PROJ_VERSION_STRING, pid);

  /* IPアドレスの取得 */
  //if (0 != get_abox_ipaddr()) {
  if (0 != get_smc3a_cgi_ipaddr()) {
    logger_write(LOG_DEBUG, "get_smc3a_cgi_ipaddr failed. %s", HERE);
  }
  else {
    logger_write(LOG_DEBUG, "get_smc3a_cgi_ipaddr success. %s", HERE);
  }

  /* autoConnect.stat がONになっているかをチェック */
  if (stat(ABOXD_AUTOCONNECT_STATUS, &st) == 0) {
    memset(l_buf, 0, sizeof(l_buf));
    l_fp = fopen(ABOXD_AUTOCONNECT_STATUS, "r");
    if (l_fp != (FILE*)NULL) {
      while(NULL != (fgets(l_buf, sizeof(l_buf), l_fp))) {
        if ((l_cp = strchr(l_buf, '\n')) != (char*)NULL) {
          *l_cp = '\0';
        }
        break;
      }
      fclose(l_fp);
      l_fp = (FILE*)NULL;
      if (strcmp(l_buf, "ON") == 0) {
        g_autoConnect = true;
      }
    }
    else {
      g_autoConnect = false;
    }
  }
  else {
    g_autoConnect = false;
  }
  if (g_prev_autoConnect != g_autoConnect) { 
    if (g_prev_autoConnect == true && g_autoConnect == false) {
    }
    g_prev_autoConnect = g_autoConnect;
  }

  if (g_autoConnect == true) { // 自律RTSP接続モードONの時のみactivate実行
  /* アクティベーション実行 */
    activ = activate_abox(shm_ptr->cacert);
    if (0 == activ) {
      logger_write(LOG_DEBUG, "activate_abox success. %s", HERE);
      /* confの読み込み */
      if (0 == load_shm()) {
        logger_write(LOG_DEBUG, "load_shm() is 0. %s", HERE);
        /* ループ開始時刻の初期化 */
        //lastsync = 0;
      }
      else {
        logger_write(LOG_DEBUG, "load_shm() is not 0. %s", HERE);
      }
      shm_ptr->activ_status = true;
    }
    else {
      logger_write(LOG_DEBUG, "activate_abox failed. %s", HERE);
      shm_ptr->activ_status = false;
    }
  }
#ifdef DEBUG
  shm_ptr->activ_status = true;
  activ = 0;
  shm_ptr->id = 100;       // dummy camera_id
  strcpy(shm_ptr->uniquekey, get_hw_str());
  shm_ptr->live = 1;       // LIVE ON
  shm_ptr->type = CAMERA_TYPE_AUTORTSP;       // type = AUTORTSP
  shm_ptr->ovf_type = 2;   // rtsp LIVE あり
  shm_ptr->disabled = 0;   // disabled OFF
  shm_ptr->loglevel = 7;   // loglevel DEBUG
  shm_ptr->remotehelp = 1; // remote helper ON
#endif // DEBUG

  /* API用バッファの確保 */
  buffer_len = sizeof(char) * FETCH_SIZE_8xLONG;
  buffer = (char *) malloc(buffer_len);
  if (NULL == buffer) {
    fprintf(stderr, "%s: buffer malloc failed.\n", ABOXD_PROG_NAME);
    if (NULL != cfg) {
      free(cfg);
    }
    dispose_shm();
    msqueue_close();
    return ABOXD_EXIT_ERR_MALLOC;
  }

  /* IPアドレス通知 */
  // ここでの通知は中止。getall のあと、サーバから取得したadress とruntime が異なっていたら、
  // SMC3A Plus のCGIを使用してIPを一旦セットし、それからpush
  //if (0 == activ) {
  //  push_abox_ipaddr();
  //}

  /* daemon化時のループ処理 */

  while (1) {
    logger_write(LOG_DEBUG, "main loop start %s", HERE);
    /* SIGTERM|SIGINT|SIGQUITを受信したら終了 */
    if (1 == interrupted) {
      break;
    }

    logger_write(LOG_DEBUG, "call start msqueue_read() %s", HERE);
    /* mqueueが存在する場合はコマンド実行 */
    msqueue_read();

    logger_write(LOG_DEBUG, "call end msqueue_read() %s", HERE);

    /* このループの開始時刻 */
    seq = time(NULL);

    if (l_prev_seq <= 0 || (seq - l_prev_seq) > ABOXD_EVT_TRIGGER_SHORT) {
      if (l_hbfp != (FILE*)NULL) {
        fclose(l_hbfp);
        l_hbfp = (FILE*)NULL;
      }
      l_hbfp = fopen(AGENTBOX_HEARTBEAT, "w");
      if (l_hbfp != (FILE*)NULL) {
        fclose(l_hbfp);
        l_hbfp = (FILE*)NULL;
      }
      l_prev_seq = seq;

      /* autoConnect.stat がONになっているかをチェック */
      if (stat(ABOXD_AUTOCONNECT_STATUS, &st) == 0) {
        memset(l_buf, 0, sizeof(l_buf));
        l_fp = fopen(ABOXD_AUTOCONNECT_STATUS, "r");
        if (l_fp != (FILE*)NULL) {
          while(NULL != (fgets(l_buf, sizeof(l_buf), l_fp))) {
            if ((l_cp = strchr(l_buf, '\n')) != (char*)NULL) {
              *l_cp = '\0';
            }
            break;
          }
          fclose(l_fp);
          l_fp = (FILE*)NULL;
          if (strcmp(l_buf, "ON") == 0) {
            g_autoConnect = true;
          }
        }
        else {
          g_autoConnect = false;
        }
      }
      else {
        g_autoConnect = false;
      }
      if (g_prev_autoConnect != g_autoConnect) { 
        if (g_prev_autoConnect == true && g_autoConnect == false) {
          logger_write(LOG_ALERT, "change state autoConnectMode ON -> OFF, %s", HERE);
          // deactivate 処理
          l_rtn = deactivate_abox();
          if (l_rtn == 0) {
            logger_write(LOG_ALERT, "deactivate success, %s", HERE);
          }
          else {
            logger_write(LOG_ERR, "deactivate failed(rtn:%d), %s", l_rtn, HERE);
          }
          logger_write(LOG_ALERT, "call kill_unneeded_tunnels(1) %s", HERE);
          kill_unneeded_tunnels(1);
          logger_write(LOG_ALERT, "call killall_rtsp() %s", HERE);
          killall_rtsp();
          activ = -1;
        }
        else if (g_prev_autoConnect == false && g_autoConnect == true) {
          logger_write(LOG_ALERT, "change state autoConnectMode OFF -> ON, %s", HERE);
        }
        g_prev_autoConnect = g_autoConnect;
      }
    }

    if (l_prev_seq_micro <= 0 || (seq - l_prev_seq_micro) > ABOXD_EVT_TRIGGER_MICRO) {
      logger_write(LOG_DEBUG, "SD card status check, %s", HERE);
      l_rtn = get_storage_usage();
      if (l_rtn == SDCARD_SAFE_WRITABLE) {
        g_isSetSD = true;
        if (g_prev_isSetSD != g_isSetSD) {
          if (l_sdfp != (FILE*)NULL) {
            fclose(l_sdfp);
            l_sdfp = (FILE*)NULL;
          }
          l_sdfp = fopen(IS_SETSD_FLG, "w");
          if (l_sdfp != (FILE*)NULL) {
            fclose(l_sdfp);
            l_sdfp = (FILE*)NULL;
          }
          logger_write(LOG_DEBUG, "SD card mounted and status normal, %s", HERE);
        }
        if (stat(IS_SD_THR_OVER_FLG, &st) == 0) {
          unlink(IS_SD_THR_OVER_FLG);
        }
      }
      else if (l_rtn == SDCARD_THR_OVER) {
        if (l_sdthrfp != (FILE*)NULL) {
          fclose(l_sdthrfp);
          l_sdthrfp = (FILE*)NULL;
        }
        l_sdthrfp = fopen(IS_SD_THR_OVER_FLG, "w");
        if (l_sdthrfp != (FILE*)NULL) {
          fclose(l_sdthrfp);
          l_sdthrfp = (FILE*)NULL;
        }
      } 
      else {
        g_isSetSD = false;
        if (g_prev_isSetSD != g_isSetSD) {
          logger_write(LOG_ALERT, "SD card not mounted or status is abnormal(%d) %s", 
            l_rtn, HERE);
        }
        if (stat(IS_SETSD_FLG, &st) == 0) {
          unlink(IS_SETSD_FLG);
        }
      }
      if (g_prev_isSetSD != g_isSetSD) {
        g_prev_isSetSD = g_isSetSD;
      }

      // SDカード保存機能有効フラグの存在チェック
      if (stat(IS_SAVESDCARD_FLG, &st) == 0) {
        g_isSaveSDcard = true;
      }
      else {
        g_isSaveSDcard = false;
      }
      if (g_prev_isSaveSDcard != g_isSaveSDcard) {
        if (g_isSaveSDcard == true) {
          logger_write(LOG_ALERT, "SD card recording function is enable. %s", HERE);
        }
        else {
          logger_write(LOG_ALERT, "SD card recording function is disable. %s", HERE);
        }
        g_prev_isSaveSDcard = g_isSaveSDcard;
      }

      if (g_autoConnect == true) {
        // SDカード保存（バックアップ）ffmpeg プロセス起動制御実行

      /*
        // recording RTSP 監視用 thread 起動
        if (g_is_alive_pthr_bkup_rtsp == FALSE) {
          g_pthrrslt_bkup_rtsp = pthread_create(&g_pthrid_bkup_rtsp, NULL, 
            bkup_rtsp_execute_check, NULL);
          g_is_alive_pthr_bkup_rtsp = TRUE;
        }
      */
      }

      l_prev_seq_micro = seq;
    }

    //logger_write(LOG_DEBUG, "test0 agentbox_id:%d, %s", 
    //      shm_ptr->agentbox_id, HERE);

    /* アクティベート状態でのループ処理 */
    if (0 == activ) {

      if ((seq - evloop_nano) > ABOXD_EVT_TRIGGER_NANO) {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_nano) > [ABOXD_EVT_TRIGGER_NANO] %s", HERE);

        //logger_write(LOG_DEBUG, "test1 %s", HERE);
        //logger_write(LOG_DEBUG, "test1-1 %s", HERE);
        //logger_write(LOG_DEBUG, "test1-2 %s", HERE);
        //logger_write(LOG_DEBUG, "test1-3 %s", HERE);
        //logger_write(LOG_DEBUG, "test1 agentbox_id:%d, %s", 
        //  shm_ptr->agentbox_id, HERE);

        if (g_autoConnect == true) {
          /* RTSPカメラへの接続／切断確認 */
          logger_write(LOG_DEBUG, "call open_needed_rtsp() %s", HERE);
          open_needed_rtsp();

        }
        else {
          //logger_write(LOG_DEBUG, "l_ssh_web_status(%d) is not 0, cannot call open_needed_rtsp() %s", l_ssh_web_status, HERE);
          logger_write(LOG_DEBUG, "autoConnect is false, disable call open_needed_rtsp() %s", HERE);
        }

        evloop_nano = seq;
      }
      else {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_nano) <= [ABOXD_EVT_TRIGGER_NANO] %s", HERE);
      }

      if ((seq - evloop_micro) > ABOXD_EVT_TRIGGER_MICRO) {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_micro) > [ABOXD_EVT_TRIGGER_MICRO] %s", HERE);


        if (g_autoConnect == true) {
          /* transfer 起動チェック */
          //prepare_transfer();

          /* SSHトンネルの維持 */
          l_kind = 1;
          l_ssh_web_status = store_ssh_tunnels(l_kind);
          if (0 == l_ssh_web_status) {
            logger_write(LOG_DEBUG, "call open_needed_tunnels(%d) %s", l_kind, HERE);
            l_ssh_web_status = open_needed_tunnels(l_kind);
          }
        }
#ifdef DEBUG
        l_ssh_web_status = 0;
#endif // DEBUG
        /* remotehelp SSHトンネルcheck */
        l_kind = 2;
        if (0 == store_ssh_tunnels(l_kind)) {
          logger_write(LOG_DEBUG, "call open_needed_tunnels(%d) %s", l_kind, HERE);
          open_needed_tunnels(l_kind);
        }
        /* 不要なSSHトンネルプロセスがある場合は終了 */
        logger_write(LOG_DEBUG, "call kill_unneeded_tunnels(0) %s", HERE);
        kill_unneeded_tunnels(0);

        evloop_micro = seq;
      }
      else {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_micro) <= [ABOXD_EVT_TRIGGER_MICRO] %s", HERE);
      }

      if ((seq - evloop_short) > ABOXD_EVT_TRIGGER_SHORT) {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_short) > [ABOXD_EVT_TRIGGER_SHORT] %s", HERE);
        evloop_short = seq;
      }
      else {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_short) <= [ABOXD_EVT_TRIGGER_SHORT] %s", HERE);
      }

      if ((seq - evloop_middle) > ABOXD_EVT_TRIGGER_MIDDLE) {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_middle) > [ABOXD_EVT_TRIGGER_MIDDLE] %s", HERE);
        evloop_middle = seq;
      }
      else {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_middle) > [ABOXD_EVT_TRIGGER_MIDDLE] %s", HERE);
      }

      if ((seq - evloop_long) > ABOXD_EVT_TRIGGER_LONG) {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_long) > [ABOXD_EVT_TRIGGER_LONG] %s", HERE);
        /* アクティベーション情報の更新 */
        verify_activation(shm_ptr->cacert);
        evloop_long = seq;
      }
      else {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_long) > [ABOXD_EVT_TRIGGER_LONG] %s", HERE);
      }

      if ((seq - evloop_world) > ABOXD_EVT_TRIGGER_WORLD) {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_world) > [ABOXD_EVT_TRIGGER_WORLD] %s", HERE);
        evloop_world = seq;
      }
      else {
        logger_write(LOG_DEBUG, "Event Loop (seq - evloop_world) <= [ABOXD_EVT_TRIGGER_WORLD] %s", HERE);
      }

      /* interval秒毎にAPI経由でサーバとデータ同期する */
      if ((seq - lastsync) > shm_ptr->interval) {

        logger_write(LOG_DEBUG, "%s", "------ Polling Running ------");
        //if (shm_ptr->version[0] == '\0') {
        if (shm_ptr->activ_status == true) {
          logger_write(LOG_INFO, "send firmware version, %s", HERE);
          send_version();
        }
        //}
        logger_write(LOG_DEBUG, "call get_api_response() %s", HERE);

        memset(buffer, 0, buffer_len);
        if (200 == get_api_response_code(buffer, buffer_len, AGENTBOX_CFG_API_GETALL, "")) {
          g_getallcount = 1;
          g_getallfailcount = 0;

          //FILE *l_tmpfp;
          //l_tmpfp = fopen("/tmp/test_getall.json", "w");
          //if (l_tmpfp != (FILE*)NULL) {
          //  fprintf(l_tmpfp, "%s\n", buffer);
          //  fflush(l_tmpfp);
          //  fclose(l_tmpfp);
          //}
          logger_write(LOG_DEBUG, "buffer(%s) value by get_api_response() %s", buffer, HERE);
          /* レスポンスをパースし、共有メモリに読み込み */
          logger_write(LOG_DEBUG, "call json2shm() %s", HERE);
          res_json2shm = json2shm(buffer);
          if (0 == res_json2shm) {
            logger_write(LOG_DEBUG, "return value 0 json2shm() %s", HERE);

            /* 最終同期時刻の更新 */
            //lastsync = time(NULL);

            /* 時刻設定 */
            logger_write(LOG_DEBUG, "call run_timesync() %s", HERE);
            run_timesync();

            /* 内部RTSPServer(CH1)への接続／切断確認 */
            logger_write(LOG_DEBUG, "call open_needed_rtsp() %s", HERE);
            open_needed_rtsp();

            /* ファイルに同期情報を書き出し */
            logger_write(LOG_DEBUG, "call print_shm() %s", HERE);
            print_shm(AGENTBOX_CFG_SHM_STORED_PATH);
          } else if(1 == res_json2shm) {
            /* レスポンスが200OKであり、success=0　かつ　特定のメッセージ */
            /* cameraが登録されていない */
            logger_write(LOG_DEBUG, "call remove_activatedkey() %s", HERE);
            logger_write(LOG_DEBUG, "activ:%d:%s", activ, HERE);
            /* アクティベートキーを削除することで次のアクティベートでactivateを実施する */
            remove_activatedkey(shm_ptr->cacert);
            /* フラグを非アクティベート状態にしておき次のループでアクティベート処理を行う */
            activ = -3; //activate_aboxの戻り値の代わりに仮設定する(0以外)
            logger_write(LOG_DEBUG, "activ:%d:%s", activ, HERE);
            shm_ptr->activ_status = false;
            logger_write(LOG_DEBUG, "end check chk_response_json:%s", HERE);
          }
          if (get_smc3a_cgi_ipaddr() == 0) {
            // runtime のアドレス設定と、getall で取得したアドレス設定に差分があれば、
            // 一度、CGIにてアドレスを更新後、push
            if (shm_ptr->runtime_addr.dhcp != shm_ptr->discover ||
                strcmp(shm_ptr->runtime_addr.ipaddr, shm_ptr->addr.ipaddr) != 0 ||
                strcmp(shm_ptr->runtime_addr.netmask, shm_ptr->addr.netmask) != 0 ||
                strcmp(shm_ptr->runtime_addr.gateway, shm_ptr->addr.gateway) != 0 ||
                strcmp(shm_ptr->runtime_addr.dns1, shm_ptr->addr.dns1) != 0 ||
                strcmp(shm_ptr->runtime_addr.dns2, shm_ptr->addr.dns2) != 0) {
              if (set_smc3a_cgi_ipaddr() == 0) {
                if (get_smc3a_cgi_ipaddr() == 0) {
                  push_abox_ipaddr();
                }
              }
            }
          }
          shm_ptr->poll_status = true;
        }
        else {
          shm_ptr->poll_status = false;
          g_getallfailcount++;
          if (g_getallfailcount > 5) {
            activ = force_reactivate_abox(shm_ptr->cacert);
            if (0 == activ) {
              g_getallfailcount = 0;
              shm_ptr->activ_status = true;
            }
            else {
              shm_ptr->activ_status = false;
            }
          }
        }
        logger_write(LOG_DEBUG, "%s", "------ Polling End ------");
        /* 最終同期時刻の更新 */
        lastsync = time(NULL);
      }
      else {
            logger_write(LOG_DEBUG, "(seq - lastsync) > shm_ptr->interval, Polling no Running %s", HERE);
      }

      logger_write(LOG_DEBUG, "wait 5s. %s", HERE);
      /* 指定秒ループをwaitする */
      sleep(5);
    } else {
      /* アクティベーション失敗 */
      if (ABOXD_ACTIV_FAIL_MAX > fail_counter) {
        ++fail_counter;
        /* 15秒wait後、アクティベーション再試行 */
        sleep(15);

        if (g_autoConnect == true) { // 自律RTSP接続モードONの時のみactivate実行
          activ = activate_abox(shm_ptr->cacert);
          if (0 == activ) {
            fail_counter = 0;
            /* confの読み込み */
            if (0 == load_shm()) {
              /* ループ開始時刻の初期化 */
              //lastsync = 0;
            }
            shm_ptr->activ_status = true;
          }
          else {
            shm_ptr->activ_status = false;
          }
        }
      } else {
        if (g_autoConnect == true) { // 自律RTSP接続モードONの時のみactivate実行
          logger_write(LOG_ERR, "%s could not activated, force reactivate %s", ABOXD_PROG_NAME, HERE);
          if (ABOXD_ACTIV_FAIL_MAX+2 > fail_counter) {
            activ = force_reactivate_abox(shm_ptr->cacert);
            if (0 == activ) {
              fail_counter = 0;
              /* confの読み込み */
              if (0 == load_shm()) {
              }
              shm_ptr->activ_status = true;
            }
            else {
              shm_ptr->activ_status = false;
              ++fail_counter;
              sleep(15);
            }
          }
          else {
            if (prev_fail_count_time <= 0) {
              prev_fail_count_time = seq; 
            }
            logger_write(LOG_ERR, "%s could not force reactivated, for while a wait. %s", ABOXD_PROG_NAME, HERE);
            sleep(5);
            if (seq - prev_fail_count_time > 10 * 60) {
              logger_write(LOG_ERR, "%s could not force reactivated, passed 10min. %s", ABOXD_PROG_NAME, HERE);
              fail_counter = 0;
              prev_fail_count_time = 0;
            }
          }
        }
        else {
          sleep(15);
        }
      }
    }
    /* 10ms wait 無限ループ発生時 CPU暴走対策 */
    usleep(10 * 1000);
    logger_write(LOG_DEBUG, "main loop end, goto start. %s", HERE);
  }

  /* プロセス終了の処理 */
  logger_write(LOG_INFO, "%s will be shutdown. %s", ABOXD_PROG_NAME, HERE);
  free(buffer);
  /* PIDファイル除去 */
  if (0 != remove(shm_ptr->pidfile)) {
    perror("removal PID file error.\n");
  }
  /* 共有メモリのデタッチ */
  if (0 != dispose_shm()) {
    exitcode = ABOXD_EXIT_ERR_DETACH_SHM;
  }
  /* メッセージキューのクローズ */
  if (0 != msqueue_close()) {
    exitcode = ABOXD_EXIT_ERR_MSQ_CLOSE;
  }

  return exitcode;
}
