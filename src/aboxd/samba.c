/**
 * @file    samba.c
 * @brief   smbユーザ処理とconf編集ユーティリティ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_SAMBA_C
#define AGENTBOX_SAMBA_C
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <stdlib.h>
#include "abox_macro.h"
#include "shm.h"
#include "logger.h"
#include "process.h"
#include "storage.h"
#include "samba.h"
#include "rtsp.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;

/** メッセージキューを作成する共通キー。実体はaboxd.c */
extern int msq_id;



/**
 * 共有ディレクトリ公開および転送エージェントを
 * 起動するためのセットアップを行う。
 *
 * @retval  0 正常終了
 * @retval -1 sambaアカウントが有効化されていない
 * @retval -2 config作成用ファイルハンドルの作成に失敗
 */
int prepare_transfer(void)
{
  FILE* fp;
  static char cloudaddr[128];
  static char cloudlogin[32];
  static int ls_is_first = false;

  if (ls_is_first == false) {
     memset(cloudaddr, 0, sizeof(cloudaddr));
     memset(cloudlogin, 0, sizeof(cloudlogin));
     ls_is_first = true;
  }

  /* transfer.conf.shの作成 */
  if (strcmp(cloudaddr, shm_ptr->cloudaddr) != 0 ||
      strcmp(cloudlogin, shm_ptr->cloudlogin) != 0) {
    strcpy(cloudaddr, shm_ptr->cloudaddr);
    strcpy(cloudlogin, shm_ptr->cloudlogin);
    if (NULL != (fp = fopen(AGENTBOX_CFG_TRANSFER_CFG_PATH, "wb"))) {
      fprintf(fp, "remote_host=%s\n", shm_ptr->cloudaddr);
      fprintf(fp, "remote_dir=/samba/%s\n", shm_ptr->cloudlogin);
      fclose(fp);
    } else {
      return -2;
    }
  }
  /* Samba公開ディレクトリを共有メモリに保存 */
  sprintf(shm_ptr->smbshare, SMB_SHARED_FULLPATH, shm_ptr->cloudlogin);

  /* transferの有効・無効化 */
  if (1 == shm_ptr->disabled) {
    kill_process(AGENTBOX_CFG_TRANSFER_BIN_PATH);
  } else {
    check_run_process(
      AGENTBOX_CFG_TRANSFER_PID_PATH,
      AGENTBOX_CFG_TRANSFER_EXECUTABLE
    );
  }
  return 0;
}
#endif // AGENTBOX_SAMBA_C
