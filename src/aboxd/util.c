/**
 * @file    util.c
 * @brief   ユーティリティ関数のごった煮
 * @author  cool-revo inc.
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <openssl/md5.h>
#include <openssl/rsa.h>
#include <openssl/engine.h>
#include <openssl/pem.h>
#include "abox_macro.h"
#include "util.h"
#include "logger.h"


/**
 * 引数で与えられた文字列に対するMD5ダイジェストを作り、bufferにコピーする
 *
 * @param[out] dest 作成されたダイジェストを保持するメモリ領域
 * @param[in] src ダイジェストを作成する文字列
 * @retval  0 成功時
 * @retval -1 失敗時
 */
int md5_digest(char* dest, const char* src)
{
  int i;
  unsigned char hash[MD5_DIGEST_LENGTH];
  MD5_CTX ctx;

  if (strlen(src) <= 0) {
    return -1;
  }
  MD5_Init(&ctx);
  MD5_Update(&ctx, src, strlen(src));
  MD5_Final(hash, &ctx);
  for (i = 0; i < MD5_DIGEST_LENGTH; i++) {
    sprintf(dest + (i * 2), "%02x", hash[i]);
  }
  dest[16] = '\0';
  return 0;
}

int md5_digest_bin(unsigned char* dest, int *size, const char* src)
{
  unsigned char hash[MD5_DIGEST_LENGTH];
  MD5_CTX ctx;

  memset(hash, 0, sizeof(hash));
  if (strlen(src) <= 0) {
    return -1;
  }
  MD5_Init(&ctx);
  MD5_Update(&ctx, src, strlen(src));
  MD5_Final(hash, &ctx);
  memcpy(dest, hash, MD5_DIGEST_LENGTH);
  *size = MD5_DIGEST_LENGTH;
  return 0;
}


/**
 * NULLポインタを安全に処理するatoi
 *
 * @param[in] src 数値化する文字列
 * @return intに変換された文字列、ただし入力文字列が\0の場合は0を返す
 */
int atoi_safe(const char* src)
{
  if (NULL == src || (NULL != src && '\0' == *src)) {
    return 0;
  }
  return (int) strtol(src, (char **) NULL, 10);
}


/**
 * NULLポインタを安全に処理するstrtof
 *
 * @param[in] src 数値化する文字列
 * @return doubleに変換された文字列、ただし入力文字列が\0の場合は0を返す
 */
float atof_safe(const char* src)
{
  if (NULL == src || (NULL != src && '\0' == *src)) {
    return 0;
  }
  return strtof(src, (char **) NULL);
}


/**
 * NULLポインタを安全に処理するstrcpy
 *
 * @param[in,out] dest 文字列をコピーする対象メモリ領域へのポインタ
 * @param[in] src コピーする文字列
 * @return コピーした文字列、ただし入力文字列が\0の場合はNULL
 */
char* strcpy_safe(char* dest, const char* src)
{
  if (NULL == src || (NULL != src && '\0' == *src)) {
    dest[0] = '\0';
    return NULL;
  }
  return strcpy(dest, src);
}


/**
 * なんちゃってRFC2822なフォーマットの日付を返す
 *
 * LOCALE等に邪魔されないように、引数で渡されたUNIX時間をGMTで出力する
 * 主にCookieをセットするHTTPヘッダ内での使用を想定
 *
 * @param[in] epoch フォーマットしたいUNIX時間
 * @return 整形された文字列へのポインタ
 */
char* date_rfc2822(time_t epoch) {
  const struct tm* tm;
  static char rfc_time[32];
  static char* rfc_time_p;
  static const char* month_names[] = {
    "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  };
  static const char* day_names[] = {
    "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
  };
  tm = gmtime(&epoch);
  rfc_time_p = rfc_time;
  sprintf(rfc_time_p, "%s, %02d %s %4d %02d:%02d:%02d GMT",
    day_names[tm->tm_wday],
    tm->tm_mday,
    month_names[tm->tm_mon],
    tm->tm_year + 1900,
    tm->tm_hour,
    tm->tm_min,
    tm->tm_sec
  );
  return rfc_time_p;
}


/**
 * カーネルバージョンを含めたエージェントボックスの
 * 長いバージョン文字列を返す
 *
 * @retval string 正常終了時はバージョン番号を表す文字列
 * @retval NULL   異常時はNULLを返す
 */
char* get_abox_version_long(void)
{
  FILE* fp;
  char* middle_ver;
  char* kernel_ver;
  static char version[BUF_IDENT];

  middle_ver = (char *) malloc(sizeof(char) * BUF_HWADDR * 2);
  kernel_ver = (char *) malloc(sizeof(char) * BUF_HWADDR);
  if (NULL == middle_ver || NULL == kernel_ver) {
    if (NULL != middle_ver) {
      free(middle_ver);
    } 
    if (NULL != kernel_ver) {
      free(kernel_ver);
    } 
    return NULL;
  }
  memset(middle_ver, 0, sizeof(char) * BUF_HWADDR * 2);
  memset(kernel_ver, 0, sizeof(char) * BUF_HWADDR);
  strcpy(middle_ver, get_abox_version());

  fp = popen(AGENTBOX_CFG_CMD_KERN_VER, "r");
  if (NULL == fp) {
    if (NULL != middle_ver) {
      free(middle_ver);
    } 
    if (NULL != kernel_ver) {
      free(kernel_ver);
    } 
    return NULL;
  }
  fscanf(fp, "%s\n", kernel_ver);
  pclose(fp);

  sprintf(version, "Agentbox %s (kernel %s)", middle_ver, kernel_ver);
  return version;
}


/**
 * SMC-6A のF/Wバージョン文字列を返す
 *
 * @retval 正常終了時はバージョン番号(SMC_xx, WCP_xx, SMC2_xx, WCP2_xx)を表す文字列
 * @retval ---- 異常時は(----)を返す
 */
char* get_abox_version(void)
{
  FILE* fp;
  FILE* pp;
  static char version[BUF_IDENT];
  char agent_version[10];
  char cmdstr[512];
  char tmpbuf[128];
  char *cp = (char*)NULL;

  logger_write(LOG_DEBUG, "get_abox_version start, %s", HERE);

  memset(agent_version, 0, sizeof(agent_version));
  memset(tmpbuf, 0, sizeof(tmpbuf));

  logger_write(LOG_DEBUG, "get_abox_version::version file:%s, %s", AGENTBOX_CFG_FIRMVER_PATH, HERE);
  fp = fopen(AGENTBOX_CFG_FIRMVER_PATH, "r");
  if (NULL != fp) {
    fscanf(fp, "%s\n", agent_version);
    fclose(fp);
  }

  logger_write(LOG_DEBUG, "get_abox_version::agent_version:%s, %s", agent_version, HERE);

  sprintf(cmdstr, "wget -T 1 -q -O - http://127.0.0.1/GetVer.cgi | sed -e 's/<br>.*$//g' -e 's/^Version=\\(.*\\)/\\1/'");
  //sprintf(cmdstr, "wget -T 1 -q -O - http://127.0.0.1/GetVer.cgi | sed -e 's/<br>.*$//g' -e 's/^Version=[A-Z]*[0-9]*\\.[0-9]*\\.[0-9]*\\.\\(.*\\)_.*/\\1/' -e 's/\\([0-9]*\\)S$/SMC_\\1/' -e 's/\\([0-9]*\\)W$/WCP_\\1/' -e 's/\\([0-9]*\\)S2$/SMC2_\\1/' -e 's/\\([0-9]*\\)W2$/WCP2_\\1/'");
  logger_write(LOG_DEBUG, "get_abox_version::cmdstr:%s, %s", cmdstr, HERE);
  pp = popen(cmdstr, "r");
  if (NULL != pp) {
    while(fgets(tmpbuf, sizeof(tmpbuf), pp) != NULL) {
      if ((cp = strchr(tmpbuf, '\n')) != (char*)NULL) {
        *cp = '\0';
      }
      break;
    }
    pclose(pp);
  }
  if (tmpbuf[0] != '\0') {
    strcpy(version, tmpbuf);
  }
  else {
    memset(version, 0, sizeof(version));
  }
  logger_write(LOG_DEBUG, "get_abox_version::version:%s, %s", version, HERE);

  if (version[0] != '\0' && agent_version[0] != '\0') {
    //strcat(version, "_");
    //strcat(version, agent_version);
  }
  else if (version[0] == '\0' && agent_version[0] != '\0') {
    sprintf(version, "--------");
    //sprintf(version, "--------_");
    //strcat(version, agent_version);
  }
  else if (version[0] == '\0' && agent_version[0] == '\0') {
    sprintf(version, "--------");
  }
  logger_write(LOG_DEBUG, "get_abox_version::version:%s, %s", version, HERE);

  return version;
}


/**
 * 文字列からある語を検索して置換する。対象文字列がない場合は
 * そのまま元の文字列を維持する
 *
 * @param[in] needle 検索する文字列
 * @param[in] replace 置き換える文字列
 * @param[in,out] haystack 検索対象文字列
 */
void str_replace(const char* needle, const char* replace, char* haystack)
{
  char* buffer;
  char* p;

  buffer = (char *) malloc(sizeof(char) * strlen(haystack) + 1);
  if (NULL == buffer) {
    return;
  } 
  while (NULL != (p = strstr(haystack, needle))) {
    *p = '\0';
    p += strlen(needle);
    strcpy(buffer, p);
    strcat(haystack, replace);
    strcat(haystack, buffer);
  }
  free(buffer);
}


/**
 * RSA秘密鍵の存在と最低限のフォーマットチェックを行う
 *
 * 引数で指定したパスにファイルがあるかどうかをチェックし、
 * 存在する場合はフォーマットがRSA秘密鍵として正しいかを
 * 読み込んで確認する
 *
 * @param[in] pem_path 秘密鍵保存先パス
 * @retval  0 秘密鍵は存在し、鍵のフォーマットとして正しい
 * @retval -1 秘密鍵が存在しない
 * @retval -2 秘密鍵の内容が空か正しくない内容で保存されている
 * @retval -3 秘密鍵ファイルのオープンに失敗
 * @retval -100以下 指定したパスは秘密鍵でなく、通常ファイルでもない
 * @par -100以下の戻り値の詳細は以下のとおり
 *      - <b>-101</b> FIFO
 *      - <b>-102</b> キャラクタ・デバイスファイル
 *      - <b>-104</b> ディレクトリ
 *      - <b>-106</b> ブロック・デバイスファイル
 *      - <b>-112</b> シンボリックリンク
 *      - <b>-114</b> ソケットファイル
 *      - <b>-100</b> 未知のファイル
 */
int verify_pem(const char* pem_path)
{
  int result;
  struct stat st;
  struct rsa_st* pubkey;
  FILE* fp;

  if (0 != stat(pem_path, &st)) {
    return -1;
  }
  switch (st.st_mode & S_IFMT) {
    case S_IFIFO:
      result = -101;
      break;
    case S_IFCHR:
      result = -102;
      break;
    case S_IFDIR:
      result = -104;
      break;
    case S_IFBLK:
      result = -106;
      break;
    case S_IFREG:
      if (NULL != (fp = fopen(pem_path, "rb"))) {
        pubkey = PEM_read_RSAPrivateKey(fp, NULL, NULL, NULL);
        result = (pubkey == NULL ? -2 : 0);
        RSA_free(pubkey);
        fclose(fp);
      } else {
        result = -3;
      }
      break;
    case S_IFLNK:
      result = -112;
      break;
    case S_IFSOCK:
      result = -114;
      break;
    default:
      result = -100;
      break;
  }
  return result;
}


/**
 * RSA秘密鍵のチェックを行い、失敗時には指定のパスに失敗試行数を記録する
 *
 * RSA秘密鍵再取得までの最大試行失敗回数は定数
 * AGENTBOX_CFG_REVOKE_KEY_LIMITとして abox_config.h に定義する
 * 戻り値-100のファイルタイプは verify_pem(const char*) の戻り値と
 * 同じものである
 *
 * @param[in] pem_path チェックする秘密鍵のパス
 * @param[in] failure_path チェック失敗回数を保存するパス
 * @retval  0 秘密鍵は存在し、正常なフォーマットである
 * @retval -1 秘密鍵が存在しない
 * @retval -2 秘密鍵として正常でない
 * @retval -3 秘密鍵のオープンに失敗
 * @retval -100以下 秘密鍵のパスに非通常ファイルが存在
 * @sa abox_config.h RSA秘密鍵再取得までの最大試行失敗回数
 */
int validate_pem(const char* pem_path,
                 const char* failure_path)
{
  struct stat st;
  char countstr[11];
  char* countstr_p;
  int count = 0;
  int verified;
  FILE* fp;

  /* RSA鍵の正当性チェック */
  verified = verify_pem(pem_path);
  if (0 == verified) {
    if (0 == stat(failure_path, &st)) {
      remove(failure_path);
    }
    return 0;
  }

  /* 既にキャッシュファイルが存在する場合は内容を読み、現在の値を保持する */
  if (0 == stat(failure_path, &st)) {
    memset(countstr, 0, sizeof(char) * 11);
    if (NULL != (fp = fopen(failure_path, "r"))) {
      while (NULL != fgets(countstr, 11, fp)) {
        countstr_p = strchr(countstr, '\n');
        if (NULL != countstr_p) {
          *countstr_p = '\0';
        }
        count = (int) strtoul(countstr, (char **) NULL, 10);
      }
      fclose(fp);
    }
  }

  /* 失敗回数をカウントアップし、ファイルに保存する */
  if (-3 == verified) {
    count += 1;
    if (NULL != (fp = fopen(failure_path, "w"))) {
      fprintf(fp, "%d\n", count);
      fclose(fp);
    }
    if (AGENTBOX_CFG_REVOKE_KEY_LIMIT >= count) {
      return -2;
    }
  }

  return verified;
}

/**
 * RSA秘密鍵のチェックを行い、失敗時には指定のパスに失敗試行数を記録する
 *
 * @param[in] failure_path チェック失敗回数を保存するパス
 * @retval  0
 * @retval -1 指定パスの記録用ファイルへの書き込みに失敗
 * @retval -2 指定パスの記録用ファイルオープンに失敗
 */
int add_failcounter(const char* failure_path)
{
  struct stat st;
  char countstr[11];
  char* countstr_p;
  int count = 0;
  FILE* fp;

  if (0 == stat(failure_path, &st)) {
    memset(countstr, 0, sizeof(char) * 11);
    if (NULL != (fp = fopen(failure_path, "r"))) {
      while (NULL != fgets(countstr, 11, fp)) {
        countstr_p = strchr(countstr, '\n');
        if (NULL != countstr_p) {
          *countstr_p = '\0';
        }
        count = (int) strtoul(countstr, (char **) NULL, 10);
      }
      fclose(fp);
    } else {
      return -2;
    }
  }
  count += 1;
  if (NULL != (fp = fopen(failure_path, "w"))) {
    fprintf(fp, "%d\n", count);
    fclose(fp);
    return 0;
  } else {
    return -1;
  }

}
