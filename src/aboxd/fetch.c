/**
 * @file    fetch.c
 * @brief   libcurlを用いたリモートコンテンツ取得ユーティリティ
 * @author  cool-revo inc.
 */
#include <stdlib.h>
#include <string.h>
#include "fetch.h"


/**
 * メモリをファイルディスクリプタと同様のインタフェイスで操作するためのラッパー
 *
 * @return MEMFILE* ハンドルへのポインタ
 */
MEMFILE* memfopen(void)
{
  MEMFILE* mf = (MEMFILE *) malloc(sizeof(MEMFILE));

  if (mf) {
    mf->data = NULL;
    mf->size = 0;
  }
  return mf;
}


/**
 * ファイルディスクリプタに見立てたメモリを解放する
 *
 * @param[in] mf 解放するハンドルのポインタ
 */
void memfclose(MEMFILE* mf)
{
  if (NULL == mf) {
    return;
  }
  if (mf->data) {
    free(mf->data);
  }
  free(mf);
}


/**
 * CURLOPT_WRITEFUNCTIONに設定するコールバック
 *
 * フェチしたブロック単位で都度コールバックを起動し、順次メモリをアロケートする
 *
 * @param[in] ptr
 * @param[in] size
 * @param[in] nmemb
 * @param[in] stream
 * @return block 
 */
size_t memfwrite(char* ptr, size_t size, size_t nmemb, void* stream)
{
  MEMFILE* mf = (MEMFILE *) stream;
  size_t block = size * nmemb;

  if (!mf) {
    return block;
  }
  if (!mf->data) {
    mf->data = (char *) malloc(block);
  } else {
    mf->data = (char *) realloc(mf->data, mf->size + block);
  }
  if (mf->data) {
    memcpy(mf->data + mf->size, ptr, block);
    mf->size += block;
  }
  return block;
}


/**
 * CURLOPT_WRITEFUNCTIONに設定するコールバック
 * 
 * @param[in,out] mf memfopenで開かれたハンドルのポインタ
 * @return buf ハンドルにコピーした領域サイズ
 */
char* memfstrdup(MEMFILE* mf)
{
  char* buf;

  if (0 == mf->size) {
    return NULL;
  }
  buf = (char *) malloc(mf->size + 1);
  memcpy(buf, mf->data, mf->size);
  buf[mf->size] = 0;
  return buf;
}
