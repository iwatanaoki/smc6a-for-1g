#ifndef AGENTBOX_REBOOT_C
#define AGENTBOX_REBOOT_C
#endif // AGENTBOX_REBOOT_C
/**
 * @file    reboot.c
 * @brief   再起動・システム停止他ハード制御
 * @author  cool-revo inc.
 */
#include <stdlib.h>
#include <stdio.h>
#include "abox_config.h"
#include "shm.h"
#include "logger.h"
#include "auth.h"
#include "process.h"
#include "reboot.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;


/**
 * クラウドのエージェントボックスステータスを更新してから
 * 再起動を実行する
 *
 */
void safe_reboot(void)
{
  int abox_id;
  int api_result;
  char* data;
  char* response;
  char* cmdstr;
  size_t buffer_len;

  buffer_len = sizeof(char) * BUF_1024;
  data     = (char *) malloc(buffer_len);
  response = (char *) malloc(buffer_len);
  cmdstr   = (char *) malloc(buffer_len);
  if (NULL == data || NULL == response || NULL == cmdstr) {
    logger_write(LOG_ERR,
      "data or response or cmdstr malloc failed. %s", HERE);
    if (NULL != data) {
      free(data);
    }
    if (NULL != response) {
      free(response);
    }
    if (NULL != cmdstr) {
      free(cmdstr);
    }
    return;
  }

  logger_write(LOG_DEBUG,
    "Reboot IPC message caught. Agentbox will be reboot. %s", HERE);
  abox_id = shm_ptr->id;
  sprintf(data, "id=%d&status=%d", abox_id, ABOX_STATUS_REBOOTING);
  api_result = get_api_response(response, buffer_len,
    AGENTBOX_CFG_API_CAMERA_HEALTH, data
  );
  if (0 == api_result) {
    logger_write(LOG_DEBUG,
      "get_api_response() success:%s, data:%s. %s",
      AGENTBOX_CFG_API_CAMERA_HEALTH, data, HERE);
  }
  else {
    logger_write(LOG_ALERT,
      "get_api_response() failed(%d):%s, data:%s. %s",
      api_result, AGENTBOX_CFG_API_CAMERA_HEALTH, data, HERE);
  }
  free(cmdstr);
  free(response);
  free(data);

  logger_write(LOG_ALERT,
    "execute %s. %s",AGENTBOX_CFG_CMD_FAST_REBOOT, HERE);
  run_system(AGENTBOX_CFG_CMD_FAST_REBOOT);

}
