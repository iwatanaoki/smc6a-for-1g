/**
 * @file    logger.c
 * @brief   ロギング関連
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_LOGGER_C
#define AGENTBOX_LOGGER_C
#include <string.h>
#include <stdio.h>
#include <stdarg.h>
#include <time.h>
#include <sys/time.h>
/* #include <sys/stat.h> */
#include <unistd.h>
#include <pthread.h>
#include "logger.h"
#include "shm.h"
#include "rtsp.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;

/** ログ出力のためのファイルハンドル */
FILE* applog = (FILE*)NULL;
FILE* applog_rtsp = (FILE*)NULL;
/** ログをまとめて書き出す（オープンしたまま）出力のするための行数カウント変数 */
unsigned long writecount = 0;
unsigned long writecount_rtsp = 0;

/** テキスト化されたレベルをログファイルに書き込むための定義 */
const static char lvname[10][7] = {
  {"EMERG "},
  {"ALERT "},
  {"CRIT  "},
  {"ERROR "},
  {"WARN  "},
  {"NOTICE"},
  {"INFO  "},
  {"DEBUG "},
  {"XDEBUG"},
  {"NONE  "},
};

/** ローカルプロトタイプ */
FILE **get_applog(void);
unsigned long *get_writecount(void);
char *get_filename(void);

/**
 * ログファイルのハンドルを開く
 *
 * @retval  0 正常終了
 * @retval -1 ファイルオープンに失敗
 * @retval -2 ログファイルパスが未指定
 */
int logger_open(void)
{
  FILE **applog;
  char filename[BUF_128];
  memset(filename, 0, sizeof(char)*BUF_128);
  strcpy(filename, get_filename());
  applog = get_applog();

  if (0 == strlen(filename)) {
    return -2;
  }
  if (*applog == (FILE*)NULL) {
    if (NULL == (*applog = fopen(filename, "a+"))) {
      return -1;
    }
  }
  return 0;
}


/**
 * ログファイルのハンドルを閉じる
 *
 * @retval  0 正常終了
 * @retval -1 ファイルのクローズ失敗
 */
int logger_close(void)
{
  FILE **applog;
  applog = get_applog();

  if (*applog != (FILE*)NULL) {
    fsync(fileno(*applog));
    if (0 != fclose(*applog)) {
      *applog = (FILE*)NULL;
      return -1;
    }
    *applog = (FILE*)NULL;
  }
  return 0;
}


/**
 * ログファイルの初期化
 *
 * @retval  0 正常終了
 * @retval -1 ファイルオープンに失敗
 * @retval -2 ログファイルパスが未指定
 */
/*
int logger_start(void)
{
  struct stat sb;

  if (0 == strlen(shm_ptr->logfile)) {
    return -2;
  }
  if (0 == stat(shm_ptr->logfile, &sb)) {

  }

  return 0;
}
*/


/**
 * ログレベルに応じ、ログファイルに追記する
 *
 * @param[in] level 追記するログのレベル
 * @param[in] fmt 追記するメッセージのフォーマット
 * @param[in] ... メッセージに対する可変長置き換え値
 * @retval  0 正常終了
 * @retval -1 不正なログレベルが引数で渡された
 * @retval -2 ログファイルのオープンに失敗
 * @retval -3 ログファイルのクローズに失敗
 */
int logger_write(unsigned int level, const char* fmt, ...)
{
  va_list args;
  struct timeval timer;
  struct tm* date;
  //char logmsg[1025];
  char logmsg[2048];
  char daypart[37];
  int ret = 0;
  FILE **applog;
  unsigned long *writecount;

  applog = get_applog();
  writecount = get_writecount();

  if (LOG_EMERG > level || level > LOG_NONE) {
    return -1;
  }
  if (LOG_NONE == shm_ptr->loglevel) {
    return ret;
  }

  if (level <= shm_ptr->loglevel) {

    //memset(logmsg,  0, sizeof(char) * 1025);
    memset(logmsg,  0, sizeof(char) * 2048);
    memset(daypart, 0, sizeof(char) * 37);

    gettimeofday(&timer, NULL);
    date = localtime(&timer.tv_sec);
    sprintf(daypart, "%d-%02d-%02d %02d:%02d:%02d.%06lu [%s]",
      date->tm_year + 1900, date->tm_mon + 1, date->tm_mday,
      date->tm_hour, date->tm_min, date->tm_sec, timer.tv_usec,
      lvname[level]
    );
    daypart[36] = '\0';

    va_start(args, fmt);
    vsnprintf(logmsg, 1024, fmt, args);
    va_end(args);

    if (*writecount == 0) {
      if (*applog == (FILE*)NULL) {
        if (0 != logger_open()) {
          ret = -2;
          return(ret);
        }
      }
      *writecount = 1;
    }
    else if (*writecount >= LOG_WRITE_LINES_PER_OPEN) {
      if (0 != logger_close()) {
        ret = -3;
        return(ret);
      }
      if (0 != logger_open()) {
        ret = -2;
        return(ret);
      }
      *writecount = 1;
    }
    else {
      (*writecount)++;
    }

    if (*applog != (FILE*)NULL) {
      fprintf(*applog, "%s %s\n", daypart, logmsg);
      fflush(*applog);
    }

    /* EXTREME DEBUG時はメッセージも出力する */
    if (LOG_XDEBUG == shm_ptr->loglevel) {
      printf("%s %s\n", daypart, logmsg);
      fflush(stdout);
    }
  }
  return ret;
}

/**
 * applog取得
 *
 * @return ファイルハンドルポインタ
 */
FILE **get_applog(void)
{
  if(g_pthrid_bkup_rtsp == pthread_self()) {
    return &applog_rtsp;
  } else {
    return &applog;
  }
}

/**
 * writecount取得
 *
 * @return 書き込みカウンタポインタ
 */
unsigned long *get_writecount(void)
{
  if(g_pthrid_bkup_rtsp == pthread_self()) {
    return &writecount_rtsp;
  } else {
    return &writecount;
  }
}

/**
 * ログファイル名取得
 *
 * @return ログファイル名
 */
char *get_filename(void)
{
  if(g_pthrid_bkup_rtsp == pthread_self()) {
    return LOG_FNAME_THREAD_RTSP;
  } else {
    return shm_ptr->logfile;
  }
}
#endif /* AGENTBOX_LOGGER_C */
