/**
 * @file    storage.c
 * @brief   内部および外部ストレージの状態取得ユーティリティ
 * @author  cool-revo inc.
 */

#ifndef AGENTBOX_STORAGE_C
#define AGENTBOX_STORAGE_C

#include <sys/types.h>
#include <sys/statvfs.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <pwd.h>
#include <ftw.h>
#include "aboxd.h"
#include "abox_macro.h"
#include "shm.h"
#include "logger.h"
#include "storage.h"



/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;


/**
 * ディスク使用量が所定の閾値を超えていないかどうかを返す
 *
 * 引数にはブロックデバイスまたはディレクトリを与える。ブロックデバイスの
 * 場合はそのディスク全体に対して、ディレクトリの場合はそのディレクトリの
 * 実体が保管されているデバイスの容量について確認する。
 *
 * 設定されていない場合は90%を超えて使用しているかどうかを確認する。
 *
 * @param[in] path テストするブロックデバイスまたはディレクトリ
 * @retval  0 使用量は閾値を超えていない
 * @retval -1 使用量が閾値を超えている
 */
int get_storage_usage(void)
{
  struct statvfs vfs;
  double blocks;
  double bfree;
  double maxusage;
  double threshold;
  int l_rtn = 0;
  struct stat st;

  char l_realpath[PATH_MAX];

  memset(l_realpath, 0, sizeof(l_realpath));
  if (NULL == realpath(DEF_SDCARD_MOUNTPOINT, l_realpath)) {
    logger_write(LOG_DEBUG, "cannot get realpath from %s. %s", DEF_SDCARD_MOUNTPOINT, HERE);
    return SDCARD_MOUNTPOINT_ABNORMAL; // -1
  }
  logger_write(LOG_DEBUG, "realpath:%s. %s", l_realpath, HERE);

  memset(&st, 0, sizeof(st));
  l_rtn = stat(DEF_SDCARD_DEVICE, &st);
  if (l_rtn < 0) {
    logger_write(LOG_ALERT, "%s is cannot get stat(rtn:%d), device is not found. %s", 
      DEF_SDCARD_DEVICE, l_rtn, HERE);
    return SDCARD_UNMOUNTED;         // -2
  }

  blocks = 0;
  bfree = 0;
  memset(&vfs, 0, sizeof(vfs));
  l_rtn = statvfs(l_realpath, &vfs);
  if (l_rtn < 0) {
    logger_write(LOG_ALERT, "%s is cannot get statvfs(rtn:%d). %s", 
      l_realpath, l_rtn, HERE);
      return SDCARD_STATVFS_FAILED;  // -3
  }
  blocks  = ((double) vfs.f_bsize) * ((double) vfs.f_blocks);
  bfree   = ((double) vfs.f_bsize) * ((double) vfs.f_bfree);

  threshold = blocks * 0.9;

  if ((vfs.f_flag & ST_NODEV) == ST_NODEV) {
    logger_write(LOG_ALERT, "Disk diallow access. %s", HERE);
  }

  logger_write(LOG_DEBUG, "Disk usage summary: %s", HERE);
  logger_write(LOG_DEBUG, " blocks   : %16.0f[B] %10.2f[MB] %6.2f[GB]",
    blocks, blocks/1000.0/1000.0, blocks/1000.0/1000.0/1000.0);
  logger_write(LOG_DEBUG, " bfree    : %16.0f[B] %10.2f[MB] %6.2f[GB]",
    bfree, bfree/1000.0/1000.0, bfree/1000.0/1000.0/1000.0);
  logger_write(LOG_DEBUG, " threshold: %16.0f[B] %10.2f[MB] %6.2f[GB]",
    threshold, threshold/1000.0/1000.0, threshold/1000.0/1000.0/1000.0);


  if (((blocks - bfree) / blocks) * 100.0 >= threshold) {
    logger_write(LOG_ALERT, "Disk over threshold(%6.2f): %s", threshold, HERE);
    return SDCARD_THR_OVER;      // -4
  }
  if ((blocks/1000.0/1000.0/1000.0) < 1.0) { // デバイスの全容量が1GB 未満の場合もエラーとする
    logger_write(LOG_ALERT, "Disk size under 1GB", HERE);
    return SDCARD_UNDER_1GB;     // -5
  }
  return SDCARD_SAFE_WRITABLE;   // 0
}

#endif // AGENTBOX_STORAGE_C

