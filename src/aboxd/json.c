/**
 * @file    json.c
 * @brief   libjanssonを使用した共有メモリユーティリティ
 * @author  cool-revo inc.
 */
#include <string.h>
#include "shm.h"
#include "ipc.h"
#include "json.h"
#include "util.h"
#include "logger.h"
#include "svms.h"
#include "rtsp.h"
#include "reboot.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;

/** メッセージキューを作成する共通キー。実体はaboxd.c */
extern int msq_id;

/**
 * json形式のフォーマットをパースして共有メモリに展開する
 *
 * @param[in] src json文字列
 * @retval  0 正常終了
 * @retval  1 管理サーバに登録されていない
 * @retval -1
 */
int json2shm(const char* src)
{
  json_error_t error;
  json_t* rset  = NULL;
  json_t* stack = NULL;
  json_t* val   = NULL;
  json_t* cval  = NULL;
  const char* key;  /* main loop key-value */
  const char* ckey; /* childrens loop key-value */
  size_t i;
  json_int_t num_success;
  char message[BUF_1024];
  int ret_val = 0;

  if (NULL == src || (NULL != src && '\0' == *src)) {
    logger_write(LOG_DEBUG, "src is null in json2shm() %s", HERE);
    return(-1);
  }

  rset = json_loads(src, 0, &error);
  if (NULL == rset) {
    logger_write(LOG_DEBUG, "rset is null by json_loads() %s", HERE);
    return(-1);
  }
  num_success = json_integer_value(json_object_get(rset, "success"));
  if (1 == num_success) {
    logger_write(LOG_DEBUG, "json_integer_value() is 1 %s", HERE);
    stack = json_object_get(rset, "camera");
    logger_write(LOG_DEBUG, "rset is 'camera' by json_object_get() %s", HERE);
    json_object_foreach(stack, key, val) {
      if (NULL != key && NULL != val) {
        //logger_write(LOG_DEBUG, "key:%s,val:%s, %s", key, val, HERE);
        if (       0 == strcmp(key, "id")) {
          shm_ptr->id = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "member_id")) {
          shm_ptr->member_id = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "group_id")) {
          shm_ptr->group_id = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "user_id")) {
          shm_ptr->user_id = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "uniquekey")) {
          strcpy_safe(shm_ptr->uniquekey, json_string_value(val));
        } else if (0 == strcmp(key, "name")) {
          strcpy_safe(shm_ptr->name, json_string_value(val));
        } else if (0 == strcmp(key, "discover")) {
          shm_ptr->discover = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "ipaddr")) {
          strcpy_safe(shm_ptr->addr.ipaddr, json_string_value(val));
        } else if (0 == strcmp(key, "netmask")) {
          strcpy_safe(shm_ptr->addr.netmask, json_string_value(val));
        } else if (0 == strcmp(key, "broadcast")) {
          strcpy_safe(shm_ptr->addr.broadcast, json_string_value(val));
        } else if (0 == strcmp(key, "gateway")) {
          strcpy_safe(shm_ptr->addr.gateway, json_string_value(val));
        } else if (0 == strcmp(key, "dns1")) {
          strcpy_safe(shm_ptr->addr.dns1, json_string_value(val));
        } else if (0 == strcmp(key, "dns2")) {
          strcpy_safe(shm_ptr->addr.dns2, json_string_value(val));
        } else if (0 == strcmp(key, "hwaddr")) {
          strcpy_safe(shm_ptr->hwaddr, json_string_value(val));
        } else if (0 == strcmp(key, "disabled")) {
          shm_ptr->disabled = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "login")) {
          strcpy_safe(shm_ptr->login, json_string_value(val));
        } else if (0 == strcmp(key, "passwd")) {
          strcpy_safe(shm_ptr->passwd, json_string_value(val));
        } else if (0 == strcmp(key, "ntpdate")) {
          shm_ptr->ntpdate = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "ntpfqdn")) {
          strcpy_safe(shm_ptr->ntpfqdn, json_string_value(val));
        } else if (0 == strcmp(key, "status")) {
          shm_ptr->status = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "agentbox_id")) {
          shm_ptr->agentbox_id = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "wondergate_id")) {
          shm_ptr->wondergate_id = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "bitrate")) {
          shm_ptr->bitrate = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "preserve")) {
          shm_ptr->preserve = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "type")) {
          shm_ptr->type = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "disabled")) {
          shm_ptr->disabled = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "nocharged")) {
          shm_ptr->nocharged = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "surveil")) {
          shm_ptr->surveil = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "live")) {
          shm_ptr->live = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "motion")) {
          shm_ptr->motion = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "ptz")) {
          shm_ptr->ptz = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "liveurl")) {
          strcpy_safe(shm_ptr->live_url, json_string_value(val));
        } else if (0 == strcmp(key, "ptzupurl")) {
          strcpy_safe(shm_ptr->ptz_tu, json_string_value(val));
        } else if (0 == strcmp(key, "ptzdownurl")) {
          strcpy_safe(shm_ptr->ptz_td, json_string_value(val));
        } else if (0 == strcmp(key, "ptzlefturl")) {
          strcpy_safe(shm_ptr->ptz_pl, json_string_value(val));
        } else if (0 == strcmp(key, "ptzrighturl")) {
          strcpy_safe(shm_ptr->ptz_pr, json_string_value(val));
        } else if (0 == strcmp(key, "ptzzoominurli")) {
          strcpy_safe(shm_ptr->ptz_zi, json_string_value(val));
        } else if (0 == strcmp(key, "ptzzoomouturl")) {
          strcpy_safe(shm_ptr->ptz_zo, json_string_value(val));
        } else if (0 == strcmp(key, "extension")) {
          strcpy_safe(shm_ptr->extension, json_string_value(val));
        } else if (0 == strcmp(key, "searchpath")) {
          strcpy_safe(shm_ptr->search_path, json_string_value(val));
        } else if (0 == strcmp(key, "ovf_type")) {
          shm_ptr->ovf_type = atoi_safe(json_string_value(val));
          logger_write(LOG_DEBUG, "ovf_type:%d %s",
             shm_ptr->ovf_type, HERE);
        } else if (0 == strcmp(key, "ovf_uri")) {
          strcpy_safe(shm_ptr->ovf_uri, json_string_value(val));
          logger_write(LOG_DEBUG, "ovf_uri:%s %s",
            shm_ptr->ovf_uri, HERE);
        }
      }
    }
    json_decref(stack);
    stack = json_object_get(rset, "system");
    logger_write(LOG_DEBUG, "rset is 'system' by json_object_get() %s", HERE);
    json_object_foreach(stack, key, val) {
    logger_write(LOG_DEBUG, "'system' key:%s,value:%s %s", key, json_string_value(val), HERE);
      if (NULL != key && NULL != val) {
        if (       0 == strcmp(key, "loglevel")) {
          shm_ptr->loglevel = strtoul(json_string_value(val), (char **) NULL, 10);
        } else if (0 == strcmp(key, "helper")) {
          shm_ptr->remotehelp = strtoul(json_string_value(val), (char **) NULL, 10);
        }
      }
    }
    logger_write(LOG_DEBUG, "next 'group', call json_decref() %s", HERE);

    json_decref(stack);
    stack = json_object_get(rset, "group");
    logger_write(LOG_DEBUG, "rset is 'group' by json_object_get() %s", HERE);
    json_object_foreach(stack, key, val) {
      if (NULL != key && NULL != val) {
        if (       0 == strcmp(key, "layer")) {
          shm_ptr->layer  = atoi_safe(json_string_value(val));
        } else if (0 == strcmp(key, "name")) {
          strcpy_safe(shm_ptr->group_name, json_string_value(val));
        } else if (0 == strcmp(key, "login")) {
          strcpy_safe(shm_ptr->grouplogin, json_string_value(val));
        } else if (0 == strcmp(key, "passwd")) {
          strcpy_safe(shm_ptr->grouppasswd, json_string_value(val));
        } else if (0 == strcmp(key, "cloudaddr")) {
          strcpy_safe(shm_ptr->cloudaddr, json_string_value(val));
        } else if (0 == strcmp(key, "clouddir")) {
          strcpy_safe(shm_ptr->clouddir, json_string_value(val));
        } else if (0 == strcmp(key, "cloudlogin")) {
          strcpy_safe(shm_ptr->cloudlogin, json_string_value(val));
        } else if (0 == strcmp(key, "cloudpasswd")) {
          strcpy_safe(shm_ptr->cloudpasswd, json_string_value(val));
        }
      }
    }
    json_decref(stack);
    stack = json_object_get(rset, "parent");
    logger_write(LOG_DEBUG, "rset is 'parent' by json_object_get() %s", HERE);
    json_object_foreach(stack, key, val) {
      if (NULL != key && NULL != val) {
        if (0 == strcmp(key, "name")) {
          strcpy_safe(shm_ptr->parent_name, json_string_value(val));
        }
      }
    }
    json_decref(stack);
    stack = json_object_get(rset, "root");
    logger_write(LOG_DEBUG, "rset is 'root' by json_object_get() %s", HERE);
    json_object_foreach(stack, key, val) {
      if (NULL != key && NULL != val) {
        if (0 == strcmp(key, "name")) {
          strcpy_safe(shm_ptr->root_name, json_string_value(val));
        }
      }
    }
    json_decref(stack);
    stack = json_object_get(rset, "csserver");
    logger_write(LOG_DEBUG, "rset is 'csserver' by json_object_get() %s", HERE);
    if (json_is_array(stack))  {
      json_array_foreach(stack, i, val) {
        json_object_foreach(val, ckey, cval) {
          if (NULL != ckey && NULL != cval) {
            if (       0 == strcmp(ckey, "host")) {
              strcpy_safe(shm_ptr->csserver[i].host, json_string_value(cval));
            } else if (0 == strcmp(ckey, "prefix")) {
              strcpy_safe(shm_ptr->csserver[i].prefix, json_string_value(cval));
            }
          }
        }
        json_decref(cval);
      }
    }
    json_decref(stack);
    stack = json_object_get(rset, "reboot");
    if (NULL != stack) {
      logger_write(LOG_ALERT, "get reboot request. call safe_reboot(). %s", HERE);
      //msqueue_send("reboot");
      safe_reboot();
    }
    json_decref(stack);

    if (NULL != val) {
      json_decref(val);
    }

    json_decref(rset);
  } else if (0 == num_success) {
    logger_write(LOG_DEBUG, "num_success:%lld:%s", num_success, HERE);
    ret_val = 0;
    strcpy_safe(message, json_string_value(json_object_get(rset, "message")));
    if(NULL != strstr(message, AGENTBOX_CFG_APIRES_MSG_NOTACTIVATED)) {
      ret_val = 1;
    }
    json_decref(rset);

    logger_write(LOG_DEBUG, "strstr:%s:%s", strstr(message, AGENTBOX_CFG_APIRES_MSG_NOTACTIVATED), HERE);

    return ret_val;
  }
  logger_write(LOG_DEBUG, "end json2shm() %s", HERE);
  return 0;
}

/**
 * json形式の文字列データから任意のキーにあたる値をパースしてポインタにコピーする
 *
 * @param[in,out] dest jsonからパースした値をコピーするメモリのポインタ
 * @param[in] src ソースとなるjson文字列
 * @param[in] keyname パースしたいjsonのキー名
 * @retval  0 正常終了
 * @retval -1 jsonソースの読み込みに失敗
 * @retval -2
 */
int json_simple_val(char* dest, const char* src, const char* keyname)
{
  json_error_t error;
  json_t* stack = NULL;
  json_t* val   = NULL;
  const char* key;
  int retval = -2;

  stack = json_loads(src, 0, &error);
  if (NULL == stack) {
    return -1;
  }
  json_object_foreach(stack, key, val) {
    if (0 == strcmp(key, keyname)) {
      strcpy_safe(dest, json_string_value(val));
      retval = 0;
    }
    json_decref(val);
  }
  json_decref(stack);

  return retval;
}

/**
 * json形式のフォーマットをパースして展開する
 *  GetCameraStatus用
 *  cam_statへは、特に詰めないため、cam_stat_size分確認を行う。
 *  cam_stat_size以上のデータがある場合、無視する。
 *
 * @param[in]  src json文字列
 * @param[out] cam_stat カメラステータスバッファ
 * @retval  0 正常終了
 * @retval -1
 */
int json2camstat(const char *src, svms_resp_camera_stat_t *resp_cs)
{
  json_error_t error;
  json_t* rset  = NULL;
  json_t* stack = NULL;
  json_t* val   = NULL;
  json_t* cval  = NULL;
  const char* ckey; /* childrens loop key-value */
  size_t i;

  logger_write(LOG_DEBUG, "json start %s", HERE);
  memset(resp_cs, 0, sizeof(*resp_cs));

  rset = json_loads(src, 0, &error);
  if (rset == NULL) {
    logger_write(LOG_ERR, "%s:%s", error.text, HERE);
    return -1;
  }
  strcpy_safe(resp_cs->status, json_string_value(json_object_get(rset, "status")));
  if (0 == strcasecmp(SVMS_API_GCS_RESP_OK, resp_cs->status)) {
    logger_write(LOG_DEBUG, "status:ok(%s):%s", resp_cs->status, HERE);
    stack = json_object_get(rset, "data");
    if (json_is_array(stack))  {
      json_array_foreach(stack, i, val) {
        json_object_foreach(val, ckey, cval) {
          if ((NULL != ckey && NULL != cval) && i < SVMS_GCS_CAMERA_NUM) {
            if (       0 == strcmp(ckey, "id")) {
              resp_cs->cam_stat[i].id = atoi_safe(json_string_value(cval));
            } else if (0 == strcmp(ckey, "name")) {
              strcpy_safe(resp_cs->cam_stat[i].name, json_string_value(cval));
            } else if (0 == strcmp(ckey, "uri")) {
              strcpy_safe(resp_cs->cam_stat[i].uri, json_string_value(cval));
            } else if (0 == strcmp(ckey, "uniquekey")) {
              strcpy_safe(resp_cs->cam_stat[i].uniquekey, json_string_value(cval));
            } else if (0 == strcmp(ckey, "status")) {
              strcpy_safe(resp_cs->cam_stat[i].status, json_string_value(cval));
            } else if (0 == strcmp(ckey, "edgeStatus")) {
              strcpy_safe(resp_cs->cam_stat[i].edge_status, json_string_value(cval));
            } else if (0 == strcmp(ckey, "exportStatus")) {
              strcpy_safe(resp_cs->cam_stat[i].export_status, json_string_value(cval));
            } else if (0 == strcmp(ckey, "cameras_err")) {
              strcpy_safe(resp_cs->cam_stat[i].cameras_err, json_string_value(cval));
            } else if (0 == strcmp(ckey, "record_err")) {
              strcpy_safe(resp_cs->cam_stat[i].record_err, json_string_value(cval));
            } else if (0 == strcmp(ckey, "export_err")) {
              strcpy_safe(resp_cs->cam_stat[i].export_err, json_string_value(cval));
            }
          }
        }
      }
    }
    json_decref(stack);
  } else {
    //ステータスエラーor非取得
    logger_write(LOG_ERR, "status:NG(%s):%s", resp_cs->status, HERE);
  }
  json_decref(rset);

  return 0;
}

