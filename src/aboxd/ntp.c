/**
 * @file    ntp.c
 * @brief   時刻同期とその手法決定、サービス起動に関するユーティリティ
 * @author  cool-revo inc.
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "abox_macro.h"
#include "shm.h"
#include "logger.h"
#include "process.h"
#include "ntp.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;


/**
 * 時刻設定のNTP参照・HTTP参照を切り替える
 *
 * @param[in] type NTPサーバを使用するかどうか（0:HTTP参照 1:NTP参照）
 * @param[in] service NTPサービスを提供するサーバFQDN
 * @retval 0 正常終了
 * @retval -1 引数が範囲外
 * @retval -2 FQDN不正のためHTTP参照に変更保存した
 * @sa <ntp.h> set_ntpserver戻り値
 */
int set_ntpserver(int type, const char* service)
{
  int invalid = NTPSERVER_SAVE_SUCCEEDED;
  FILE* fp;

  if ((TIMESYNC_USE_NTP != type) && (TIMESYNC_WITHOUT_NTP != type)) {
    return NTPSERVER_SAVE_FAILED;
  }

  if ((TIMESYNC_USE_NTP == type) && ('\0' != *service) && (0 < strlen(service))) {
    shm_ptr->ntpdate = TIMESYNC_USE_NTP;
    /* 共有メモリにNTPサーバ名を保存 */
    strcpy(shm_ptr->ntpfqdn, service);

    /* yx_config_ntp.iniを作成 */
    if (NULL != (fp = fopen(AGENTBOX_CFG_YX_NTP_INI_PATH, "wb"))) {
      fprintf(fp, "url=%s\n", service);
      fclose(fp);
    }
  } else {
    shm_ptr->ntpdate = TIMESYNC_WITHOUT_NTP;
    /* 共有メモリのNTPサーバ名領域を0埋め */
    memset(shm_ptr->ntpfqdn, 0, sizeof(shm_ptr->ntpfqdn));

    /* yx_config_ntp.iniが存在したら削除 */
    remove(AGENTBOX_CFG_YX_NTP_INI_PATH);

    if (TIMESYNC_USE_NTP == type) {
      invalid = NTPSERVER_SAVE_INVALID;
    }
  }
  return invalid;
}


/**
 * 時刻設定用クライアントを実行する
 *
 * NTPサーバを利用する際には、その設定をyx_config_ntp.iniに
 * 書き出して起動時に参照する
 *
 * @retval  0 正常終了
 * @retval -1 プロセスの起動に失敗
 */
int run_timesync(void)
{
  char* cmdstr;
  int estatus;

  kill_process("ntpd -p");
  kill_process(AGENTBOX_CFG_POORNTPD_BIN_PATH);
  cmdstr = (char *) malloc(sizeof(char) * BUF_200);
  if (NULL == cmdstr) {
    logger_write(LOG_ERR, "  cmdstr malloc failed. %s", HERE);
    return -2;
  }

  if (TIMESYNC_USE_NTP == shm_ptr->ntpdate) {
    sprintf(cmdstr, "ntpd -p %s", shm_ptr->ntpfqdn);
  } else {
    sprintf(cmdstr, "%s -s %s/%s",
      AGENTBOX_CFG_POORNTPD_BIN_PATH,
      shm_ptr->server,
      AGENTBOX_CFG_API_POORNTP_MEPOCH
    );
  }
  estatus = run_system(cmdstr);
  if (0 != estatus) {
    estatus = -1;
  }
  free(cmdstr);
  logger_write(LOG_DEBUG, "  run_timesync() finished. %s", HERE);

  return estatus;
}
