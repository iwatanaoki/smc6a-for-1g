/**
 * @file    shmcpy.c
 * @brief   共有メモリ入出力ユーティリティ
 * @author  cool-revo inc.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ini.h>
#include <unistd.h>
#include "abox_macro.h"
#include "shm.h"
#include "util.h"

#include "logger.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;

/** 共有メモリIDを格納する。実体はaboxd.c */
extern int shm_id;

/** 共有メモリIDをファイルに出力する。実体はaboxd.c */
extern const char* shm_file;

/** SHMファイルディスクリプタ */
FILE* shm_fp;


/**
 * 共有メモリにデータを展開する
 *
 * 管理サーバから取得済みのデータがあればそれを、ファイルが存在しない場合は
 * ファクトリーデフォルトのファイルを読み込む
 *
 * @retval  0 正常終了
 * @retval -1 ファイルオープンに失敗
 * @retval -2 メモリ確保に失敗
 * @retval  n パースエラーが発生した行番号
 */
int load_shm(void)
{
  int retval = 0;

  logger_write(LOG_DEBUG, "call ini_parse(%s) in load_shm(). %s", AGENTBOX_CFG_SHM_STORED_PATH, HERE);
  retval = ini_parse(AGENTBOX_CFG_SHM_STORED_PATH, parse_shm_ini, shm_ptr);
  if (-1 == retval) {
    logger_write(LOG_DEBUG, "retval:%d by ini_parse(%s) in load_shm(). %s", retval, AGENTBOX_CFG_SHM_STORED_PATH, HERE);
    perror(AGENTBOX_CFG_SHM_STORED_PATH);
    retval = ini_parse(AGENTBOX_CFG_SHM_DEFAULT_PATH, parse_shm_ini, shm_ptr);
    if (-1 == retval) {
      logger_write(LOG_DEBUG, "retval:%d by ini_parse(%s) in load_shm(). %s", retval, AGENTBOX_CFG_SHM_DEFAULT_PATH, HERE);
      perror(AGENTBOX_CFG_SHM_DEFAULT_PATH);
    }
    else {
      logger_write(LOG_DEBUG, "retval:%d by ini_parse(%s) in load_shm(). %s", retval, AGENTBOX_CFG_SHM_DEFAULT_PATH, HERE);
      perror(AGENTBOX_CFG_SHM_DEFAULT_PATH);
    }
    return retval;
  }
  else {
    logger_write(LOG_DEBUG, "retval:%d by ini_parse(%s) in load_shm(). %s", retval, AGENTBOX_CFG_SHM_STORED_PATH, HERE);
  }
  return 0;
}


/**
 * .iniファイルからのパース用コールバック
 *
 * libini の関数 ini_parse(const char*, ini_handler, void*) のコールバックとして使用し、
 * .iniにダンプされたファイルから共有メモリの内容をレストアする
 *
 * @param[in,out] user 値をコピーする対象へのポインタ
 * @param[in] section .iniのセクション部(ブラケット[]で囲まれた箇所)の名称
 * @param[in] name    .iniのパラメータ名
 * @param[in] value   .iniのパラメータの値
 * @retval 0 正常終了(パースエラーなく成功)
 * @retval 0以外 最初にパースエラーが発生した行番号
 */
int parse_shm_ini(void* user, const char* section, const char* name, const char* value)
{
  char idxstr[4];
  int idx;
  int i;

  if (NULL == user || NULL == section || NULL == name || NULL == value) {
    return -1;
  }

  ABOX_UNUSED(user);
  memset(idxstr, 0, sizeof(idxstr));

  if (0 == strcmp(section, "shm_abox")) {
    if (0 ==        strcmp(name, "id")) {
      shm_ptr->id = atoi_safe(value);
    } else if (0 == strcmp(name, "member_id")) {
      shm_ptr->member_id = atoi_safe(value);
    } else if (0 == strcmp(name, "group_id")) {
      shm_ptr->group_id = atoi_safe(value);
    } else if (0 == strcmp(name, "user_id")) {
      shm_ptr->user_id = atoi_safe(value);
    } else if (0 == strcmp(name, "uniquekey")) {
      strcpy_safe(shm_ptr->uniquekey, value);
    } else if (0 == strcmp(name, "name")) {
      strcpy_safe(shm_ptr->name, value);
    } else if (0 == strcmp(name, "hwaddr")) {
      strcpy_safe(shm_ptr->hwaddr, value);
    } else if (0 == strcmp(name, "disabled")) {
      shm_ptr->disabled = atoi_safe(value);
    } else if (0 == strcmp(name, "login")) {
      strcpy_safe(shm_ptr->login, value);
    } else if (0 == strcmp(name, "passwd")) {
      strcpy_safe(shm_ptr->passwd, value);
    } else if (0 == strcmp(name, "ntpdate")) {
      shm_ptr->ntpdate = atoi_safe(value);
    } else if (0 == strcmp(name, "ntpfqdn")) {
      strcpy_safe(shm_ptr->ntpfqdn, value);
    } else if (0 == strcmp(name, "status")) {
      shm_ptr->status = atoi_safe(value);
    } else if (0 == strcmp(name, "layer")) {
      shm_ptr->layer  = atoi_safe(value);
    } else if (0 == strcmp(name, "group_name")) {
      strcpy_safe(shm_ptr->group_name, value);
    } else if (0 == strcmp(name, "grouplogin")) {
      strcpy_safe(shm_ptr->grouplogin, value);
    } else if (0 == strcmp(name, "grouppasswd")) {
      strcpy_safe(shm_ptr->grouppasswd, value);
    } else if (0 == strcmp(name, "cloudaddr")) {
      strcpy_safe(shm_ptr->cloudaddr, value);
    } else if (0 == strcmp(name, "clouddir")) {
      strcpy_safe(shm_ptr->clouddir, value);
    } else if (0 == strcmp(name, "cloudlogin")) {
      strcpy_safe(shm_ptr->cloudlogin, value);
    } else if (0 == strcmp(name, "cloudpasswd")) {
      strcpy_safe(shm_ptr->cloudpasswd, value);
    } else if (0 == strcmp(name, "ipaddr")) {
      strcpy_safe(shm_ptr->addr.ipaddr, value);
    } else if (0 == strcmp(name, "broadcast")) {
      strcpy_safe(shm_ptr->addr.broadcast, value);
    } else if (0 == strcmp(name, "netmask")) {
      strcpy_safe(shm_ptr->addr.netmask, value);
    } else if (0 == strcmp(name, "gateway")) {
      strcpy_safe(shm_ptr->addr.gateway, value);
    } else if (0 == strcmp(name, "dns1")) {
      strcpy_safe(shm_ptr->addr.dns1, value);
    } else if (0 == strcmp(name, "dns2")) {
      strcpy_safe(shm_ptr->addr.dns2, value);
    } else if (0 == strcmp(name, "agentbox_id")) {
      shm_ptr->agentbox_id = atoi_safe(value);
    } else if (0 == strcmp(name, "wondergate_id")) {
      shm_ptr->wondergate_id = atoi_safe(value);
    } else if (0 == strcmp(name, "bitrate")) {
      shm_ptr->bitrate = atoi_safe(value);

    } else if (0 == strcmp(name, "preserve")) {
      shm_ptr->preserve = atoi_safe(value);
    } else if (0 == strcmp(name, "type")) {
      shm_ptr->type = atoi_safe(value);
    } else if (0 == strcmp(name, "nocharged")) {
      shm_ptr->nocharged = atoi_safe(value);
    } else if (0 == strcmp(name, "discover")) {
      shm_ptr->discover = atoi_safe(value);
    } else if (0 == strcmp(name, "surveil")) {
      shm_ptr->surveil = atoi_safe(value);
    } else if (0 == strcmp(name, "promotion")) {
      shm_ptr->promotion = atoi_safe(value);
    } else if (0 == strcmp(name, "recording")) {
      shm_ptr->recording = atoi_safe(value);
    } else if (0 == strcmp(name, "placed")) {
      strcpy_safe(shm_ptr->placed, value);
    } else if (0 == strcmp(name, "live")) {
      shm_ptr->live = atoi_safe(value);
    } else if (0 == strcmp(name, "port")) {
      shm_ptr->port = atoi_safe(value);
    } else if (0 == strcmp(name, "motion")) {
      shm_ptr->motion = atoi_safe(value);
    } else if (0 == strcmp(name, "ptz")) {
      shm_ptr->ptz = atoi_safe(value);
    } else if (0 == strcmp(name, "live_url")) {
      strcpy_safe(shm_ptr->live_url, value);
    } else if (0 == strcmp(name, "ptz_tu")) {
      strcpy_safe(shm_ptr->ptz_tu, value);
    } else if (0 == strcmp(name, "ptz_td")) {
      strcpy_safe(shm_ptr->ptz_td, value);
    } else if (0 == strcmp(name, "ptz_pl")) {
      strcpy_safe(shm_ptr->ptz_pl, value);
    } else if (0 == strcmp(name, "ptz_pr")) {
      strcpy_safe(shm_ptr->ptz_pr, value);
    } else if (0 == strcmp(name, "ptz_zi")) {
      strcpy_safe(shm_ptr->ptz_zi, value);
    } else if (0 == strcmp(name, "ptz_zo")) {
      strcpy_safe(shm_ptr->ptz_zo, value);
    } else if (0 == strcmp(name, "extension")) {
      strcpy_safe(shm_ptr->extension, value);
    } else if (0 == strcmp(name, "search_path")) {
      strcpy_safe(shm_ptr->search_path, value);
    } else if (0 == strcmp(name, "ovf_type")) {
      shm_ptr->ovf_type = atoi_safe(value);
    } else if (0 == strcmp(name, "ovf_uri")) {
      strcpy_safe(shm_ptr->ovf_uri, value);
    }
  } else if (0 == strncmp(section, "system", 6)) {
    if (0        == strcmp(name, "loglevel")) {
      shm_ptr->loglevel = strtoul(value, (char **) NULL, 10);
    } else if (0 == strcmp(name, "remotehelp")) {
      shm_ptr->remotehelp = strtoul(value, (char **) NULL, 10);
    }
  } else if (0 == strncmp(section, "csserver", 8)) {
    memset(idxstr, 0, sizeof(idxstr));
    strncpy(idxstr, (section + 9), 1);
    idxstr[1] = '\0';
    idx = atoi(idxstr);
    for (i = 0; i < 2; ++i) {
      if (i == idx) {
        if (       0 == strcmp(name, "host")) {
          strcpy_safe(shm_ptr->csserver[idx].host, value);
        } else if (0 == strcmp(name, "prefix")) {
          strcpy_safe(shm_ptr->csserver[idx].prefix, value);
        }
      }
    }
  }

  return 0;
}


/**
 * SHMの内容をFILEストリームに書き出す
 *
 * @param[in,out] path ファイルを出力するパス
 * @retval  0 正常終了
 * @retval -1 ファイルオープンに失敗
 */
int print_shm(const char* path)
{
  int i;
  int force_stdxxx = 0;
  FILE* fp;

  if (NULL == path || (NULL != path && '\0' == *path)) {
    return -2;
  }

  if (0 == strcmp(path, "stdout")) {
    fp = stdout;
    force_stdxxx = 1;
  } else if (0 == strcmp(path, "stderr")) {
    fp = stderr;
    force_stdxxx = 1;
  } else {
    fp = fopen(path, "wb");
    if (NULL == fp) {
      return -1;
    }
  }
  /* abox shm */
  fprintf(fp, "[%s]\n", "shm_abox");
  fprintf(fp, "id          = %d\n", shm_ptr->id);
  fprintf(fp, "member_id   = %d\n", shm_ptr->member_id);
  fprintf(fp, "group_id    = %d\n", shm_ptr->group_id);
  fprintf(fp, "user_id     = %d\n", shm_ptr->user_id);
  fprintf(fp, "uniquekey   = %s\n", shm_ptr->uniquekey);
  fprintf(fp, "name        = %s\n", shm_ptr->name);
  fprintf(fp, "ipaddr      = %s\n", shm_ptr->addr.ipaddr);
  fprintf(fp, "netmask     = %s\n", shm_ptr->addr.netmask);
  fprintf(fp, "broadcast   = %s\n", shm_ptr->addr.broadcast);
  fflush(fp);

  fprintf(fp, "gateway     = %s\n", shm_ptr->addr.gateway);
  fprintf(fp, "dns1        = %s\n", shm_ptr->addr.dns1);
  fprintf(fp, "dns2        = %s\n", shm_ptr->addr.dns2);
  fprintf(fp, "hwaddr      = %s\n", shm_ptr->hwaddr);
  fprintf(fp, "disabled    = %d\n", shm_ptr->disabled);
  fprintf(fp, "login       = %s\n", shm_ptr->login);
  fprintf(fp, "passwd      = %s\n", shm_ptr->passwd);
  fprintf(fp, "ntpdate     = %d\n", shm_ptr->ntpdate);
  fprintf(fp, "ntpfqdn     = %s\n", shm_ptr->ntpfqdn);
  fflush(fp);

  fprintf(fp, "status      = %d\n", shm_ptr->status);
  fprintf(fp, "layer       = %d\n", shm_ptr->layer);
  fprintf(fp, "group_name  = %s\n", shm_ptr->group_name);
  fprintf(fp, "grouplogin  = %s\n", shm_ptr->grouplogin);
  fprintf(fp, "grouppasswd = %s\n", shm_ptr->grouppasswd);
  fprintf(fp, "cloudaddr   = %s\n", shm_ptr->cloudaddr);
  fflush(fp);

  fprintf(fp, "clouddir    = %s\n", shm_ptr->clouddir);
  fprintf(fp, "cloudlogin  = %s\n", shm_ptr->cloudlogin);
  fprintf(fp, "cloudpasswd = %s\n", shm_ptr->cloudpasswd);
  fprintf(fp, "parent_name = %s\n", shm_ptr->parent_name);
  fprintf(fp, "root_name   = %s\n", shm_ptr->root_name);
  fflush(fp);

  /* camera information */
  fprintf(fp, "agentbox_id   = %d\n", shm_ptr->agentbox_id);
  fprintf(fp, "wondergate_id = %d\n", shm_ptr->wondergate_id);
  fprintf(fp, "bitrate     = %d\n", shm_ptr->bitrate);
  fprintf(fp, "preserve    = %d\n", shm_ptr->preserve);
  fprintf(fp, "type        = %d\n", shm_ptr->type);
  fprintf(fp, "disabled    = %d\n", shm_ptr->disabled);
  fprintf(fp, "nocharged   = %d\n", shm_ptr->nocharged);
  fflush(fp);

  fprintf(fp, "discover    = %d\n", shm_ptr->discover);
  fprintf(fp, "surveil     = %d\n", shm_ptr->surveil);
  fprintf(fp, "promotion   = %d\n", shm_ptr->promotion);
  fprintf(fp, "recording   = %d\n", shm_ptr->recording);
  fprintf(fp, "placed      = %s\n", shm_ptr->placed);
  fprintf(fp, "live        = %d\n", shm_ptr->live);
  fprintf(fp, "port        = %d\n", shm_ptr->port);
  fprintf(fp, "motion      = %d\n", shm_ptr->motion);
  fprintf(fp, "ptz         = %d\n", shm_ptr->ptz);
  fprintf(fp, "live_url    = %s\n", shm_ptr->live_url);
  fflush(fp);

  fprintf(fp, "ptz_tu      = %s\n", shm_ptr->ptz_tu);
  fprintf(fp, "ptz_td      = %s\n", shm_ptr->ptz_td);
  fprintf(fp, "ptz_pl      = %s\n", shm_ptr->ptz_pl);
  fprintf(fp, "ptz_pr      = %s\n", shm_ptr->ptz_pr);
  fprintf(fp, "ptz_zi      = %s\n", shm_ptr->ptz_zi);
  fprintf(fp, "ptz_zo      = %s\n", shm_ptr->ptz_zo);
  fprintf(fp, "extension   = %s\n", shm_ptr->extension);
  fprintf(fp, "search_path = %s\n", shm_ptr->search_path);
  fprintf(fp, "ovf_type    = %d\n", shm_ptr->ovf_type);
  fprintf(fp, "ovf_uri     = %s\n", shm_ptr->ovf_uri);

  /* system params */
  fputs("\n[system]\n", fp);
  fprintf(fp, "loglevel    = %d\n", shm_ptr->loglevel);
  fprintf(fp, "remotehelp  = %d\n", shm_ptr->remotehelp);
  fflush(fp);

  /* runtime params */
  fputs("\n[runtime]\n", fp);
  fprintf(fp, "interval = %ld\n", shm_ptr->interval);
  fprintf(fp, "pidfile  = %s\n", shm_ptr->pidfile);
  fprintf(fp, "shmfile  = %s\n", shm_ptr->shmfile);
  fprintf(fp, "logfile  = %s\n", shm_ptr->logfile);
  fprintf(fp, "cacert   = %s\n", shm_ptr->cacert);
  fprintf(fp, "server   = %s\n", shm_ptr->server);
  fprintf(fp, "csurl      = %s\n", shm_ptr->csurl);
  fprintf(fp, "tunnelurl  = %s\n", shm_ptr->tunnelurl);
  fprintf(fp, "sshtunnel[0].server_vip = %s\n", shm_ptr->sshtunnel[0].server_vip);
  fprintf(fp, "sshtunnel[0].server_port = %d\n", shm_ptr->sshtunnel[0].server_port);
  fprintf(fp, "sshtunnel[1].server_vip = %s\n", shm_ptr->sshtunnel[1].server_vip);
  fprintf(fp, "sshtunnel[1].server_port = %d\n", shm_ptr->sshtunnel[1].server_port);
  fprintf(fp, "smbshare = %s\n", shm_ptr->smbshare);
  fprintf(fp, "runtime_addr.dhcp      = %d\n", shm_ptr->runtime_addr.dhcp);
  fprintf(fp, "runtime_addr.ipaddr    = %s\n", shm_ptr->runtime_addr.ipaddr);
  fflush(fp);

  fprintf(fp, "runtime_addr.netmask   = %s\n", shm_ptr->runtime_addr.netmask);
  fprintf(fp, "runtime_addr.broadcast = %s\n", shm_ptr->runtime_addr.broadcast);
  fprintf(fp, "runtime_addr.gateway   = %s\n", shm_ptr->runtime_addr.gateway);
  fprintf(fp, "runtime_addr.dns1      = %s\n", shm_ptr->runtime_addr.dns1);
  fprintf(fp, "runtime_addr.dns2      = %s\n", shm_ptr->runtime_addr.dns2);
  fflush(fp);

  /* cache servers */
  for (i = 0; i < (sizeof(shm_ptr->csserver) / sizeof(shm_ptr->csserver[0])); ++i) {
    fprintf(fp, "\n[csserver_%d]\n", i);
    fprintf(fp, "host     = %s\n", shm_ptr->csserver[i].host);
    fprintf(fp, "prefix   = %s\n", shm_ptr->csserver[i].prefix);
    fflush(fp);
  }

  /* close discripter */
  if (0 == force_stdxxx) {
    fsync(fileno(fp));
    fclose(fp);
  }
  return 0;
}

