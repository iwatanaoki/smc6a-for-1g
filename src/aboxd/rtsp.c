/**
 * @file    rtsp.c
 * @brief   RTSPカメラ接続・解除・再接続
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_RTSP_C
#define AGENTBOX_RTSP_C

#include <unistd.h>
#include <sys/stat.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <pthread.h>
#include "abox_config.h"
#include "shm.h"
#include "logger.h"
#include "auth.h"
#include "json.h"
#include "process.h"
#include "rtsp.h"
#include "util.h"
#include "base64utl.h"
#include "aboxd.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;

extern int g_isSetSD;   // SDカードがセットされたかどうか(false:未セット、true:セット）
extern int g_prev_isSetSD;   // (直前状態）SDカードがセットされたかどうか(false:未セット、true:セット）
extern int g_autoConnect;   // 自律接続RTSPモード
extern int g_isSaveSDcard;  // SDカード保存機能有効状態（false:無効、true:有効)
extern int g_isDisconnectRtspServer;  // rtspサーバ接続可否フラグ（false:接続可、true:不可）
extern int g_prev_isDisconnectRtspServer;


/**
 * RTSP起動チェック自体を pthread 化する
 */
void *bkup_rtsp_execute_check(void *args) {
  time_t seq = 0; //ループ開始時刻
  time_t l_prev_seq_nano = 0;
  struct stat st;

  logger_close();
  logger_open();

  logger_write(LOG_INFO, "rtsp thread: start thread rtsp_execute_check: %s", HERE);

  while(1) {
    /* このループの開始時刻 */
    seq = time(NULL);

    if (l_prev_seq_nano <= 0 || (seq - l_prev_seq_nano) > ABOXD_EVT_TRIGGER_NANO) {
      logger_write(LOG_DEBUG, "rtsp thread: status check, %s", HERE);

      // smcrtsp.treasure-tv.jp へのping 死活監視(アクセスできない生成）フラグの存在チェック
      if (stat(IS_DISCONNECT_RTSPSERVER_FLG, &st) == 0) {
        g_isDisconnectRtspServer = true;
      }
      else {
        g_isDisconnectRtspServer = false;
      }
      if (g_prev_isDisconnectRtspServer != g_isDisconnectRtspServer) {
        if (g_isDisconnectRtspServer == true) {
          logger_write(LOG_ALERT, "disconnected to smcrtsp.treasure-tv.jp. %s", HERE);
        }
        else {
          logger_write(LOG_ALERT, "connected is smcrtsp.treasure-tv.jp. %s", HERE);
        }
        g_prev_isDisconnectRtspServer = g_isDisconnectRtspServer;
      }

      if (g_autoConnect == true) {
        // SDカード保存（バックアップ）ffmpeg プロセス起動制御実行
        open_backup_recording_rtsp();
      }

      l_prev_seq_nano = seq;
    }


    logger_write(LOG_DEBUG, "rtsp thread: wait 2s. %s", HERE);
    /* 指定秒ループをwaitする */
    sleep(2);
  }

  logger_write(LOG_ALERT, "rtsp thread: end thread rtsp_execute_check: %s", HERE);
  logger_close();
  g_is_alive_pthr_bkup_rtsp = FALSE;
  pthread_exit(NULL);

  return;
}

/**
 * 関連付けられた監視カメラがRTSP対応の場合、
 * リレーだけの場合は ffmpeg により stunnel 経由リレー接続プロセスの起動、
 * ローカル保存の場合は ffmpeg により 5分尺毎に指定のローカル格納先に保存
 * するプロセスを起動する
 *
 * @retval  0 正常終了
 * @retval  1 リレープロセスが生成できない
 * @retval  2 ovf_uri が空
 * @retval  3 録画プロセスが生成できない
 * @retval  4 録画用ovf_uri が空
 */
int open_needed_rtsp(void)
{
  int ret = 0;
  int l_rtn = 0;
  int i = 0;
  int j = 0;
  int k = 0;
  char cmdstr[BUF_2048];
  char tmpcmdstr[BUF_2048];
  char tmpcmdstr2[BUF_2048];
  char linebuf[BUF_128];
  FILE *cmdPipe = (FILE*)NULL;
  char *tmpp = (char*)NULL;
  int  cmdRelayPid = 0;
  int  cmdRelayffcheckPid = 0;
  int  tokenSec = 0;
  char tokenKey[BUF_512];
  struct stat st;
  int  isChangeOvfUri = false;
  
  logger_write(LOG_DEBUG, "control connection to local camera function start. %s", HERE);

  memset(tokenKey, 0, sizeof(tokenKey));

  logger_write(LOG_DEBUG, "id:%d,uniquekey:%s,type:%d,disabled:%d,ovf_type:%d,ovf_uri:%s, %s",
    shm_ptr->id,
    shm_ptr->uniquekey,
    shm_ptr->type,
    shm_ptr->disabled,
    shm_ptr->ovf_type,
    shm_ptr->ovf_uri,
    HERE);
  /* IDが空でない && uniquekey が空でない */
  if (0 != shm_ptr->id && 
      '\0' != shm_ptr->uniquekey[0]) {

    cmdRelayPid = 0;
    sprintf(tmpcmdstr, "pgrep -f \"^ffmpeg .*%s.*livestream.*\"",
      shm_ptr->uniquekey);
    logger_write(LOG_DEBUG, "rtsp relay proccess alive check %s, %s", tmpcmdstr, HERE);
    cmdPipe = popen(tmpcmdstr, "r");
    if (NULL != cmdPipe) {
      while (NULL != fgets(linebuf, sizeof(linebuf), cmdPipe)) {
        if (NULL != (tmpp = strchr(linebuf, '\n'))) {
          *tmpp = '\0';
          cmdRelayPid = atoi(linebuf);
          break;
        }
      }
      pclose(cmdPipe);
      cmdPipe = (FILE*)NULL;
    }
    cmdRelayffcheckPid = 0;
    sprintf(tmpcmdstr, "pgrep -f \"^ffcheck .*%s LIVE\"",
      shm_ptr->uniquekey);
    logger_write(LOG_DEBUG, "ffcheck for rtsp relay proccess alive check %s, %s", tmpcmdstr, HERE);
    cmdPipe = popen(tmpcmdstr, "r");
    if (NULL != cmdPipe) {
      while (NULL != fgets(linebuf, sizeof(linebuf), cmdPipe)) {
        if (NULL != (tmpp = strchr(linebuf, '\n'))) {
          *tmpp = '\0';
          cmdRelayffcheckPid = atoi(linebuf);
        }
      }
      pclose(cmdPipe);
      cmdPipe = (FILE*)NULL;
    }
    if (cmdRelayPid > 0) {
      if (cmdRelayffcheckPid > 0) {
        logger_write(LOG_DEBUG, "ffcheck is alive(pid:%d), %s", cmdRelayffcheckPid, HERE);
      }
      else {
        logger_write(LOG_DEBUG, "ffcheck is dead(pid:%d), %s", cmdRelayffcheckPid, HERE);
      }
    }

    // 既にRTSPリレーコマンドが起動済み
    if (cmdRelayPid > 0) {
      logger_write(LOG_DEBUG, "ffmpeg rtsp relay for %s is already running(pid:%d). %s", 
        shm_ptr->uniquekey, cmdRelayPid, HERE);
      // カメラ種別が自律接続RTSPでない、またはdiabled である、
      // またはlive ONでない、
      // またはffcheck が起動していない、
      // または ovf_type:2(サーバ側で保存),3(リレーのみ）,4(エージェントボックス保存）
      // いずれでもない
      // 場合は 起動プロセスを強制終了
      if (1 == shm_ptr->disabled
      || CAMERA_TYPE_AUTORTSP != shm_ptr->type
      || 0 == shm_ptr->live
      || 0 >= cmdRelayffcheckPid
      || (2 != shm_ptr->ovf_type
         && 3 != shm_ptr->ovf_type
         && 4 != shm_ptr->ovf_type)) {
        logger_write(LOG_ALERT, "force kill relay process(pid:%d) of %s, because it is disable mode. %s", 
          cmdRelayPid, shm_ptr->uniquekey, HERE);
        kill(cmdRelayPid, SIGKILL);
      }
      // 上記以外であれば、起動していてOKなのでそのまま
      else {
      }
    }
    // RTSPリレーコマンドは起動していない
    else {
      // カメラ種別がAUTORTSPであり、diabled でなく、
      // live ONであり、
      // ovf_type:2(サーバ側で保存),3(リレーのみ）,4(エージェントボックス保存）
      // のいずれかであれば
      // プロセス起動
      if (0 == shm_ptr->disabled
      && CAMERA_TYPE_AUTORTSP == shm_ptr->type
      && 1 == shm_ptr->live
      && (2 == shm_ptr->ovf_type
       || 3 == shm_ptr->ovf_type
       || 4 == shm_ptr->ovf_type)) {

        logger_write(LOG_DEBUG, "%s relay process not runnning, starting. %s", 
          shm_ptr->uniquekey, HERE);
        tokenSec = getSecureTokenSec();
        getSecureTokenKey(tokenSec, shm_ptr->uniquekey, tokenKey);
        memset(tmpcmdstr, 0, sizeof(tmpcmdstr));
        memset(tmpcmdstr2, 0, sizeof(tmpcmdstr2));
        sprintf(tmpcmdstr, RTSP_RELAY_CMD,
          SMC3APLUS_LOCAL_RTSP_URL,
          shm_ptr->uniquekey,
          tokenSec, tokenKey);
        logger_write(LOG_DEBUG, "cmd: %s %s", tmpcmdstr, HERE);
        k=0;
        for(j=0; j<strlen(tmpcmdstr); j++, k++) {
          if (tmpcmdstr[j] == '&') {
            tmpcmdstr2[k] = '\\';
            k++;
          }
          tmpcmdstr2[k] = tmpcmdstr[j];
        }
        logger_write(LOG_DEBUG, "change cmd: %s %s", tmpcmdstr2, HERE);
        //sprintf(cmdstr, "nohup %s 2>&1 | split -b 1000000 - /mtd/coolrevo/var/log/ffmpeg_live_%s.log. &", 
        //  tmpcmdstr2, shm_ptr->uniquekey);
        //sprintf(cmdstr, "nohup %s > /dev/null 2>&1 &", tmpcmdstr2);
        sprintf(cmdstr, "nohup %s 2>&1 | ffcheck %s LIVE > /dev/null &",
              tmpcmdstr2,
              shm_ptr->uniquekey);
        logger_write(LOG_DEBUG, "execute: %s %s", cmdstr, HERE);
        l_rtn = WEXITSTATUS(system(cmdstr));
        if (l_rtn == 0) {
          logger_write(LOG_INFO, "%s relay process start success. %s", 
            shm_ptr->uniquekey, HERE);
        }
        else {
          logger_write(LOG_ERR, "%s relay process start failed. %s", 
            shm_ptr->uniquekey, HERE);
          ret = 1;
        }
      }
      // 上記以外であれば、起動しなくてOKなのでそのまま
      else {
      }
    }
  }

  // 対象外のRTSPプロセスが起動していたら、強制終了
  kill_other_camera_rtsp();

  return ret;
}

/**
 * ローカル保存の場合は ffmpeg により 5分尺毎に指定のローカル格納先に保存
 * するプロセスを起動する
 *
 * @retval  0 正常終了
 * @retval  2 ovf_uri が空
 * @retval  3 録画プロセスが生成できない
 * @retval  4 録画用ovf_uri が空
 */
int open_backup_recording_rtsp(void)
{
  int ret = 0;
  int l_rtn = 0;
  int i = 0;
  int j = 0;
  int k = 0;
  char cmdstr[BUF_2048];
  char tmpcmdstr[BUF_2048];
  char tmpcmdstr2[BUF_2048];
  char linebuf[BUF_128];
  FILE *cmdPipe = (FILE*)NULL;
  char *tmpp = (char*)NULL;
  int  cmdRecPid = 0;
  struct stat st;
  int  isChangeOvfUri = false;
  
  logger_write(LOG_DEBUG, "control connection to local camera function start. %s", HERE);

  logger_write(LOG_DEBUG, "id:%d,uniquekey:%s,type:%d,disabled:%d,ovf_type:%d,ovf_uri:%s, %s",
    shm_ptr->id,
    shm_ptr->uniquekey,
    shm_ptr->type,
    shm_ptr->disabled,
    shm_ptr->ovf_type,
    shm_ptr->ovf_uri,
    HERE);
  /* IDが空でない && uniquekey が空でない */
  if (0 != shm_ptr->id && 
      '\0' != shm_ptr->uniquekey[0]) {

    cmdRecPid = 0;
    sprintf(tmpcmdstr, "pgrep -f \"^ffmpeg .*segment.*%s.*\"",
      shm_ptr->uniquekey);
    logger_write(LOG_DEBUG, "rtsp recording proccess alive check %s, %s", tmpcmdstr, HERE);
    cmdPipe = popen(tmpcmdstr, "r");
    if (NULL != cmdPipe) {
      while (NULL != fgets(linebuf, sizeof(linebuf), cmdPipe)) {
        if (NULL != (tmpp = strchr(linebuf, '\n'))) {
          *tmpp = '\0';
          cmdRecPid = atoi(linebuf);
          break;
        }
      }
      pclose(cmdPipe);
      cmdPipe = (FILE*)NULL;
    }

    // 既にRTSP録画コマンドが起動済み
    if (cmdRecPid > 0) {
      logger_write(LOG_DEBUG, "ffmpeg rtsp Recording for %s is already running(pid:%d). %s", 
        shm_ptr->uniquekey, cmdRecPid, HERE);
      // カメラ種別が自律接続RTSPでない、またはdiabled である、
      // LIVE ONではない、
      // SDカードがセットされていない、
      // SDカード保存機能が有効でない、
      // rtspserver への接続ができている、
      // ovf_type = 2(LIVE+センターVOD変換）でない
      // 場合は 起動プロセスを強制終了
      if (1 == shm_ptr->disabled
      || CAMERA_TYPE_AUTORTSP != shm_ptr->type
      || 0 == shm_ptr->live
      || false == g_isSetSD
      || false == g_isSaveSDcard
      || false == g_isDisconnectRtspServer
      || 2 != shm_ptr->ovf_type) {
        logger_write(LOG_ALERT, "force kill Recording process(pid:%d) of %s, because it is disable mode. %s", 
          cmdRecPid, shm_ptr->uniquekey, HERE);
        if (false == g_isDisconnectRtspServer) {
          logger_write(LOG_ALERT, 
            "disconnect RTSP server, g_isDisconnectRtspServer is false. %s", HERE);
        }
        kill(cmdRecPid, SIGKILL);
      }
      // 上記以外であれば、起動していてOKなのでそのまま
      else {
      }
    }
    // RTSP録画マンドは起動していない
    else {
      // カメラ種別が自律接続RTSPであり、diabled でなく、
      // SDカードがセットされている
      // SDカード保存機能が有効である
      // rtspserver への接続ができない、
      // ovf_type:2(センター保存)であれば
      // バックアップ録画のため、プロセス起動
      if (0 == shm_ptr->disabled
      && CAMERA_TYPE_AUTORTSP == shm_ptr->type
      && 1 == shm_ptr->live
      && true == g_isSetSD
      && true == g_isSaveSDcard
      && true == g_isDisconnectRtspServer
      && 2 == shm_ptr->ovf_type) {
        logger_write(LOG_ALERT, 
          "SMC-3A Plus is cannot rtsp recording by ffmpeg.(uniquekey:%s) %s", 
          shm_ptr->uniquekey, HERE);
        logger_write(LOG_ALERT, "%s recording process not runnning, starting. %s", 
          shm_ptr->uniquekey, HERE);
        memset(tmpcmdstr, 0, sizeof(tmpcmdstr));
        sprintf(tmpcmdstr, "%s/rtsp/%s",
          LOCAL_RECORDING_TOPDIR,
          shm_ptr->uniquekey);
        if (0 != stat(tmpcmdstr, &st)) {
          logger_write(LOG_ERR, "target dir %s is not exists, mkdir, %s",
            tmpcmdstr, HERE);
          sprintf(tmpcmdstr2, "mkdir -p %s", tmpcmdstr);
          l_rtn = WEXITSTATUS(system(tmpcmdstr2));
          if (l_rtn == 0) {
            logger_write(LOG_DEBUG, "target dir %s is mkdir success, %s",
            tmpcmdstr, HERE);
          }
          else {
            logger_write(LOG_ERR, "target dir %s is cannot mkdir, %s",
            tmpcmdstr, HERE);
            return 4;
          }
        }
        memset(tmpcmdstr, 0, sizeof(tmpcmdstr));
        memset(tmpcmdstr2, 0, sizeof(tmpcmdstr2));
        sprintf(tmpcmdstr, RTSP_RECORDING_CMD,
          SMC3APLUS_LOCAL_RTSP_URL,
          LOCAL_RECORDING_TOPDIR,
          shm_ptr->uniquekey,
          shm_ptr->uniquekey);
        k=0;
        for(j=0; j<strlen(tmpcmdstr); j++, k++) {
          if (tmpcmdstr[j] == '&') {
            tmpcmdstr2[k] = '\\';
            k++;
          }
          tmpcmdstr2[k] = tmpcmdstr[j];
        }
        logger_write(LOG_DEBUG, "change cmd: %s %s", tmpcmdstr2, HERE);
        sprintf(cmdstr, "nohup %s > /dev/null 2>&1 &", tmpcmdstr2);
      //sprintf(cmdstr, "nohup %s 2>&1 | split -b 1000000 - /mtd/coolrevo/var/log/ffmpeg_live_%s.log. &", 
      //  tmpcmdstr2, shm_ptr->uniquekey);
        logger_write(LOG_DEBUG, "execute: %s %s", cmdstr, HERE);
        l_rtn = WEXITSTATUS(system(cmdstr));
        if (l_rtn == 0) {
          logger_write(LOG_INFO, "%s recording process start success. %s", 
            shm_ptr->uniquekey, HERE);
        }
        else {
          logger_write(LOG_ERR, "%s recording process start failed. %s", 
            shm_ptr->uniquekey, HERE);
          ret = 3;
        }
      }
      // 上記以外であれば、起動しなくてOKなのでそのまま
      else {
      }
    }
  }

  // 対象外のRTSPプロセスが起動していたら、強制終了
  //kill_other_camera_rtsp();

  return ret;
}

/**
 * nginx secureToken 第一パラメータ（秒数）を算出
 *
 * @retval 秒数
 */
int getSecureTokenSec(void)
{
  time_t l_now = 0;
 
  l_now = time(NULL);
  l_now += 60 * 60;
  return l_now;
}

/**
 * nginx secureToken 第二パラメータ（秘密鍵）を算出
 *
 * @param[in]  i_sec         :キー生成に使用する秒数
 * @param[in]  i_uniquekey   :カメラuniquekey
 * @param[out] o_createkey   :生成したsecureToken用キー(呼出側で領域確保したポインタを指定）
 * @retval 0 正常終了
 * @retval 1 i_uniquekey がNULL
 * @retval 2 o_createkey がNULL
 */
int getSecureTokenKey(int i_sec, char *i_uniquekey, char *o_createkey)
{
  int l_rtn = 0;
  char tmpbuf[BUF_512];
  unsigned char md5tmpbuf[BUF_512];
  char base64tmpbuf[BUF_512];
  int  md5size = 0;
  int  base64size = 0;
  int  i = 0;
  int  j = 0;

  if (NULL == i_uniquekey || (NULL != i_uniquekey && '\0' == *i_uniquekey)) {
    l_rtn = 1;
    return l_rtn;
  }
  if (NULL == o_createkey) {
    l_rtn = 2;
    return l_rtn;
  }

  memset(tmpbuf, 0, sizeof(tmpbuf));
  memset(md5tmpbuf, 0, sizeof(md5tmpbuf));
  memset(base64tmpbuf, 0, sizeof(base64tmpbuf));

  sprintf(tmpbuf, "%s%s/livestream%d", 
    RTSP_SECURETOKEN_SECRETKEY,
    i_uniquekey, i_sec);

  logger_write(LOG_DEBUG, "secureToken base key:%s, %s", tmpbuf, HERE);

  md5_digest_bin(md5tmpbuf, &md5size, tmpbuf);
  //logger_write(LOG_DEBUG, "secureToken md5 key:%s, %s", md5tmpbuf, HERE);
  encode_base64(md5tmpbuf, md5size, base64tmpbuf, &base64size);
  logger_write(LOG_DEBUG, "secureToken base64encode key:%s, %s", base64tmpbuf, HERE);
  for(i=0,j=0; i<base64size && i<sizeof(base64tmpbuf); i++) {
    if (base64tmpbuf[i] == '=') {
      continue;
    }
  /*
    if (base64tmpbuf[i] == '+') {
      if (i+1 <base64size && i+1<sizeof(base64tmpbuf)) {
        if (base64tmpbuf[i+1] == '/') {
          base64tmpbuf[i] = '-';
          base64tmpbuf[i+1] = '_';
        }
      }
    }
  */
    if (base64tmpbuf[i] == '+') {
      base64tmpbuf[i] = '-';
    }
    else if (base64tmpbuf[i] == '/') {
      base64tmpbuf[i] = '_';
    }
    *(o_createkey+j) = base64tmpbuf[i];
    j++;
  }
  logger_write(LOG_DEBUG, "secureToken base64encode urlencode key:%s, %s", o_createkey, HERE);
  //strcpy(o_createkey, base64tmpbuf);

  return l_rtn;
}

/**
 * 対象外カメラのRTSPプロセス(ffmpeg)の起動が残っていたら終了
 *
 * @retval 0 正常終了
 */
int kill_other_camera_rtsp(void)
{
  int l_rtn = 0;
  int cmdPid = 0;
  int rtspRelayPid = 0;
  int rtspRecPid = 0;
  int rtspPidNum = 0;
  char tmpcmdstr[BUF_200];
  char linebuf[BUF_200];
  char *tmpp = (char*)NULL;
  FILE *cmdPipe = (FILE*)NULL;
  int isFound = false;

  logger_write(LOG_DEBUG, "other camera rtsp ffmpeg process force stop. %s", HERE);

  logger_write(LOG_DEBUG, "id:%d,uniquekey:%s,type:%d,disabled:%d,ovf_type:%d,ovf_uri:%s, %s",
      shm_ptr->id,
      shm_ptr->uniquekey,
      shm_ptr->type,
      shm_ptr->disabled,
      shm_ptr->ovf_type,
      shm_ptr->ovf_uri,
     HERE);
  /* IDが空でない && uniquekey が空でない */
  if (0 != shm_ptr->id
  && '\0' != shm_ptr->uniquekey[0]) {
    cmdPid = 0;
    sprintf(tmpcmdstr, "pgrep -f \"^ffmpeg .*%s.*livestream.*\"",
      shm_ptr->uniquekey);
    logger_write(LOG_DEBUG, "rtsp relay proccess alive check %s, %s", tmpcmdstr, HERE);
    cmdPipe = popen(tmpcmdstr, "r");
    if (NULL != cmdPipe) {
      while (NULL != fgets(linebuf, sizeof(linebuf), cmdPipe)) {
        if (NULL != (tmpp = strchr(linebuf, '\n'))) {
          *tmpp = '\0';
          cmdPid = atoi(linebuf);
        }
        if (cmdPid > 0) {
          rtspRelayPid = cmdPid;
          logger_write(LOG_DEBUG, "target %s rtsp relay alive process(PID:%d), %s", 
            shm_ptr->uniquekey, rtspRelayPid, HERE);
          break;
        }
      }
      pclose(cmdPipe);
      cmdPipe = (FILE*)NULL;
    }

    cmdPid = 0;
    sprintf(tmpcmdstr, "pgrep -f \"^ffmpeg .*segment.*%s.*\"",
      shm_ptr->uniquekey);
    logger_write(LOG_DEBUG, "rtsp rec proccess alive check %s, %s", tmpcmdstr, HERE);
    cmdPipe = popen(tmpcmdstr, "r");
    if (NULL != cmdPipe) {
      while (NULL != fgets(linebuf, sizeof(linebuf), cmdPipe)) {
        if (NULL != (tmpp = strchr(linebuf, '\n'))) {
          *tmpp = '\0';
          cmdPid = atoi(linebuf);
        }
        if (cmdPid > 0) {
          rtspRecPid = cmdPid;
          logger_write(LOG_DEBUG, "target %s rtsp alive rec process(PID:%d), %s", 
            shm_ptr->uniquekey, rtspRecPid, HERE);
          break;
        }
      }
      pclose(cmdPipe);
      cmdPipe = (FILE*)NULL;
    }
  }

  sprintf(tmpcmdstr, "pgrep -f \"^ffmpeg .*\"");
  logger_write(LOG_DEBUG, "all rtsp proccess alive check %s, %s", tmpcmdstr, HERE);
  cmdPipe = popen(tmpcmdstr, "r");
  if (NULL != cmdPipe) {
    while (NULL != fgets(linebuf, sizeof(linebuf), cmdPipe)) {
      cmdPid = 0;
      if (NULL != (tmpp = strchr(linebuf, '\n'))) {
        *tmpp = '\0';
        cmdPid = atoi(linebuf);
      }
      if (cmdPid > 0) {
        isFound = false;
        if (cmdPid == rtspRelayPid ||
            cmdPid == rtspRecPid) {
          isFound = true;
        }
        if (isFound != true) {
          logger_write(LOG_DEBUG, "other target rtsp proccess(PID:%d) alive, force stop, %s", 
            cmdPid, HERE);
          kill(cmdPid, SIGKILL);
        }
      }
    }
    pclose(cmdPipe);
    cmdPipe = (FILE*)NULL;
  }

  return l_rtn;
}

/**
 * 全てのRTSP録画プロセス(ffmpeg)を修了する
 *
 * @retval 0 正常終了
 */
int killall_recording_rtsp(void)
{
  int l_rtn = 0;
  int cmdRecPid = 0;
  char cmdstr[BUF_200];
  char linebuf[BUF_200];
  char *tmpp = (char*)NULL;
  FILE *cmdPipe = (FILE*)NULL;

  logger_write(LOG_DEBUG, "all recording rtsp ffmpeg process force stop. %s", HERE);

  sprintf(cmdstr, "pgrep -f \"^ffmpeg .*segment.*\"");
  logger_write(LOG_DEBUG, "rtsp recording proccess alive check %s, %s", cmdstr, HERE);
  cmdPipe = popen(cmdstr, "r");
  if (NULL != cmdPipe) {
    while (NULL != fgets(linebuf, sizeof(linebuf), cmdPipe)) {
      cmdRecPid = 0;
      if (NULL != (tmpp = strchr(linebuf, '\n'))) {
        *tmpp = '\0';
        cmdRecPid = atoi(linebuf);
        if (cmdRecPid > 0) {
          logger_write(LOG_DEBUG, "rtsp recording proccess found(PID:%d), kill proccess, %s", 
            cmdRecPid, HERE);
          kill(cmdRecPid, SIGKILL);
        }
      }
    }
    pclose(cmdPipe);
    cmdPipe = (FILE*)NULL;
  }
  return l_rtn;
}

/**
 * 全てのRTSP接続プロセス(ffmpeg)を修了する
 *
 * @retval 0 正常終了
 */
int killall_rtsp(void)
{
  int l_rtn = 0;
 
  logger_write(LOG_DEBUG, "all ffmpeg process force stop. %s", HERE);
  l_rtn = WEXITSTATUS(system("killall -KILL ffmpeg"));
  return l_rtn;
}

#endif // AGENTBOX_RTSP_C
