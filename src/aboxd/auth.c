/**
 * @file    auth.c
 * @brief   agentboxアカウント取得・認証関係
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_AUTH_C
#define AGENTBOX_AUTH_C
#endif AGENTBOX_AUTH_C

#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <sys/stat.h>
#include <openssl/sha.h>
#include "abox_macro.h"
#include "shm.h"
#include "logger.h"
#include "util.h"
#include "auth.h"
#include "sshtunnel.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;


/**
 * 引数で与えられた文字列に対するSHA256ダイジェストを作り、メモリにコピーする
 *
 * @param[in,out] dest 作成されたダイジェストを書き込むメモリ領域へのポインタ
 * @param[in] src ダイジェストを作成する文字列
 * @retval  0 正常終了
 * @retval -1 ダイジェスト作成に失敗
 */
int sha256_digest(char* dest, const char* src)
{
  int i;
  unsigned char hash[SHA256_DIGEST_LENGTH];
  SHA256_CTX ctx;

  if (strlen(src) <= 0) {
    return -1;
  }
  SHA256_Init(&ctx);
  SHA256_Update(&ctx, src, strlen(src));
  SHA256_Final(hash, &ctx);
  for (i = 0; i < SHA256_DIGEST_LENGTH; i++) {
    sprintf(dest + (i * 2), "%02x", hash[i]);
  }
  dest[64] = '\0';
  return 0;
}


/**
 * HWアドレス(MAC)を取得し、メモリにコピーする
 *
 * このシステムにおいてHWADDRの正しい書式は ^([0-9a-f]{2}:){5}[0-9a-f]{2}$
 *
 * @param[in,out] dest HWアドレス文字列を書き込む領域へのポインタ
 * @retval  0 正常終了
 * @retval -1 デバイスクラスのオープンに失敗
 * @retval -2 HWアドレスの取得に失敗
 */
int get_hw_addr(char* dest)
{
  char mac[18];
  FILE* fp;

  if (NULL != (fp = fopen("/sys/class/net/eth0/address", "r"))) {
    fscanf(fp, "%s\n", mac);
    fclose(fp);
    if (17 != strlen(mac)) {
      return -2;
    }
    strncpy(dest, mac, (17 * sizeof(char)));
    dest[17] = '\0';
    return 0;
  } else {
    return -1;
  }
}


/**
 * HWアドレス(MAC)を取得し、文字列のポインタを返す
 *
 * 取得したHWアドレスは(:)を除去、英字は大文字に変換して返される
 *
 * @return 整形後のHWアドレス文字列へのポインタ、失敗時はNULL
 */
char* get_hw_str(void)
{
  static char mac[13];
  char rawaddr[18];
  char schar[1];
  char ucase[1];
  int i = 0;
  FILE* fp;

  if (NULL != (fp = fopen("/sys/class/net/eth0/address", "r"))) {
    fscanf(fp, "%s\n", rawaddr);
    fclose(fp);
    if (17 != strlen(rawaddr)) {
      return NULL;
    }
    memset(mac, 0, sizeof(char) * 13);
    for (i = 0; i < 18; i++) {
      strncpy(schar, rawaddr + i, sizeof(char) * 1);
      if (0 != strncmp(schar, ":", sizeof(char) * 1)) {
        sprintf(ucase, "%c", toupper(*schar));
        strncat(mac, ucase, sizeof(char) * 1);
      }
    }
    mac[12] = '\0';
    return mac;
  } else {
    return NULL;
  }
}


/**
 * 特定のルールに基づき作成される端末固有キーを取得し、メモリにコピーする
 *
 * 現状、固有キー作成のルールはHWアドレスから作成したSHA256ダイジェストである。
 * saltを作成してより強固な情報にすることが将来的に必要かも。
 *
 * @param[in,out] dest キーを書き込む領域へのポインタ
 * @retval  0 正常終了
 * @retval -1 HWADDRの取得に失敗
 * @retval -2 ダイジェスト作成に失敗
 */
int get_uniquekey(char* dest)
{
  char mac[18];
  char *l_hwstr = (char*)NULL;

  //if (0 != get_hw_addr(mac)) {
  //  return -1;
  //}
  //if (0 != sha256_digest(dest, mac)) {
  //  return -2;
  //}

  // SMC-3A Plus では、agentbox, wondergate01 のような HASH uniquekey ではなく
  // カメラ識別キーとして、MACアドレスから：を除いた大文字英文字＋数字文字列
  /* HWアドレス文字列の取得 */
  if (dest == (char*)NULL) {
    return -2;
  }
  l_hwstr = get_hw_str();
  if (l_hwstr == (char*)NULL) {
    return -1;
  }
  strcpy(dest, l_hwstr);
  return 0;
}


/**
 * SMC-3A Plusをアクティベートする
 *
 * 保存先パスに既にファイルが存在し、RSA秘密鍵として正しいフォーマットで
 * あればアクティベート済みと見做す。
 *
 * アクティベートに成功するとSMBサーバ用SSHアクセス用RSA秘密鍵が
 * レスポンスとして戻る。認証情報の送信と、レスポンスとして受信した
 * 秘密鍵を端末にファイルとし保存成功した事をもってアクティベート
 * 成功と見做す。
 *
 * @param[in,out] ssh_key_path 秘密鍵保存先パス
 * @retval  0 アクティベート済み、または正常にアクティベート終了
 * @retval -1 HWアドレスの取得に失敗
 * @retval -2 POST長が0
 * @retval -3 SSH鍵保存のためのディスクリプタ作成失敗
 * @retval -4 pemでないコンテンツがレスポンスとして返された
 * @retval -100 秘密鍵保存先パスの事前チェックに失敗
 */
int activate_abox(const char* ssh_key_path)
{
  char url[BUF_200];
  char response[FETCH_SIZE_SHORT];
  int api_result;
  int verified;
  FILE* fp;

  /* 既にSSH鍵がある場合、アクティベート済みと見做す */
  verified = validate_pem(ssh_key_path, ACTIVATION_VERIFY_FAILURES);
  switch (verified) {
    case 0:
      return 0;
    case -1:
      /* through */
    case -2:
      logger_write(LOG_ALERT,
        "  validate_pem() returns %d %s", verified, HERE);
      logger_write(LOG_ALERT,
        "  activation key does not exist, or is corrupted. try to fetch new one. %s", HERE);
      break;
    case -3:
      logger_write(LOG_ERR,
        "  validate_pem() returns %d %s", verified, HERE);
      logger_write(LOG_ERR,
        "  activation key exists, but fopen() failed. store failed counts and retry later. %s", HERE);
      return -100;
    default:
      logger_write(LOG_EMERG,
        "  validate_pem() returns %d %s", verified, HERE);
      logger_write(LOG_EMERG,
        "  activation key path is not regular file. %s", HERE);
      logger_write(LOG_EMERG,
        "  **THIS AGENTBOX IS CORRUPTED. PLEASE REPLACE NEW ONE**. %s", HERE);
      return -100;
  }

  /* レスポンス用の器を0で初期化 */
  memset(response, 0, sizeof(char) * FETCH_SIZE_SHORT);
  memset(url     , 0, sizeof(char) * BUF_200);

  /* curlの実行 */
  strcat(url, (verified == -2 ? AGENTBOX_CFG_API_REACTIVATE :
                                AGENTBOX_CFG_API_ACTIVATE));
  api_result = get_api_response(response,
    sizeof(char) * FETCH_SIZE_SHORT, url, "");
  /* 戻り値0でなければ失敗 */
  if (0 != api_result) {
    return -2;
  }
  /* SSH鍵を抽出してファイルに保存 */
  if (NULL != strstr(response, "BEGIN RSA")
  &&  NULL != strstr(response, "END RSA")) {
    if (NULL != (fp = fopen(ssh_key_path, "wb"))) {
      fputs(response, fp);
      fclose(fp);
      chmod(ssh_key_path, 0600);
      return 0;
    }
    return -3;
  }
  return -4;
}

/**
 * SMC-6Aを強制リアクティベートする
 *
 * アクティベートに成功するとSMBサーバ用SSHアクセス用RSA秘密鍵が
 * レスポンスとして戻る。認証情報の送信と、レスポンスとして受信した
 * 秘密鍵を端末にファイルとし保存成功した事をもってアクティベート
 * 成功と見做す。
 *
 * @param[in,out] ssh_key_path 秘密鍵保存先パス
 * @retval  0 アクティベート済み、または正常にアクティベート終了
 * @retval -1 HWアドレスの取得に失敗
 * @retval -2 POST長が0
 * @retval -3 SSH鍵保存のためのディスクリプタ作成失敗
 * @retval -4 pemでないコンテンツがレスポンスとして返された
 * @retval -100 秘密鍵保存先パスの事前チェックに失敗
 */
int force_reactivate_abox(const char* ssh_key_path)
{
  char url[BUF_200];
  char response[FETCH_SIZE_SHORT];
  int api_result;
  int verified;
  FILE* fp;

  /* レスポンス用の器を0で初期化 */
  memset(response, 0, sizeof(char) * FETCH_SIZE_SHORT);
  memset(url     , 0, sizeof(char) * BUF_200);

  /* curlの実行 */
  strcat(url, AGENTBOX_CFG_API_REACTIVATE);
  api_result = get_api_response(response,
    sizeof(char) * FETCH_SIZE_SHORT, url, "");
  /* 戻り値0でなければ失敗 */
  if (0 != api_result) {
    return -2;
  }
  /* SSH鍵を抽出してファイルに保存 */
  if (NULL != strstr(response, "BEGIN RSA")
  &&  NULL != strstr(response, "END RSA")) {
    if (NULL != (fp = fopen(ssh_key_path, "wb"))) {
      fputs(response, fp);
      fclose(fp);
      chmod(ssh_key_path, 0600);
      return 0;
    }
    return -3;
  }
  return -4;
}


/**
 * SMC-6Aをデアクティベートする
 *
 * @retval  0 正常終了
 * @retval -1 デアクティベートに失敗
 */
int deactivate_abox(void)
{
  char response[BUF_128];
  int api_result;

  memset(response, 0, sizeof(char) * BUF_128);
  api_result = get_api_response(response,
    sizeof(char) * BUF_128,
    AGENTBOX_CFG_API_DEACTIVATE, ""
  );
  if (0 == api_result) {
    //remove(shm_ptr->cacert);
    //remove(SSH_TUNNELLING_KEY);
    //remove(AGENTBOX_CFG_SHM_STORED_PATH);
  }
  // deactivate 処理成功していなくても、
  // 無条件にキーファイル類を削除するように修正
  remove(shm_ptr->cacert);
  remove(SSH_TUNNELLING_KEY);
  remove(AGENTBOX_CFG_SHM_STORED_PATH);
  remove(SSH_TUNNELLING_KEY_CERT_PUB);
  remove(SSH_TUNNELLING_KEY_PUB);
  logger_write(LOG_ALERT, "activatedkey removed:%s, %s", shm_ptr->cacert, HERE);
  logger_write(LOG_ALERT, "tunnelkey removed:%s, %s", SSH_TUNNELLING_KEY, HERE);
  logger_write(LOG_ALERT, "shm.ini removed:%s, %s", AGENTBOX_CFG_SHM_STORED_PATH, HERE);
  logger_write(LOG_ALERT, "tunnelkey removed:%s, %s", SSH_TUNNELLING_KEY_CERT_PUB, HERE);
  logger_write(LOG_ALERT, "tunnelkey removed:%s, %s", SSH_TUNNELLING_KEY_PUB, HERE);
  return (0 == api_result) ? 0 : -1;
}

/**
 * アクティベートキーを削除（リネーム）する
 * 管理サーバで拠点が変更された場合に、アクティベートキーを削除することで
 * （通常のルートで）再度アクティベートを実行する
 *
 * @param[int] ssh_key_path 秘密鍵保存先パス
 *
 * @return  結果
 * @retval  0 正常終了
 */
int remove_activatedkey(const char* ssh_key_path)
{
  struct stat st;
  char bk_ssh_key_path[BUF_128];

  memset(&st, 0, sizeof(st));
  if (0 == stat(ssh_key_path, &st)) {
    sprintf(bk_ssh_key_path, "%s.bk", ssh_key_path);
    rename(ssh_key_path, bk_ssh_key_path);
    logger_write(LOG_INFO, "activatedkey removed:%s:%s:%s", ssh_key_path, bk_ssh_key_path, HERE);
  }
  return 0;
}

/**
 * アクティベートされたAgentboxの認証情報を定期的に精査する
 *
 * アクティベートにより生成されたSMBサーバ用SSHアクセス用RSA秘密鍵が
 * パスに存在し、RSA秘密鍵として正しいフォーマットであれば、認証状態は
 * 正しいと見做す。
 * ファイルが存在しない、内容が空である等不正状態が検知された場合は再度
 * 秘密鍵を取得する
 *
 * @param[in,out] ssh_key_path 秘密鍵保存先パス
 */
void verify_activation(const char* ssh_key_path)
{
  char response[FETCH_SIZE_SHORT];
  int api_result;
  int verified;
  FILE* fp;

  /* 秘密鍵がある場合、正当性を確認する */
  verified = validate_pem(ssh_key_path, ACTIVATION_VERIFY_FAILURES);
  switch (verified) {
    case 0:
      logger_write(LOG_DEBUG,
        "  activation status is good. %s", HERE);
      return;
    case -1:
      /* through */
    case -2:
      logger_write(LOG_ALERT,
        "  validate_pem() returns %d %s", verified, HERE);
      logger_write(LOG_ALERT,
        "  activation status is BAD. try to fetch new one. %s", HERE);
      break;
    case -3:
      logger_write(LOG_ERR,
        "  validate_pem() returns %d %s", verified, HERE);
      logger_write(LOG_ERR,
        "  verify_activation() failed, store failed counts and retry later. %s", HERE);
      return;
    default:
      logger_write(LOG_EMERG,
        "  validate_pem() returns %d %s", verified, HERE);
      logger_write(LOG_EMERG,
        "  activation key path is not regular file. %s", HERE);
      logger_write(LOG_EMERG,
        "  **THIS AGENTBOX IS CORRUPTED. PLEASE REPLACE NEW ONE**. %s", HERE);
      return;
  }

  /* レスポンス用の器を0で初期化 */
  memset(response, 0, sizeof(char) * FETCH_SIZE_SHORT);
  /* curlの実行 */
  api_result = get_api_response(response,
    sizeof(char) * FETCH_SIZE_SHORT, AGENTBOX_CFG_API_ACTIVATE, "");
  /* 戻り値0でなければ失敗 */
  if (0 != api_result) {
    logger_write(LOG_ALERT,
      "  try to fetch new key was FAILED! %s", HERE);
    return;
  }
  /* SSH鍵を抽出してファイルに保存 */
  if (NULL != strstr(response, "BEGIN RSA")
  &&  NULL != strstr(response, "END RSA")) {
    if (NULL != (fp = fopen(ssh_key_path, "wb"))) {
      fputs(response, fp);
      fclose(fp);
      chmod(ssh_key_path, 0600);
      logger_write(LOG_NOTICE,
        "  NEW activation key was saved. %s", HERE);
    }
  }
  return;
}


/**
 * サーバのAPIパスに対してパラメタ送信し、取得したレスポンスをメモリにコピーする
 *
 * @param[in,out] dest レスポンスを書き込む領域へのポインタ
 * @param[in] buffer_len 書き込み最長サイズ
 * @param[in] api_url APIリクエストパス
 * @param[in] data query形式で記された送信パラメタ
 * @retval  0 正常終了
 * @retval -1 端末固有キー生成に失敗
 * @retval -2 curl実行に失敗
 * @retval -3 POST用メモリ確保(malloc)に失敗
 */
int get_api_response(char* dest, size_t buffer_len,
                     const char* api_url, const char* data)
{
  const char* grue = "&";
  const char* str_hwaddr;
  char* post_data;
  char hash[BUF_IDENT];
  char url[BUF_200];
  char header[BUF_32];
  char user_agent[256];
  struct curl_slist* resolver = NULL;
  struct curl_slist* http_header = NULL;
  CURL* curl;
  CURLcode res;
  MEMFILE* mf = NULL;
  size_t data_len;
  long post_len;
  long resp_code = 0;

  /* 端末固有キーの生成と取得 */
  if (0 != get_uniquekey(hash)) {
    return -1;
  }

  /* POSTデータ長取得 */
  if (NULL == data || (NULL != data && '\0' == *data)) {
    data_len = 0;
  } else {
    data_len = strlen(data);
  }

  /* POSTデータ用メモリ確保 */
  post_data = (char *) malloc(sizeof(char) * (data_len + 100));
  if (NULL == post_data) {
    return -3;
  }
  memset(post_data, 0, sizeof(char) * (data_len + 100));

  /* POSTデータがあれば本文に追加 */
  if (0 < data_len) {
    strcat(post_data, data);
    strcat(post_data, grue);
  }

  /* APIリクエスト用パラメタの付与 */
  strcat(post_data, "actas=Camera&uniquekey=");
  //strcat(post_data, "actas=Wondergate&uniquekey=");
  //strcat(post_data, "actas=Agentbox&uniquekey=");
  strcat(post_data, hash);
  memset(url, 0, sizeof(url));
  sprintf(url, "%s/%s", shm_ptr->server, api_url);
  post_len = (long) strlen(post_data);

  /* HWアドレス文字列の取得 */
  //str_hwaddr = strdup(get_hw_str());
  if (shm_ptr->uniquekey != NULL && shm_ptr->uniquekey[0] != '\0') {
    str_hwaddr = shm_ptr->uniquekey;
  }
  else {
    str_hwaddr = strdup(get_hw_str());
  }

  /* 拡張HTTPヘッダ */
  memset(header, 0, sizeof(char) * BUF_32);
  sprintf(header, "x-smc-aboxaddr: %s", str_hwaddr);
  http_header = curl_slist_append(http_header, header);

  /* resolv.confが壊れても管理サーバの名前解決を可能にする */
  resolver = curl_slist_append(NULL, "smc-cloud.jp:443:202.32.197.24");

  /* ユーザエージェント文字列 */
  sprintf(user_agent, FETCH_USER_AGENT,
    PROJ_VERSION_STRING, str_hwaddr, get_abox_version()
  );

  /* DEBUG Messages */
  logger_write(LOG_DEBUG, "  execute get_api_response():%s", HERE);
  logger_write(LOG_DEBUG, "    url  : %s, %s", url, HERE);
  logger_write(LOG_DEBUG, "    data : %s, %s", post_data, HERE);

  /* curlの実行 */
  curl = curl_easy_init();
  if (NULL == curl) {
    res = CURLE_FAILED_INIT;
  } else {
    mf = memfopen();
    if (NULL == mf) {
      logger_write(LOG_ERR, "  mf malloc failed %s", HERE);
      return -4;
    }
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, post_len);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYSTATUS, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_easy_setopt(curl, CURLOPT_RESOLVE, resolver);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, http_header);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, user_agent);
    //curl_easy_setopt(curl, CURLOPT_TIMEOUT, 15);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 5);
    curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 5);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, mf);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, memfwrite);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1); // for multi-thread
    res = curl_easy_perform(curl);
    if (res == CURLE_OK) {
      if (mf->size >= buffer_len) {
        strncpy(dest, mf->data, buffer_len);
        dest[buffer_len - 1] = '\0';
      } else {
        strncpy(dest, mf->data, mf->size);
        dest[mf->size] = '\0';
      }
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &resp_code);
    }
    memfclose(mf);
    curl_easy_cleanup(curl);
  }
  curl_slist_free_all(resolver);
  curl_slist_free_all(http_header);
  free(post_data);

  /* 戻り値0、レスポンスコード200でなければ失敗 */
  if (0 != res || 200 != resp_code) {
    if (resp_code != 0) {
      logger_write(LOG_ERR,
        "  get_api_response() finished with bad status.res:%d,[HTTPstatus=%d] %s",
        res, resp_code, HERE);
    }
    else {
      logger_write(LOG_WARNING,
        "  get_api_response() finished with bad status.res:%d,[HTTPstatus=%d] %s",
        res, resp_code, HERE);
    }
    return -5;
  }
  logger_write(LOG_DEBUG,
    "  get_api_response() successfully finished.[HTTPstatus=%d] %s",
    resp_code, HERE);
  return 0;
}

/**
 * サーバのAPIパスに対してパラメタ送信し、取得したレスポンスをメモリにコピーする
 * 戻り値としてレスポンスコードを返す
 *
 * @param[in,out] dest レスポンスを書き込む領域へのポインタ
 * @param[in] buffer_len 書き込み最長サイズ
 * @param[in] api_url APIリクエストパス
 * @param[in] data query形式で記された送信パラメタ
 * @retval >0 レスポンスコード
 * @retval  0 サーバ応答コードが受信されていない
 * @retval -1 端末固有キー生成に失敗
 * @retval -2 curl実行に失敗
 * @retval -3 POST用メモリ確保(malloc)に失敗
 */
int get_api_response_code(char* dest, size_t buffer_len,
                     const char* api_url, const char* data)
{
  const char* grue = "&";
  const char* str_hwaddr;
  char* post_data;
  char hash[BUF_IDENT];
  char url[BUF_200];
  char header[BUF_32];
  char user_agent[256];
  struct curl_slist* resolver = NULL;
  struct curl_slist* http_header = NULL;
  CURL* curl;
  CURLcode res;
  MEMFILE* mf = NULL;
  size_t data_len;
  long post_len;
  long resp_code = 0;

  /* 端末固有キーの生成と取得 */
  if (0 != get_uniquekey(hash)) {
    return -1;
  }

  /* POSTデータ長取得 */
  if (NULL == data || (NULL != data && '\0' == *data)) {
    data_len = 0;
  } else {
    data_len = strlen(data);
  }

  /* POSTデータ用メモリ確保 */
  post_data = (char *) malloc(sizeof(char) * (data_len + 100));
  if (NULL == post_data) {
    return -3;
  }
  memset(post_data, 0, sizeof(char) * (data_len + 100));

  /* POSTデータがあれば本文に追加 */
  if (0 < data_len) {
    strcat(post_data, data);
    strcat(post_data, grue);
  }

  /* APIリクエスト用パラメタの付与 */
  strcat(post_data, "actas=Camera&uniquekey=");
  //strcat(post_data, "actas=Wondergate&uniquekey=");
  //strcat(post_data, "actas=Agentbox&uniquekey=");
  strcat(post_data, hash);
  memset(url, 0, sizeof(url));
  sprintf(url, "%s/%s", shm_ptr->server, api_url);
  post_len = (long) strlen(post_data);

  /* HWアドレス文字列の取得 */
  //str_hwaddr = strdup(get_hw_str());
  if (shm_ptr->uniquekey != NULL && shm_ptr->uniquekey[0] != '\0') {
    str_hwaddr = shm_ptr->uniquekey;
  }
  else {
    str_hwaddr = strdup(get_hw_str());
  }

  /* 拡張HTTPヘッダ */
  memset(header, 0, sizeof(char) * BUF_32);
  sprintf(header, "x-smc-aboxaddr: %s", str_hwaddr);
  http_header = curl_slist_append(http_header, header);

  /* resolv.confが壊れても管理サーバの名前解決を可能にする */
  resolver = curl_slist_append(NULL, "smc-cloud.jp:443:202.32.197.24");

  /* ユーザエージェント文字列 */
  sprintf(user_agent, FETCH_USER_AGENT,
    PROJ_VERSION_STRING, str_hwaddr, get_abox_version()
  );

  /* DEBUG Messages */
  logger_write(LOG_DEBUG, "  execute get_api_response_code():%s", HERE);
  logger_write(LOG_DEBUG, "    url  : %s, %s", url, HERE);
  logger_write(LOG_DEBUG, "    data : %s, %s", post_data, HERE);

  /* curlの実行 */
  curl = curl_easy_init();
  if (NULL == curl) {
    res = CURLE_FAILED_INIT;
  } else {
    mf = memfopen();
    if (NULL == mf) {
      logger_write(LOG_ERR, "  mf malloc failed %s", HERE);
      return -4;
    }
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, post_len);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYSTATUS, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_easy_setopt(curl, CURLOPT_RESOLVE, resolver);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, http_header);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, user_agent);
    //curl_easy_setopt(curl, CURLOPT_TIMEOUT, 15);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 5);
    curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 5);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, mf);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, memfwrite);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1); // for multi-thread
    res = curl_easy_perform(curl);
    if (res == CURLE_OK) {
      if (mf->size >= buffer_len) {
        strncpy(dest, mf->data, buffer_len);
        dest[buffer_len - 1] = '\0';
      } else {
        strncpy(dest, mf->data, mf->size);
        dest[mf->size] = '\0';
      }
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &resp_code);
    }
    memfclose(mf);
    curl_easy_cleanup(curl);
  }
  curl_slist_free_all(resolver);
  curl_slist_free_all(http_header);
  free(post_data);

//  /* 戻り値0、レスポンスコード200でなければ失敗 */
//  if (0 != res || 200 != resp_code) {
  /* get_api_responseと違い、レスポンスコードを呼出側でチェックすること */
  /* 戻り値0でなければ失敗 */
  if (0 != res) {
    if (resp_code != 200 && resp_code != 0) {
      logger_write(LOG_ERR,
        "  get_api_response_code() finished with bad status.res:%d,[HTTPstatus=%d] %s",
        res,resp_code, HERE);
    }
    else {
      logger_write(LOG_WARNING,
        "  get_api_response_code() finished with bad status.res:%d,[HTTPstatus=%d] %s",
        res,resp_code, HERE);
    }
    return -5;
  }
  logger_write(LOG_DEBUG,
    "  get_api_response_code() successfully finished.[HTTPstatus=%d] %s",
    resp_code, HERE);
  return resp_code;
}

/**
 * サーバのAPIパスに対してパラメタ送信し、取得したレスポンスをメモリにコピーする
 *
 * @param[in,out] dest レスポンスを書き込む領域へのポインタ
 * @param[in] buffer_len 書き込み最長サイズ
 * @param[in] api_url APIリクエストパス
 * @param[in] data query形式で記された送信パラメタ
 * @param[in] otherurl other server top url
 * @retval  0 正常終了
 * @retval -1 端末固有キー生成に失敗
 * @retval -2 curl実行に失敗
 * @retval -3 POST用メモリ確保(malloc)に失敗
 */
int get_api_response_otherserver(char* dest, size_t buffer_len,
                     const char* api_url, const char* data, const char* otherurl)
{
  const char* grue = "&";
  const char* str_hwaddr;
  char* post_data;
  char hash[BUF_IDENT];
  char url[BUF_200];
  char header[BUF_64];
  char user_agent[256];
  struct curl_slist* resolver = NULL;
  struct curl_slist* http_header = NULL;
  CURL* curl;
  CURLcode res;
  MEMFILE* mf = NULL;
  size_t data_len;
  long post_len;
  long resp_code = 0;
  char *urlencode_data = (char*)NULL;

  /* 端末固有キーの生成と取得 */
  if (0 != get_uniquekey(hash)) {
    return -1;
  }

  /* other url check */
  if (otherurl == NULL || (otherurl != NULL && *otherurl == '\0')) {
    return -2;
  }

  /* POSTデータ長取得 */
  if (NULL == data || (NULL != data && '\0' == *data)) {
    data_len = 0;
  } else {
    data_len = strlen(data);
  }

  /* POSTデータ用メモリ確保 */
  post_data = (char *) malloc(sizeof(char) * (data_len + 100));
  if (NULL == post_data) {
    return -3;
  }
  memset(post_data, 0, sizeof(char) * (data_len + 100));

  /* POSTデータがあれば本文に追加 */
  if (0 < data_len) {
    strcat(post_data, data);
    strcat(post_data, grue);
  }

  /* APIリクエスト用パラメタの付与 */
  strcat(post_data, "key=");
  strcat(post_data, hash);
  memset(url, 0, sizeof(url));
  sprintf(url, "%s/%s", otherurl, api_url);
  post_len = (long) strlen(post_data);

  /* HWアドレス文字列の取得 */
  //str_hwaddr = strdup(get_hw_str());
  if (shm_ptr->uniquekey != NULL && shm_ptr->uniquekey[0] != '\0') {
    str_hwaddr = shm_ptr->uniquekey;
  }
  else {
    str_hwaddr = strdup(get_hw_str());
  }

  /* 拡張HTTPヘッダ */
  memset(header, 0, sizeof(char) * BUF_64);
  sprintf(header, "x-smc-aboxaddr: %s", str_hwaddr);
  http_header = curl_slist_append(http_header, header);

  /* resolv.confが壊れても管理サーバの名前解決を可能にする */
  resolver = curl_slist_append(NULL, "smc-cloud.jp:443:202.32.197.24");

  /* ユーザエージェント文字列 */
  sprintf(user_agent, FETCH_USER_AGENT,
    PROJ_VERSION_STRING, str_hwaddr, get_abox_version()
  );

  /* DEBUG Messages */
  logger_write(LOG_DEBUG, "  execute get_api_response_otherserver():%s", HERE);
  logger_write(LOG_DEBUG, "    url    : %s, %s", url, HERE);
  logger_write(LOG_DEBUG, "    data   : %s, %s", post_data, HERE);

  /* curlの実行 */
  curl = curl_easy_init();
  if (NULL == curl) {
    res = CURLE_FAILED_INIT;
  } else {
    mf = memfopen();
    if (NULL == mf) {
      logger_write(LOG_ERR, "  mf malloc failed %s", HERE);
      return -4;
    }
    curl_easy_setopt(curl, CURLOPT_POST, 1);
    curl_easy_setopt(curl, CURLOPT_URL, url);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS, post_data);
    curl_easy_setopt(curl, CURLOPT_POSTFIELDSIZE, post_len);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYSTATUS, 0);
    curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
    curl_easy_setopt(curl, CURLOPT_RESOLVE, resolver);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, http_header);
    curl_easy_setopt(curl, CURLOPT_USERAGENT, user_agent);
    //curl_easy_setopt(curl, CURLOPT_TIMEOUT, 15);
    curl_easy_setopt(curl, CURLOPT_TIMEOUT, 5);
    curl_easy_setopt(curl, CURLOPT_DNS_CACHE_TIMEOUT, 5);
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, mf);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, memfwrite);
    curl_easy_setopt(curl, CURLOPT_NOSIGNAL, 1); // for multi-thread
    res = curl_easy_perform(curl);
    if (res == CURLE_OK) {
      if (mf->size >= buffer_len) {
        strncpy(dest, mf->data, buffer_len);
        dest[buffer_len - 1] = '\0';
      } else {
        strncpy(dest, mf->data, mf->size);
        dest[mf->size] = '\0';
      }
      curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &resp_code);
    }
    memfclose(mf);
    curl_easy_cleanup(curl);
  }
  curl_slist_free_all(resolver);
  curl_slist_free_all(http_header);
  free(post_data);

  /* 戻り値0、レスポンスコード200でなければ失敗 */
  if (0 != res || 200 != resp_code) {
    if (resp_code != 0) {
      logger_write(LOG_ERR,
        "  get_api_response_otherserver() finished with bad status.res:%d,[HTTPstatus=%d] %s",
        res, resp_code, HERE);
    }
    else {
      logger_write(LOG_WARNING,
        "  get_api_response_otherserver() finished with bad status.res:%d,[HTTPstatus=%d] %s",
        res, resp_code, HERE);
    }
    return -5;
  }
  logger_write(LOG_DEBUG,
    "  get_api_response_otherserver() successfully finished.[HTTPstatus=%d] %s",
    resp_code, HERE);
  return 0;
}
