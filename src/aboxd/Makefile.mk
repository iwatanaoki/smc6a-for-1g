### aboxd ######################################################

ABOXD_SRCDIR=$(ABSDIR)/aboxd
ifeq "$(strip $(ABOXD_SRCDIR))" ""
  ABOXD_SRCDIR=.
endif
ABOXD_SRCS=$(notdir $(wildcard $(ABOXD_SRCDIR)/*.c))
ABOXD_OBJS=$(notdir $(ABOXD_SRCS:.c=.o))
ABOXD_TGTS=aboxd
ABOXD_LIBS=$(LIBS) -lpcre -lm

################################################################

$(ABOXD_OBJS):
	cd $(ABOXD_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c $(@F:.o=.c) -o $@

$(ABOXD_TGTS): $(ABOXD_OBJS)
	cd $(ABOXD_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LINKER_FLAGS) $(LDFLAGS) $(ABOXD_OBJS) -o $@ $(ABOXD_LIBS)
ifeq ($(BUILD_TYPE),Release)
	cd $(ABOXD_SRCDIR);
	#$(STRIP) $@
endif

aboxd-install: $(ABOXD_TGTS) install-dir
	cp $(ABOXD_SRCDIR)/$(ABOXD_TGTS) $(INSTALL_DIR)$(DISTDIR)/usr/sbin/$(ABOXD_TGTS)

aboxd-uninstall: $(ABOXD_TGTS)
	$(RM) $(INSTALL_DIR)$(DISTDIR)/usr/sbin/$(ABOXD_TGTS)

aboxd-clean:
	cd $(ABOXD_SRCDIR); \
	$(RM) $(ABOXD_TGTS) *.o

aboxd-dry:
	@cd $(ABOXD_SRCDIR); \
	echo "ABOXD_SRCS='$(ABOXD_SRCS)'"; \
	echo "ABOXD_OBJS='$(ABOXD_OBJS)'"; \
	echo "ABOXD_TGTS='$(ABOXD_TGTS)'"
