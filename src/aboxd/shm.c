/**
 * @file    shm.c
 * @brief   共有メモリ管理
 * @author  cool-revo inc.
 */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <limits.h>
#include <errno.h>
#include <sys/shm.h>
#include "shm.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;

/** 共有メモリIDを格納する。実体はaboxd.c */
extern int shm_id;

/** 共有メモリIDをファイルに出力する。実体はaboxd.c */
extern const char* shm_file;


/**
 * 共有メモリをアタッチする
 *
 * @retval  0 正常終了
 * @retval -1 共有メモリ初期化、shmid生成 shmget() に失敗
 * @retval -2 shmidを使って領域へのアタッチ shmat() に失敗
 * @retval -3 shmidのファイルへの書き出しに失敗
 */
int create_shm(void)
{
  FILE* shm_fp;

  /* 共有メモリのアタッチ */
  shm_id = shmget(IPC_PRIVATE, (sizeof(ABOX_SHM) * 2), (0666 | IPC_CREAT));
  if(shm_id == -1) {
    return -1;
  }
  shm_ptr = (ABOX_SHM *) shmat(shm_id, NULL, 0);
  if (shm_ptr == (void *) - 1) {
    return -2;
  }
  /* 共有メモリIDをファイルに出力する */
  shm_fp = fopen(shm_file, "w");
  if (NULL == shm_fp) {
    return -3;
  }
  fprintf(shm_fp, "%d\n", shm_id);
  fclose(shm_fp);
  zerowrite_shm();
  return 0;
}


/**
 * 共有メモリをデタッチする
 *
 * @retval  0 正常終了
 * @retval -1 共有メモリ領域からのデタッチ shmdt() に失敗
 * @retval -2 ID:shmidを持つ共有メモリ領域消去 shmctl() に失敗
 * @retval -3 書き出し済のshmidファイルの消去に失敗
 */
int dispose_shm(void)
{
  /* 共有メモリのデタッチ */
  if (shmdt(shm_ptr) == -1) {
    return -1;
  }
  if (shmctl(shm_id, IPC_RMID, 0) == -1) {
    return -2;
  }
  /* 共有メモリIDファイルを消去する */
  if (-1 == remove(shm_file)) {
    return -3;
  }
  return 0;
}


/**
 * 共有メモリ構造体に0を書き込む
 *
 * @return int 正常終了時 0
 */
int zerowrite_shm(void)
{
  int i;

  /* abox shm */
  shm_ptr->id             = 0;
  shm_ptr->member_id      = 0;
  shm_ptr->group_id       = 0;
  shm_ptr->user_id        = 0;
  shm_ptr->auth_uniquekey[0]   = '\0';
  shm_ptr->uniquekey[0]   = '\0';
  shm_ptr->name[0]        = '\0';
  shm_ptr->hwaddr[0]      = '\0';
  shm_ptr->disabled       = 0;
  strncpy(shm_ptr->login, "user", 4);
  strncpy(shm_ptr->passwd, "smc3aplus", 8);
  shm_ptr->ntpdate        = 0;
  shm_ptr->ntpfqdn[0]     = '\0';
  shm_ptr->layer          = 0;
  shm_ptr->group_name[0]  = '\0';
  shm_ptr->grouplogin[0]  = '\0';
  shm_ptr->grouppasswd[0] = '\0';
  shm_ptr->cloudaddr[0]   = '\0';
  shm_ptr->clouddir[0]    = '\0';
  shm_ptr->cloudlogin[0]  = '\0';
  shm_ptr->cloudpasswd[0] = '\0';
  shm_ptr->root_name[0]   = '\0';
  shm_ptr->parent_name[0] = '\0';
  shm_ptr->loglevel       = 7; /* 7:LOG_DEBUG */
  shm_ptr->remotehelp     = 0;
  shm_ptr->poll_status    = false; // 接続前は false をセット
  shm_ptr->activ_status   = false; // 接続前は false をセット
  shm_ptr->addr.ipaddr[0]  = '\0';
  shm_ptr->addr.netmask[0] = '\0';
  shm_ptr->addr.broadcast[0] = '\0';
  shm_ptr->addr.gateway[0] = '\0';
  shm_ptr->addr.dns1[0]    = '\0';
  shm_ptr->addr.dns2[0]    = '\0';

  shm_ptr->agentbox_id = 0;
  shm_ptr->wondergate_id = 0;
  shm_ptr->hwaddr[0]   = '\0';
  shm_ptr->bitrate     = 0;
  shm_ptr->preserve    = 0;
  shm_ptr->type        = 0;
  shm_ptr->nocharged   = 0;
  shm_ptr->discover    = 0;
  shm_ptr->status      = 0;
  shm_ptr->surveil     = 0;
  shm_ptr->promotion   = 0;
  shm_ptr->recording   = 0;
  shm_ptr->placed[0]   = '\0';
  shm_ptr->live        = 0;
  shm_ptr->port        = 0;
  shm_ptr->motion      = 0;
  shm_ptr->ptz         = 0;
  shm_ptr->live_url[0] = '\0';
  shm_ptr->ptz_tu[0]   = '\0';
  shm_ptr->ptz_td[0]   = '\0';
  shm_ptr->ptz_pl[0]   = '\0';
  shm_ptr->ptz_pr[0]   = '\0';
  shm_ptr->ptz_zi[0]   = '\0';
  shm_ptr->ptz_zo[0]   = '\0';
  shm_ptr->extension[0]    = '\0';
  shm_ptr->search_path[0]  = '\0';

  shm_ptr->ovf_type        = 0;
  shm_ptr->ovf_uri[0]      = '\0';

  /* runtime params */
  shm_ptr->interval   = 0;
  shm_ptr->pidfile[0] = '\0';
  shm_ptr->shmfile[0] = '\0';
  shm_ptr->logfile[0] = '\0';
  shm_ptr->cacert[0]  = '\0';
  shm_ptr->server[0]  = '\0';
  shm_ptr->smbshare[0]  = '\0';
  shm_ptr->csurl[0]     = '\0';
  shm_ptr->tunnelurl[0]     = '\0';
  shm_ptr->sshtunnel[0].server_vip[0]     = '\0';
  shm_ptr->sshtunnel[0].server_port = 0;
  shm_ptr->sshtunnel[1].server_vip[0]     = '\0';
  shm_ptr->sshtunnel[1].server_port = 0;
  shm_ptr->runtime_addr.dhcp       = 1;
  shm_ptr->runtime_addr.ipaddr[0]  = '\0';
  shm_ptr->runtime_addr.netmask[0] = '\0';
  shm_ptr->runtime_addr.broadcast[0] = '\0';
  shm_ptr->runtime_addr.gateway[0] = '\0';
  shm_ptr->runtime_addr.dns1[0]    = '\0';
  shm_ptr->runtime_addr.dns2[0]    = '\0';
  /* cache servers */
  for (i = 0; i < (sizeof(shm_ptr->csserver) / sizeof(csserver_t)); ++i) {
    shm_ptr->csserver[i].host[0] = '\0';
    shm_ptr->csserver[i].prefix[0] = '\0';
  }

  return 0;
}


/**
 * \0デリミタを追加した文字列をコピーする
 *
 * @param[in,out] dest 文字列をコピーするポインタ
 * @param[in] src コピーする文字列
 * @param[in] size コピーする文字数
 */
void shm_strval(char* dest, const char* src, size_t size)
{
  if (NULL == src || NULL == dest) {
    return;
  }
  if ('\0' == *src || 0 <= size) {
    dest[0] = '\0';
  } else {
    strncpy(dest, src, size);
    dest[size - 1] = '\0';
  }
}


/**
 * 文字列をintの範囲内に納まる数値に変換して返す
 *
 * 変換後の値がintの範囲内に納まらない場合はINT_MAX、INT_MINに
 * キャップして返す
 *
 * @param[in] src 変換する文字列
 * @return 変換された文字列
 */
int shm_intval(const char* src)
{
  int conv;
  long sl;
  char* endp;

  if (NULL == src || (NULL != src && '\0' == *src)) {
    return 0;
  }
  errno = 0;
  sl = strtol(src, &endp, 10);

  if (endp == src) {
    conv = 0;
  } else if ('\0' != *endp) {
    conv = 0;
  } else if (LONG_MIN == sl && ERANGE == errno) {
    conv = INT_MIN;
  } else if (LONG_MAX == sl && ERANGE == errno) {
    conv = INT_MAX;
  } else if (sl > INT_MAX) {
    conv = INT_MAX;
  } else if (sl < INT_MIN) {
    conv = INT_MIN;
  } else {
    conv = (int) sl;
  }

  return conv;
}


/**
 * 文字列を符号付きintの範囲内に納まる数値に変換して返す
 *
 * 変換後の値が符号付きintの範囲内に納まらない場合は0、あるいは
 * UINT_MAXにキャップして返す
 *
 * @param[in] src 変換する文字列
 * @return 変換された文字列
 */
unsigned int shm_uintval(const char* src)
{
  unsigned int conv;
  unsigned long sl;
  char* endp;

  if (NULL == src || (NULL != src && '\0' == *src)) {
    return 0;
  }
  errno = 0;
  sl = strtoul(src, &endp, 10);

  if (endp == src) {
    conv = 0;
  } else if ('\0' != *endp) {
    conv = 0;
  } else if (ULONG_MAX == sl && ERANGE == errno) {
    conv = UINT_MAX;
  } else if (sl > UINT_MAX) {
    conv = UINT_MAX;
  } else if (sl < 0) {
    conv = 0;
  } else {
    conv = (unsigned int) sl;
  }

  return conv;
}
