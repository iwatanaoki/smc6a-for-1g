/**
 * @file    ipc.c
 * @brief   プロセス間メッセージング管理
 * @author  cool-revo inc.
 * @version 0.0.1
 */
#include <sys/shm.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>
#include "abox_config.h"
#include "shm.h"
#include "logger.h"
#include "auth.h"
#include "ntp.h"
#include "util.h"
#include "process.h"
#include "ipaddr.h"
#include "sshtunnel.h"
#include "reboot.h"
#include "ipc.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;

/** メッセージキューを作成する共通キー。実体はaboxd.c */
extern int msq_id;


/**
 * System V IPCメッセージキューを開く
 *
 * ここでキューのオープンに失敗した場合、Agentbox内で行われる
 * すべてのIPCメッセージングは失敗する
 *
 * @retval  0 正常終了
 * @retval -1 msqidの取得に失敗
 */
int msqueue_open(void)
{
  if (-1 == (msq_id = msgget(ABOX_MSGQ, (0666 | IPC_CREAT)))) {
    perror("msgget");
    return -1;
  }
  return 0;
}


/**
 * System V IPCメッセージキューを閉じる
 *
 * msqueue_open()によって開かれたキューを閉じる。
 *
 * @retval  0 正常終了
 * @retval -1 msqidの取得に失敗
 */
int msqueue_close(void)
{
  if (-1 == (msgctl(msq_id, IPC_RMID, NULL))) {
    perror("msgctl");
    return -1;
  }
  return 0;
}


/**
 * System V IPCメッセージを送信する
 *
 * @param[in] message sysvメッセージ本文のテキスト
 * @retval  0 正常終了
 * @retval -1 メッセージの送信に失敗
 */
int msqueue_send(const char* message)
{
  msq_t ipcmsg;

  ipcmsg.type = ABOX_QTYPE_MSG;
  sprintf(ipcmsg.msg, "%s", message);

  if (-1 == msgsnd(msq_id, &ipcmsg, ABOX_MSGQ_BUF, 0)) {
    perror("msqueue_send");
    return -1;
  }
  return 0;
}


/**
 * System V IPCメッセージキューから特定KEYを持つメッセージを開き、
 * コマンドを実行する
 *
 * @return 0 正常終了
 */
int msqueue_read(void)
{
  msq_t ipcmsg;
  const char* delim = ",";
  //char* rawmsg = NULL;
  char rawmsg[BUF_4096];
  char* tmp_rawmsg = NULL;
  char* ipc_argv[10];
  int ipc_argc = 0;
  int i = 0;
  ipaddr_t* addr;
  int l_kind = 2; // 1: web access via ssh tunnel, 2: ssh shell access.

 /*
  logger_write(LOG_DEBUG,
    " checking exists status of IPC message before malloc. %s", HERE);

  rawmsg = (char *) malloc(sizeof(ipcmsg.msg));
  if (NULL == rawmsg) {
    logger_write(LOG_ERR, "rawmsg malloc failed. %s", HERE);
    return -1;
  }

  logger_write(LOG_DEBUG,
    " checking exists status of IPC message after malloc. %s", HERE);
 */

  logger_write(LOG_DEBUG,
    " checking exists status of IPC message. %s", HERE);

  while (-1 != (msgrcv(msq_id, &ipcmsg, ABOX_MSGQ_BUF, ABOX_QTYPE_MSG, (0 | IPC_NOWAIT)))) {
    logger_write(LOG_DEBUG,
      "Received IPC Messaging loop start. %s", HERE);
    //memset(rawmsg, 0, sizeof(ipcmsg.msg));
    memset(rawmsg, 0, sizeof(rawmsg));
    for(i=0; i<10; i++) {
      ipc_argv[i] = (char*)NULL;
    }
    strncpy(rawmsg, ipcmsg.msg, sizeof(ipcmsg.msg));
    tmp_rawmsg = (char*)rawmsg;
    for (ipc_argc = 0; ipc_argc < 10; ipc_argc++) {
      if (NULL == (ipc_argv[ipc_argc] = strtok(tmp_rawmsg, delim))) {
        break;
      }
      tmp_rawmsg = NULL;
    }

    if (ipc_argv[0] == (char*)NULL) {
      logger_write(LOG_DEBUG,
        "Received IPC Messaging is null %s", HERE);
      break;
    }
    else {
      logger_write(LOG_DEBUG,
        "Received IPC Messaging = %s %s", ipc_argv[0], HERE);
    }

    if (ipc_argv[0] != (char*)NULL && ipc_argv[0][0] == '\0') {
      logger_write(LOG_WARNING,
        "Received IPC Messaging, but ipc_argv[0][0] is '\\0', check loop break. %s", HERE);
      break;
    }

    if (0 == strcmp(ipc_argv[0], "ifcfg")) {
      /* IPアドレスの設定 */
      addr = (ipaddr_t *) malloc(sizeof(ipaddr_t));
      if (NULL == addr) {
        logger_write(LOG_ERR,
          "addr malloc failed, Received IPC Messaging = %s %s", ipc_argv[0], HERE);
        break;
      }
      strcpy_safe(addr->ipaddr,  ipc_argv[1]);
      strcpy_safe(addr->netmask, ipc_argv[2]);
      strcpy_safe(addr->gateway, ipc_argv[3]);
      strcpy_safe(addr->dns1,    ipc_argv[4]);
      strcpy_safe(addr->dns2,    ipc_argv[5]);
      set_abox_ipaddr(addr);
      free(addr);

    } else if (0 == strcmp(ipc_argv[0], "ifyxcfg")) {
      /* IPアドレスの設定 */
      addr = (ipaddr_t *) malloc(sizeof(ipaddr_t));
      if (NULL == addr) {
        logger_write(LOG_ERR,
          "addr malloc failed, Received IPC Messaging = %s %s", ipc_argv[0], HERE);
        break;
      }
      strcpy_safe(addr->ipaddr,    ipc_argv[1]);
      strcpy_safe(addr->netmask,   ipc_argv[2]);
      strcpy_safe(addr->gateway,   ipc_argv[3]);
      strcpy_safe(addr->dns1,      ipc_argv[4]);
      strcpy_safe(addr->dns2,      ipc_argv[5]);
      save_abox_ipaddr(addr);
      free(addr);

    } else if (0 == strcmp(ipc_argv[0], "reboot")) {
      /* 再起動 */
      print_shm(AGENTBOX_CFG_SHM_STORED_PATH);
      safe_reboot();

    } else if (0 == strcmp(ipc_argv[0], "fastreboot")) {
      /* 高速再起動 */
      print_shm(AGENTBOX_CFG_SHM_STORED_PATH);
      run_system(AGENTBOX_CFG_CMD_FAST_REBOOT);

    } else if (0 == strcmp(ipc_argv[0], "sshtunnel")) {
      /* SSHトンネルの維持 */
      l_kind = 2;
      if (0 == store_ssh_tunnels(l_kind)) {
        open_needed_tunnels(AGENTBOX_CFG_TUNNEL_SHARROWTEST);
        kill_unneeded_tunnels(0);
      }

    } else if (0 == strcmp(ipc_argv[0], "ntp")) {
      /* 時刻設定のNTP参照・HTTP参照を切り替える */
      if (0 == set_ntpserver(atoi(ipc_argv[1]), ipc_argv[2])) {
        run_timesync();
      }

    } else if (0 == strcmp(ipc_argv[0], "deactivate")) {
      /* エージェントボックスをデアクティベートする */
      if (0 == delete_tunnelkey()) {
        killall_tunnels();
      }
      if (0 == deactivate_abox()) {
        shm_ptr->disabled = 1;
        run_system("/mtd/coolrevo/usr/sbin/factorydefault.sh");
      }

    } else if (0 == strcmp(ipc_argv[0], "erase")) {
      /* エージェントボックスのデータを消去する */
      killall_tunnels();
      shm_ptr->disabled = 1;
      run_system("/mtd/coolrevo/usr/sbin/factorydefault.sh");

    } else {
      logger_write(LOG_WARNING,
        "Received Invalid IPC Messaging. This Message will be ignored. %s", HERE);
    }
    /* 10ms wait 無限ループ発生時 CPU暴走対策 */
    usleep(10 * 1000);  
  }
  //free(rawmsg);

  logger_write(LOG_DEBUG,
    " checking exists status of IPC message loop end. %s", HERE);

  return 0;
}
