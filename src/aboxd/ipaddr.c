#ifndef AGENTBOX_IPADDR_C
#define AGENTBOX_IPADDR_C
/**
 * @file    ipaddr.c
 * @brief   IPアドレス管理
 * @author  cool-revo inc.
 */
#include <net/route.h>
#include <sys/ioctl.h>
#include <arpa/inet.h>
#include <ifaddrs.h>
#include <net/if.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <pcre.h>
#include "abox_macro.h"
#include "shm.h"
#include "logger.h"
#include "util.h"
#include "ipaddr_p.h"
#include "ipaddr.h"
#include "auth.h"

/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;


/**
 * SMC-3A Plus のIPアドレスをCGIから取得し、共有メモリに書き込む
 *
 * @retval  0 正常終了
 * @retval -3 メモリのアロケートに失敗
 */
int get_smc3a_cgi_ipaddr(void)
{
  int ret = -3;
  ipaddr_t* addr;
  char l_cmdbuf[512];
  char l_buf[512];
  FILE *l_pp = (FILE*)NULL;
  char *l_cp = (char*)NULL;

  addr = (ipaddr_t *) malloc(sizeof(ipaddr_t));
  if (NULL == addr) {
    logger_write(LOG_ERR, "get_smc3a_cgi_ipaddr():addr malloc failed. %s", HERE);
    return -1;
  }
  memset(addr->ipaddr,    0, sizeof(char) * BUF_IPADDR);
  memset(addr->netmask,   0, sizeof(char) * BUF_IPADDR);
  memset(addr->broadcast, 0, sizeof(char) * BUF_IPADDR);
  memset(addr->gateway,   0, sizeof(char) * BUF_IPADDR);
  memset(addr->dns1,      0, sizeof(char) * BUF_IPADDR);
  memset(addr->dns2,      0, sizeof(char) * BUF_IPADDR);

   
  memset(l_cmdbuf, 0, sizeof(l_cmdbuf));
  sprintf(l_cmdbuf, "wget -q -O - http://127.0.0.1/GetNetwork.cgi | sed 's/<br>/\\n/g'");
  logger_write(LOG_DEBUG, "cmbbuf:%s, %s", l_cmdbuf, HERE);
  ret = -2;
  l_pp = popen(l_cmdbuf, "r");
  if (l_pp != (FILE*)NULL) {
    while(NULL != (fgets(l_buf, sizeof(l_buf), l_pp))) {
      if ((l_cp = strchr(l_buf, '\n')) != (char*)NULL) {
        *l_cp = '\0';
      }
      logger_write(LOG_DEBUG, "buf:%s, %s", l_buf, HERE);
      if (strstr(l_buf, "Proto=") != (char*)NULL) {
        shm_ptr->runtime_addr.dhcp = 1;
        if (*(l_buf+6) != '\0') {
          shm_ptr->runtime_addr.dhcp = atoi(l_buf+6);
        }
        ret = 0;
      }
      else if (strstr(l_buf, "Ipaddr=") != (char*)NULL) {
        if (*(l_buf+7) != '\0') {
          strcpy_safe(shm_ptr->runtime_addr.ipaddr   , l_buf+7);
        }
        else {
          shm_ptr->runtime_addr.ipaddr[0] = '\0';
        }
        ret = 0;
      }
      else if (strstr(l_buf, "Netmask=") != (char*)NULL) {
        if (*(l_buf+8) != '\0') {
          strcpy_safe(shm_ptr->runtime_addr.netmask   , l_buf+8);
        }
        else {
          shm_ptr->runtime_addr.netmask[0] = '\0';
        }
        ret = 0;
      }
      else if (strstr(l_buf, "Gateway=") != (char*)NULL) {
        if (*(l_buf+8) != '\0') {
          strcpy_safe(shm_ptr->runtime_addr.gateway   , l_buf+8);
        }
        else {
          shm_ptr->runtime_addr.gateway[0] = '\0';
        }
        ret = 0;
      }
      else if (strstr(l_buf, "Dns=") != (char*)NULL) {
        if (*(l_buf+4) != '\0') {
          strcpy_safe(shm_ptr->runtime_addr.dns1   , l_buf+4);
        }
        else {
          shm_ptr->runtime_addr.dns1[0] = '\0';
        }
        ret = 0;
      }
      else if (strstr(l_buf, "Dns1=") != (char*)NULL) {
        if (*(l_buf+5) != '\0') {
          strcpy_safe(shm_ptr->runtime_addr.dns2   , l_buf+5);
        }
        else {
          shm_ptr->runtime_addr.dns2[0] = '\0';
        }
        ret = 0;
      }
      memset(l_buf, 0, sizeof(l_buf));
    }
    pclose(l_pp);
  }

  logger_write(LOG_NOTICE, "get_smc3a_cgi_ipaddr(): %s", HERE);
  logger_write(LOG_NOTICE, "  dhcp   : %d", shm_ptr->runtime_addr.dhcp);
  logger_write(LOG_NOTICE, "  ipaddr : %s", shm_ptr->runtime_addr.ipaddr);
  logger_write(LOG_NOTICE, "  netmask: %s", shm_ptr->runtime_addr.netmask);
  logger_write(LOG_NOTICE, "  broad  : %s", shm_ptr->runtime_addr.broadcast);
  logger_write(LOG_NOTICE, "  gateway: %s", shm_ptr->runtime_addr.gateway);
  logger_write(LOG_NOTICE, "  dns1   : %s", shm_ptr->runtime_addr.dns1);
  logger_write(LOG_NOTICE, "  dns2   : %s", shm_ptr->runtime_addr.dns2);
  logger_write(LOG_NOTICE, "status   : %d", ret);

  return ret;
}

/**
 * SMC-3A Plus のIPアドレスをCGIにより、shm_ptr->addr の情報にてアドレスを設定する
 *
 * @retval  0 正常終了
 * @retval -3 メモリのアロケートに失敗
 */
int set_smc3a_cgi_ipaddr(void)
{
  int ret = -1;
  ipaddr_t* addr;
  char l_cmdbuf[1024];

  memset(l_cmdbuf, 0, sizeof(l_cmdbuf));

  if (shm_ptr->discover == 1) {
    sprintf(l_cmdbuf, "wget -q -O - http://127.0.0.1/SetNetwork.cgi?Proto=%d",
      shm_ptr->discover);
  }
  else if (shm_ptr->discover == 0) {
    if (shm_ptr->addr.dns1[0] == '\0') {
      sprintf(l_cmdbuf, "wget -q -O - http://127.0.0.1/SetNetwork.cgi?Proto=%d\\&Ipaddr=%s\\&Netmask=%s\\&Gateway=%s",
        shm_ptr->discover,
        shm_ptr->addr.ipaddr,
        shm_ptr->addr.netmask,
        shm_ptr->addr.gateway
      );
    }
    else if (shm_ptr->addr.dns1[0] != '\0' &&
             shm_ptr->addr.dns2[0] == '\0') {
      sprintf(l_cmdbuf, "wget -q -O - http://127.0.0.1/SetNetwork.cgi?Proto=%d\\&Ipaddr=%s\\&Netmask=%s\\&Gateway=%s\\&Dns=%s",
        shm_ptr->discover,
        shm_ptr->addr.ipaddr,
        shm_ptr->addr.netmask,
        shm_ptr->addr.gateway,
        shm_ptr->addr.dns1
      );
    }
    else if (shm_ptr->addr.dns1[0] != '\0' &&
             shm_ptr->addr.dns2[0] != '\0') {
      sprintf(l_cmdbuf, "wget -q -O - http://127.0.0.1/SetNetwork.cgi?Proto=%d\\&Ipaddr=%s\\&Netmask=%s\\&Gateway=%s\\&Dns=%s\\&Dns1=%s",
        shm_ptr->discover,
        shm_ptr->addr.ipaddr,
        shm_ptr->addr.netmask,
        shm_ptr->addr.gateway,
        shm_ptr->addr.dns1,
        shm_ptr->addr.dns2
      );
    }
  }
  logger_write(LOG_DEBUG, "set_smc3a_cgi_addr():cmd:%s. %s", l_cmdbuf, HERE);
  ret = WEXITSTATUS(system(l_cmdbuf));
  logger_write(LOG_DEBUG, "set_smc3a_cgi_addr():ret:%d, %s", ret, HERE);

  return(ret);
}

/**
 * IPアドレスを求め、共有メモリに書き込む
 *
 * アドレスの取得はコマンドライン ip を使用して求める
 *
 * @retval  0 正常終了
 * @retval -1 ip -f 出力結果からのデータ取得に失敗
 * @retval -2 CIDR <--> ネットマスクの変換に失敗
 * @retval -3 メモリのアロケートに失敗
 */
int get_abox_ipaddr(void)
{
  int ret = -3;
  ipaddr_t* addr;

  addr = (ipaddr_t *) malloc(sizeof(ipaddr_t));
  if (NULL == addr) {
    logger_write(LOG_ERR, "get_abox_ipaddr():addr malloc failed. %s", HERE);
    return -1;
  }
  memset(addr->ipaddr,    0, sizeof(char) * BUF_IPADDR);
  memset(addr->netmask,   0, sizeof(char) * BUF_IPADDR);
  memset(addr->broadcast, 0, sizeof(char) * BUF_IPADDR);
  memset(addr->gateway,   0, sizeof(char) * BUF_IPADDR);
  memset(addr->dns1,      0, sizeof(char) * BUF_IPADDR);
  memset(addr->dns2,      0, sizeof(char) * BUF_IPADDR);
  ret = get_src_address(addr);

  if (0 == ret) {
    strcpy_safe(shm_ptr->runtime_addr.ipaddr   , addr->ipaddr);
    strcpy_safe(shm_ptr->runtime_addr.netmask  , addr->netmask);
    strcpy_safe(shm_ptr->runtime_addr.broadcast, addr->broadcast);
    strcpy_safe(shm_ptr->runtime_addr.gateway  , addr->gateway);
    strcpy_safe(shm_ptr->runtime_addr.dns1     , addr->dns1);
    strcpy_safe(shm_ptr->runtime_addr.dns2     , addr->dns2);
  }
  logger_write(LOG_NOTICE, "get_abox_ipaddr(): %s", HERE);
  logger_write(LOG_NOTICE, "  ipaddr : %s", shm_ptr->runtime_addr.ipaddr);
  logger_write(LOG_NOTICE, "  netmask: %s", shm_ptr->runtime_addr.netmask);
  logger_write(LOG_NOTICE, "  broad  : %s", shm_ptr->runtime_addr.broadcast);
  logger_write(LOG_NOTICE, "  gateway: %s", shm_ptr->runtime_addr.gateway);
  logger_write(LOG_NOTICE, "  dns1   : %s", shm_ptr->runtime_addr.dns1);
  logger_write(LOG_NOTICE, "  dns2   : %s", shm_ptr->runtime_addr.dns2);
  logger_write(LOG_NOTICE, "status   : %d", ret);

  return ret;
}


/**
 * SMC-3A PlusのIPを設定し、共有メモリに書き込む
 *
 * @param[in,out] user 設定するIPアドレスデータ構造体へのポインタ
 * @return bits ビット各位に各パラメタのアサイン成否を代入したOR値
 *              上位からipaddr,netmask,broadcast,gateway,dns
 */
int set_abox_ipaddr(void* user)
{
  ipaddr_t* addr;
  FILE* fp;
  const char* ifname;
  int bits = 31;

  /* インタフェース名。将来引数化されるかも */
  ifname = "eth0";

  addr = (ipaddr_t *) user;

  /* IPv4アドレス */
  if (0 == setipv4addr(addr->ipaddr, ifname)) {
    strcpy_safe(shm_ptr->runtime_addr.ipaddr , addr->ipaddr);
    bits = (bits ^ 16);
  }
  /* サブネットマスク */
  if (0 == strlen(addr->netmask)) {
    // 空欄の場合は/24にフォールバック
    cidr2addr(addr->netmask, 24);
  }
  if (0 == setsubnetmask(addr->netmask, ifname)) {
    strcpy_safe(shm_ptr->runtime_addr.netmask, addr->netmask);
    bits = (bits ^ 8);
  }
  /* ブロードキャストアドレス */
  strcpy_safe(addr->broadcast, calc_broadcast(addr->ipaddr, addr->netmask));
  if (0 == setbroadcast(addr->broadcast, ifname)) {
    strcpy_safe(shm_ptr->runtime_addr.broadcast, addr->broadcast);
    bits = (bits ^ 4);
  }
  /* ゲートウェイアドレス */
  if (0 < strlen(addr->gateway)) {
    if (0 == emptyroutes(ifname)) {
      if (0 == setroute(addr->ipaddr, addr->netmask, ifname)) {
        if (0 == setgateway(addr->gateway, ifname)) {
          strcpy_safe(shm_ptr->runtime_addr.gateway, addr->gateway);
          bits = (bits ^ 2);
        }
      }
    }
  }
  /* ネームサーバ */
  if (0 < strlen(addr->dns1)) {
    if (NULL != (fp = fopen("/etc/resolv.conf", "wb"))) {
      strcpy_safe(shm_ptr->runtime_addr.dns1, addr->dns1);
      strcpy_safe(shm_ptr->runtime_addr.dns2, addr->dns2);
      bits = (bits ^ 1);

      if (0 < strlen(addr->dns1)) {
        fprintf(fp, "nameserver %s\n", addr->dns1);
      }
      if (0 < strlen(addr->dns2)) {
        fprintf(fp, "nameserver %s\n", addr->dns2);
      }
      fclose(fp);
    }
  }
  logger_write(LOG_NOTICE, "set_abox_ipaddr(): %s", HERE);
  logger_write(LOG_NOTICE, "  ipaddr : %s", addr->ipaddr);
  logger_write(LOG_NOTICE, "  netmask: %s", addr->netmask);
  logger_write(LOG_NOTICE, "  broad  : %s", addr->broadcast);
  logger_write(LOG_NOTICE, "  gateway: %s", addr->gateway);
  logger_write(LOG_NOTICE, "  dns1   : %s", addr->dns1);
  logger_write(LOG_NOTICE, "  dns2   : %s", addr->dns2);
  logger_write(LOG_NOTICE, "  executed status = %d", bits);

  return bits;
}


/**
 * SMC-3A PlusのIPを設定せずに共有メモリに書き込む
 *
 * @param[in,out] user 設定するIPアドレスデータ構造体へのポインタ
 * @retval  0 正常終了
 * @retval -1 書き込みのためのメモリアロケーションに失敗
 */
int save_abox_ipaddr(void* user)
{

  ipaddr_t* addr;
  yx_config_t* yx;

  addr = (ipaddr_t *) user;
  if (NULL == user) {
    logger_write(LOG_ERR, "    user is null. %s", HERE);
    return -1;
  }

  strcpy_safe(shm_ptr->runtime_addr.ipaddr ,   addr->ipaddr);
  strcpy_safe(shm_ptr->runtime_addr.netmask,   addr->netmask);
  strcpy_safe(shm_ptr->runtime_addr.broadcast, calc_broadcast(
                                               addr->ipaddr, addr->netmask));
  strcpy_safe(shm_ptr->runtime_addr.gateway,   addr->gateway);
  strcpy_safe(shm_ptr->runtime_addr.dns1,      addr->dns1);
  if (strlen(addr->dns2) > 0) {
    strcpy_safe(shm_ptr->runtime_addr.dns2,    addr->dns2);
  } else {
    shm_ptr->runtime_addr.dns2[0] = '\0';
  }
  yx = (yx_config_t *) malloc(sizeof(yx_config_t));
  if (NULL != yx) {
    zerowrite_yx_config(yx);
    yx->network_type = 0;
    strcpy_safe(yx->ver, "1.0");
    strcpy_safe(yx->device_name, "eth0");
    if (strlen(shm_ptr->hwaddr) > 0) {
      strcpy_safe(yx->mac_address, shm_ptr->hwaddr);
    }
    strcpy_safe(yx->ip_address,      shm_ptr->runtime_addr.ipaddr);
    strcpy_safe(yx->netmask_address, shm_ptr->runtime_addr.netmask);
    strcpy_safe(yx->gate_address,    shm_ptr->runtime_addr.gateway);
    strcpy_safe(yx->dns1,            shm_ptr->runtime_addr.dns1);
    if (TRUE == match_ipaddr(shm_ptr->runtime_addr.dns2)
    ||  TRUE == match_hostaddr(shm_ptr->runtime_addr.dns2)) {
      strcpy_safe(yx->dns2, shm_ptr->runtime_addr.dns2);
    }
    print_yx_config(yx);
    free(yx);
  } else {
    logger_write(LOG_CRIT, "  FAILED to alloc writing memory! %s", HERE);
    logger_write(LOG_CRIT, "  Agentbox will use OLD Address, or DHCP next boot. %s", HERE);
  }
  logger_write(LOG_NOTICE, "save_abox_ipaddr(): %s", HERE);
  logger_write(LOG_NOTICE, "  ipaddr   : %s", shm_ptr->runtime_addr.ipaddr);
  logger_write(LOG_NOTICE, "  netmask  : %s", shm_ptr->runtime_addr.netmask);
  logger_write(LOG_NOTICE, "  broadcast: %s", shm_ptr->runtime_addr.broadcast);
  logger_write(LOG_NOTICE, "  gateway  : %s", shm_ptr->runtime_addr.gateway);
  logger_write(LOG_NOTICE, "  dns1     : %s", shm_ptr->runtime_addr.dns1);
  logger_write(LOG_NOTICE, "  dns2     : %s", shm_ptr->runtime_addr.dns2);

  return 0;
}


/**
 * AgentboxのIPアドレスを取得する
 *
 * @param[in,out] user アドレスをコピーするポインタ
 * @retval  0 アドレス取得に成功
 * @retval -1 アドレス取得に失敗
 * @retval -2 アロケーションに失敗
 * @retval -3 引数がまさかのNULL
 */
int get_src_address(void* user)
{
  ipaddr_t* addr;
  int ret;

  if (NULL == user) {
    logger_write(LOG_ERR, "    user is null. %s", HERE);
    return -3;
  }
  addr = (ipaddr_t *) user;
  ret = parse_ifconfig(addr);

  return (ret == 0 ? 0 : -1);
}



/**
 * ネットワーク設定をyx_config_network.iniに書き出す
 *
 * 共有メモリ上のクラウドから同期されたネットワーク設定が
 * 正当なアドレス表記であれば、内容をconfigファイルとして
 * 保存する
 */
void set_abox_nwsettings(void)
{
  yx_config_t* yx;
  const char* ifname = "eth0";

  if (TRUE == match_ipaddr(shm_ptr->addr.ipaddr)
  &&  TRUE == match_ipaddr(shm_ptr->addr.netmask)
  &&  TRUE == match_ipaddr(shm_ptr->addr.broadcast)
  &&  TRUE == match_ipaddr(shm_ptr->addr.gateway)
  &&  (  TRUE == match_ipaddr(shm_ptr->addr.dns1)
      || TRUE == match_hostaddr(shm_ptr->addr.dns1))) {

    yx = (yx_config_t *) malloc(sizeof(yx_config_t));
    if (NULL != yx) {
      zerowrite_yx_config(yx);

      yx->network_type = 0;
      yx->dhcp_timeout = 15;
      yx->primary = 1;
      strcpy_safe(yx->ver, "1.0");
      strcpy_safe(yx->device_name, ifname);
      strcpy_safe(yx->mac_address, shm_ptr->hwaddr);
      strcpy_safe(yx->ip_address, shm_ptr->addr.ipaddr);
      strcpy_safe(yx->netmask_address, shm_ptr->addr.netmask);
      strcpy_safe(yx->gate_address, shm_ptr->addr.gateway);
      strcpy_safe(yx->dns1, shm_ptr->addr.dns1);
      if (TRUE == match_ipaddr(shm_ptr->addr.dns2)
      ||  TRUE == match_hostaddr(shm_ptr->addr.dns2)) {
        strcpy_safe(yx->dns2, shm_ptr->addr.dns2);
      }
      print_yx_config(yx);

      free(yx);
    }
  } else {
    logger_write(LOG_ERR,
      "  set_abox_nwsettings() ipaddr error. %s", HERE);
    logger_write(LOG_ERR,
      "    addr.ipaddr    = %s %s", shm_ptr->addr.ipaddr, HERE);
    logger_write(LOG_ERR,
      "    addr.netmask   = %s %s", shm_ptr->addr.netmask, HERE);
    logger_write(LOG_ERR,
      "    addr.broadcast = %s %s", shm_ptr->addr.broadcast, HERE);
    logger_write(LOG_ERR,
      "    addr.gateway   = %s %s", shm_ptr->addr.gateway, HERE);
    logger_write(LOG_ERR,
      "    addr.dns1      = %s %s", shm_ptr->addr.dns1, HERE);
    logger_write(LOG_ERR,
      "    addr.dns2      = %s %s", shm_ptr->addr.dns2, HERE);
  }
}


/**
 * ip -f およびresolv.confからアドレスを取得し、コピー先に書き込む
 * a312(Busybox)の ip の出力に依存するため、転用時は出力フォーマットに
 * 注意して行うこと
 *
 * @param[in,out] user 値をコピーするメモリ領域へのポインタ
 * @retval  0 正常終了
 * @retval -1 ip -f 出力結果からのデータ取得に失敗
 * @retval -2 CIDR <--> ネットマスクの変換に失敗
 * @retval -3 route 出力結果からのデータ取得に失敗
 */
int parse_ifconfig(void* user)
{
  ipaddr_t* addr;
  FILE* fp;
  char ipaddr[BUF_IPADDR];
  char netmask[BUF_IPADDR];
  char bcast[BUF_IPADDR];
  char gateway[BUF_IPADDR];
  char dns1[BUF_IPADDR];
  char dns2[BUF_IPADDR];
  int captured;
  int rawstatus;
  int estatus;
  int cidr;
  char *cp = (char*)NULL;

  addr = (ipaddr_t *) user;

  logger_write(LOG_DEBUG,
    "  parse_ifconfig() gather active network settings. %s", HERE);

  if (NULL == (fp = popen(AGENTBOX_CFG_CMD_IP, "r"))) {
    logger_write(LOG_CRIT,
      "    Open command process(ip -f) failed! %s", HERE);
    return -1;
  }
  memset(ipaddr,  0, sizeof(char) * BUF_IPADDR);
  memset(netmask, 0, sizeof(char) * BUF_IPADDR);
  memset(bcast,   0, sizeof(char) * BUF_IPADDR);
  /* キャプチャ結果は xxx.xxx.xxx.xxx,nn,xxx.xxx.xxx.xxx で返る */
  captured = fscanf(fp, "%s %s %s\n", ipaddr, netmask, bcast);
  rawstatus = pclose(fp);
  if (!WIFEXITED(rawstatus)) {
    logger_write(LOG_ERR,
      "    Command(ip -f) finished abnormal status[rawstatus=%d] %s",
      rawstatus, HERE);
    return -1;
  }
  estatus = WEXITSTATUS(rawstatus);
  if (0 != estatus) {
    logger_write(LOG_ERR,
      "    Command(ip -f) finished status is not zero[estaus=%d] %s",
      estatus, HERE);
    return -1;
  }
  if (3 != captured) {
    logger_write(LOG_ERR,
      "    Captured params(addr, mask, bcast) is not 3! %s", HERE);
    return -1;
  }
  cidr = (int) strtol(netmask, (char **) NULL, 10);
  if ((0 < cidr) && (cidr <= 32)) {
    if (0 != cidr2addr(netmask, cidr)) {
      return -2;
    }
  }
  strcpy_safe(addr->ipaddr, ipaddr);
  strcpy_safe(addr->netmask, netmask);
  strcpy_safe(addr->broadcast, bcast);


  if (NULL == (fp = popen(AGENTBOX_CFG_CMD_ROUTE, "r"))) {
    logger_write(LOG_CRIT,
      "    Open command process(route) failed! %s", HERE);
    return -3;
  }
  captured = fscanf(fp, "%s\n", gateway);
  rawstatus = pclose(fp);
  if (!WIFEXITED(rawstatus)) {
    logger_write(LOG_ERR,
      "    Command process(route) finished abnormal status[rawstatus=%d] %s",
      rawstatus, HERE);
    return -1;
  }
  estatus = WEXITSTATUS(rawstatus);
  if (0 != estatus) {
    logger_write(LOG_ERR,
      "    Command process(route) finished status is not zero[estaus=%d] %s",
      estatus, HERE);
    return -1;
  }
  if (1 != captured) {
    logger_write(LOG_ERR,
      "    Captured param is not 1! %s", HERE);
    return -1;
  }
  strcpy_safe(addr->gateway, gateway);


  if (NULL == (fp = popen(AGENTBOX_CFG_CMD_RESOLVCONF, "r"))) {
    logger_write(LOG_CRIT,
      "    Reading resolv.conf failed! %s", HERE);
    return -1;
  }
  /* キャプチャ結果は [FQDN|IPv4]空白区切り、最大2 で返る */
  captured = fscanf(fp, "%[^ ,\n]%*1[ ,]%s\n", dns1, dns2);
  rawstatus = pclose(fp);
  if (!WIFEXITED(rawstatus)) {
    logger_write(LOG_ERR,
      "    Reading resolv.conf finished abnormal status[rawstatus=%d] %s",
      rawstatus, HERE);
    return -1;
  }
  estatus = WEXITSTATUS(rawstatus);
  if (0 != estatus) {
    logger_write(LOG_ERR,
      "    Reading resolv.conf finished status is not zero[estaus=%d] %s",
      estatus, HERE);
    return -1;
  }
  if (1 == captured) {
    dns2[0] = '\0';
  } else if (0 == captured) {
    logger_write(LOG_ERR, "    Reading resolv.conf captured nothing. %s", HERE);
    logger_write(LOG_ERR, "    So, Alternative Nameserver 8.8.8.8 added. %s", HERE);
    strcpy(dns1, "8.8.8.8");
    dns2[0] = '\0';
  }
  strcpy_safe(addr->dns1, dns1);
  if ((cp = strchr(dns2, ',')) != (char*)NULL) {
    *cp = '\0';
  }
  strcpy_safe(addr->dns2, dns2);

  return 0;
}


/**
 * IPアドレスとネットマスクからネットワークアドレスを算出し、返す
 *
 * @param[in] addr インタフェースに振られたIPv4アドレス
 * @param[in] mask インタフェースに振られたネットマスク
 * @return ネットワークアドレスの文字列(4オクテット表記)
 */
char* calc_network(const char* addr, const char* mask)
{
  unsigned int aoct[4], moct[4], noct[4];
  int idx;
  static char naddr[16];

  addr2oct(addr, aoct);
  addr2oct(mask, moct);

  for (idx = 0; idx < 4; idx++) {
    noct[idx] = aoct[idx] & moct[idx];
  }
  sprintf(naddr, "%d.%d.%d.%d", noct[0], noct[1], noct[2], noct[3]);
  logger_write(LOG_DEBUG,
    "  calc_network() network address for %s is: %s %s", addr, naddr, HERE);
  return naddr;
}


/**
 * IPアドレスとネットマスクからブロードキャストアドレスを算出し、返す
 *
 * @param[in] addr インタフェースに振られたIPv4アドレス
 * @param[in] mask インタフェースに振られたネットマスク
 * @return ブロードキャストアドレスの文字列(4オクテット表記)
 */
char* calc_broadcast(const char* addr, const char* mask)
{
  unsigned int aoct[4], moct[4], boct[4];
  int sub;
  int idx;
  static char bcast[16];

  addr2oct(addr, aoct);
  addr2oct(mask, moct);

  memset(bcast, 0, sizeof(char) * 16);

  for (idx = 0; idx < 4; idx++) {
    if (255 == moct[idx]) {
      boct[idx] = aoct[idx];
    } else if (0 == moct[idx]) {
      boct[idx] = 255;
    } else {
      boct[idx] = 0;
      sub = 256 - moct[idx];
      do {
        boct[idx] += sub;
      } while (aoct[idx] > boct[idx]);
      boct[idx] -= 1;
    }
  }
  sprintf(bcast, "%d.%d.%d.%d", boct[0], boct[1], boct[2], boct[3]);
  logger_write(LOG_DEBUG,
    "  calc_broadcast() broadcast address for %s is: %s %s", addr,  bcast, HERE);
  return bcast;
}


/**
 * CIDRからサブネットマスクを返す
 *
 * @param[in,out] dest ネットマスクを書き込むメモリ領域へのポインタ
 * @param[in] cidr [ip.v4.addr]/に続くCIDRビット値
 * @retval  0 正常終了
 * @retval -1 CIDRの許容範囲(0 - 32)外のビット値が指定された
 * @retval -2 CIDRからIPv4への変換に失敗した
 */
int cidr2addr(char* dest, const int cidr)
{
  int bit[32];
  int subnet[4] = {0, 0, 0, 0};
  int i, j;

  if ((0 > cidr) || (cidr > 32)) {
    return -1;
  }

  for (i = 0; i < 32; ++i) {
    bit[i] = 0;
  }
  for (j = 0; j < cidr; ++j) {
    bit[j] = 1;
  }

  if (0 != bmp2dec(bit, subnet)) {
    return -2;
  }

  sprintf(dest, "%d.%d.%d.%d",
    subnet[0], subnet[1], subnet[2], subnet[3]);
  return 0;
}


/**
 * ホスト名からIPv4アドレスを返す
 *
 * @param[in,out] dest 値をコピーする対象へのポインタ
 * @param[in] host 対応するIPv4アドレスを求めたいホスト名
 * @retval  0 ホスト名に対応するIPv4アドレスが取得できた
 * @retval -1 ホスト名<->アドレスの解決ができなかった
 * @retval -2 nslookupからの出力が異常終了した
 */
int host2addr(char* dest, const char* host)
{
  char cmdstr[BUF_128];
  char addr[BUF_IPADDR];
  char* nl;
  int cmdstat = 0;
  int rawstatus;
  int estatus;
  FILE* fp;

  memset(cmdstr, 0, sizeof(cmdstr));
  memset(addr,   0, sizeof(addr));
  sprintf(cmdstr, AGENTBOX_CFG_CMD_HOST2ADDR, host);

  if (NULL != (fp = popen(cmdstr, "r"))) {
    while (NULL != fgets(addr, sizeof(addr), fp)) {
      nl = strchr(addr, '\n');
      if (NULL != nl) {
        *nl = '\0';
      }
    }
    rawstatus = pclose(fp);
    if (!WIFEXITED(rawstatus)) {
      logger_write(LOG_ERR,
        "    Command host2addr finished abnormal status[rawstatus=%d] %s",
        rawstatus, HERE);
      return -2;
    }
    estatus = WEXITSTATUS(rawstatus);
    if (0 != estatus) {
      logger_write(LOG_ERR,
        "    Command host2addr finished status is not zero[estaus=%d] %s",
        estatus, HERE);
      return -2;
    }

    if (match_ipaddr(addr)) {
      logger_write(LOG_DEBUG,
        "    Converting hostname %s to %s %s", host, addr, HERE);
      strncpy(dest, addr, strlen(addr));
    } else {
      logger_write(LOG_ERR,
        "    Converting hostname was failed. %s", HERE);
      if (NULL == host||'\0' == host[0]) {
        logger_write(LOG_ERR,
          "    Argument host is NULL. %s", HERE);
      } else {
        logger_write(LOG_ERR,
          "    Argument host is %s. %s", host, HERE);
      }
      cmdstat = -1;
    }
  } else {
    logger_write(LOG_CRIT,
      "  host2addr() open file pointer failed! %s", HERE);
    cmdstat = -1;
  }
  return cmdstat;
}


/**
 * IPv4アドレスの文字列を分解し、各オクテットを数値化して返す
 *
 * @param[in] addr IPv4アドレスの文字列ポインタ
 * @param[in,out] oct 各オクテットを数値化した値が格納される配列
 * @return 各オクテットの数値化成功フラグがビット化された数値[0-15]
 */
int addr2oct(const char* addr, unsigned int oct[])
{
  char o[4][4] = {
    {0},{0},{0},{0}
  };
  unsigned long conv;
  unsigned int idx;
  int converted = 15;

  if (4 == sscanf(addr, "%[^ .]%*1[ .]%[^ .]%*1[ .]%[^ .]%*1[ .]%s", o[0], o[1], o[2], o[3])) {
    logger_write(LOG_DEBUG,
      "  addr2oct() scaned: %s, %s, %s, %s %s",
      o[0], o[1], o[2], o[3], HERE);
    for (idx = 0; idx < 4; idx++) {
      conv = strtoul(o[idx], (char **) NULL, 10);
      if (0 <= conv && conv <= 255) {
        oct[idx] = (unsigned int) conv;
        converted = converted ^ (1 << idx);
      }
    }
  }
  logger_write(LOG_DEBUG,
    "  addr2oct() converted: %d, %d, %d, %d %s",
    oct[0], oct[1], oct[2], oct[3], HERE);
  return converted;
}


/**
 * 2進数ビットパターンを10進数化して返す
 *
 * @param[in] bit 2進数ビットパターン
 * @param[in,out] octet ビットパターンを10進数化した値[0-255]を格納する配列
 * @return 成功時0、
 */
int bmp2dec(const int bit[], int octet[])
{
  int weigh[8] = {128, 64, 32, 16, 8, 4, 2, 1};
  int pos = 0;
  int end = 0;
  int i, j;
  int dec;

  for (i = 0; i < 4; i++) {
    dec = 0;
    for (j = 0; j < 8; j++) {
      if (bit[pos] == 1) {
        dec += weigh[j];
      }
      pos++;
    }
    if (0 <= dec && dec <= 255) {
      octet[i] = dec;
    } else {
      end -= 1;
    }
  }
  logger_write(LOG_DEBUG,
    "  bmp2dec() before: %d %d %d %d %s",
    bit[0], bit[1], bit[2], bit[3], HERE);
  logger_write(LOG_DEBUG,
    "  bmp2dec() converted: %d %d %d %d %s",
    octet[0], octet[1], octet[2], octet[3], HERE);
  return end;
}


/**
 * yx_config_network.iniのパース用コールバック
 *
 * libini の関数 ini_parse(const char*, ini_handler, void*) のコールバックとして使用する
 *
 * @sa libini.h
 * @param[in,out] user 値をコピーする対象へのポインタ
 * @param[in] section .iniのセクション部(ブラケット[]で囲まれた箇所)の名称
 * @param[in] name    .iniのパラメータ名
 * @param[in] value   .iniのパラメータの値
 * @retval 0 正常終了(パースエラーなく成功)
 * @retval 0以外 最初にパースエラーが発生した行番号
 */
int parse_yx_config(void* user, const char* section, const char* name, const char* value)
{
  yx_config_t* yx;

  ABOX_UNUSED(section);
  yx = (yx_config_t *) user;
  if (       0 == strcmp(name, "device_name")) {
    strcpy_safe(yx->device_name, value);
  } else if (0 == strcmp(name, "network_type")) {
    yx->network_type = atoi_safe(value);
  } else if (0 == strcmp(name, "mac_address")) {
    strcpy_safe(yx->mac_address, value);
  } else if (0 == strcmp(name, "ip_address")) {
    strcpy_safe(yx->ip_address, value);
  } else if (0 == strcmp(name, "gate_address")) {
    strcpy_safe(yx->gate_address, value);
  } else if (0 == strcmp(name, "netmask_address")) {
    strcpy_safe(yx->netmask_address, value);
  } else if (0 == strcmp(name, "dns1")) {
    strcpy_safe(yx->dns1, value);
  } else if (0 == strcmp(name, "dns2")) {
    strcpy_safe(yx->dns2, value);
  } else if (0 == strcmp(name, "primary")) {
    yx->primary = atoi_safe(value);
  } else if (0 == strcmp(name, "ver")) {
    strcpy_safe(yx->ver, value);
  } else if (0 == strcmp(name, "dhcp_timeout")) {
    yx->dhcp_timeout = atoi_safe(value);
  } else if (0 == strcmp(name, "service_name")) {
    strcpy_safe(yx->service_name, value);
  } else if (0 == strcmp(name, "username")) {
    strcpy_safe(yx->username, value);
  } else if (0 == strcmp(name, "passwd")) {
    strcpy_safe(yx->passwd, value);
  }
  return 0;
}


/**
 * A312用ネットワーク設定をファイルとして出力する
 *
 * @param[in] yx ファイルに出力するyxconfigデータ構造体
 * @retval  0 正常終了
 * @retval -1 ファイル保存のディスクリプタ作成に失敗
 */
int print_yx_config(yx_config_t* yx)
{
  FILE* fp;

  if (NULL != (fp = fopen(AGENTBOX_CFG_YX_NETWORK_INI_PATH, "wb"))) {
    fprintf(fp, "device_name=%s\n", yx->device_name);
    fprintf(fp, "network_type=%d\n", yx->network_type);
    fprintf(fp, "mac_address=%s\n", yx->mac_address);
    fprintf(fp, "ip_address=%s\n", yx->ip_address);
    fprintf(fp, "gate_address=%s\n", yx->gate_address);
    fprintf(fp, "netmask_address=%s\n", yx->netmask_address);
    fprintf(fp, "dns1=%s\n", yx->dns1);
    fprintf(fp, "dns2=%s\n", yx->dns2);
    fprintf(fp, "primary=%d\n", yx->primary);
    fprintf(fp, "ver=%s\n", yx->ver);
    fprintf(fp, "dhcp_timeout=%d\n", yx->dhcp_timeout);
    fprintf(fp, "service_name=%s\n", yx->service_name);
    fprintf(fp, "username=%s\n", yx->username);
    fprintf(fp, "passwd=%s\n", yx->passwd);
    fclose(fp);
    return 0;
  }
  logger_write(LOG_ERR,
    "  print_yx_config() file pointer open error. %s", HERE);
  return -1;
}


/**
 * yx_config_t構造体に0を書き込む
 *
 * @param[in,out] user 値をコピーする対象yx_config_t構造体へのポインタ
 */
void zerowrite_yx_config(void* user)
{
  yx_config_t* yx;

  yx = (yx_config_t *) user;
  yx->device_name[0]     = '\0';
  yx->network_type       = 1;
  yx->mac_address[0]     = '\0';
  yx->ip_address[0]      = '\0';
  yx->gate_address[0]    = '\0';
  yx->netmask_address[0] = '\0';
  yx->dns1[0]            = '\0';
  yx->dns2[0]            = '\0';
  yx->primary            = 0;
  yx->ver[0]             = '\0';
  yx->dhcp_timeout       = 15;
  yx->service_name[0]    = '\0';
  yx->username[0]        = '\0';
  yx->passwd[0]          = '\0';
}


/**
 * 引数で与えられた文字列がIPv4アドレスの表記にマッチするかを確認する
 *
 * @param[in] needle
 * @retval TRUE  IPアドレス表記にマッチする
 * @retval FALSE IPアドレス表記にマッチしない
 */
int match_ipaddr(const char* needle)
{
  pcre* re;
  const char* errstr;
  int matched;
  int erroffset;
  int ovector[100];

  if (NULL == needle || (0 == strlen(needle))) {
    return FALSE;
  }
  re = pcre_compile(REGEX_IPV4ADDR, PCRE_CASELESS,
    &errstr, &erroffset, 0);
  if (NULL == re) {
    return FALSE;
  }
  matched = pcre_exec(re, NULL, needle, (int) strlen(needle),
    0, 0, ovector, sizeof(ovector));
  pcre_free(re);
  return ((matched > 0) ? TRUE : FALSE);
}


/**
 * 引数で与えられた文字列がインターネットホストの表記にマッチするかを確認する
 *
 * @param[in] needle
 * @retval TRUE インターネットホスト表記にマッチする
 * @retval FALSE インターネットホスト表記にマッチしない
 */
int match_hostaddr(const char* needle)
{
  pcre* re;
  const char* errstr;
  int matched;
  int erroffset;
  int ovector[100];

  if (NULL == needle || (0 == strlen(needle))) {
    return FALSE;
  }
  re = pcre_compile(REGEX_HOSTNAME, PCRE_EXTENDED,
    &errstr, &erroffset, 0);
  if (NULL == re) {
    return FALSE;
  }
  matched = pcre_exec(re, NULL, needle, (int) strlen(needle),
    0, 0, ovector, sizeof(ovector));
  pcre_free(re);
  return ((matched > 0) ? TRUE : FALSE);
}


/**
 * 正当なIPv4アドレス表記になっている文字列を指定の
 * ポインタにコピーする
 * 文字列が不正な場合はzerostrの値により変化する
 * zerostrがtrue(=1) の場合、destの先頭を'\0'にする
 * zerostrがfalse(=0) の場合、destの内容は操作しない
 *
 * @param[in] addr コピーしたいIPv4アドレス文字列
 * @param[in,out] dest コピー先を指すポインタ
 * @param[in] zerostr 空文字列で埋めるかどうかのフラグ
 */
void safe_addrcpy(char* addr, char* dest, int zerostr)
{
  char* convaddr;
  char* nl;

  if (NULL == dest || (NULL != dest && '\0' == dest[0])) {
    return;
  }
  if (true == zerostr) {
    dest[0] = '\0';
  }
  if (NULL == addr || (NULL != addr && '\0' == addr[0])) {
    return;
  }

  nl = strchr(addr, '\n');
  if (NULL != nl) {
    *nl = '\0';
  }

  if (TRUE == match_ipaddr(addr)) {
    strncpy(dest, addr, strlen(addr));
  } else {
    convaddr = malloc(sizeof(char) * BUF_IPADDR);
    if (NULL == convaddr) {
      logger_write(LOG_ERR, "  convaddr malloc failed. %s", HERE);
      return;
    }
    logger_write(LOG_DEBUG, "  call host2addr() %s", HERE);
    if (0 != host2addr(convaddr, addr)) {
      strncpy(dest, convaddr, strlen(convaddr));
    }
    free(convaddr);
  }
}


/* プライベート関数群 */


/**
 * 引数で指定したインタフェースにアサインされたIPv4アドレスを取得する
 *
 * @param[in,out] dest 取得したアドレスを書き込む領域へのポインタ
 * @param[in] dev アドレスを取得するインタフェース名
 * @retval  0 正常終了
 * @retval -1 socketのオープンに失敗
 * @retval -2 ioctlシステムコール失敗
 * @retval -3 インタフェースにアドレスのアサインがない
 */
int getipv4addr(char* dest, const char* dev)
{
  struct sockaddr_in sin;
  struct ifreq ifr;
  int stats = 0;
  int sock  = 0;

  if (NULL == dest || (NULL != dest && '\0' == *dest)) {
    logger_write(LOG_ERR, "  dest is null. %s", HERE);
    return -1;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -3;
  }

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (-1 == sock) {
    logger_write(LOG_ERR,
      "  getipv4addr() socket cannot opened. %s", HERE);
    return -1;
  }

  strcpy(ifr.ifr_name, dev);
  stats = ioctl(sock, SIOCGIFADDR, &ifr);

  if (-1 == stats) {
    close(sock);
    logger_write(LOG_ERR,
      "  getipv4addr() ioctl:SIOCGIFADDR error[%d] %s", stats, HERE);
    return -2;
  } else if (AF_INET != ifr.ifr_addr.sa_family) {
    /* v4アドレスのアサインがない */
    close(sock);
    logger_write(LOG_ERR,
      "  getipv4addr() IPv4 address may not assigned.[%d] %s", stats, HERE);
    return -3;
  } else {
    memcpy(&sin, &ifr.ifr_addr, sizeof(struct sockaddr_in));
    strcpy(dest, inet_ntoa(sin.sin_addr));
    close(sock);
  }
  return 0;
}


/**
 * 引数で指定したインタフェースにIPv4アドレスを設定する
 *
 * @param[in] addr 設定するIPv4アドレス(4オクテット表記)
 * @param[in] dev アドレスを設定するインタフェース名
 * @retval  0 正常終了
 * @retval -1 socketのオープンに失敗
 * @retval -2 ioctlシステムコール失敗
 */
int setipv4addr(const char* addr, const char* dev)
{
  struct sockaddr_in sin;
  struct ifreq ifr;
  int stats = 0;
  int sock  = 0;

  if (NULL == addr || (NULL != addr && '\0' == *addr)) {
    logger_write(LOG_ERR, "  addr is null. %s", HERE);
    return -1;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -1;
  }

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (-1 == sock) {
    logger_write(LOG_ERR,
      "  setipv4addr() socket cannot opened. %s", HERE);
    return -1;
  }

  strcpy(ifr.ifr_name, dev);
  memset(&sin, 0, sizeof(struct sockaddr_in));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = inet_addr(addr);
  memcpy(&ifr.ifr_addr, &sin, sizeof(struct sockaddr_in));

  stats = ioctl(sock, SIOCSIFADDR, &ifr);
  close(sock);

  if (-1 == stats) {
    logger_write(LOG_ERR,
      "  setipv4addr() ioctl:SIOCSIFADDR error.[%d] %s", stats, HERE);
    return -2;
  }
  logger_write(LOG_DEBUG,
    "  setipv4addr() IPv4 address was assigned[%s] %s", addr, HERE);
  return 0;
}


/**
 * 引数で指定したインタフェースにアサインされたサブネットマスクを取得する
 *
 * @param[in,out] dest 取得したマスクを書き込む領域へのポインタ
 * @param[in] dev アドレスを取得するインタフェース名
 * @retval  0 正常終了
 * @retval -1 socketのオープンに失敗
 * @retval -2 ioctlシステムコール失敗
 */
int getsubnetmask(char* dest, const char* dev)
{
  struct sockaddr_in sin;
  struct ifreq ifr;
  int stats = 0;
  int sock  = 0;

  if (NULL == dest || (NULL != dest && '\0' == *dest)) {
    logger_write(LOG_ERR, "  dest is null. %s", HERE);
    return -1;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -1;
  }

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (-1 == sock) {
    logger_write(LOG_ERR,
      "  getsubnetmask() socket cannot opened. %s", HERE);
    return -1;
  }

  strcpy(ifr.ifr_name, dev);
  stats = ioctl(sock, SIOCGIFNETMASK, &ifr);

  if (-1 == stats) {
    close(sock);
    logger_write(LOG_ERR,
      "  getsubnetmask() ioctl:SIOCGIFNETMASK error[%d] %s", stats, HERE);
    return -2;
  } else {
    memcpy(&sin, &ifr.ifr_netmask, sizeof(struct sockaddr_in));
    strcpy(dest, inet_ntoa(sin.sin_addr));
    close(sock);
  }
  return 0;
}


/**
 * 引数で指定したインタフェースにサブネットマスクを設定する
 *
 * @param[in] addr 設定するマスク(4オクテット表記)
 * @param[in] dev マスクを設定するインタフェース名
 * @retval  0 正常終了
 * @retval -1 socketのオープンに失敗
 * @retval -2 ioctlシステムコール失敗
 */
int setsubnetmask(const char* addr, const char* dev)
{
  struct sockaddr_in sin;
  struct ifreq ifr;
  int stats = 0;
  int sock  = 0;

  if (NULL == addr || (NULL != addr && '\0' == *addr)) {
    logger_write(LOG_ERR, "  addr is null. %s", HERE);
    return -1;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -1;
  }

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (-1 == sock) {
    logger_write(LOG_ERR,
      "  setsubnetmask() socket cannot opened. %s", HERE);
    return -1;
  }

  strcpy(ifr.ifr_name, dev);
  memset(&sin, 0, sizeof(struct sockaddr_in));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = inet_addr(addr);
  memcpy(&ifr.ifr_netmask, &sin, sizeof(struct sockaddr_in));

  stats = ioctl(sock, SIOCSIFNETMASK, &ifr);
  close(sock);

  if (-1 == stats) {
    logger_write(LOG_ERR,
      "  setsubnetmask() ioctl:SIOCSIFNETMASK error[%d] %s", stats, HERE);
    return -2;
  }
  logger_write(LOG_DEBUG,
    "  setsubnetmask() netmask was assigned[%s] %s", addr, HERE);
  return 0;
}


/**
 * 引数で指定したインタフェースにアサインされたブロードキャストアドレスを取得する
 *
 * @param[in,out] dest 取得したアドレスを書き込む領域へのポインタ
 * @param[in] dev アドレスを取得したいインタフェース名
 * @retval  0 正常終了
 * @retval -1 socketのオープンに失敗
 * @retval -2 ioctlシステムコール失敗
 */
int getbroadcast(char* dest, const char* dev)
{
  struct sockaddr_in sin;
  struct ifreq ifr;
  int stats = 0;
  int sock  = 0;

  if (NULL == dest || (NULL != dest && '\0' == *dest)) {
    logger_write(LOG_ERR, "  dest is null. %s", HERE);
    return -1;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -1;
  }

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (-1 == sock) {
    logger_write(LOG_ERR,
      "  getbroadcast() socket cannot opened. %s", HERE);
    return -1;
  }

  strcpy(ifr.ifr_name, dev);
  stats = ioctl(sock, SIOCGIFBRDADDR, &ifr);
  close(sock);

  if (-1 == stats) {
    logger_write(LOG_ERR,
      "  getbroadcast() ioctl:SIOCGIFBRDADDR error[%d] %s", stats, HERE);
    return -2;
  } else {
    memcpy(&sin, &ifr.ifr_broadaddr, sizeof(struct sockaddr_in));
    strcpy(dest, inet_ntoa(sin.sin_addr));
  }
  return 0;
}


/**
 * 引数で指定したインタフェースにブロードキャストアドレスを設定する
 *
 * @param[in] addr 設定するブロードキャストアドレス(4オクテット表記)
 * @param[in] dev ブロードキャストアドレスを設定するインタフェース名
 * @retval  0 正常終了
 * @retval -1 socketのオープンに失敗
 * @retval -2 ioctlシステムコール失敗
 */
int setbroadcast(const char* addr, const char* dev)
{
  struct sockaddr_in sin;
  struct ifreq ifr;
  int stats = 0;
  int sock  = 0;

  if (NULL == addr || (NULL != addr && '\0' == *addr)) {
    logger_write(LOG_ERR, "  addr is null. %s", HERE);
    return -1;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -1;
  }

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (-1 == sock) {
    logger_write(LOG_ERR,
      "  setbroadcast() socket cannot opened. %s", HERE);
    return -1;
  }

  strcpy(ifr.ifr_name, dev);
  memset(&sin, 0, sizeof(struct sockaddr_in));
  sin.sin_family = AF_INET;
  sin.sin_addr.s_addr = inet_addr(addr);
  memcpy(&ifr.ifr_broadaddr, &sin, sizeof(struct sockaddr_in));

  stats = ioctl(sock, SIOCSIFBRDADDR, &ifr);
  close(sock);

  if (-1 == stats) {
    logger_write(LOG_ERR,
      "  setbroadcast() ioctl:SIOCSIFBRDADDR error[%d] %s", stats, HERE);
    return -2;
  }
  logger_write(LOG_DEBUG,
    "  setbroadcast() broadcast was assigned[%s] %s", addr, HERE);
  return 0;
}


/**
 * 引数で指定したインタフェースにルーティングを追加する
 *
 * @param[in] addr インタフェースに設定するIPv4アドレス(4オクテット表記)
 * @param[in] mask ルーティングに対応するサブネットマスク(4オクテット表記)
 * @param[in] dev ルーティングを設定するインタフェース名
 * @retval  0 正常終了
 * @retval -1 socketのオープンに失敗
 * @retval -2 ioctlシステムコール失敗
 */
int setroute(const char* addr, const char* mask, const char* dev)
{
  struct sockaddr_in gw, dst, msk;
  struct rtentry rt;
  char netaddr[BUF_IPADDR];
  int stats = 0;
  int sock  = 0;

  if (NULL == addr || (NULL != addr && '\0' == *addr)) {
    logger_write(LOG_ERR, "  addr is null. %s", HERE);
    return -1;
  }
  if (NULL == mask || (NULL != mask && '\0' == *mask)) {
    logger_write(LOG_ERR, "  mask is null. %s", HERE);
    return -1;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -1;
  }

  memset(netaddr, 0, sizeof(char) * BUF_IPADDR);
  strncpy(netaddr, calc_network(addr, mask), BUF_IPADDR);

  logger_write(LOG_DEBUG,
    "  setroute(): address=%s mask=%s %s", addr, mask, HERE);
  logger_write(LOG_DEBUG,
    "    calculated network address:%s %s", netaddr, HERE);

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (-1 == sock) {
    logger_write(LOG_ERR,
      "  setroute() socket cannot opened. %s", HERE);
    return -1;
  }

  memset(&rt, 0, sizeof(rt));

  memset(&gw, 0, sizeof(struct sockaddr_in));
  gw.sin_family = AF_INET;
  gw.sin_addr.s_addr = INADDR_ANY;

  memset(&dst, 0, sizeof(struct sockaddr_in));
  dst.sin_family = AF_INET;
  dst.sin_addr.s_addr = inet_addr(netaddr);

  memset(&msk, 0, sizeof(struct sockaddr_in));
  msk.sin_family = AF_INET;
  msk.sin_addr.s_addr = inet_addr(mask);

  memcpy(&rt.rt_dst, &dst, sizeof(dst));
  memcpy(&rt.rt_genmask, &msk, sizeof(msk));
  memcpy(&rt.rt_gateway, &gw, sizeof(gw));

  rt.rt_flags = RTF_UP;
  rt.rt_dev = (char *) dev;
  rt.rt_metric = 0;

  stats = ioctl(sock, SIOCADDRT, &rt);
  close(sock);

  if (0 > stats) {
    logger_write(LOG_ERR,
      "  setroute() ioctl:SIOCADDRT error[%d] %s", stats, HERE);
    return -2;
  }
  logger_write(LOG_DEBUG,
    "  setroute() added new route[%s/%s] %s", netaddr, mask, HERE);
  return 0;
}


/**
 * 引数で指定したインタフェースにゲートウェイを設定する
 *
 * @param[in] addr 設定するゲートウェイ(4オクテット表記)
 * @param[in] dev ゲートウェイを設定するインタフェース名
 * @retval  0 正常終了
 * @retval -1 socketのオープンに失敗
 * @retval -2 ioctlシステムコール失敗
 */
int setgateway(const char* addr, const char* dev)
{
  struct sockaddr_in gw, dst, msk;
  struct rtentry rt;
  int stats = 0;
  int sock  = 0;

  if (NULL == addr || (NULL != addr && '\0' == *addr)) {
    logger_write(LOG_ERR, "  addr is null. %s", HERE);
    return -1;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -1;
  }

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (-1 == sock) {
    logger_write(LOG_ERR,
      "  setgateway() socket cannot opened. %s", HERE);
    return -1;
  }

  memset(&rt, 0, sizeof(rt));

  memset(&gw, 0, sizeof(gw));
  gw.sin_family = AF_INET;
  gw.sin_addr.s_addr = inet_addr(addr);

  memset(&dst, 0, sizeof(dst));
  dst.sin_family = AF_INET;
  dst.sin_addr.s_addr = INADDR_ANY;

  memset(&msk, 0, sizeof(msk));
  msk.sin_family = AF_INET;
  msk.sin_addr.s_addr = INADDR_ANY;

  rt.rt_flags = RTF_UP | RTF_GATEWAY;
  rt.rt_dev = (char *) dev;
  rt.rt_metric = 0;

  memcpy(&rt.rt_dst, &dst, sizeof(dst));
  memcpy(&rt.rt_genmask, &msk, sizeof(msk));
  memcpy(&rt.rt_gateway, &gw, sizeof(gw));

  stats = ioctl(sock, SIOCADDRT, &rt);
  close(sock);

  if (0 > stats) {
    logger_write(LOG_ERR,
      "  setgateway() ioctl:SIOCADDRT error[%d] %s", stats, HERE);
    return -2;
  }
  logger_write(LOG_DEBUG,
    "  setgateway() added new gateway[%s] %s", addr, HERE);
  return 0;
}


/**
 * 引数で指定したインタフェースのルートを空にする
 *
 * @param[in] dev ゲートウェイを設定するインタフェース名
 * @retval  0 正常終了
 */
int emptyroutes(const char* dev)
{
  const char* readcmdstr  = "route -n|grep %s|awk '{print $1 \" \" $3 \" \" $5}'";
  const char* writecmdstr = "ip route del %s metric %d dev %s";
  char readcmd[BUF_128];
  char writecmd[BUF_128];
  char writearg[BUF_64];
  char line[BUF_64];
  char addr[BUF_IPADDR];
  char mask[BUF_IPADDR];
  char metr[8];
  unsigned int metric;
  FILE* fp;

  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return -1;
  }

  memset(readcmd, 0, sizeof(char) * BUF_128);
  sprintf(readcmd, readcmdstr, dev);
  if (NULL != (fp = popen(readcmd, "r"))) {
    memset(line, 0, sizeof(char) * BUF_64);
    while (NULL != fgets(line, sizeof(line), fp)) {
      memset(addr, 0, sizeof(char) * BUF_IPADDR);
      memset(mask, 0, sizeof(char) * BUF_IPADDR);
      memset(metr, 0, sizeof(char) * 8);
      if (3 == sscanf(line, "%s %s %s\n", addr, mask, metr)) {
        memset(writecmd, 0, sizeof(char) * BUF_128);
        memset(writearg, 0, sizeof(char) * BUF_64);
        logger_write(LOG_DEBUG, "  emptyroutes() scanned routes: %s", addr, HERE);
        logger_write(LOG_DEBUG, "    addr   = %s %s", addr, HERE);
        logger_write(LOG_DEBUG, "    mask   = %s %s", mask, HERE);
        logger_write(LOG_DEBUG, "    metric = %s %s", metr, HERE);
        if (0 == strncmp(addr, "0.0.0.0", 7)) {
          strcat(writearg, "default");
        } else {
          sprintf(writearg, "%s/%s", addr, mask);
        }
        metric = (unsigned int) strtoul(metr, (char **) NULL, 10);

        sprintf(writecmd, writecmdstr, writearg, metric, dev);
        logger_write(LOG_DEBUG,
          "    deleted route:%s %s", writecmd, HERE);
        system(writecmd);
      }
    }
    pclose(fp);
  }
  return 0;
}


/**
 * 引数で指定したインタフェースにアサインされたIPv6アドレスを取得する
 *
 * @param[in,out] dest 取得したアドレスを書き込む領域へのポインタ
 * @param[in] dev アドレスを取得したいインタフェース名
 */
void getipv6addr(char* dest, const char* dev)
{
  struct ifaddrs* if_list = NULL;
  struct ifaddrs* ifa = NULL;
  void* tmp = NULL;

  if (NULL == dest || (NULL != dest && '\0' == *dest)) {
    logger_write(LOG_ERR, "  dest is null. %s", HERE);
    return;
  }
  if (NULL == dev || (NULL != dev && '\0' == *dev)) {
    logger_write(LOG_ERR, "  dev is null. %s", HERE);
    return;
  }

  getifaddrs(&if_list);
  for (ifa = if_list; ifa != NULL; ifa = ifa->ifa_next) {
    if (0 == strcmp(ifa->ifa_name, dev)) {
      if (!ifa->ifa_addr) {
        /* 非リンクアップ・若しくはアドレスのアサイン無し */
        continue;
      } else {
        if (AF_INET6 == ifa->ifa_addr->sa_family) {
          tmp = &((struct sockaddr_in6 *) ifa->ifa_addr)->sin6_addr;
          inet_ntop(AF_INET6, tmp, dest, INET6_ADDRSTRLEN);
          break;
        } else {
          /* sa_family が AF_PACKET(17) または AF_INET6(23) */
        }
      }
    }
  }
  freeifaddrs(if_list);
}

/**
 * クラウド上の監視カメラIPアドレス情報を更新する
 *
 * get_abox_ipaddrが実行されていること
 *
 * @param[in] addr 新しく保存するIPアドレス構造体へのポインタ
 * @param[in] idx SMC-3A Plusに関連付けられた監視カメラの添番
 * @retval  0 正常終了
 * @retval -1 端末固有キー生成に失敗
 * @retval -2 curl実行に失敗
 * @retval -3 POST用メモリ確保(malloc)に失敗
 * @retval -4 アクティベーション
 */
int push_abox_ipaddr(void)
{
  char* data;
  char* response;
  char* cmdstr;
  int api_result;
  size_t buffer_len;
  static ipaddr_t s_prev_addr;
  static int s_count = 0;

  if (s_count == 0) {
    memset(&s_prev_addr, 0, sizeof(s_prev_addr));
    s_count = 1;
  }

  //アクティベーションチェック
  if(true != shm_ptr->activ_status) {
    return -4;
  }

  buffer_len = sizeof(char) * BUF_1024;
  data     = (char *) malloc(buffer_len);
  response = (char *) malloc(buffer_len);
  cmdstr   = (char *) malloc(buffer_len);
  if (NULL == data || NULL == response || NULL == cmdstr) {
    if (NULL != data) {
      free(data);
    }
    if (NULL != response) {
      free(response);
    }
    if (NULL != cmdstr) {
      free(cmdstr);
    }
    return -3;
  }

  if (strcmp(s_prev_addr.ipaddr, shm_ptr->runtime_addr.ipaddr) != 0 ||
      strcmp(s_prev_addr.netmask, shm_ptr->runtime_addr.netmask) != 0 ||
      strcmp(s_prev_addr.gateway, shm_ptr->runtime_addr.gateway) != 0 ||
      strcmp(s_prev_addr.dns1, shm_ptr->runtime_addr.dns1) != 0) {
    logger_write(LOG_DEBUG, "prev_addr and runtime_addr is found diffrenet. %s" , HERE);
    strcpy_safe(s_prev_addr.ipaddr, shm_ptr->runtime_addr.ipaddr);
    strcpy_safe(s_prev_addr.netmask, shm_ptr->runtime_addr.netmask);
    strcpy_safe(s_prev_addr.gateway, shm_ptr->runtime_addr.gateway);
    strcpy_safe(s_prev_addr.dns1, shm_ptr->runtime_addr.dns1);
  }
  else {
    logger_write(LOG_DEBUG, "prev_addr and runtime_addr is not found diffrenet, setip skip %s" , HERE);
    if (NULL != data) {
      free(data);
    }
    if (NULL != response) {
      free(response);
    }
    if (NULL != cmdstr) {
      free(cmdstr);
    }
    return 0;
  }

  sprintf(data, "id=%d&ipaddr=%s&netmask=%s&gateway=%s&dns1=%s&dns2=",
    shm_ptr->id,
    shm_ptr->runtime_addr.ipaddr,
    shm_ptr->runtime_addr.netmask,
    shm_ptr->runtime_addr.gateway,
    shm_ptr->runtime_addr.dns1
  );
  if (0 < strlen(shm_ptr->runtime_addr.dns2)) {
    strncat(data, shm_ptr->runtime_addr.dns2, strlen(shm_ptr->runtime_addr.dns2));
  }
  api_result = get_api_response(response, buffer_len,
    AGENTBOX_CFG_API_CAMERA_SETIP, data
  );
  if (0 == api_result) {
    //msqueue_send("sshtunnel");
  }
  free(cmdstr);
  free(response);
  free(data);

  return api_result;
}
#endif // AGENTBOX_IPADDR_C
