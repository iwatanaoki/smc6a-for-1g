/**
 * @file    process.c
 * @brief   外部コマンド実行とステータス回収
 * @author  cool-revo inc.
 */
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <errno.h>
#include <sys/stat.h>
#include <linux/limits.h>
#include "process.h"
#include "logger.h"


/**
 * pidファイルに対応するプロセスが存在するかをチェックし、
 * 状態によってプログラムを開始する
 *
 * @todo check_processの結果が-3の場合、psからprogimageの存在をチェック
 *
 * @param[in] pidfile pidファイルのパス
 * @param[in] progimage プログラム名
 * @retval  1 新プロセスとして実行
 * @retval  0 プロセスは生存
 * @retval -1 実行不能
 */
int check_run_process(const char* pidfile, const char* progimage)
{
  int proc;

  proc = check_process(pidfile);
  switch (proc) {
    case 0:
      return 0;
    case -1:
      if (0 == remove(pidfile)) {
        run_system(progimage);
      }
      return 1;
    case -2:
      return -1;
    case -3:
      run_system(progimage);
      return 1;
    default:
      return -1;
  }
}


/**
 * ゾンビ対策用system()ラッパー
 *
 * 遺言を聞き届けた後にそれを投げ捨てて無かったことにする
 *
 * @param[in] progimage 実行コマンドの文字列
 * @retval  0 コマンドが正常終了した
 * @retval  0,-255以外 WEXITSTATUSで取得したコマンドの終了状態
 * @retval -255 引数で渡されたコマンド文字列が空である
 */
int run_system(const char* progimage)
{
  int estatus;
  int code;

  if (NULL == progimage) {
    logger_write(LOG_ERR, "  run_system() progimage is null. %s", HERE);
    return -255;
  }

  if (0 == strlen(progimage)) {
    return -255;
  }
  logger_write(LOG_DEBUG, "  run_system() spawn new process: %s", HERE);
  code = system(progimage);
  estatus = WEXITSTATUS(code);
  if (0 == estatus) {
    logger_write(LOG_DEBUG,
      "  run_system() successfully finished. %s", HERE);
    logger_write(LOG_DEBUG,
      "  program image is %s %s", progimage, HERE);
    return 0;
  } else {
    logger_write(LOG_WARNING,
      "  run_system() finished with NON-ZERO status code(%d). %s", estatus, HERE);
    logger_write(LOG_INFO,
      "  program image is %s %s", progimage, HERE);
    return estatus;
  }
}


/**
 * pidファイルに対応するプロセスが存在するかをチェックする
 *
 * @param[in] pidfile pidファイルのパス
 * @retval  0 プロセスは生存
 * @retval -1 pidに対応するprocが存在しない(=プロセスなしとする)
 * @retval -2 pidファイルが存在しない(=プロセスなしとする)
 * @retval -3 pidのパースに失敗
 */
int check_process(const char* pidfile)
{
  char path[32];
  char pidstr[16];
  char* pidstr_p;
  struct stat sb;
  pid_t pid = 0;
  FILE* fp;

  if (NULL == pidfile || (NULL != pidfile && '\0' == *pidfile)) {
    logger_write(LOG_ERR, "  check_process() pidfile is null. %s", HERE);
    return -4;
  }

  if (NULL == (fp = fopen(pidfile, "r"))) {
    return -3;
  }
  while (NULL != fgets(pidstr, sizeof(pidstr), fp)) {
    pidstr_p = strchr(pidstr, '\n');
    if (NULL != pidstr_p) {
      *pidstr_p = '\0';
    }
    pid = (pid_t) strtoul(pidstr, (char **) NULL, 10);
  }
  fclose(fp);
  if (0 == pid) {
    return -2;
  }
  snprintf(path, 31, "/proc/%d", pid);
  if (0 != stat(path, &sb)) {
    return -1;
  }
  return 0;
}


/**
 * 引数で渡したプログラム名に対応するプロセスがあったら終了する
 *
 * プロセスの検索は pgrep -f で実行するため、プロセスイメージと
 * 同じ書式にする必要がある(フルパス表記ならパス付きで)
 *
 * @param[in] progimage 終了するプログラムのプロセス名
 * @retval    0 プロセスの終了に成功
 * @retval -250 引数で指定したプログラムが見つからない
 * @retval -252 プロセスを検索するpgrepに失敗
 * @retval それ以外 プロセス終了時にプログラムから渡されたステータスコード
 */
int kill_process(const char* progimage)
{
  int estatus = 0;
  int code;
  char grepcmd[NAME_MAX];
  char pidstr[16];
  char* pidstr_p;
  pid_t progpid;
  FILE* fp;

  if (NULL == progimage || (NULL != progimage && '\0' == *progimage)) {
    logger_write(LOG_ERR, "  kill_process() progimage is null. %s", HERE);
    return -1;
  }

  memset(grepcmd, 0, sizeof(char) * NAME_MAX);
  sprintf(grepcmd, "pgrep -f '%s'", progimage);

  logger_write(LOG_DEBUG,
    "  kill_process() find PID to kill with pgrep: %s %s", grepcmd, HERE);

  if (NULL != (fp = popen(grepcmd, "r"))) {

    while (NULL != fgets(pidstr, sizeof(pidstr), fp)) {
      pidstr_p = strchr(pidstr, '\n');
      if (NULL != pidstr_p) {
        *pidstr_p = '\0';
      }
      progpid = (unsigned int) strtoul(pidstr, (char **) NULL, 10);
      if (0 < progpid) {

        logger_write(LOG_DEBUG, "  PID for killing is %d. %s", progpid, HERE);

        code = kill(progpid, SIGTERM);
        estatus = WEXITSTATUS(code);
        if (0 == estatus) {
          logger_write(LOG_DEBUG,
            "  Process successfully killed.[%s] %s", progimage, HERE);
        } else {
          logger_write(LOG_ERR,
            "  Killing process was exited with abnormal status.[%d] %s",
            estatus, HERE);
          switch (errno) {
            case EINVAL:
              logger_write(LOG_ERR,
                "  Specified SIGNAL is invalid.[SIGTERM] %s", HERE);
              break;
            case EPERM:
              logger_write(LOG_ERR,
                "  We have no permission to send SIGNALs. %s", HERE);
              break;
            case ESRCH:
              logger_write(LOG_ERR,
                "  Specified PID is not found.[%d] %s", progpid, HERE);
              break;
            default:
              logger_write(LOG_ERR,
                "  For some other reason, the process cannot killed %s", HERE);
              break;
          }
        }
      } else {
        logger_write(LOG_ERR, "  Invalid PID was found, ignored. %s", HERE);
        estatus = -250;
      }
    }
    pclose(fp);
  } else {
    logger_write(LOG_ERR, "  NO PID was found. %s", HERE);
    estatus = -252;
  }
  logger_write(LOG_DEBUG, "  kill_process() finished. %s", HERE);

  return estatus;
}
