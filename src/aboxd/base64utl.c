#ifndef BASE64UTL_C
#define BASE64UTL_C

#include "base64utl.h"
 
static const char encDat[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
 
int encode_base64(char *src_dat, int src_siz, char *dst_dat, int *dst_siz)
{
    int remain;
    int enc_siz;
    int i;
    char tmp[4];
 
    if ((NULL == src_dat) || (0 >= src_siz) || (NULL == dst_dat) || (NULL == dst_siz))
    {
        printf("[ERR] invalid param.\n");
        return -1;
    }
 
    remain = src_siz;
    enc_siz = 0;
    i = 0;
 
    while (0 < remain)
    {
        if (3 <= remain)
        {
            tmp[0] = ((src_dat[3*i + 0] >> 2) & 0x3F);
            tmp[1] = (((src_dat[3*i + 0] << 4) & 0x30) | ((src_dat[3*i + 1] >> 4) & 0xF));
            tmp[2] = (((src_dat[3*i + 1] << 2) & 0x3C) | ((src_dat[3*i + 2] >> 6) & 0x3));
            tmp[3] = (src_dat[3*i + 2] & 0x3F);
            dst_dat[4*i + 0] = encDat[tmp[0]];
            dst_dat[4*i + 1] = encDat[tmp[1]];
            dst_dat[4*i + 2] = encDat[tmp[2]];
            dst_dat[4*i + 3] = encDat[tmp[3]];
            remain -= 3;
        }
        else if (2 == remain)
        {
            tmp[0] = ((src_dat[3*i + 0] >> 2) & 0x3F);
            tmp[1] = (((src_dat[3*i + 0] << 4) & 0x30) | ((src_dat[3*i + 1] >> 4) & 0xF));
            tmp[2] = ((src_dat[3*i + 1] << 2) & 0x3C);
            dst_dat[4*i + 0] = encDat[tmp[0]];
            dst_dat[4*i + 1] = encDat[tmp[1]];
            dst_dat[4*i + 2] = encDat[tmp[2]];
            dst_dat[4*i + 3] = '=';
            remain -= 2;
        }
        else  /* 1 == remain */
        {
            tmp[0] = ((src_dat[3*i + 0] >> 2) & 0x3F);
            tmp[1] = ((src_dat[3*i + 0] << 4) & 0x30);
            dst_dat[4*i + 0] = encDat[tmp[0]];
            dst_dat[4*i + 1] = encDat[tmp[1]];
            dst_dat[4*i + 2] = '=';
            dst_dat[4*i + 3] = '=';
            remain -= 1;
        }
        enc_siz += 4;
        i++;
    }
    *dst_siz = enc_siz;
 
    return 0; /* SUCCESS */
}
#endif // BASE64UTL_C
