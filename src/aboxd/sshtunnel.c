#ifndef AGENTBOX_SSHTUNNEL_C
#define AGENTBOX_SSHTUNNEL_C
/**
 * @file    sshtunnel.c
 * @brief   SSHトンネル接続・解除・再接続
 * @author  cool-revo inc.
 */
#include <sys/stat.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "abox_config.h"
#include "shm.h"
#include "logger.h"
#include "auth.h"
#include "json.h"
#include "process.h"
#include "sshtunnel.h"
#include "util.h"


/** 共有メモリ構造体を指すポインタ。実体はaboxd.c */
extern ABOX_SHM* shm_ptr;


/**
 * SSHトンネリング専用の秘密鍵をクラウドから取得する
 * i_mode(I) 0: pem check
 *           1: pem no check
 *
 * @retval  0 正常終了
 * @retval -1 端末固有キー生成に失敗
 * @retval -2 curl実行に失敗
 * @retval -3 レスポンスjsonのパースに失敗
 * @retval -4 秘密鍵ファイル保存のファイルポインタ生成に失敗
 * @retval -100 秘密鍵保存先パスの事前チェックに失敗
 */
int get_tunnelkey(int i_mode, int i_kind)
{
  char url[BUF_200];
  char data[BUF_32];
  char response[BUF_2048];
  char pem[BUF_2048];
  char vip[BUF_32];
  char portstr[BUF_32];
  int api_result;
  int verified;
  int estatus = 0;
  FILE* fp = (FILE*)NULL;
  int l_kind = 1; // 1: web access via ssh tunnel, 2: ssh shell access.

  if (i_kind < 1 || 2 < i_kind) {
    logger_write(LOG_ALERT,
      "  i_kind(%d) must be specified 1 or 2. force set default value 1. %s", i_kind, HERE);
  }
  else {
    logger_write(LOG_DEBUG, "  i_kind is %d. %s", i_kind, HERE);
    l_kind = i_kind;
  }

  logger_write(LOG_DEBUG, "  i_mode is %d. %s", i_mode, HERE);

   
  if (i_mode == 0) { // if i_mode is 0, check pem.
    /* 既にSSH鍵がある場合は動作をキャンセル */
    verified = validate_pem(SSH_TUNNELLING_KEY, SSH_TUNNELLING_FAILURES);
    logger_write(LOG_DEBUG, "  call validate_pem, verified:%d. %s", verified, HERE);
    switch (verified) {
      case 0:
        /* alive status ssh tunnel at this time, IF it is NG, call get_api_response */
        if (shm_ptr->sshtunnel[l_kind-1].server_vip[0] != '\0' && 
            shm_ptr->sshtunnel[l_kind-1].server_port > 0) {
          logger_write(LOG_DEBUG,
            "  validate_pem() check is normal. returns 0 %s", HERE);
          return 0;
        }
        else {
          logger_write(LOG_DEBUG,
            "  server_vip:%s server_port:%d is not set, nessesary reauth. %s",
              shm_ptr->sshtunnel[l_kind-1].server_vip, 
              shm_ptr->sshtunnel[l_kind-1].server_port, HERE);
        }
      case -1:
        /* through */
      case -2:
        logger_write(LOG_ALERT,
          "  validate_pem() returns %d %s", verified, HERE);
        logger_write(LOG_ALERT,
          "  tunnelling key does not exist, or is corrupted. try to fetch new one. %s", HERE);
        break;
      case -3:
        logger_write(LOG_ERR,
          "  validate_pem() returns %d %s", verified, HERE);
        logger_write(LOG_ERR,
          "  tunnelling key exists, but fopen() failed. store failed counts and retry later. %s", HERE);
        return -100;
      default:
        logger_write(LOG_EMERG,
          "  validate_pem() returns %d %s", verified, HERE);
        logger_write(LOG_EMERG,
          "  tunnelling key path is not regular file. %s", HERE);
        logger_write(LOG_EMERG,
          "  **THIS AGENTBOX IS CORRUPTED. PLEASE REPLACE NEW ONE**. %s", HERE);
        return -100;
    }
  }
  else { // if i_mode is not 0, no check pem.
    logger_write(LOG_NOTICE,
      "  mode is %d, no pem check. %s", i_mode, HERE);
  }

  memset(response, 0, sizeof(char) * BUF_2048);
  memset(pem,      0, sizeof(char) * BUF_2048);
  memset(url     , 0, sizeof(char) * BUF_200);
  memset(data,     0, sizeof(char) * BUF_32);

  // device type = 3 (camera)
  sprintf(data, "devicetype=3&kind=%d", l_kind);
  strcpy(url, AGENTBOX_CFG_AUTH_TUNNEL_KEY);
  api_result = get_api_response_otherserver(response,
    sizeof(char) * BUF_2048, url, data, shm_ptr->tunnelurl
  );
  if (0 == api_result) {
    // get private key 
    if (0 == json_simple_val(pem, response, "private_key")) {
      if (NULL != (fp = fopen(SSH_TUNNELLING_KEY, "wb"))) {
        fputs(pem, fp);
        fclose(fp);
        chmod(SSH_TUNNELLING_KEY, 0600);
        logger_write(LOG_DEBUG,
          "  tunnelling key write success. %s", HERE);
      } else {
        estatus = -4;
      }
    } else {
      estatus = -3;
    }
    // get server vip
    if (0 == json_simple_val(vip, response, "server_vip")) {
      logger_write(LOG_DEBUG,
        "  tunnelling server_vip:%s. %s", vip, HERE);
      strcpy(shm_ptr->sshtunnel[l_kind-1].server_vip, vip);      
    } else {
      estatus = -5;
    }
    // get server asinged port
    if (0 == json_simple_val(portstr, response, "server_port")) {
      logger_write(LOG_DEBUG,
        "  tunnelling server_port:%s. %s", portstr, HERE);
      shm_ptr->sshtunnel[l_kind-1].server_port = atoi(portstr);
    } else {
      estatus = -6;
    }

  } else {
    estatus = api_result;
  }
  return estatus;
}

/**
 * SSHトンネリングstatus check
 *
 */
int check_status_tunnel(int i_kind)
{
  char url[BUF_200];
  char data[BUF_32];
  char response[BUF_2048];
  char pem[BUF_2048];
  char vip[BUF_32];
  char portstr[BUF_32];
  int api_result;
  int verified;
  int estatus = 0;
  FILE* fp;
  int l_kind = 1; // 1: web access via ssh tunnel, 2: ssh shell access.

  if (i_kind < 1 || 2 < i_kind) {
    logger_write(LOG_ALERT,
      "  i_kind(%d) must be specified 1 or 2. force set defalt value 1. %s", i_kind, HERE);
  }
  else {
    logger_write(LOG_DEBUG, "  i_kind is %d. %s", i_kind, HERE);
    l_kind = i_kind;
  }
   

  memset(response, 0, sizeof(char) * BUF_2048);
  memset(pem,      0, sizeof(char) * BUF_2048);
  memset(url     , 0, sizeof(char) * BUF_200);
  memset(data,     0, sizeof(char) * BUF_32);

  // device type = 3 (camera)
  sprintf(data, "devicetype=3&kind=%d", l_kind);
  strcpy(url, AGENTBOX_CFG_CHECK_TUNNEL);
  api_result = get_api_response_otherserver(response,
    sizeof(char) * BUF_2048, url, data, shm_ptr->tunnelurl
  );
  if (0 == api_result) {
    // exist target_rel_tunnel_devices record.
    // get server vip
    if (0 == json_simple_val(vip, response, "server_vip")) {
      logger_write(LOG_DEBUG,
        "  tunnelling server_vip:%s. %s", vip, HERE);
      strcpy(shm_ptr->sshtunnel[l_kind-1].server_vip, vip);      
    } else {
      estatus = -5;
    }
    // get server asinged port
    if (0 == json_simple_val(portstr, response, "server_port")) {
      logger_write(LOG_DEBUG,
        "  tunnelling server_port:%s. %s", portstr, HERE);
      shm_ptr->sshtunnel[l_kind-1].server_port = atoi(portstr);
    } else {
      estatus = -6;
    }

  } else {
    estatus = api_result;
  }
  return estatus;
}


/**
 * SSHトンネリング専用の秘密鍵を廃棄する
 *
 * @retval  0 正常終了
 * @retval -1 端末固有キー廃棄に失敗
 * @retval -2 curl実行に失敗
 * @retval -3 レスポンスjsonのパースに失敗
 * @retval -5 秘密鍵ファイルが存在しない
 */
int delete_tunnelkey(void)
{
  int estatus = 0;
/*
  char data[BUF_32];
  char response[BUF_1024];
  char pem[BUF_32];
  int api_result;
  struct stat st;

  if (0 != stat(SSH_TUNNELLING_KEY, &st)) {
    return -5;
  }

  memset(response, 0, sizeof(char) * BUF_1024);
  memset(pem,      0, sizeof(char) * BUF_32);
  memset(data,     0, sizeof(char) * BUF_32);

  sprintf(data, "id=%d", shm_ptr->id);
  api_result = get_api_response(response,
    sizeof(char) * BUF_1024,
    AGENTBOX_CFG_API_RM_TUNNEL_KEY, data
  );
  if (0 == api_result) {
    if (0 == json_simple_val(pem, response, "private_key")) {
      if (0 == strcmp(pem, "deleted")) {
        remove(SSH_TUNNELLING_KEY);
        estatus = 0;
      } else {
        estatus = -1;
      }
    } else {
      estatus = -3;
    }
  } else {
    estatus = api_result;
  }
*/

  return estatus;
}


/**
 * SSHトンネルをstatus check
 *
 *
 * @retval  0 正常終了
 * @retval -1 トンネル用秘密鍵がない、あるいは壊れている
 */
int store_ssh_tunnels(int i_kind)
{
  struct stat st;
  char* prev;
  char** forwarders;
  int rows;
  int csservers;
  int prev_exists = -1;
  int row_exists;
  int row_duplicate;
  int i, j, k, l, idx;
  size_t buffer_len;
  FILE* p_list;
  FILE* p_prev;
  FILE* p_diff;
  int l_rtn = 0;
  int l_mode;
  int l_kind = 1; // 1: web access via ssh tunnel, 2: ssh shell access.
  int l_result = 0;
  char l_cmdstr[BUF_512];

  logger_write(LOG_DEBUG, " store_ssh_tunnels(kind:%d) start. %s", i_kind, HERE);

  if (i_kind < 1 || 2 < i_kind) {
    logger_write(LOG_ALERT,
      "  i_kind(%d) must be specified 1 or 2. force set defalt value 1. %s", i_kind, HERE);
  }
  else {
    logger_write(LOG_DEBUG, "  i_kind is %d. %s", i_kind, HERE);
    l_kind = i_kind;
  }

  // if wondergate is disabled, not open sshtunnel.
  if (1 == shm_ptr->disabled) {
    l_result = -5;
    logger_write(LOG_DEBUG, " agentbox is disabled, not call get_tunnelkey. return to parent:%d. %s", l_result, HERE);
    return l_result;
  }

  if (l_kind == 2) {
    // kind is ssh terminal access mode and remotehelp is not ON, error skip.
    if (shm_ptr->remotehelp != 1) {
      l_result = -6;
      logger_write(LOG_DEBUG, " remotehelp(%d) is not ON, not call get_tunnelkey. return to parent:%d. %s", shm_ptr->remotehelp, l_result, HERE);
      return l_result;
    }
    else {
      logger_write(LOG_DEBUG, " remotehelp(%d) is ON, call get_tunnelkey.  %s", shm_ptr->remotehelp, HERE);
    }
  }

  if (shm_ptr->sshtunnel[l_kind-1].server_vip[0] == '\0' ||
      shm_ptr->sshtunnel[l_kind-1].server_port <= 1) {
    l_mode = 1; // pem no check
  }
  else {
    l_mode = 0; // pem check
    l_rtn = check_status_tunnel(l_kind);
    if (l_rtn != 0) {
      if (l_rtn != -5) {
        logger_write(LOG_ERR, " check_status_tunnel is failed:rtn:%d, %s", l_rtn, HERE);
      }
      else {
        logger_write(LOG_WARNING, " check_status_tunnel is failed:rtn:%d, %s", l_rtn, HERE);
      }
      sprintf(l_cmdstr, "^ssh .* -p9876 .* %d:*:.* rmtods@%s",
        shm_ptr->sshtunnel[l_kind-1].server_port,
        shm_ptr->sshtunnel[l_kind-1].server_vip
      );
      logger_write(LOG_DEBUG, " kill ssh process cmd:%s, %s", l_cmdstr, HERE);
      kill_process(l_cmdstr);
      l_mode = 1; // pem no check
    }
    else {
      l_result = 1;
      logger_write(LOG_INFO, " ssh tunnel status is normal, rtn:%d,return to parent:%d, %s", 
        l_rtn, l_result, HERE);
      // ssh exist check 
      sprintf(l_cmdstr, "pgrep -f '^ssh .* -p9876 .* %d:*:.* rmtods@%s' > /dev/null 2>&1",
        shm_ptr->sshtunnel[l_kind-1].server_port,
        shm_ptr->sshtunnel[l_kind-1].server_vip
      );
      logger_write(LOG_DEBUG, " ssh process exist check cmd:%s, %s", l_cmdstr, HERE);
      l_rtn = WEXITSTATUS(system(l_cmdstr));
      if (l_rtn != 0) { // not exist
        logger_write(LOG_WARNING, " ssh process not exist check cmd:%s, rtn:%d, %s", l_cmdstr, l_rtn, HERE);
        l_mode = 1; // pem no check
      }
      else {
        return l_result;
      }
    }
  }
  l_rtn = get_tunnelkey(l_mode, l_kind);
  logger_write(LOG_DEBUG, " get_tunnelkey(mode:%d,kind:%d) return %d. %s", 
     l_mode, l_kind, l_rtn, HERE);
  if (l_rtn != 0) {
    l_result = -1;
    logger_write(LOG_ERR, " get_tunnelkey(mode:%d,kind:%d) failed, rtn:%d, return to parent:%d. %s", 
       l_mode, l_kind, l_rtn, l_result, HERE);
    return l_result;
  }
  else {
    l_result = 0;
    logger_write(LOG_INFO, " get_tunnelkey(mode:%d,kind:%d) success, rtn:%d, return to parent:%d. %s", 
       l_mode, l_kind, l_rtn, l_result, HERE);
    return l_result;
  }

  return 0;
}


/**
 * 新方式のSSHTUNNELサーバにSSHトンネルを開く
 *
 * @param[in] i_kind
 * @retval  0 正常終了
 * @retval -1 秘密鍵が存在しない
 * @retval -2 トンネル維持するためのfwrds.listの読み込みに失敗
 */
int open_needed_tunnels(int i_kind)
{
  char row[BUF_200];
  char cmdstr[BUF_2048];
  int pid_check = 0;
  int row_alive = 0;
  int rawstatus = 0;
  int estatus = 0;
  int l_rtn = 0;
  int l_kind = 1;
  //int l_local_destport = 81;
  int l_local_destport = 80;

  logger_write(LOG_DEBUG, " open_needed_tunnels(kind:%d) start. %s", i_kind, HERE);

  if (i_kind < 1 || 2 < i_kind) {
    logger_write(LOG_ALERT,
      "  i_kind(%d) must be specified 1 or 2. force set defalt value 1. %s", i_kind, HERE);
  }
  else {
    logger_write(LOG_DEBUG, "  i_kind is %d. %s", i_kind, HERE);
    l_kind = i_kind;
  }

  // if agentbox is disabled, not open sshtunnel.
  if (1 == shm_ptr->disabled) {
    l_rtn = -3;
    logger_write(LOG_DEBUG, " agentbox is disabled, not open tunnel. return %d. %s", l_rtn, HERE);
    return l_rtn;
  }

  sprintf(cmdstr, "^ssh .* -p9876 .* %d:*:.* rmtods@%s .*",
    shm_ptr->sshtunnel[l_kind-1].server_port,
    shm_ptr->sshtunnel[l_kind-1].server_vip
  );
  logger_write(LOG_DEBUG, " kill ssh process cmd:%s, %s", cmdstr, HERE);
  kill_process(cmdstr);

  /* トンネルを開く */
  //if (0 != row_alive) {
    switch(l_kind) {
      case 1:  // web access via ssh tunnel
        l_local_destport = SSH_TUNNELLING_MAPPING_PORT;
        break;
      case 2:  // ssh terminal access
        l_local_destport = SSH_TUNNELLING_TERMINAL_MAPPING_PORT;
        break;
    }
    memset(cmdstr, 0, sizeof(cmdstr));
    sprintf(cmdstr, SSH_TUNNELLING_CMD,
      SSH_TUNNELLING_PORT,
      SSH_TUNNELLING_KEY,
      shm_ptr->sshtunnel[l_kind-1].server_port, l_local_destport,
      SSH_TUNNELLING_USR, shm_ptr->sshtunnel[l_kind-1].server_vip
    );
    logger_write(LOG_INFO, "  Open new process:[%s] %s", cmdstr, HERE);
    if (0 != run_system(cmdstr)) {
      estatus = add_failcounter(SSH_TUNNELLING_FAILURES);
      switch (estatus) {
        case -1:
          logger_write(LOG_ERR,
            "  SSH tunnelling failure counts cannot save[write error]. %s", HERE);
          break;
        case -2:
          logger_write(LOG_ERR,
            "  SSH tunnelling failure counts cannot save[read error]. %s", HERE);
          break;
        default:
          logger_write(LOG_DEBUG,
            "  SSH tunnelling failure counts successfully saved. %s", HERE);
          break;
      }
    }

    memset(row, 0, sizeof(row));
    return 0;
}


/**
 * 配信サーバに対するSSHトンネルのプロセスが存在するかをチェックし、
 * プロセスが起動していればそのPIDを返す
 *
 * 重複しているSSHトンネルがある場合は、一旦すべて停止する
 *
 * @param
 * @retval  0以上 正常終了(トンネルプロセスが存在する)
 * @retval  0 正常終了(トンネルプロセスは存在しない)
 * @retval -1 pgrepプロセスが正常ステータス以外で終了
 * @retval -2 pgrep実行のためのファイルポインタ生成に失敗
 */
int kill_duplicated_tunnels()
{
  return 0;
}


/**
 * 既に管理下にないカメラのsshトンネリングが存在する場合
 * プロセスを終了する
 *
 * ここで終了可能なsshプロセスはあくまで管理下にないカメラに
 * 関係するものだけで、重複プロセスは考慮に入れない
 * i_mode : 1 とすると、remotehelper 以外は強制的に終了する(disabled = 1ではなくても）
 *
 * @return 0　正常終了
 */
int kill_unneeded_tunnels(int i_mode)
{
  char row[BUF_200];
  char cmdstr[BUF_200];
  FILE* l_pp = (FILE*)NULL;
  char *l_cp = (char*)NULL;
  int  l_pid = 0;
  int  l_rtn = 0;

  logger_write(LOG_DEBUG, " start kill_unneeded_tunnels, %s", HERE);
  sprintf(cmdstr, "pgrep -f \"^ssh .* -p9876 .*\"");
  logger_write(LOG_DEBUG, " execute:%s , %s", cmdstr, HERE);
  l_pp = popen(cmdstr, "r");
  if (l_pp == (FILE*)NULL) {
    logger_write(LOG_DEBUG, " cannot get proccess id, %s", HERE);
    return -1;
  }

  while (NULL != (fgets(row, sizeof(row), l_pp))) {
    if ((l_cp = strchr(row, '\n')) != (char*)NULL) {
      *l_cp = '\0';
    }
    l_pid = atoi(row);
    if (l_pid <= 0) {
      logger_write(LOG_DEBUG, " cannot get proccess id, %s", HERE);
      continue;
    }
    if (1 == shm_ptr->disabled || i_mode == 1) {
      logger_write(LOG_WARNING, " this SMC-3A Plus is disabled or i_mode == 1, all ssh process force stop(pid:%d), %s", l_pid, HERE);
      kill(l_pid, SIGTERM); 
      continue;
    }
    sprintf(cmdstr, 
      "netstat -antp | egrep \".*[ |\\t]* %d\\/ssh\" | awk '{print $6}' | grep ESTABLISHED", 
      l_pid);

    logger_write(LOG_DEBUG, " session status check:%s, %s", cmdstr, HERE);
    l_rtn = 0;
    l_rtn = WEXITSTATUS(system(cmdstr));
    if (l_rtn != 0) {
      logger_write(LOG_ALERT, " exists abnormal session(pid:%d), force shutdown it, %s", 
        l_pid, HERE);
      kill(l_pid, SIGTERM);
    }
  }
  pclose(l_pp);
  l_pp = (FILE*)NULL;

  if (1 == shm_ptr->disabled || i_mode == 1) {
    logger_write(LOG_DEBUG, " this unit is disabled or i_mode == 1, server info clear, %s", HERE);
    shm_ptr->sshtunnel[0].server_vip[0] = '\0';
    shm_ptr->sshtunnel[0].server_port = 0;
  }

  // not use remotehelp mode
  if (shm_ptr->remotehelp != 1) {
    logger_write(LOG_DEBUG, " remotehelp(%d) is not set, check status ssh for remotehelp, %s", 
      shm_ptr->remotehelp, HERE);
    if (shm_ptr->sshtunnel[1].server_vip[0] != '\0' &&
        shm_ptr->sshtunnel[1].server_port > 0) {
      sprintf(cmdstr, "pgrep -f \"^ssh .* -p9876 .* %d:*:.* rmtods@%s.*\"",
        shm_ptr->sshtunnel[1].server_port,
        shm_ptr->sshtunnel[1].server_vip);
      logger_write(LOG_DEBUG, " get remotehelp sssh process pid, execute:%s , %s", cmdstr, HERE);
      l_pp = popen(cmdstr, "r");
      if (l_pp == (FILE*)NULL) {
        logger_write(LOG_DEBUG, " cannot get proccess id of remotehelp ssh, %s", HERE);
        shm_ptr->sshtunnel[1].server_vip[0] = '\0';
        shm_ptr->sshtunnel[1].server_port = 0;
        return -2;
      }

      while (NULL != (fgets(row, sizeof(row), l_pp))) {
        if ((l_cp = strchr(row, '\n')) != (char*)NULL) {
          *l_cp = '\0';
        }
        l_pid = atoi(row);
        if (l_pid <= 0) {
          logger_write(LOG_DEBUG, " cannot get proccess id of remote help ssh, %s", HERE);
          continue;
        }
        logger_write(LOG_WARNING, " remotehelp(%d) is OFF, can get proccess id(%d) of remote help ssh, force stop, %s", 
          shm_ptr->remotehelp, l_pid, HERE);
        kill(l_pid, SIGTERM);
      }
      pclose(l_pp);
      l_pp = (FILE*)NULL;
      shm_ptr->sshtunnel[1].server_vip[0] = '\0';
      shm_ptr->sshtunnel[1].server_port = 0;
    }
    else {
      logger_write(LOG_DEBUG, " connect server info is not set for remotehelp, %s", HERE);
    }
  }


  return 0;
}


/**
 * 全てのsshトンネリングプロセスを終了する
 *
 * @return kill_processの戻り値
 * @sa <process.c>
 */
int killall_tunnels(void)
{
  return kill_process("^ssh -fgnNT .*[^s]rmtods@");
}
#endif // AGENTBOX_SSHTUNNEL_C
