### libini #####################################################

LIBINI_SRCDIR=$(ABSDIR)/libini
ifeq "$(strip $(LIBINI_SRCDIR))" ""
  LIBINI_SRCDIR=.
endif
LIBINI_SRCS=$(notdir $(wildcard $(LIBINI_SRCDIR)/*.c))
LIBINI_OBJS=$(notdir $(LIBINI_SRCS:.c=.o))
LIBINI_TGTS=libini.a

LIBINI_CFLAGS=-I../include -g -O2 $(CFLAGS)

################################################################

$(LIBINI_OBJS):
	cd $(LIBINI_SRCDIR); \
	$(CC) $(LIBINI_CFLAGS) -c $(@F:.o=.c) -o $@

$(LIBINI_TGTS): $(LIBINI_OBJS)
	cd $(LIBINI_SRCDIR); \
	$(AR) rcs $@ $(LIBINI_OBJS)

libini: $(LIBINI_TGTS)

libini-install: libini install-dir
	cp $(LIBINI_SRCDIR)/$(LIBINI_TGTS) $(INSTALL_DIR)$(DISTDIR)/usr/lib/$(LIBINI_TGTS)

libini-uninstall: libini
	$(RM) $(INSTALL_DIR)$(DISTDIR)/usr/lib/$(LIBINI_TGTS)

libini-clean:
	cd $(LIBINI_SRCDIR); \
	$(RM) $(LIBINI_TGTS) *.o

libini-dry:
	@cd $(LIBINI_SRCDIR); \
	echo "LIBINI_SRCS='$(LIBINI_SRCS)'"; \
	echo "LIBINI_OBJS='$(LIBINI_OBJS)'"; \
	echo "LIBINI_TGTS='$(LIBINI_TGTS)'"
