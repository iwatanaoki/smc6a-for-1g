/**
 * @file    ini.c
 * @brief   windows設定ファイル.iniをパースするライブラリ
 */
#include <stdio.h>
#include <ctype.h>
#include <string.h>

#include "ini.h"

#if INI_USE_STACK == 0
#include <stdlib.h>
#endif

/**
 * @def MAX_SECTION
 * iniファイルに含む事ができる最大セクション数
 */
#define MAX_SECTION 100
/**
 * @def MAX_NAME
 * キー名とすることができる最長文字数
 */
#define MAX_NAME    50


static char* rstrip(char*);
static char* lskip(const char*);
static char* find_chars_or_comment(const char*, const char*);
static char* strncpy0(char*, const char*, size_t);


/** 
 * 引数で与えられた文字列の位置から後ろにある空白を除去して返す
 *
 * @param[in] str 空白をトリムする文字列
 * @return 空白除去済みの文字列へのポインタ
 */
static char* rstrip(char* str)
{
  char* p = str + strlen(str);
  while (p > str && isspace((unsigned char)(*--p))) {
    *p = '\0';
  }
  return str;
}

/**
 * 引数で与えられた文字列で空白以外の文字が左から最初に出現する位置のポインタを返す
 *
 * @param[in] str 検索対象文字列
 * @return 空白以外の文字が現れる位置のポインタ
 */
static char* lskip(const char* str)
{
  while (*str && isspace((unsigned char)(*str))) {
    str++;
  }
  return (char *) str;
}

/**
 * 引数で与えられた文字列の文字またはインラインコメントが
 * 最初に出現する位置のポインタを返す
 *
 * 文字またはインラインコメントが存在しない場合は
 * 検索対象とした文字列をそのまま返す
 *
 * INI_ALLOW_INLINE_COMMENTS=1の場合に使用
 * インラインコメントの開始表現の前には空白が必ず必要
 *
 * @param[in] str 検索対象文字列
 * @param[in] chars 検索する文字
 * @retval charsのうちどれかが存在する場合は最初に出現する位置を指すポインタ
 * @retval charsのどの文字も存在しない場合はstrのポインタを返す
 */
static char* find_chars_or_comment(const char* str, const char* chars)
{
#if INI_ALLOW_INLINE_COMMENTS != 0
  int was_space = 0;
  while (*str && (!chars || !strchr(chars, *str)) && !(was_space && strchr(INI_INLINE_COMMENT_PREFIXES, *str))) {
    was_space = isspace((unsigned char)(*str));
    str++;
  }
#else
  while (*str && (!chars || !strchr(chars, *str))) {
    str++;
  }
#endif
  return (char *)str;
}

/**
 * \0デリミタを追加した文字列srcをdestにstrncpyする
 *
 * @param[in,out] dest 文字列をコピーする対象
 * @param[in] src コピーする文字列
 * @param[in] size コピーする文字数
 * @return デリミタを追加した文字列へのポインタ
 */
static char* strncpy0(char* dest, const char* src, size_t size)
{
  strncpy(dest, src, size);
  dest[size - 1] = '\0';
  return dest;
}


/* ini.h DocComment参照 */
int ini_parse_stream(ini_reader reader, void* stream, ini_handler handler, void* user)
{
#if INI_USE_STACK == 0
  char* line;
#else
  char line[INI_MAX_LINE];
#endif
  char section[MAX_SECTION] = "";
  char prev_name[MAX_NAME]  = "";
  char* start;
  char* end;
  char* name;
  char* value;
  int lineno = 0;
  int error  = 0;

  if (NULL == user) {
    return -1;
  }

#if INI_USE_STACK == 0
  line = (char *) malloc(INI_MAX_LINE);
  if (!line) {
    return -2;
  }
#endif
  while (NULL != reader(line, INI_MAX_LINE, stream)) {
    lineno++;
    
    start = line;
#if INI_ALLOW_BOM != 0
    /* BOMを許可する場合はBOMのバイト分スキップ */
    if ((1 == lineno)
    &&  ((unsigned char) start[0] == 0xEF)
    &&  ((unsigned char) start[1] == 0xBB)
    &&  ((unsigned char) start[2] == 0xBF)) {
      start += 3;
    }
#endif
    start = lskip(rstrip(start));
    if (*start == ';' || *start == '#') {
      /* Python configparserの場合 ; と # の両文字をコメント行先頭に置けるので、これらも除外 */
    }
#if INI_ALLOW_MULTILINE != 0
    else if (*prev_name && *start && start > line) {
      /* 空白文字から開始される行は前のセクション・キーまたは値の続きとして扱う */
      if (!handler(user, section, prev_name, start) && !error) {
        error = lineno;
      }
    }
#endif
    else if (*start == '[') {
      /* '[' で始まる行はセクション行(の筈) */
      end = find_chars_or_comment(start + 1, "]");
      if (*end == ']') {
        *end = '\0';
        strncpy0(section, start + 1, sizeof(section));
        *prev_name = '\0';
      } else if (!error) {
        /* セクション終了文字 ']' が見つからない場合は error に行番号を入れる */
        error = lineno;
      }
    } else if (*start) {
      /* コメント行でない場合は キー名[=:]値 が存在(する筈) */
      end = find_chars_or_comment(start, "=:");
      if (*end == '=' || *end == ':') {
        *end = '\0';
        name = rstrip(start);
        value = lskip(end + 1);
#if INI_ALLOW_INLINE_COMMENTS != 0
        end = find_chars_or_comment(value, NULL);
        if (*end) {
          *end = '\0';
        }
#endif
        rstrip(value);
        /* キー名[=:]値のペアがあったら handler をコールする */
        strncpy0(prev_name, name, sizeof(prev_name));
        if (!handler(user, section, name, value) && !error) {
          error = lineno;
        }
      } else if (!error) {
        /* キー名[=:]値のペアが見つからない場合は error に行番号を入れる */
        error = lineno;
      }
    }
#if INI_STOP_ON_FIRST_ERROR != 0
    if (error) {
      break;
    }
#endif
  }
#if !INI_USE_STACK
  free(line);
#endif
  
  return error;
}

/* ini.h DocComment参照 */
int ini_parse_file(FILE* file, ini_handler handler, void* user)
{
  return ini_parse_stream((ini_reader)fgets, file, handler, user);
}

/* ini.h DocComment参照 */
int ini_parse(const char* filename, ini_handler handler, void* user)
{
  FILE* file = (FILE*)NULL;
  int rtn = 0;

  if (NULL == user) {
    fprintf(stderr, "ini_parse::user is null\n");
    fflush(stderr);
    return(-1);
  }
  if (NULL == filename || (NULL != filename && '\0' == *filename)) {
    fprintf(stderr, "ini_parse::filename is null\n");
    fflush(stderr);
    return(-1);
  }

  file = fopen(filename, "r");
  if (NULL == file) {
    fprintf(stderr, "ini_parse::file is null\n");
    fflush(stderr);
    return(-1);
  }
  rtn = ini_parse_file(file, handler, user);
  if (rtn < 0) {
    fprintf(stderr, "ini_parse::after call ini_parse_file(), rtn:%d\n", rtn);
    fflush(stderr);
  }
  fclose(file);
  return(rtn);
}
