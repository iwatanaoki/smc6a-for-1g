### cr_nslookup ###################################################

CR_NSLOOKUP_SRCDIR=$(ABSDIR)/cr_nslookup
ifeq "$(strip $(CR_NSLOOKUP_SRCDIR))" ""
  CR_NSLOOKUP_SRCDIR=.
endif
CR_NSLOOKUP_SRCS=$(notdir $(wildcard $(CR_NSLOOKUP_SRCDIR)/*.c))
CR_NSLOOKUP_OBJS=$(notdir $(CR_NSLOOKUP_SRCS:.c=.o))
CR_NSLOOKUP_TGTS=cr_nslookup
#CR_NSLOOKUP_LIBS=-lm -lini -lcrypto -lssl -lcurl
CR_NSLOOKUP_LIBS=-lm

CR_NSLOOKUP_DEPS=

################################################################

$(CR_NSLOOKUP_OBJS):
	cd $(CR_NSLOOKUP_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c $(@F:.o=.c) -o $@

$(CR_NSLOOKUP_TGTS): $(CR_NSLOOKUP_OBJS) cr_nslookup-depnds
	cd $(CR_NSLOOKUP_SRCDIR); \
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LINKER_FLAGS) $(LDFLAGS) $(CR_NSLOOKUP_OBJS) $(CR_NSLOOKUP_DEPS) -o $@ $(CR_NSLOOKUP_LIBS)
ifeq ($(BUILD_TYPE),Release)
	cd $(CR_NSLOOKUP_SRCDIR); \
	$(STRIP) $@
endif

cr_nslookup: $(CR_NSLOOKUP_TGTS)

cr_nslookup-depnds:
	cd $(CR_NSLOOKUP_SRCDIR);

cr_nslookup-install: cr_nslookup install-dir
	cp $(CR_NSLOOKUP_SRCDIR)/$(CR_NSLOOKUP_TGTS) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(CR_NSLOOKUP_TGTS)

cr_nslookup-uninstall: cr_nslookup
	$(RM) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(CR_NSLOOKUP_TGTS)

cr_nslookup-clean:
	cd $(CR_NSLOOKUP_SRCDIR); \
	$(RM) $(CR_NSLOOKUP_TGTS) *.o

cr_nslookup-dry:
	@cd $(CR_NSLOOKUP_SRCDIR); \
	echo "CR_NSLOOKUP_SRCS='$(CR_NSLOOKUP_SRCS)'"; \
	echo "CR_NSLOOKUP_OBJS='$(CR_NSLOOKUP_OBJS)'"; \
	echo "CR_NSLOOKUP_TGTS='$(CR_NSLOOKUP_TGTS)'"; \
	echo "CR_NSLOOKUP_LIBS='$(CR_NSLOOKUP_LIBS)'"
