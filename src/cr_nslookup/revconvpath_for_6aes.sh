#!/bin/sh

for i in *.c
do
  FOUND=`grep "/www/mtd/coolrevo" $i | wc -l`
  if [ ${FOUND} -ne 0 ]; then
    echo "/www/mtd/coolrevo found in $i"
    sed 's/\/www\/mtd/coolrevo/\/mtd/coolrevo/g' $i > $i.tmp
    mv $i.tmp $i
    echo "/www/mtd/coolrevo -> /mtd/coolrevo recovered in $i"
  fi
done

exit
