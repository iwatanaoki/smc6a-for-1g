/**
 * @file    ffcheck.c
 * @brief   ffmpeg のログ標準出力を監視し、一定時間出力が亡くなった場合、
 * 第一、第二パラメータで指定した文字列をふくむ ffmpeg プロセスを pgrep 
 * で調べ、そのIPをSIGKILLで強制終了し、自分自身も終了する
 * @author  cool-revo inc.
 */
#include <stdio.h>
#include <stdlib.h>
#include <netdb.h>
#include <errno.h>

/*!
 * @brief      ホスト名を表示する
 * @param[in]  host  ホストエントリ
 */
static void
print_hostname(struct hostent *host)
{
    int i;

    /* 正式ホスト名を表示する */
    fprintf(stdout, "genelic hostname = %s\n", host->h_name);

    /* エイリアス名一覧を表示する */
    for(i=0; host->h_aliases[i]; i++){
        fprintf(stdout, "alias(%d)         = %s\n", i, host->h_aliases[i]);
    }
}

/*!
 * @brief      IPアドレス(IPv4)を表示する
 * @param[in]  host  ホストエントリ
 */
static void
print_ipaddr_v4(struct hostent *host)
{
    int i;

    /* IPv4ならば4バイト */
    if(host->h_length != 4){
        fprintf(stdout, "IPv6 address.");
        return;
    }

    for(i=0; host->h_addr_list[i]; i++){
        fprintf(stdout, "IP address(%d)    = %d.%d.%d.%d\n" , i, 
                (unsigned char)*((host->h_addr_list[i])) ,
                (unsigned char)*((host->h_addr_list[i]) + 1) ,
                (unsigned char)*((host->h_addr_list[i]) + 2) ,
                (unsigned char)*((host->h_addr_list[i]) + 3)
        );
    }
}

/*!
 * @brief      ホストの名前解決を行う。
 * @param[in]  hostname  ホスト名
 * @return     成功ならば0、失敗ならば-1を返す。
 */
static int
resolve_hostname(char *hostname)
{
    struct hostent *host = NULL;

    /* ホスト情報を取得する */
    host = gethostbyname(hostname);
    if(host == NULL){
        fprintf(stderr, "gethostbyname() failed: %s\n", strerror(errno));
        return(-1);
    }

    /* ホスト情報を出力する */
    print_hostname(host);
    print_ipaddr_v4(host);

    return(0);
}

/*!
 * @brief   main routine
 * @return  成功ならば0、失敗ならば-1を返す。
 */
int
main(int argc, char *argv[])
{
    int rc = 0;

    if(argc != 2){
        fprintf(stdout, "%s <hostname>\n", argv[0]);
        return(-1);
    }

    rc = resolve_hostname(argv[1]);
    if(rc != 0){
        return(-1);
    }

    return(0);
}

