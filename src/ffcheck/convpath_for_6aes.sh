#!/bin/sh

for i in *.c
do
  FOUND=`grep "/coolrevo" $i | wc -l`
  if [ ${FOUND} -ne 0 ]; then
    echo "/coolrevo found in $i"
    sed 's/\/coolrevo/\/www\/coolrevo/g' $i > $i.tmp
    mv $i.tmp $i
    echo "/coolrevo -> /www/coolrevo replace in $i"
  fi
done

exit
