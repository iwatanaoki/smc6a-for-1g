### ffcheck ###################################################

FFCHECK_SRCDIR=$(ABSDIR)/ffcheck
ifeq "$(strip $(FFCHECK_SRCDIR))" ""
  FFCHECK_SRCDIR=.
endif
FFCHECK_SRCS=$(notdir $(wildcard $(FFCHECK_SRCDIR)/*.c))
FFCHECK_OBJS=$(notdir $(FFCHECK_SRCS:.c=.o))
FFCHECK_TGTS=ffcheck
#FFCHECK_LIBS=-lm -lini -lcrypto -lssl -lcurl
FFCHECK_LIBS=-lm

FFCHECK_DEPS=

################################################################

$(FFCHECK_OBJS):
	cd $(FFCHECK_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c $(@F:.o=.c) -o $@

$(FFCHECK_TGTS): $(FFCHECK_OBJS) ffcheck-depnds
	cd $(FFCHECK_SRCDIR); \
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LINKER_FLAGS) $(LDFLAGS) $(FFCHECK_OBJS) $(FFCHECK_DEPS) -o $@ $(FFCHECK_LIBS)
ifeq ($(BUILD_TYPE),Release)
	cd $(FFCHECK_SRCDIR); \
	$(STRIP) $@
endif

ffcheck: $(FFCHECK_TGTS)

ffcheck-depnds:
	cd $(FFCHECK_SRCDIR);

ffcheck-install: ffcheck install-dir
	cp $(FFCHECK_SRCDIR)/$(FFCHECK_TGTS) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(FFCHECK_TGTS)

ffcheck-uninstall: ffcheck
	$(RM) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(FFCHECK_TGTS)

ffcheck-clean:
	cd $(FFCHECK_SRCDIR); \
	$(RM) $(FFCHECK_TGTS) *.o

ffcheck-dry:
	@cd $(FFCHECK_SRCDIR); \
	echo "FFCHECK_SRCS='$(FFCHECK_SRCS)'"; \
	echo "FFCHECK_OBJS='$(FFCHECK_OBJS)'"; \
	echo "FFCHECK_TGTS='$(FFCHECK_TGTS)'"; \
	echo "FFCHECK_LIBS='$(FFCHECK_LIBS)'"
