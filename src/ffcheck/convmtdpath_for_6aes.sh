#!/bin/sh

for i in *.c
do
  FOUND=`grep "/mtd/coolrevo" $i | wc -l`
  if [ ${FOUND} -ne 0 ]; then
    echo "/mtd/coolrevo found in $i"
    sed 's/\/mtd/coolrevo/\/mtd\/mtd/coolrevo/g' $i > $i.tmp
    mv $i.tmp $i
    echo "/mtd/coolrevo -> /mtd/mtd/coolrevo replace in $i"
  fi
done

exit
