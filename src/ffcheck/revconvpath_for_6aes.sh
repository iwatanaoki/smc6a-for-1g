#!/bin/sh

for i in *.c
do
  FOUND=`grep "/www/coolrevo" $i | wc -l`
  if [ ${FOUND} -ne 0 ]; then
    echo "/www/coolrevo found in $i"
    sed 's/\/www\/coolrevo/\/coolrevo/g' $i > $i.tmp
    mv $i.tmp $i
    echo "/www/coolrevo -> /coolrevo recovered in $i"
  fi
done

exit
