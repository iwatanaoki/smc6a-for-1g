#!/bin/sh

for i in *.c
do
  FOUND=`grep "/mtd/mtd/coolrevo" $i | wc -l`
  if [ ${FOUND} -ne 0 ]; then
    echo "/mtd/mtd/coolrevo found in $i"
    sed 's/\/mtd\/mtd/coolrevo/\/mtd/coolrevo/g' $i > $i.tmp
    mv $i.tmp $i
    echo "/mtd/mtd/coolrevo -> /mtd/coolrevo recovered in $i"
  fi
done

exit
