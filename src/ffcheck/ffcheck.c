/**
 * @file    ffcheck.c
 * @brief   ffmpeg のログ標準出力を監視し、一定時間出力が亡くなった場合、
 * 第一、第二パラメータで指定した文字列をふくむ ffmpeg プロセスを pgrep 
 * で調べ、そのIPをSIGKILLで強制終了し、自分自身も終了する
 * @author  cool-revo inc.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <signal.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>

#include "aboxd.h"


#define DEF_ALARM_THR_SEC  15  // 15sec
#define DEF_FFCHECK_LOGFILE "/mtd/coolrevo/var/log/application.log"

#define DEF_FFMPEG_DOWN_TIME "/mtd/coolrevo/tmp/ffmpeg_down_time.txt"
#define DEF_FFMPEG_START_TIME "/mtd/coolrevo/tmp/ffmpeg_start_time.txt"

#define DATE_LENGTH         64
#define BUFF_LENGTH_SHORT   64
#define BUFF_LENGTH        512
#define BUFF_LENGTH_LONG  2048
#define FNAME_LENGTH       256

#define OK_LOGCOUNT_THR    80

#define DEF_LOG_ERR   1
#define DEF_LOG_WRN   2
#define DEF_LOG_INF   3
#define DEF_LOG_DTL   4
#define DEF_LOG_DBG   5

#define logMessage(a,c){ffcheckLogMessage(a,__FILE__,__LINE__,c);}
#define logMessage2(a,fmt, ...) {ffcheckLogMessage2(a,__FILE__,__LINE__,fmt,__VA_ARGS__);}


/* 外部変数 */
int g_logLevel = DEF_LOG_DBG;
char g_uniquekey[BUFF_LENGTH_SHORT];
char g_mode[BUFF_LENGTH_SHORT];
char g_mycmd[FNAME_LENGTH];

char g_start[DATE_LENGTH];
char g_down[DATE_LENGTH];


/* プロトタイプ */
void sigcatch(int);
void ffcheckLogMessage(int, char*, int, char*);
void ffcheckLogMessage2(int, char*, int, char*, ...);
static char* getnowdate(char*);
void writedate(char*, char*);

void createDisconnectFlg(void);
void deleteDisconnectFlg(void);




/**
 * メイン処理
 *
 * @param[in] argc コマンドラインオプション数
 * @param[in] argv コマンドラインオプションを表す配列
 * @return int 終了ステータス
 */
int main(int argc, char** argv)
{
  char l_buff[BUFF_LENGTH_LONG];
  int l_rtn = 0;
  size_t l_size = 0;

  int l_started = 0;

  int l_okcount = 0;

  struct stat st;

  /* シグナルの処理 */
  signal(SIGPIPE, sigcatch);
  signal(SIGINT,  sigcatch);
  signal(SIGTERM, sigcatch);
  signal(SIGALRM, sigcatch);
  signal(SIGSEGV, sigcatch);
  signal(SIGBUS, sigcatch);

  if (argc != 3) {
    logMessage2(DEF_LOG_ERR,
      "parameters(count:%d) is wrong. 1:uniquekey, 2:LIVE or VOD.", argc);
    exit(1);
  }

  strcpy(g_mycmd, argv[0]);

  strcpy(g_uniquekey, argv[1]);
  strcpy(g_mode, argv[2]);

  logMessage2(DEF_LOG_DBG,
    "uniquekey:%s, mode:%s", g_uniquekey, g_mode);

  memset(g_start, 0, sizeof(g_start));
  memset(g_down, 0, sizeof(g_down));

  alarm(DEF_ALARM_THR_SEC);

  while (1) {
    memset(l_buff, 0, sizeof(l_buff));
    //getnowdate(g_down);
    l_size = read(fileno(stdin), l_buff, sizeof(l_buff));
    if (l_size < 0) {
      logMessage2(DEF_LOG_ERR,
        "fread() error:(%d), exit,uniquekey:%s, mode:%s", 
        l_size, g_uniquekey, g_mode);
      l_rtn = l_size;
      break;
    }
    else if (l_size > 0) {
      l_rtn = write(fileno(stdout), l_buff, l_size);
      if (l_rtn < 0) {
        logMessage2(DEF_LOG_ERR,
          "fwrite() error:(%d), exit, uniquekey:%s, mode:%s", 
          l_rtn, g_uniquekey, g_mode);
        break;
      }
      fflush(stdout);
      //if (l_started == 0) {
      //  getnowdate(g_start);
      //  l_started = 1;
      //}
    }
    else {
      logMessage2(DEF_LOG_ERR,
        "fwrite() read 0 byte, exit, uniquekey:%s, mode:%s",
        g_uniquekey, g_mode);
      l_rtn = 2;
      //if (stat(DEF_FFMPEG_START_TIME, &st) == 0) {
      //  unlink(DEF_FFMPEG_START_TIME);
      //  getnowdate(g_down);
      //  writedate(DEF_FFMPEG_DOWN_TIME, g_down);
      //}
      // disconnect フラグを生成する方式に変更(ping だと接続できないことが
      createDisconnectFlg();
      break;
    }
    alarm(0);
    //if (l_started == 1 && g_start[0] != '\0') {
    //  writedate(DEF_FFMPEG_START_TIME, g_start);
    //  memset(g_start, 0, sizeof(g_start));
    //}
    //memset(g_down, 0, sizeof(g_down));
    alarm(DEF_ALARM_THR_SEC);

    l_okcount++;
    if (l_okcount >= OK_LOGCOUNT_THR) {
      //logMessage2(DEF_LOG_DTL,
      //  "data found, okcount:%d >= %d, uniquekey:%s, mode:%s",
      //  l_okcount, OK_LOGCOUNT_THR, g_uniquekey, g_mode);
      deleteDisconnectFlg();
      l_okcount = 0;
    }
  }

  exit(l_rtn);
}

static char* getnowdate(char* i_datestr) {
  time_t l_now = 0;
  struct tm* l_strtim = (struct tm*)NULL;

  if (i_datestr == (char*)NULL) {
    return((char*)NULL);
  }

  time(&l_now);
  l_strtim = localtime(&l_now);

  sprintf(i_datestr, "%04d-%02d-%02d %02d:%02d:%02d",
    l_strtim->tm_year + 1900,
    l_strtim->tm_mon + 1,
    l_strtim->tm_mday,
    l_strtim->tm_hour,
    l_strtim->tm_min,
    l_strtim->tm_sec
  );
  return(i_datestr);
}


void ffcheckLogMessage(int i_logLevel, char *i_FILE, int i_LINE, char *i_errmsg) {
  char l_buff2[4096];
  char l_msg[4096];
  char l_nowdatestr[DATE_LENGTH];
  FILE *l_fp = (FILE*)NULL;
  char l_mode[8];

  if (i_logLevel > g_logLevel) {
    return;
  }
  if (i_errmsg == (char*)NULL || (i_errmsg != (char*)NULL && *i_errmsg == '\0')) {
    return;
  }

  switch (i_logLevel) {
    case DEF_LOG_ERR:
      strcpy(l_mode, "ERR");
      break;
    case DEF_LOG_WRN:
      strcpy(l_mode, "WRN");
      break;
    case DEF_LOG_INF:
      strcpy(l_mode, "INF");
      break;
    case DEF_LOG_DTL:
      strcpy(l_mode, "DTL");
      break;
    case DEF_LOG_DBG:
      strcpy(l_mode, "DBG");
      break;
  }

  strncpy(l_buff2, i_errmsg, sizeof(l_buff2)-1);
  l_buff2[sizeof(l_buff2)-1] = '\0';

  sprintf(l_msg, "%s :%s::%s:%s:%d:%s", 
    getnowdate(l_nowdatestr), l_mode, g_mycmd, i_FILE, i_LINE, l_buff2);
  l_msg[2048-1] = '\0';

  l_fp = fopen(DEF_FFCHECK_LOGFILE, "a+");
  if (l_fp != (FILE*)NULL) {
    fprintf(l_fp, "%s\n", l_msg);
    fflush(l_fp);
    fclose(l_fp);
    l_fp = (FILE*)NULL;
  }
  else {
    fprintf(stdout, "%s\n", l_msg);
    fflush(stdout);
  }

  return;
}

void ffcheckLogMessage2(int i_logLevel, char *i_FILE, int i_LINE, char *i_fmt, ...) {
  va_list arg;
  char l_buff[8192];
  char l_buff2[4096];
  char l_msg[8192];
  char l_nowdatestr[DATE_LENGTH];
  FILE *l_fp = (FILE*)NULL;
  char l_mode[8];

  if (i_logLevel > g_logLevel) {
    return;
  }

  switch (i_logLevel) {
    case DEF_LOG_ERR:
      strcpy(l_mode, "ERR");
      break;
    case DEF_LOG_WRN:
      strcpy(l_mode, "WRN");
      break;
    case DEF_LOG_INF:
      strcpy(l_mode, "INF");
      break;
    case DEF_LOG_DTL:
      strcpy(l_mode, "DTL");
      break;
    case DEF_LOG_DBG:
      strcpy(l_mode, "DBG");
      break;
  }

  va_start(arg, i_fmt);
  vsprintf(l_buff, i_fmt, arg);
  va_end(arg);

  strncpy(l_buff2, l_buff, sizeof(l_buff2)-1);
  l_buff2[sizeof(l_buff2)-1] = '\0';

  sprintf(l_msg, "%s :%s::%s:%s:%d:%s", 
    getnowdate(l_nowdatestr), l_mode, g_mycmd, i_FILE, i_LINE, l_buff2);
  l_msg[2048-1] = '\0';

  l_fp = fopen(DEF_FFCHECK_LOGFILE, "a+");
  if (l_fp != (FILE*)NULL) {
    fprintf(l_fp, "%s\n", l_msg);
    fflush(l_fp);
    fclose(l_fp);
    l_fp = (FILE*)NULL;
  }
  else {
    fprintf(stdout, "%s\n", l_msg);
    fflush(stdout);
  }

  return;
}

/**
 * シグナル処理のコールバック
 *
 * @param[in] sig シグナル番号
 */
void sigcatch(int i_sig)
{
  char l_buff[BUFF_LENGTH];
  char l_tmpbuff[BUFF_LENGTH];
  int l_rtn = 0;
  FILE *l_pp = (FILE*)NULL;
  int l_pid = -1;
  char *l_cp = (char*)NULL;

  switch(i_sig) {
    case SIGPIPE:
      logMessage2(DEF_LOG_ERR, 
        "SIGPIPE(%d) catched, exit(%d),uniquekey:%s,mode:%s", 
         i_sig, i_sig, g_uniquekey, g_mode);
      exit(i_sig);
      break;
    case SIGINT:
    case SIGTERM:
      logMessage2(DEF_LOG_WRN, 
        "signal(%d) catched, exit(%d),uniquekey:%s,mode:%s", 
          i_sig, i_sig, g_uniquekey, g_mode);
      exit(i_sig);
      break;
    case SIGSEGV:
      logMessage2(DEF_LOG_ERR, 
        "SIGSEGV(%d) catched, exit(%d),uniquekey:%s,mode:%s", 
         i_sig, i_sig, g_uniquekey, g_mode);
      exit(i_sig);
      break;
    case SIGBUS:
      logMessage2(DEF_LOG_ERR, 
        "SIGBUS(%d) catched, exit(%d),uniquekey:%s,mode:%s", 
         i_sig, i_sig, g_uniquekey, g_mode);
      exit(i_sig);
      break;
    case SIGALRM:
      logMessage2(DEF_LOG_WRN, 
        "SIGALRM catched, %d sec over without no data. exit(%d). uniquekey:%s,mode:%s", 
        DEF_ALARM_THR_SEC, i_sig, g_uniquekey, g_mode);
      memset(l_buff, 0, sizeof(l_buff));
      sprintf(l_buff, "pgrep -f \".*ffmpeg .*%s.* .*%s.*\"",
        g_mode, g_uniquekey);
      l_pp = popen(l_buff, "r");
      if (l_pp != (FILE*)NULL) {
        memset(l_tmpbuff, 0, sizeof(l_tmpbuff));
        while (fgets(l_tmpbuff, sizeof(l_tmpbuff), l_pp) != (char*)NULL) {
          if (NULL != (l_cp = strchr(l_tmpbuff, '\n'))) {
            *l_cp = '\0';
            l_pid = atoi(l_tmpbuff);
            break;
          }
        }
        pclose(l_pp);
        l_pp = (FILE*)NULL;
      }
      logMessage2(DEF_LOG_INF, 
        "pid(%d) is found by cmd:%s", l_pid, l_buff);
      if (l_pid > 1) {
        kill(l_pid, SIGKILL);
        logMessage2(DEF_LOG_WRN, 
          "force exit(SIGKILL) pid(%d),uniquekey:%s,mode:%s", 
          l_pid, g_uniquekey, g_mode);
      }
      //if (g_down[0] != '\0') {
      //  writedate(DEF_FFMPEG_DOWN_TIME, g_down);
      //}
      // disconnect フラグを生成する方式に変更(ping だと接続できないことが
      createDisconnectFlg();
      exit(i_sig);
      break;
  }
  return;
}

void writedate(char *i_fname, char *i_date) {
  FILE *l_fp = (FILE*)NULL;

  if (i_fname == (char*)NULL) {
    return;
  }
  if (i_date == (char*)NULL) {
    return;
  }

  if (strcmp(g_mode, "LIVE") != 0) { // LIVE 以外は記録しないようする(LIVEでしか使用しない）
    return;
  }

  l_fp = fopen(i_fname, "w");
  if (l_fp != (FILE*)NULL) {
    fprintf(l_fp, "%s\n", i_date);
    fflush(l_fp);
    fclose(l_fp);
    l_fp = (FILE*)NULL;
  }

  return;
}

void createDisconnectFlg(void) {
  struct stat st;
  FILE *l_fp = (FILE*)NULL;

  if (strcmp(g_mode, "LIVE") != 0) { 
    // LIVE 以外は生成しないようにする(LIVEでしか使用しない）
    return;
  }

  // disconnect フラグを生成する方式に変更(ping だと接続できないことが
  // 頻発するため)
  if (stat(IS_DISCONNECT_RTSPSERVER_FLG, &st) != 0) {
    l_fp = fopen(IS_DISCONNECT_RTSPSERVER_FLG, "w");
    if (l_fp != (FILE*)NULL) {
      fclose(l_fp);
    }
  }

  return;
}

void deleteDisconnectFlg(void) {
  struct stat st;

  if (strcmp(g_mode, "LIVE") != 0) { 
    // LIVE 以外は削除しないようにする(LIVEでしか使用しない）
    return;
  }

  if (stat(IS_DISCONNECT_RTSPSERVER_FLG, &st) == 0) {
    unlink(IS_DISCONNECT_RTSPSERVER_FLG);
  }

  return;
}
