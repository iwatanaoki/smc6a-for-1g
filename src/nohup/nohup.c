#ifndef CR_NOHUP_C
#define CR_NOHUP_C

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define MAXFD 64

int main(int argc, char *argv[]) {
  int i = 0;
  int fd = 0;
  int pid = 0;
  int exit_status = 0;

  //for(i=0; i<MAXFD; i++) {
  //  close(i);
  //}

  if((fd = open("/dev/null", O_RDWR, 0) != -1)){
    dup2(fd, 0);
    dup2(fd, 1);
    dup2(fd, 2);
    //if(fd < 2){
    //  close(fd);
    //}
  }

  execvp(argv[1], &(argv[1]));
  return(exit_status);
}
#endif // CR_NOHUP_C
