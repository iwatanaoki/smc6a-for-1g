### nohup ###################################################

NOHUP_SRCDIR=$(ABSDIR)/nohup
ifeq "$(strip $(NOHUP_SRCDIR))" ""
  NOHUP_SRCDIR=.
endif
NOHUP_SRCS=$(notdir $(wildcard $(NOHUP_SRCDIR)/*.c))
NOHUP_OBJS=$(notdir $(NOHUP_SRCS:.c=.o))
NOHUP_TGTS=nohup
#NOHUP_LIBS=-lm -lini -lcrypto -lssl -lcurl
NOHUP_LIBS=-lm

NOHUP_DEPS=

################################################################

$(NOHUP_OBJS):
	cd $(NOHUP_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c $(@F:.o=.c) -o $@

$(NOHUP_TGTS): $(NOHUP_OBJS) nohup-depnds
	cd $(NOHUP_SRCDIR); \
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $(LINKER_FLAGS) $(LDFLAGS) $(NOHUP_OBJS) $(NOHUP_DEPS) -o $@ $(NOHUP_LIBS)
ifeq ($(BUILD_TYPE),Release)
	cd $(NOHUP_SRCDIR); \
	$(STRIP) $@
endif

nohup: $(NOHUP_TGTS)

nohup-depnds:
	cd $(NOHUP_SRCDIR);

nohup-install: nohup install-dir
	cp $(NOHUP_SRCDIR)/$(NOHUP_TGTS) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(NOHUP_TGTS)

nohup-uninstall: nohup
	$(RM) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(NOHUP_TGTS)

nohup-clean:
	cd $(NOHUP_SRCDIR); \
	$(RM) $(NOHUP_TGTS) *.o

nohup-dry:
	@cd $(NOHUP_SRCDIR); \
	echo "NOHUP_SRCS='$(NOHUP_SRCS)'"; \
	echo "NOHUP_OBJS='$(NOHUP_OBJS)'"; \
	echo "NOHUP_TGTS='$(NOHUP_TGTS)'"; \
	echo "NOHUP_LIBS='$(NOHUP_LIBS)'"
