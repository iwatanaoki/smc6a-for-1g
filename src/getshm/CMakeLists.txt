add_executable(getshm
  getshm.c
)
set(INCLUDE_DIR "../include")
target_include_directories(getshm PUBLIC ${INCLUDE_DIR})

link_directories(
  ${agentbox_SOURCE_DIR}/build/i586-pc-linux-gnu/usr/lib
  ${agentbox_SOURCE_DIR}/build/i586-pc-linux-gnu/lib
  ${agentbox_TOOLCHAIN_ROOT}/lib
)
target_link_libraries(getshm ini)

target_compile_options(getshm PRIVATE -s -O2 -Wall -fno-strict-aliasing -Wstrict-prototypes -Wmissing-prototypes -Wmissing-declarations -Wnested-externs -Wbad-function-cast -Wold-style-definition -Wdeclaration-after-statement)
