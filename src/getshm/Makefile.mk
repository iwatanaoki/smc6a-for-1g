### getshm #####################################################

GETSHM_SRCDIR=$(ABSDIR)/getshm
ifeq "$(strip $(GETSHM_SRCDIR))" ""
  GETSHM_SRCDIR=.
endif
GETSHM_SRCS=$(notdir $(wildcard $(GETSHM_SRCDIR)/*.c))
GETSHM_OBJS=$(notdir $(GETSHM_SRCS:.c=.o))
GETSHM_TGTS=getshm
GETSHM_LIBS=-L$(SMC3APLUSDIR)/libini -lini

################################################################

$(GETSHM_OBJS):
	cd $(GETSHM_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c $(@F:.o=.c) -o $@

$(GETSHM_TGTS): $(GETSHM_OBJS)
	cd $(GETSHM_SRCDIR); \
	$(CC) $(CPPFLAGS) $(CFLAGS) $(LINKER_FLAGS) $(LDFLAGS) $(GETSHM_OBJS) -o $@ $(GETSHM_LIBS)
ifeq ($(BUILD_TYPE),Release)
	cd $(GETSHM_SRCDIR); \
	$(STRIP) $@
endif

getshm-install: $(GETSHM_TGTS) install-dir
	cp $(GETSHM_SRCDIR)/$(GETSHM_TGTS) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(GETSHM_TGTS)

getshm-uninstall: $(GETSHM_TGTS)
	$(RM) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(GETSHM_TGTS)

getshm-clean:
	cd $(GETSHM_SRCDIR); \
	$(RM) $(GETSHM_TGTS) *.o

getshm-dry:
	@cd $(GETSHM_SRCDIR); \
	echo "GETSHM_SRCS='$(GETSHM_SRCS)'"; \
	echo "GETSHM_OBJS='$(GETSHM_OBJS)'"; \
	echo "GETSHM_TGTS='$(GETSHM_TGTS)'"; \
	echo "GETSHM_LIBS='$(GETSHM_LIBS)'"
