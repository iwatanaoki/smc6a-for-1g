/**
 * @file    getshm.c
 * @brief   共有メモリの値を読み取るコマンドユーティリティ
 * @author  cool-revo inc.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/shm.h>
#include "shm.h"


/** 共有メモリ構造体を指すポインタ */
ABOX_SHM* shm_ptr;

/** 共有メモリIDを格納するプレースホルダ */
int shm_id;

/* プロトタイプ */
static int parse_handler(void*, const char*, const char*, const char*);
int shmopen(void);
int shmclose(void);


/**
 * .confファイルのパース用コールバック
 *
 * libini の関数 ini_parse(const char*, ini_handler, void*) のコールバックとして使用する
 *
 * @param[in,out] user 値をコピーする対象へのポインタ。対象に応じて内部でキャストする
 * @param[in] section .confのセクション部(ブラケット[]で囲まれた箇所)の名称
 * @param[in] name    .confのパラメータ名
 * @param[in] value   .confのパラメータの値
 * @retval 0 正常終了(パースエラーなく成功)
 * @sa <libini.h> ini_parse
 */
static int parse_handler(void* user, const char* section, const char* name, const char* value)
{
  config_t* pcfg;
  unsigned int conv;

  pcfg = (config_t *) user;
  if (0 == strcmp(section, "aboxd")) {
    if (0 < strlen(value)) {
      if (0 == strcmp(name, "interval")) {
        conv = strtoul(value, (char **) NULL, 10);
        if (conv < 60) {
          conv = 60;
        }
        pcfg->interval = conv;
      } else if (0 == strcmp(name, "pidfile")) {
        pcfg->pidfile = strdup(value);
      } else if (0 == strcmp(name, "shmfile")) {
        pcfg->shmfile = strdup(value);
      } else if (0 == strcmp(name, "logfile")) {
        pcfg->logfile  = strdup(value);
      } else if (0 == strcmp(name, "cacert")) {
        pcfg->cacert = strdup(value);
      } else if (0 == strcmp(name, "server")) {
        pcfg->server = strdup(value);
      } else if (0 == strcmp(name, "csurl")) {
        pcfg->csurl = strdup(value);
      } else if (0 == strcmp(name, "tunnelurl")) {
        pcfg->tunnelurl = strdup(value);
      }
    }
  }
  return 0;
}


/**
 * 共有メモリIDを取得し、成功時には共有メモリへの接続を作る
 *
 * @retval  0 共有メモリへの接続成功
 * @retval -1 shmid取得失敗
 * @retval -2 共有メモリ領域への接続失敗
 */
int shmopen(void)
{
  int retval = 0;
  struct stat st;
  FILE *shm_fp;
  config_t *cfg;

  cfg = (config_t *) malloc(sizeof(config_t));
  ini_parse("/mtd/coolrevo/etc/aboxd.conf", parse_handler, cfg);
  if (0 == stat(cfg->shmfile, &st)) {
    shm_fp = fopen(cfg->shmfile, "r");
    if (NULL != shm_fp) {
      fscanf(shm_fp, "%d\n", &shm_id);
      fclose(shm_fp);
      shm_ptr = (ABOX_SHM*) shmat(shm_id, NULL, 0);
      if (shm_ptr == (void *) - 1) {
        retval = -2;
      }
    }
  } else {
    retval = -1;
  }
  free(cfg);
  return retval;
}


/**
 * 共有メモリへの接続を解除する
 *
 * @retval  0 共有メモリへの接続解除成功
 * @retval -1 共有メモリへの接続解除に失敗
 */
int shmclose(void)
{
  if (-1 == shmdt(shm_ptr)) {
    return -1;
  }
  return 0;
}


/**
 * メイン処理
 *
 * @param[in] argc コマンドラインオプション数
 * @param[in] argv コマンドラインオプションを表す配列
 * @return int 終了ステータス
 * @retval  0 正常終了
 * @retval -1 指定したメンバは共有メモリにない
 * @retval -2 指定したメンバのインデックスは配列範囲外
 * @retval -3 共有メモリのオープンに失敗
 * @retval -250 引数が一致しない
 */
int main(int argc, char **argv)
{
  char* outputbuf;
  const char* st_member;
  const char* sub_st_member;
  int st_idx;
  int cs_idx;
  int exitcode = 0;

  /* 引数がない場合終了 */
  if (argc < 2) {
    return -250;
  }

  /* 共有メモリへの接続 */
  if (0 == shmopen()) {
    outputbuf = (char *) malloc(sizeof(char) * BUF_200);
    st_member = argv[1];
    if (argc > 3) {
      st_idx = (int)strtol(argv[2], NULL, 10);
      sub_st_member = argv[3];
    } else {
      st_idx = -1;
      sub_st_member = "nothing";
    }
    if (       0 == strcmp(st_member, "id")) {
      sprintf(outputbuf, "%d", shm_ptr->id);
    } else if (0 == strcmp(st_member, "member_id")) {
      sprintf(outputbuf, "%d", shm_ptr->member_id);
    } else if (0 == strcmp(st_member, "group_id")) {
      sprintf(outputbuf, "%d", shm_ptr->group_id);
    } else if (0 == strcmp(st_member, "user_id")) {
      sprintf(outputbuf, "%d", shm_ptr->user_id);
    } else if (0 == strcmp(st_member, "uniquekey")) {
      sprintf(outputbuf, "%s", shm_ptr->uniquekey);
    } else if (0 == strcmp(st_member, "name")) {
      sprintf(outputbuf, "%s", shm_ptr->name);
    } else if (0 == strcmp(st_member, "hwaddr")) {
      sprintf(outputbuf, "%s", shm_ptr->hwaddr);
    } else if (0 == strcmp(st_member, "disabled")) {
      sprintf(outputbuf, "%d", shm_ptr->disabled);
    } else if (0 == strcmp(st_member, "login")) {
      sprintf(outputbuf, "%s", shm_ptr->login);
    } else if (0 == strcmp(st_member, "passwd")) {
      sprintf(outputbuf, "%s", shm_ptr->passwd);
    } else if (0 == strcmp(st_member, "ntpdate")) {
      sprintf(outputbuf, "%d", shm_ptr->ntpdate);
    } else if (0 == strcmp(st_member, "ntpfqdn")) {
      sprintf(outputbuf, "%s", shm_ptr->ntpfqdn);
    } else if (0 == strcmp(st_member, "status")) {
      sprintf(outputbuf, "%d", shm_ptr->status);
    } else if (0 == strcmp(st_member, "layer")) {
      sprintf(outputbuf, "%d", shm_ptr->layer);
    } else if (0 == strcmp(st_member, "group_name")) {
      sprintf(outputbuf, "%s", shm_ptr->group_name);
    } else if (0 == strcmp(st_member, "grouplogin")) {
      sprintf(outputbuf, "%s", shm_ptr->grouplogin);
    } else if (0 == strcmp(st_member, "grouppasswd")) {
      sprintf(outputbuf, "%s", shm_ptr->grouppasswd);
    } else if (0 == strcmp(st_member, "cloudaddr")) {
      sprintf(outputbuf, "%s", shm_ptr->cloudaddr);
    } else if (0 == strcmp(st_member, "clouddir")) {
      sprintf(outputbuf, "%s", shm_ptr->clouddir);
    } else if (0 == strcmp(st_member, "cloudlogin")) {
      sprintf(outputbuf, "%s", shm_ptr->cloudlogin);
    } else if (0 == strcmp(st_member, "cloudpasswd")) {
      sprintf(outputbuf, "%s", shm_ptr->cloudpasswd);
    } else if (0 == strcmp(st_member, "parent_name")) {
      sprintf(outputbuf, "%s", shm_ptr->parent_name);
    } else if (0 == strcmp(st_member, "root_name")) {
      sprintf(outputbuf, "%s", shm_ptr->root_name);
    } else if (0 == strcmp(st_member, "loglevel")) {
      sprintf(outputbuf, "%d", shm_ptr->loglevel);
    } else if (0 == strcmp(st_member, "activ_status")) {
      sprintf(outputbuf, "%d", shm_ptr->activ_status);
    } else if (0 == strcmp(st_member, "remotehelp")) {
      sprintf(outputbuf, "%d", shm_ptr->remotehelp);
    } else if (0 == strcmp(st_member, "dhcp")) {
      sprintf(outputbuf, "%d", shm_ptr->dhcp);
    } else if (0 == strcmp(st_member, "addr.ipaddr")) {
      sprintf(outputbuf, "%s", shm_ptr->addr.ipaddr);
    } else if (0 == strcmp(st_member, "addr.netmask")) {
      sprintf(outputbuf, "%s", shm_ptr->addr.netmask);
    } else if (0 == strcmp(st_member, "addr.broadcast")) {
      sprintf(outputbuf, "%s", shm_ptr->addr.broadcast);
    } else if (0 == strcmp(st_member, "addr.gateway")) {
      sprintf(outputbuf, "%s", shm_ptr->addr.gateway);
    } else if (0 == strcmp(st_member, "addr.dns1")) {
      sprintf(outputbuf, "%s", shm_ptr->addr.dns1);
    } else if (0 == strcmp(st_member, "addr.dns2")) {
      sprintf(outputbuf, "%s", shm_ptr->addr.dns2);
    } else if (0 == strcmp(st_member, "pidfile")) {
      sprintf(outputbuf, "%s", shm_ptr->pidfile);
    } else if (0 == strcmp(st_member, "shmfile")) {
      sprintf(outputbuf, "%s", shm_ptr->shmfile);
    } else if (0 == strcmp(st_member, "logfile")) {
      sprintf(outputbuf, "%s", shm_ptr->logfile);
    } else if (0 == strcmp(st_member, "cacert")) {
      sprintf(outputbuf, "%s", shm_ptr->cacert);
    } else if (0 == strcmp(st_member, "server")) {
      sprintf(outputbuf, "%s", shm_ptr->server);
    } else if (0 == strcmp(st_member, "csurl")) {
      sprintf(outputbuf, "%s", shm_ptr->csurl);
    } else if (0 == strcmp(st_member, "tunnelurl")) {
      sprintf(outputbuf, "%s", shm_ptr->tunnelurl);
    } else if (0 == strcmp(st_member, "smbshare")) {
      sprintf(outputbuf, "%s", shm_ptr->smbshare);
    } else if (0 == strcmp(st_member, "runtime_addr.ipaddr")) {
      sprintf(outputbuf, "%s", shm_ptr->runtime_addr.ipaddr);
    } else if (0 == strcmp(st_member, "runtime_addr.netmask")) {
      sprintf(outputbuf, "%s", shm_ptr->runtime_addr.netmask);
    } else if (0 == strcmp(st_member, "runtime_addr.broadcast")) {
      sprintf(outputbuf, "%s", shm_ptr->runtime_addr.broadcast);
    } else if (0 == strcmp(st_member, "runtime_addr.gateway")) {
      sprintf(outputbuf, "%s", shm_ptr->runtime_addr.gateway);
    } else if (0 == strcmp(st_member, "runtime_addr.dns1")) {
      sprintf(outputbuf, "%s", shm_ptr->runtime_addr.dns1);
    } else if (0 == strcmp(st_member, "runtime_addr.dns2")) {
      sprintf(outputbuf, "%s", shm_ptr->runtime_addr.dns2);

    } else if (0 == strcmp(st_member, "agentbox_id")) {
      sprintf(outputbuf, "%d", shm_ptr->wondergate_id);
    } else if (0 == strcmp(st_member, "wondergate_id")) {
      sprintf(outputbuf, "%d", shm_ptr->wondergate_id);
    } else if (0 == strcmp(st_member, "uniquekey")) {
      sprintf(outputbuf, "%s", shm_ptr->uniquekey);
    } else if (0 == strcmp(st_member, "name")) {
      sprintf(outputbuf, "%s", shm_ptr->name);
    } else if (0 == strcmp(st_member, "hwaddr")) {
      sprintf(outputbuf, "%s", shm_ptr->hwaddr);
    } else if (0 == strcmp(st_member, "bitrate")) {
      sprintf(outputbuf, "%d", shm_ptr->bitrate);
    } else if (0 == strcmp(st_member, "preserve")) {
      sprintf(outputbuf, "%d", shm_ptr->preserve);
    } else if (0 == strcmp(st_member, "type")) {
      sprintf(outputbuf, "%d", shm_ptr->type);
    } else if (0 == strcmp(st_member, "disabled")) {
      sprintf(outputbuf, "%d", shm_ptr->disabled);
    } else if (0 == strcmp(st_member, "nocharged")) {
      sprintf(outputbuf, "%d", shm_ptr->nocharged);
    } else if (0 == strcmp(st_member, "discover")) {
      sprintf(outputbuf, "%d", shm_ptr->discover);
    } else if (0 == strcmp(st_member, "surveil")) {
      sprintf(outputbuf, "%d", shm_ptr->surveil);
    } else if (0 == strcmp(st_member, "promotion")) {
      sprintf(outputbuf, "%d", shm_ptr->promotion);
    } else if (0 == strcmp(st_member, "recording")) {
      sprintf(outputbuf, "%d", shm_ptr->recording);
    } else if (0 == strcmp(st_member, "placed")) {
      sprintf(outputbuf, "%s", shm_ptr->placed);
    } else if (0 == strcmp(st_member, "live")) {
      sprintf(outputbuf, "%d", shm_ptr->live);
    } else if (0 == strcmp(st_member, "port")) {
      sprintf(outputbuf, "%d", shm_ptr->port);
    } else if (0 == strcmp(st_member, "motion")) {
      sprintf(outputbuf, "%d", shm_ptr->motion);
    } else if (0 == strcmp(st_member, "ptz")) {
      sprintf(outputbuf, "%d", shm_ptr->ptz);
    } else if (0 == strcmp(st_member, "live_url")) {
      sprintf(outputbuf, "%s", shm_ptr->live_url);
    } else if (0 == strcmp(st_member, "extension")) {
      sprintf(outputbuf, "%s", shm_ptr->extension);
    } else if (0 == strcmp(st_member, "search_path")) {
      sprintf(outputbuf, "%s", shm_ptr->search_path);
    } else if (0 == strcmp(st_member, "ovf_type")) {
      sprintf(outputbuf, "%d", shm_ptr->ovf_type);
    } else if (0 == strcmp(st_member, "ovf_uri")) {
      sprintf(outputbuf, "%s", shm_ptr->ovf_uri);

    } else if (0 == strcmp(st_member, "csserver")) {
      cs_idx = sizeof(shm_ptr->csserver) / sizeof(shm_ptr->csserver[0]);
      if (0 <= cs_idx) {
        cs_idx = 2;
      }
      if (0 > st_idx || st_idx > cs_idx) {
        sprintf(outputbuf, "%s[%d] index out of lange", st_member, st_idx);
        exitcode = -2;
      } else {
        if (0        == strcmp(sub_st_member, "host")) {
          sprintf(outputbuf, "%s", shm_ptr->csserver[st_idx].host);
        } else if (0 == strcmp(sub_st_member, "prefix")) {
          sprintf(outputbuf, "%s", shm_ptr->csserver[st_idx].prefix);
        } else {
          sprintf(outputbuf, "%s could not found", sub_st_member);
          exitcode = -1;
        }
      }

    } else {
      sprintf(outputbuf, "%s could not found", st_member);
      exitcode = -1;
    }

    fputs(outputbuf, stdout);
    free(outputbuf);
    shmclose();
  } else {
    fputs("shm cannot open", stdout);
    exitcode = -3;
  }
  return exitcode;
}
