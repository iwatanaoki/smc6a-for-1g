/**
 * @file    sendevent.c
 * @brief   SMC-5A 内部で event 検知時、検知情報を通知するために実行される
 *          クラウド向けgateway コマンド
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_SENDEVENT_C
#define AGENTBOX_SENDEVENT_C
#endif // AGENTBOX_SENDEVENT_C
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/shm.h>
#ifndef FIRST_GENERATION_PRATFORM
#include "json.h"
#include "shm.h"
#include "aboxd.h"
#include "util.h"
#include "auth.h"
#include "logger.h"
#include "sendevent.h"


/**
 * .confファイルのパース用コールバック
 *
 * libini の関数 ini_parse(const char*, ini_handler, void*) のコールバック
 * として使用する
 *
 * @param[in,out] user 値をコピーする対象へのポインタ。対象に応じて内部で
 *  キャストする
 * @param[in] section .confのセクション部(ブラケット[]で囲まれた箇所)の名称
 * @param[in] name    .confのパラメータ名
 * @param[in] value   .confのパラメータの値
 * @retval 0 正常終了(パースエラーなく成功)
 * @sa <libini.h> ini_parse
 */
int parse_handler(void* user, const char* section, const char* name, const char* value)
{
  config_t* pcfg;
  unsigned int conv;

  pcfg = (config_t *) user;
  if (0 == strcmp(section, "aboxd")) {
    if (0 < strlen(value)) {
      if (0 == strcmp(name, "interval")) {
        conv = strtoul(value, (char **) NULL, 10);
        if (conv < 60) {
          conv = 60;
        }
        pcfg->interval = conv;
      } else if (0 == strcmp(name, "pidfile")) {
        pcfg->pidfile = strdup(value);
      } else if (0 == strcmp(name, "shmfile")) {
        pcfg->shmfile = strdup(value);
      } else if (0 == strcmp(name, "logfile")) {
        pcfg->logfile  = strdup(value);
      } else if (0 == strcmp(name, "cacert")) {
        pcfg->cacert = strdup(value);
      } else if (0 == strcmp(name, "server")) {
        pcfg->server = strdup(value);
      } else if (0 == strcmp(name, "csurl")) {
        pcfg->csurl = strdup(value);
      } else if (0 == strcmp(name, "tunnelurl")) {
        pcfg->tunnelurl = strdup(value);
      }
    }
  }
  return 0;
}


/**
 * 共有メモリIDを取得し、成功時には共有メモリへの接続を作る
 *
 * @retval  0 共有メモリへの接続成功
 * @retval -1 shmid取得失敗
 * @retval -2 共有メモリ領域への接続失敗
 */
int shmopen(void)
{
  int retval = 0;
  struct stat st;
  FILE *shm_fp;

  if (0 == stat(g_cfg->shmfile, &st)) {
    shm_fp = fopen(g_cfg->shmfile, "r");
    if (NULL != shm_fp) {
      fscanf(shm_fp, "%d\n", &shm_id);
      fclose(shm_fp);
      shm_ptr = (ABOX_SHM*) shmat(shm_id, NULL, 0);
      if (shm_ptr == (void *) - 1) {
        retval = -2;
      }
    }
  } else {
    retval = -1;
  }
  return retval;
}


/**
 * 共有メモリへの接続を解除する
 *
 * @retval  0 共有メモリへの接続解除成功
 * @retval -1 共有メモリへの接続解除に失敗
 */
int shmclose(void)
{
  if (-1 == shmdt(shm_ptr)) {
    return -1;
  }
  return 0;
}
#endif // FIRST_GENERATION_PRATFORM


/**
 * メイン処理
 *
 * @param[in] argc コマンドラインオプション数
 * @param[in] argv コマンドラインオプションを表す配列
 * @return int 終了ステータス
 * @retval  0 正常終了
 * @retval -1 指定したメンバは共有メモリにない
 * @retval -2 指定したメンバのインデックスは配列範囲外
 * @retval -3 共有メモリのオープンに失敗
 * @retval -250 引数が一致しない
 */
int main(int argc, char **argv)
{
  int l_exitcode = 0;

#ifndef FIRST_GENERATION_PRATFORM
  char l_param[BUF_64];
  char l_imgfile[BUF_200];
  int  l_len = 0;
  int  l_tmpcnt = 0;
  char  *l_pt = (char*)NULL;
  char  *l_tmpparam[3];
  int  l_kind = 0;
  int  l_type = 0;
  int  l_count = 0;
  int i;

  const char* conf_file_path = ABOXD_DEFAULT_CFG_PATH;


  memset(l_param, 0, sizeof(l_param));
  memset(l_imgfile, 0, sizeof(l_imgfile));
  memset(l_tmpparam, 0, sizeof(l_tmpparam));

  // HUNTからの起動でTZがGMTのままになっているので、ここで強制的に
  // JST-9を設定
  putenv("TZ=JST-9");

  /* 引数がない場合終了 */
  if (argc < 2) {
    fprintf(stderr, "parameter not found.\n");
    return -250;
  }
  else if (argc == 2) { // 検出データのみ
    fprintf(stderr, "argv[1]:%s\n", argv[1]);
    strcpy(l_param, argv[1]);
  }
  else if (argc == 3) { // 検知静止画パスあり
    fprintf(stderr, "argv[1]:%s\n", argv[1]);
    fprintf(stderr, "argv[2]:%s\n", argv[2]);
    strcpy(l_param, argv[1]);
    strcpy(l_imgfile, argv[2]);
  }

  // タイムアウトアラーム設定
  alarm(SENDEVENT_TIMEOUT_SEC);

  l_len = strlen(l_param);
  l_tmpcnt = 0;
  l_tmpparam[l_tmpcnt] = l_param;
  for(i=0; i<l_len; i++) {
    if (l_param[i] == '"') {
      i++;
      continue;
    }
    if (l_param[i] == ',') {
      l_param[i] = '\0';
      l_tmpcnt++;
      l_tmpparam[l_tmpcnt] = &(l_param[i+1]);
    }
  }
  fprintf(stderr, "tmpcnt:%d\n", l_tmpcnt);

  for(i=0; i<=l_tmpcnt; i++) {
    fprintf(stderr, "l_tmpparam[%d]:%s\n", i, l_tmpparam[i]);
    if (i == 0) { // kind
      l_kind = atoi(l_tmpparam[i]);
    }
    else if (i == 1) { 
      if (l_kind != SENDEVENT_INOUTCOUNT) { // in/out でなければ検出数
                                            // (motionの場合は検知area位置）
        l_count = atoi(l_tmpparam[i]);
        break;
      }
      else { // in/out モード
        if (strcmp(l_tmpparam[i], "in") == 0) {
          l_type = SENDEVENT_TYPE_IN;
        }
        else if (strcmp(l_tmpparam[i], "out") == 0) {
          l_type = SENDEVENT_TYPE_OUT;
        }
      }
    }
    else if (i == 2) { // count with in/out mode
      l_count = atoi(l_tmpparam[i]);
    }
  }

  fprintf(stderr, "l_kind   :%d, %s\n", l_kind, HERE); 
  fprintf(stderr, "l_type   :%d, %s\n", l_type, HERE); 
  fprintf(stderr, "l_count  :%d, %s\n", l_count, HERE); 
  fprintf(stderr, "l_imgfile:%s, %s\n", l_imgfile, HERE); 

  if (l_kind <= 0 || 
       (l_kind == 3 && 
         (l_type != SENDEVENT_TYPE_IN && l_type != SENDEVENT_TYPE_OUT))) {
    l_exitcode = 1;
    fprintf(stderr, "kind:%d or type:%d is invalid, exit:%d, %s\n",
      l_kind, l_type, l_exitcode, HERE); 
    exit(l_exitcode);
  }

  /* config ファイルロード */
  g_cfg = (config_t *) malloc(sizeof(config_t));
  if (g_cfg == NULL) {
      l_exitcode = 2;
    fprintf(stderr, "cannot open config(%s), exit:%d, %s\n",
      l_exitcode, HERE); 
    exit(l_exitcode);
  }
  ini_parse(conf_file_path, parse_handler, g_cfg);

  /* 共有メモリへの接続 */
  if (0 != shmopen()) {
    l_exitcode = 3;
    fprintf(stderr, "cannot open shared memory, exit:%d, %s\n",
      l_exitcode, HERE); 
    free(g_cfg);
    exit(l_exitcode);
  }

  for(i=0; i<argc; i++) {
    logger_write(LOG_DEBUG, "argv[%d]:%s, %s", i, argv[i], HERE); 
  }
  for(i=0; i<l_tmpcnt; i++) {
    logger_write(LOG_DEBUG, "l_tmpparam[%d]:%s, %s", i, l_tmpparam[i], HERE); 
  }
  logger_write(LOG_DEBUG, "l_kind   :%d, %s", l_kind, HERE); 
  logger_write(LOG_DEBUG, "l_type   :%d, %s", l_type, HERE); 
  logger_write(LOG_DEBUG, "l_count  :%d, %s", l_count, HERE); 
  logger_write(LOG_DEBUG, "l_imgfile:%s, %s", l_imgfile, HERE); 

  //アクティベーションチェック
  if(true != shm_ptr->activ_status) {
    l_exitcode = 4;
    logger_write(LOG_ERR, "not activate yet,exit:%d, %s",
      l_exitcode, HERE); 
    printf("not activate yet,exit:%d, %s\n",
      l_exitcode, HERE); 
    free(g_cfg);
    shmclose();
    exit(l_exitcode);
  }


  /* iM5c 連携処理 */
  // SMC-3A Plus 系では使用しない
  //send_event_im5c(l_kind, l_type, l_count);

  /* 管理サーバへの転送(apiコール)処理 */
  send_event_cloud(l_kind, l_type, l_count);

  free(g_cfg);
  shmclose();

#endif // FIRST_GENERATION_PRATFORM

  exit(l_exitcode);
}

#ifndef FIRST_GENERATION_PRATFORM

/**
 * iM5c への手動CM鳴動命令実行処理
 *
 * @param[in] i_kind 検出機能種類番号
 * @param[in] i_type i_kind:3(in/out)の時、in:1/out:2の種別
 * @param[in] i_count 検出人数
 * @return int 終了ステータス
 * @retval  0 正常終了
 * @retval 1 iM5cのIPアドレスが未取得
 * @retval 2 イベントのカウント値（人数）の値が不正
 * @retval 3 エリアカウント時のCMNO値が不正
 * @retval 4 待ち行列時のCMNO値が不正
 * @retval 5 インアウト(イン方向)時のCMNO値が不正
 * @retval 6 インアウト(アウト方向)時のCMNO値が不正
 * @retval 7 インアウト時の方向指定が不正
 * @retval 8 イベント種別(i_kind)の値が不正
 * @retval 9 iM5c CMNO API コールエラー
 */
/* SMC-3A Plus 系では使用しないので未定義とする
int send_event_im5c(int i_kind, int i_type, int i_count)
{
  int api_result;
  int l_rtn = 0;
  int l_cmno = 0;
  int l_playcount = 1;
  char baseurl[BUF_200];
  char url[BUF_200];
  char data[BUF_512];
  char response[BUF_2048];
  int  tokenSec = 0;
  char tokenKey[BUF_512];

  memset(response, 0, sizeof(char) * BUF_2048);
  memset(baseurl , 0, sizeof(char) * BUF_200);
  memset(url     , 0, sizeof(char) * BUF_200);
  memset(data,     0, sizeof(char) * BUF_512);

  if (shm_ptr->im5c_ipaddr[0] == '\0') {
    l_rtn = 1;
    logger_write(LOG_ERR, "not set IPaddr of iM5c yet, rtn:%d, %s",
      l_rtn, HERE); 
    return(l_rtn);
  }

  if (i_count <= 0) {
    l_rtn = 2;
    logger_write(LOG_ALERT, "not set count, rtn:%d, %s",
      l_rtn, HERE); 
    return(l_rtn);
  }

  if (i_kind == SENDEVENT_KIND_AREACOUNT) {
    if(shm_ptr->cmno_area > 0) {
      l_cmno = shm_ptr->cmno_area;
      if (0 < shm_ptr->playcount_area && shm_ptr->playcount_area <= 99) {
        l_playcount = shm_ptr->playcount_area;
      }
      else {
        l_playcount = 1;
      }
    }
    else {
      l_rtn = 3;
      logger_write(LOG_ERR, 
        "cmno:%d playcount:%d is invalid, rtn:%d, %s",
        shm_ptr->cmno_area, shm_ptr->playcount_area, l_rtn, HERE); 
      return(l_rtn);
    }
  }
  else if (i_kind == SENDEVENT_KIND_QUEUEDETECT) {
    if(shm_ptr->cmno_queue > 0) {
      l_cmno = shm_ptr->cmno_queue;
      if (0 < shm_ptr->playcount_queue && shm_ptr->playcount_queue <= 99) {
        l_playcount = shm_ptr->playcount_queue;
      }
      else {
        l_playcount = 1;
      }
    }
    else {
      l_rtn = 4;
      logger_write(LOG_ERR, 
        "cmno:%d playcount:%d is invalid, rtn:%d, %s",
        shm_ptr->cmno_queue, shm_ptr->playcount_queue, l_rtn, HERE); 
      return(l_rtn);
    }
  }
  else if (i_kind == SENDEVENT_INOUTCOUNT) {
    if (i_type == SENDEVENT_TYPE_IN) {
      if(shm_ptr->cmno_in > 0) {
        l_cmno = shm_ptr->cmno_in;
        if (0 < shm_ptr->playcount_in && shm_ptr->playcount_in <= 99) {
          l_playcount = shm_ptr->playcount_in;
        }
        else {
          l_playcount = 1;
        }
      }
      else {
        l_rtn = 5;
        logger_write(LOG_ERR, 
          "cmno:%d playcount:%d is invalid, rtn:%d, %s",
          shm_ptr->cmno_in, shm_ptr->playcount_in, l_rtn, HERE); 
        return(l_rtn);
      }
    }
    else if (i_type == SENDEVENT_TYPE_OUT) {
      if(shm_ptr->cmno_out > 0) {
        l_cmno = shm_ptr->cmno_out;
        if (0 < shm_ptr->playcount_out && shm_ptr->playcount_out <= 99) {
          l_playcount = shm_ptr->playcount_out;
        }
        else {
          l_playcount = 1;
        }
      }
      else {
        l_rtn = 6;
        logger_write(LOG_ERR, 
          "cmno:%d playcount:%d is invalid, rtn:%d, %s",
          shm_ptr->cmno_out, shm_ptr->playcount_out, l_rtn, HERE); 
        return(l_rtn);
      }
    }
    else {
      l_rtn = 7;
      logger_write(LOG_ERR, 
        "type:%d is invalid, rtn:%d, %s", i_type, l_rtn, HERE); 
      return(l_rtn);
    }
  }
  else {
    l_rtn = 8;
    logger_write(LOG_ERR, 
      "kind:%d is invalid, rtn:%d, %s", i_kind, l_rtn, HERE); 
    return(l_rtn);
  }

  // iM5c 手動CMAPIにアクセス
  memset(tokenKey, 0, sizeof(tokenKey));
  tokenSec = getSecureTokenSec();
  getSecureTokenKey(tokenSec, tokenKey);
  sprintf(baseurl, "http://%s", shm_ptr->im5c_ipaddr);
  sprintf(url, "cgi-bin/cmplay");
  sprintf(data,"cmno=%d&playcount=%d&e=%d&st=%s", 
    l_cmno, l_playcount, tokenSec, tokenKey);

  api_result = get_api_response_otherserver(response,
    sizeof(char) * BUF_2048, url, data, baseurl
  );
  if (0 == api_result) {
    logger_write(LOG_INFO, 
      "event post to iM5c success, %s", HERE); 
  }
  else {
    logger_write(LOG_ERR, 
      "event post to iM5c failed, api_result:%d, %s", api_result, HERE); 
    l_rtn = 3;
  }
  logger_write(LOG_DEBUG, "response:%s, %s", response, HERE); 


  return(l_rtn);
}
*/

/**
 * 管理システムへのエベント情報送信処理
 *
 * @param[in] i_kind 検出機能種類番号
 * @param[in] i_type i_kind:3(in/out)の時、in:1/out:2の種別
 * @param[in] i_count 検出人数
 * @return int 終了ステータス
 * @retval  0 正常終了
 * @retval 1 i_type が指定外
 * @retval 2 i_kind が指定外
 * @retval 3 get_api_response エラー
 */
int send_event_cloud(int i_kind, int i_type, int i_count)
{
  int api_result;
  int l_rtn = 0;
  char url[BUF_200];
  char data[BUF_512];
  char tmp_data[BUF_200];
  char response[BUF_2048];
  char l_direct[BUF_16];
  time_t l_nowtime = 0;

  memset(response, 0, sizeof(char) * BUF_2048);
  memset(url     , 0, sizeof(char) * BUF_200);
  memset(data,     0, sizeof(char) * BUF_200);

  memset(l_direct, 0, sizeof(char) * BUF_16);

  l_nowtime = time(NULL);

  strcpy(url, AGENTBOX_CFG_MOTION);
  switch(i_kind) {
    case SENDEVENT_KIND_AREACOUNT:
      sprintf(tmp_data, "kind=%d&area=%d", i_kind, i_count);
      break;
    case SENDEVENT_KIND_QUEUEDETECT:
      sprintf(tmp_data, "kind=%d&queue=%d", i_kind, i_count);
      break;
    case SENDEVENT_INOUTCOUNT:
      if (i_type == SENDEVENT_TYPE_IN) {
        strcpy(l_direct, "in");
      }
      else if (i_type == SENDEVENT_TYPE_OUT) {
        strcpy(l_direct, "out");
      }
      else {
        l_rtn = 1;
        logger_write(LOG_ERR, 
          "type:%d is invalid, rtn:%d, %s", i_type, l_rtn, HERE); 
        return(l_rtn);
      }
      sprintf(tmp_data, "kind=%d&direct=%s&count=%d",
        i_kind, l_direct, i_count);
      break;
    case SENDEVENT_KIND_MOTION:
      sprintf(tmp_data, "");
      break;
    default:
      l_rtn = 2;
      logger_write(LOG_ERR, 
        "kind:%d is invalid, rtn:%d, %s", i_kind, l_rtn, HERE); 
      return(l_rtn);
  }

  sprintf(data, "%s&detecttime=%d", tmp_data, l_nowtime);

  logger_write(LOG_DEBUG, "url  :%s, %s", url, HERE); 
  logger_write(LOG_DEBUG, "data :%s, %s", data, HERE); 

  api_result = get_api_response(response,
    sizeof(char) * BUF_2048, url, data);
  if (0 == api_result) {
    logger_write(LOG_INFO, 
      "event post to %s success, %s", url, HERE); 
  }
  else {
    logger_write(LOG_ERR, 
      "event post to %s failed, api_result:%d, %s", url, api_result, HERE); 
    l_rtn = 3;
  }
  logger_write(LOG_DEBUG, "response:%s, %s", response, HERE); 

  return(l_rtn);
}

/**
 * nginx secureToken 第一パラメータ（秒数）を算出
 *
 * @retval 秒数
 */
int getSecureTokenSec(void)
{
  time_t l_now = 0;
 
  l_now = time(NULL);
  l_now += 60 * 60;
  return l_now;
}

/**
 * nginx secureToken 第二パラメータ（秘密鍵）を算出
 *
 * @param[in]  i_sec         :キー生成に使用する秒数
 * @param[in]  i_uniquekey   :カメラuniquekey
 * @param[out] o_createkey   :生成したsecureToken用キー(呼出側で領域確保したポインタを指定）
 * @retval 0 正常終了
 * @retval 1 i_uniquekey がNULL
 * @retval 2 o_createkey がNULL
 */
int getSecureTokenKey(int i_sec, char *o_createkey)
{
  int l_rtn = 0;
  char tmpbuf[BUF_512];
  unsigned char md5tmpbuf[BUF_512];
  char base64tmpbuf[BUF_512];
  int  md5size = 0;
  int  base64size = 0;
  int  i = 0;
  int  j = 0;

  if (NULL == o_createkey) {
    l_rtn = 2;
    return l_rtn;
  }

  memset(tmpbuf, 0, sizeof(tmpbuf));
  memset(md5tmpbuf, 0, sizeof(md5tmpbuf));
  memset(base64tmpbuf, 0, sizeof(base64tmpbuf));

  sprintf(tmpbuf, "%s%d", 
    SENDEVENT_SECURETOKEN_SECRETKEY, i_sec);

  logger_write(LOG_DEBUG, "secureToken base key:%s, %s", tmpbuf, HERE);

  md5_digest_bin(md5tmpbuf, &md5size, tmpbuf);
  //logger_write(LOG_DEBUG, "secureToken md5 key:%s, %s", md5tmpbuf, HERE);
  encode_base64(md5tmpbuf, md5size, base64tmpbuf, &base64size);
  logger_write(LOG_DEBUG, "secureToken base64encode key:%s, %s", base64tmpbuf, HERE);
  for(i=0,j=0; i<base64size && i<sizeof(base64tmpbuf); i++) {
    if (base64tmpbuf[i] == '=') {
      continue;
    }
    if (base64tmpbuf[i] == '+') {
      base64tmpbuf[i] = '-';
    }
    else if (base64tmpbuf[i] == '/') {
      base64tmpbuf[i] = '_';
    }
    *(o_createkey+j) = base64tmpbuf[i];
    j++;
  }
  logger_write(LOG_DEBUG, "secureToken base64encode urlencode key:%s, %s", o_createkey, HERE);

  return l_rtn;
}
#endif // FIRST_GENERATION_PRATFORM
