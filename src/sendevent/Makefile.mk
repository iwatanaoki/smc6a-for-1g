### sendevent #####################################################

SENDEVENT_SRCDIR=$(ABSDIR)/sendevent
ifeq "$(strip $(SENDEVENT_SRCDIR))" ""
  SENDEVENT_SRCDIR=.
endif
SENDEVENT_SRCS=$(notdir $(wildcard $(SENDEVENT_SRCDIR)/*.c))
SENDEVENT_OBJS=$(notdir $(SENDEVENT_SRCS:.c=.o))
SENDEVENT_TGTS=sendevent
SENDEVENT_LIBS=-L$(SMC3APLUSDIR)/libini -lini $(LIBS) -lpcre -lm

################################################################

$(SENDEVENT_OBJS):
	cd $(SENDEVENT_SRCDIR); \
	$(CC) -DFIRST_GENERATION_PRATFORM $(CPPFLAGS) $(CFLAGS) $(LDFLAGS) -c $(@F:.o=.c) -o $@

$(SENDEVENT_TGTS): $(SENDEVENT_OBJS)
	cd $(SENDEVENT_SRCDIR); \
	$(CC) -DFIRST_GENERATION_PRATFORM $(CPPFLAGS) $(CFLAGS) $(LINKER_FLAGS) $(LDFLAGS) $(SENDEVENT_OBJS) -o $@ $(SENDEVENT_LIBS)
ifeq ($(BUILD_TYPE),Release)
	cd $(SENDEVENT_SRCDIR); \
	$(STRIP) $@
endif

sendevent-install: $(SENDEVENT_TGTS) install-dir
	cp $(SENDEVENT_SRCDIR)/$(SENDEVENT_TGTS) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(SENDEVENT_TGTS)

sendevent-uninstall: $(SENDEVENT_TGTS)
	$(RM) $(INSTALL_DIR)$(DISTDIR)/usr/bin/$(SENDEVENT_TGTS)

sendevent-clean:
	cd $(SENDEVENT_SRCDIR); \
	$(RM) $(SENDEVENT_TGTS) *.o

sendevent-dry:
	@cd $(SENDEVENT_SRCDIR); \
	echo "SENDEVENT_SRCS='$(SENDEVENT_SRCS)'"; \
	echo "SENDEVENT_OBJS='$(SENDEVENT_OBJS)'"; \
	echo "SENDEVENT_TGTS='$(SENDEVENT_TGTS)'"; \
	echo "SENDEVENT_LIBS='$(SENDEVENT_LIBS)'"
