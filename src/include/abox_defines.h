/**
 * @file    abox_defines.h
 * @brief   プロジェクト共有定義
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_ABOX_DEFINES_H
#define AGENTBOX_ABOX_DEFINES_H


#include<stdbool.h>


/** @name バージョン番号 */
/* @{ */
#define PROJ_VERSION_MAJOR     4  //!< メジャーバージョン番号
#define PROJ_VERSION_MINOR     1  //!< マイナーバージョン番号
#define PROJ_VERSION_REVISION  0  //!< リビジョン番号
/* @} */

/** @name Boolean 汎用 */
/* @{ */
#define TRUE  true                //!< true (=1)のAlternative
#define FALSE false               //!< false(=0)のAlternative
/* @} */

/** @name size_t 汎用 */
/* @{ */
#define BUF_131072    (1024 * 128)//!< バッファ領域の定義・128k
#define BUF_65536     (1024 * 64) //!< バッファ領域の定義・64k
#define BUF_32768     (1024 * 32) //!< バッファ領域の定義・32k
#define BUF_16384     (1024 * 16) //!< バッファ領域の定義・16k
#define BUF_8192      (1024 * 8)  //!< バッファ領域の定義・ 8k
#define BUF_4096      (1024 * 4)  //!< バッファ領域の定義・ 4k
#define BUF_2048      (1024 * 2)  //!< バッファ領域の定義・一般用途
#define BUF_1024       1024       //!< バッファ領域の定義・一般用途
#define BUF_512        512        //!< バッファ領域の定義・0.5k
#define BUF_200        200        //!< バッファ領域の定義・200b
#define BUF_128        128        //!< バッファ領域の定義・100b
#define BUF_64         64         //!< バッファ領域の定義・ 64b
#define BUF_32         32         //!< バッファ領域の定義・ 32b
#define BUF_16         16         //!< バッファ領域の定義・ 16b
/* @} */

/** @name size_t 特定用途 */
/* @{ */
#define BUF_DIGEST    (32 + 1)    //!< Msg Digest
#define BUF_IDENT     (64 + 1)    //!< UNIQ・UUID
#define BUF_IPADDR    (48 + 1)    //!< IPアドレス
#define BUF_HWADDR    (17 + 1)    //!< HWアドレス
/* @} */

/** @name 外部メモリタイプの定義 */
/* @{ */
#define MEMTYPE_NONE              0 //!< 外部メモリ未使用
#define MEMTYPE_HDD               1 //!< HDD
#define MEMTYPE_USB               2 //!< USB SSD/スティックmemory
/* @} */

/** @name 外部メモリステータスの定義 */
/* @{ */
#define MEMSTATE_DISABLED         0 //!< 未使用
#define MEMSTATE_NORMAL           1 //!< 正常
#define MEMSTATE_EXCEEDED         2 //!< 超過
#define MEMSTATE_ABNORMAL         3 //!< 異常
/* @} */

/** @name NTPの定義 */
/* @{ */
#define TIMESYNC_WITHOUT_NTP      0 //!< クラウドの時刻を使用する
#define TIMESYNC_USE_NTP          1 //!< NTPサーバを使用する
/* @} */

/** @name エージェントボックスステータスの定義 */
/* @{ */
#define ABOX_STATUS_SUSPENDED     0 //!< 停止中
#define ABOX_STATUS_ACTIVE        1 //!< 正常
#define ABOX_STATUS_POLLING       2 //!< 変更中
#define ABOX_STATUS_REBOOT        3 //!< 再起動
#define ABOX_STATUS_REBOOTING     4 //!< 再起動中
/* @} */

/** @name カメラ死活ステータスの定義 */
/* @{ */
#define CAMERA_STATUS_SUSPENDED   0 //!< 停止中
#define CAMERA_STATUS_ACTIVE      1 //!< 保存中
/* @} */

/** @name レコーダ死活ステータスの定義 */
/* @{ */
#define RECORDER_STATUS_UNREACHABLE          0  //!< 停止中(レコーダから応答がない)
#define RECORDER_STATUS_ALIVE                1  //!< 動作中(レコーダは生存)
#define RECORDER_STATUS_UNDEFINED            2  //!< 状態未取得
#define RECORDER_STATUS_IP_NOT_ASSIGNED  (-121) //!< クラウド上のレコーダにIPアドレスが設定されていない
#define RECORDER_STATUS_CMD_FAILED       (-123) //!< fpingコマンドの失敗
#define RECORDER_STATUS_NOT_IN_SURVEIL   (-125) //!< レコーダは監視対象に設定されていない
/* @} */

/** @name RTSPカメラステータスの定義 */
/* @{ */
#define RTSPCAMERA_STATUS_CANNOT_CONNECT          0  //!< 接続出来ず(RTSPカメラから応答がない)
#define RTSPCAMERA_STATUS_ALIVE                   1  //!< 接続OK
#define RTSPCAMERA_STATUS_CANNOT_CONNECT_FOR_LIVE 2  //!< ライブリレー接続失敗
#define RTSPCAMERA_STATUS_CANNOT_CONNECT_FOR_SAVE 3  //!< ファイル保存接続失敗
/* @} */

/** @name サービスステータスの定義 */
/* @{ */
#define SERVICE_ACTIVE            1 //!< 提供中
#define SERVICE_SUSPENDED         2 //!< 一時停止中
#define SERVICE_WITHDRAW          3 //!< 退会予定
#define SERVICE_WITHDRAWN         4 //!< 退会済み
/* @} */

/** @name カメラ検索タイプ */
/* @{ */
#define CAMERA_DISCOVER_BY_IP     0 //!< IPアドレスによる検索
#define CAMERA_DISCOVER_BY_HW     1 //!< HWアドレスによる検索
/* @} */

/** @name カメラタイプ */
/* @{ */
#define CAMERA_TYPE_STARVEDIA     1 //!< Starvedia
#define CAMERA_TYPE_AXISCORP      2 //!< Axis corp.
#define CAMERA_TYPE_SMC           3 //!< Hunt Electronic
#define CAMERA_TYPE_RTSP          4 //!< Standerd RTSP Type
#define CAMERA_TYPE_AUTORTSP      5 //!< 自律接続 RTSP Type
/* @} */

/** @name カメラ保存タイプ(WonderGateシリーズで使用) */
#define CAMSAVE_TYPE_NORMAL           0 //!< 通常(エージェントボックスと同等。すべてクラウドアップ
#define CAMSAVE_TYPE_PARTCLOUD_ONLY   1 //!< 一部指定期間のみクラウドにアップ(アップした対象ファイルは端末からは削除
#define CAMSAVE_TYPE_PARTCLOUD_BOTH   2 //!< 一部指定期間のみクラウドにアップ(あっぷしたものは端末にも残す
/* @} */

/** @name ONVIFタイプ */
/* @{ */
#define CAMERA_OVFTYPE_NONE                 0 //!< 未定義
#define CAMERA_OVFTYPE_ONVIF                1 //!< ONVIF
#define CAMERA_OVFTYPE_RTSP                 2 //!< agentbox配下のRTSPカメラの場合 RTSP to RTMP Relay(サーバ側でファイル変換)
#define CAMERA_OVFTYPE_RTSP_LIVE_ONLY       3 //!< RTSP to RTMP Relay(サーバ側でもファイル変換せずライブのみ)
#define CAMERA_OVFTYPE_RTSP_LIVE_AND_SAVE   4 //!< RTSP to RTMP Relay(サーバ側ではファイル変換為ず、他CIFSカメラと同様のローカルストレージに変換保存）
/* @} */

/** @name 録画保存設定 */
/* @{ */
#define CAMERA_SAVING_NONE                  0 //!< 未設定
#define CAMERA_SAVING_ALL_TRANS             1 //!< エージェントボックス
#define CAMERA_SAVING_ALL_SAVE              2 //!< 本機保存
#define CAMERA_SAVING_SCH_NOTLEAVE          3 //!< クラウド（本機には残さない）
#define CAMERA_SAVING_SCH_LEAVE             4 //!< クラウド（本機には残す）
/* @} */

#define PUTENV_PATH "PATH=/bin:/sbin:/usr/bin:/usr/sbin:/mtd/coolrevo/sbin:/mtd/coolrevo/bin:/mtd/coolrevo/usr/sbin:/mtd/coolrevo/usr/bin"
#define PUTENV_LD_LIBRARY_PATH "LD_LIBRARY_PATH=/mtd/coolrevo/lib:/mtd/coolrevo/usr/lib:/mtd/coolrevo/lib/expect5.45.3:/lib:/usr/lib"

#endif /* AGENTBOX_ABOX_DEFINES_H */
