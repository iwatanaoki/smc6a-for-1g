/**
 * @file    ngxctl.h
 * @brief   nginx confファイル(conf.d/localhost.conf)を、設定されている
 * カメラの状態に応じて、 リバースプロキシ仮想パスの追加・更新・削除、
 * 反映処理を行うマクロ及びプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_NGNXCTL_H
#define AGENTBOX_NGNXCTL_H

/**
 * nginx localhost.conf 
 */
#define NGNX_LOCALHOST_CONF "/wondergate/conf/conf.d/localhost.conf"


/**
 * localhost default setting first
 * @par フォーマット指定子
 *      - %%s カメラへのRTSP接続URI
 *      - %%s カメラキー
 *      - %%d securetoken number
 *      - %%s securetoken key
 */
#define NGNX_LOCALHOST_FIRST_SETTING "server {\n\
    listen       81;\n\
    server_name  127.0.0.1;\n\
\n\
    access_log  logs/nginx_access.log;\n\
    error_log  logs/nginx_error.log;\n\
\n\
    location / {\n\
        root   /wondergate/cr/www/html;\n\
        index  index.php index.html index.htm;\n\
        add_header Access-Control-Allow-Origin \"*\";\n\
        add_header Access-Control-Allow-Headers \"Origin, Authorization, Accept\";\n\
        add_header Access-Control-Allow-Credentials true;\n\
    }\n\
\n\
    location /kn/ {\n\
        proxy_pass http://127.0.0.1/;\n\
    }\n\
"

/**
 * localhost default setting end
 * @par フォーマット指定子
 *      - %%s カメラへのRTSP接続URI
 *      - %%s カメラキー
 *      - %%d securetoken number
 *      - %%s securetoken key
 */
#define NGNX_LOCALHOST_END_SETTING "\n\
}"


/**
 * 生存チェックコマンドテンプレート(ローカル)
 * @par フォーマット指定子
 *      - %%s カメラきー
 */
#define NGNX_PID_GREP "pgrep -f \"^nginx:.*master.*\""


/* プロトタイプ */
#ifdef AGENTBOX_NGNXCTL_C
int update_nginx_conf(void);
#else
extern int update_nginx_conf(void);
#endif // AGENTBOX_NGNXCTL_C


#endif /* AGENTBOX_NGNXCTL_H */
