/**
 * @file    abox_macro.h
 * @brief   共通項目 マクロ定義
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_ABOX_MACRO_H
#define AGENTBOX_ABOX_MACRO_H

#include "abox_config.h"
#include "abox_typedefs.h"


/** 未使用パラメタをコンパイラに明示する */
#define ABOX_UNUSED(x) (void)(x);

/** 文字列化汎用マクロ */
#define _stringify(s) #s

/** マクロ展開した結果を文字列にする */
#define stringify(s) _stringify(s)

/** 文字列化されたフルバージョン標記 */
#define PROJ_VERSION_STRING    stringify(PROJ_VERSION_MAJOR) "." \
                               stringify(PROJ_VERSION_MINOR) "." \
                               stringify(PROJ_VERSION_REVISION)

/** 指定した構造体メンバのサイズを取得する */
#define member_size(type, member) sizeof(((type *) 0)->member)


#endif /* AGENTBOX_ABOX_MACRO_H */
