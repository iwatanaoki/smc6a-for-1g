/**
 * @file    logger.h
 * @brief   agentbox ログ関連のプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_LOGGER_H
#define AGENTBOX_LOGGER_H


#include "abox_macro.h"


/** ファイル名(行数) のマクロ展開 */
#define HERE        "(" __FILE__ ":" stringify(__LINE__) ")"

/** @name ログレベルの定義 ログレベルをsyslogと同一に (+超debug・無効化追加) */
/* @{ */
#define LOG_EMERG   0           //!< system is unusable
#define LOG_ALERT   1           //!< action must be taken immediately
#define LOG_CRIT    2           //!< critical conditions
#define LOG_ERR     3           //!< error conditions
#define LOG_WARNING 4           //!< warning conditions
#define LOG_NOTICE  5           //!< normal but significant condition
#define LOG_INFO    6           //!< informational
#define LOG_DEBUG   7           //!< debug-level messages
#define LOG_XDEBUG  8           //!< extreme-debug-level messages
#define LOG_NONE    9           //!< logging is disabled
/* @} */

/** ログファイルポインタをオープンしておくまでの書き込み行数閾値 */
#define LOG_WRITE_LINES_PER_OPEN   2000

/* スレッド用ファイル名 */
#define LOG_FNAME_THREAD_RTSP      "/mtd/coolrevo/var/log/aboxd_rtsp.log" //!< RTSP監視スレッド
#define LOG_FNAME_SENDEVENT        "/mtd/coolrevo/var/log/sendevent.log" //!< sendevent コマンドログファイル


#ifdef AGENTBOX_LOGGER_C
/* プロトタイプ */
int logger_open(void);
int logger_close(void);
int logger_write(unsigned int level, const char* fmt, ...);
#else
extern FILE* applog;
extern FILE* applog_rtsp;
extern unsigned long writecount;
extern unsigned long writecount_rtsp;
extern int logger_open(void);
extern int logger_close(void);
extern int logger_write(unsigned int level, const char* fmt, ...);
#endif /* AGENTBOX_LOGGER_C */


#endif /* AGENTBOX_LOGGER_H */
