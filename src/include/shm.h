/**
 * @file    shm.h
 * @brief   共有メモリ 構造体定義とプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_SHM_H
#define AGENTBOX_SHM_H


#include <linux/limits.h>
#include <time.h>
#include "abox_config.h"
#include "abox_typedefs.h"
#include "ini.h"
#include "shmcpy.h"


/**
 * @struct abox_shm_st
 *
 * プロセスとCGIで設定情報を参照する共有メモリ構造体
 *
 * この構造体にメンバを追加したら、その受け手＆書き出し用のメソッドを持つ
 * shm.c、shmcpy.c、json.c の各メソッドにもメンバ分岐を追加する更新が
 * 必要になるので留意
 */
struct abox_shm_st {
  int           id;                      //!< カメラID
  int           member_id;               //!< 会員ID
  int           group_id;                //!< 拠点ID
  int           user_id;                 //!< サービサID
  int           agentbox_id;             //!< エージェントボックスID
  int           wondergate_id;           //!< wondergateID
  char          auth_uniquekey[BUF_IDENT]; //!< カメラAPI認証用（HASH)キー
  char          uniquekey[BUF_IDENT];    //!< カメラ固有キー
  char          name[BUF_200];           //!< エージェントボックス名称
  int           dhcp;                    //!< DHCP
  int           discover;                //!< ネットワーク探索のタイプ(0なら指定IPで設定、1ならifconfig などで取得してcameras/setip api でアップ）
  ipaddr_t      addr;                    //!< IPアドレス構造体
  char          hwaddr[BUF_HWADDR];      //!< MACアドレス
  char          login[BUF_IDENT];        //!< 本体adminログインID
  char          passwd[BUF_IDENT];       //!< 本体adminログインパスワード
  int           ntpdate;                 //!< NTP使用フラグ
  char          ntpfqdn[BUF_200];        //!< NTPサーバアドレス
  int           layer;                   //!< 階層レベル
  char          group_name[BUF_200];     //!< 階層名称
  char          grouplogin[BUF_IDENT];   //!< 視聴ログインID
  char          grouppasswd[BUF_IDENT];  //!< 視聴ログインパスワード
  char          cloudaddr[BUF_IDENT];    //!< クラウドIPアドレス
  char          clouddir[BUF_128];       //!< クラウド保存フォルダ
  char          cloudlogin[BUF_IDENT];   //!< クラウド保存ログインID
  char          cloudpasswd[BUF_IDENT];  //!< クラウド保存パスワード
  char          parent_name[BUF_200];    //!< 第二階層名称
  char          root_name[BUF_200];      //!< 第一階層名称
  unsigned int  loglevel;                //!< システム設定：ログレベル
  unsigned int  remotehelp;              //!< システム設定：リモートアクセス有効

  int           bitrate;                 //!< 契約ビットレート
  int           preserve;                //!< 契約保存期間
  int           type;                    //!< カメラタイプ
  int           disabled;                //!< サーバ処理一時停止フラグ
  int           nocharged;               //!< 請求対象外フラグ
  int           status;                  //!< 稼働状態
  int           surveil;                 //!< 監視対象フラグ
  int           promotion;               //!< プロモーションの対象有無
  int           recording;               //!< 録画対応フラグ
  char          placed[18];              //!< カメラ設置日
  int           live;                    //!< ライブ対応フラグ
  int           port;                    //!< フォワーディングポート番号
  int           motion;                  //!< 動体検知使用フラグ
  int           ptz;                     //!< PTZ対応フラグ
  char          live_url[BUF_200];       //!< ライブ視聴URL
  char          ptz_tu[BUF_200];         //!< PTZコマンドURL(Tilt Up)
  char          ptz_td[BUF_200];         //!< PTZコマンドURL(Tilt Down)
  char          ptz_pl[BUF_200];         //!< PTZコマンドURL(Pan Left)
  char          ptz_pr[BUF_200];         //!< PTZコマンドURL(Pan Right)
  char          ptz_zi[BUF_200];         //!< PTZコマンドURL(Zoom In)
  char          ptz_zo[BUF_200];         //!< PTZコマンドURL(Zoom Out)
  char          extension[8];            //!< 保存ファイル拡張子
  char          search_path[BUF_200];    //!< 保存ファイル検索パス

  int           ovf_type;                //!< ONVIF 種別
  char          ovf_uri[BUF_200];        //!< ONVIF カメラ接続URI

  unsigned long interval;                //!< ランタイム設定：サーバとの同期間隔(秒)
  char          pidfile[PATH_MAX];       //!< ランタイム設定：PIDファイルパス
  char          shmfile[PATH_MAX];       //!< ランタイム設定：共有メモリIDファイルパス
  char          logfile[PATH_MAX];       //!< ランタイム設定：ログファイルパス
  char          cacert[PATH_MAX];        //!< ランタイム設定：アクティベーション証明書パス
  char          server[BUF_128];         //!< ランタイム設定：サーバ名
  char          smbshare[PATH_MAX];      //!< ランタイム設定：Samba共有ディレクトリのパス
  ipaddr_t      runtime_addr;            //!< ランタイム設定：IPアドレス
  csserver_t    csserver[2];             //!< ランタイム設定：配信サーバ
  char          csurl[BUF_128];          //!< ランタイム設定：視聴システムURL
  char          tunnelurl[BUF_128];      //!< ランタイム設定：tunnel server URL
  sshtunnel_t   sshtunnel[2];            //!< 新方式のSSHTUNNEL接続info
  int           poll_status;             //!< server へのポーリング結果を格納 false/true
  int           activ_status;            //!< アクティべーション結果を格納 false/true
  
  char          version[BUF_IDENT];      //!< camera F/W ver
};
/**
 * @typedef ABOX_SHM
 * abox_shm_stのタイプ定義
 *
 * @sa abox_shm_st
 */
typedef struct abox_shm_st ABOX_SHM;


/* プロトタイプ */
int create_shm(void);
int dispose_shm(void);
int zerowrite_shm(void);
void shm_strval(char*, const char*, size_t);
int shm_intval(const char*);
unsigned int shm_uintval(const char*);

#endif /* AGENTBOX_SHM_H */
