#ifndef BASE64UTL_H
#define BASE64UTL_H

#include <stdio.h>
#include <stdlib.h>

#ifdef BASE64UTL_C
int encode_base64(char*, int, char*, int*);
#else
extern int encode_base64(char*, int, char*, int*);
#endif // BASE64UTL_C

#endif // BASE64UTL_H
