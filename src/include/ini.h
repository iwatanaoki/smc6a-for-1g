/**
 * @file  ini.h
 * @brief windows設定ファイル.iniをパースするライブラリ
 */
#ifndef __INI_H__
#define __INI_H__

#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>


/**
 * パース時のコールバックのタイプ定義
 *
 * @param[in,out] user 値をコピーする対象
 * @param[in]  section .confのセクション部(ブラケット[]で囲まれた箇所)の名称
 * @param[in]     name .confのパラメータ名
 * @param[in]    value .confのパラメータの値
 */
typedef int (*ini_handler)(void* user, const char* section, const char* name, const char* value);

/**
 * fgetsと同様の書式で読み込みを行うためのプロトタイプ
 *
 * @param[in,out] str 読み取った文字列をコピーする対象
 * @param[in]     num 読み込む1行の最大文字数
 * @param[in]  stream 読み込み対象のテキストストリーム
 */
typedef char* (*ini_reader)(char* str, int num, void* stream);

/**
 * .ini型、.conf型のファイルを読み込み、パースする
 *
 * 各行の処理において、名前=値が正常にパースできたら handler がコールバックされる。
 * handlerは成功時には0以外を、失敗時には0を返さなければならない。
 *
 * @par ini型の定義
 *      - ブラケット[]でセクション開始
 *      - 名前=値で各設定値を保持する。オペランド間の空白を許可する。
 *      - コメントの開始は;(semicolon)
 * @par conf型の定義
 *      - ブラケット[]でセクション開始
 *      - 名前=値で各設定値を保持する。オペランド間の空白を許可する。
 *      - コメントの開始は#(number sign)
 *
 * @param[in] filename パースする対象ファイルのパス
 * @param[in]  handler パース時に実行するコールバック
 * @param[in,out] user 値をコピーする対象
 * @retval  0 成功時
 * @retval  n 最初のパースエラーが発生した行番号n
 * @retval -1 ファイルオープンに失敗
 * @retval -2 メモリ確保に失敗
 */
int ini_parse(const char* filename, ini_handler handler, void* user);

/**
 * 設定ファイルをパースする
 *
 * ファイル名の変わりにファイルハンドルを渡して実行する
 *
 * @param[in]     file パースするファイルハンドル
 * @param[in]  handler パース時に実行するコールバック
 * @param[in,out] user 値をコピーする対象
 * @retval  0 成功時
 * @retval  n 最初のパースエラーが発生した行番号n
 * @retval -1 ファイルオープンに失敗
 * @retval -2 メモリ確保に失敗
 */
int ini_parse_file(FILE* file, ini_handler handler, void* user);

/**
 * i設定ファイルをパースする
 *
 *
 * ファイル名の変わりにini_reader型の関数へのポインタを渡して実行する
 *
 * @param[in] reader ini_reader型関数へのポインタ
 * @param[in] stream パースする対象テキストストリーム
 * @param[in] handler パース時に実行するコールバック
 * @param[in,out] user 値をコピーする対象ポインタ
 * @retval 0 成功時
 * @retval n 最初のパースエラーが発生した行番号n
 * @retval -1 ファイルオープンに失敗
 * @retval -2 メモリ確保に失敗
 */
int ini_parse_stream(ini_reader reader, void* stream, ini_handler handler, void* user);


/**
 * @def INI_ALLOW_MULTILINE
 * 複数行に跨がる名前=値の定義を許可する
 * - 0: 許可しない
 * - 0以外: 許可する
 */
#ifndef INI_ALLOW_MULTILINE
#define INI_ALLOW_MULTILINE 0
#endif

/**
 * @def INI_ALLOW_BOM
 * ファイル先頭にUTF-8 BOM(0xEF 0xBB 0xBF)を許可する
 * - 0: 許可しない
 * - 0以外: 許可する
 */
#ifndef INI_ALLOW_BOM
#define INI_ALLOW_BOM 0
#endif

/**
 * @def INI_ALLOW_INLINE_COMMENTS
 * インラインコメントの使用を許可する
 *
 * 許可する場合、INI_INLINE_COMMENT_PREFIXESの定義が必須となる
 * - 0: 許可しない
 * - 0以外: 許可する
 */
#ifndef INI_ALLOW_INLINE_COMMENTS
#define INI_ALLOW_INLINE_COMMENTS 0
#endif

/**
 * @def INI_INLINE_COMMENT_PREFIXES
 * インラインコメントの開始表現に使用される文字を設定する
 */
#ifndef INI_INLINE_COMMENT_PREFIXES
#define INI_INLINE_COMMENT_PREFIXES ";"
#endif

/**
 * @def INI_USE_STACK
 * パース時に使用するワークスペースを定義する
 * - 0: ヒープ(malloc/free)を使用する
 * - 0以外: スタックを使用する
 */
#ifndef INI_USE_STACK
#define INI_USE_STACK 0
#endif

/**
 * @def INI_STOP_ON_FIRST_ERROR
 * 最初のパースエラー発生時に動作を中止するかどうかを定義する
 * - 0: 動作を続行する
 * - 0以外: 動作を中止する
 */
#ifndef INI_STOP_ON_FIRST_ERROR
#define INI_STOP_ON_FIRST_ERROR 0
#endif

/**
 * @def INI_MAX_LINE
 * 設定ファイルの最大長を設定する
 */
#ifndef INI_MAX_LINE
#define INI_MAX_LINE 512
#endif

/* validate preprocess */
#if INI_ALLOW_INLINE_COMMENTS == 1
#ifndef INI_INLINE_COMMENT_PREFIXES
#error When INI_ALLOW_INLINE_COMMENTS, you must define INI_INLINE_COMMENT_PREFIXES
#endif
#endif


#ifdef __cplusplus
}
#endif

#endif /* __INI_H__ */
