/**
 * @file    recorder.h
 * @brief   アナログレコーダ死活監視に関する定義とプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_RECORDER_H
#define AGENTBOX_RECORDER_H


#include "abox_defines.h"


/* プロトタイプ */
int execute_recorders_healthcheck(void);
int get_recorder_doa(int);


#endif /* AGENTBOX_RECORDER_H */
