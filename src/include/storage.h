/**
 * @file    storage.h
 * @brief   内部および外部ストレージの状態取得ユーティリティ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_STORAGE_H
#define AGENTBOX_STORAGE_H


#define DEF_SDCARD_DEVICE       "/dev/mmcblk0p1"
#define DEF_SDCARD_MOUNTPOINT   "/www/avi"

#define SDCARD_MOUNTPOINT_ABNORMAL    -1
#define SDCARD_UNMOUNTED              -2
#define SDCARD_STATVFS_FAILED         -3
#define SDCARD_THR_OVER               -4
#define SDCARD_UNDER_1GB              -5
#define SDCARD_SAFE_WRITABLE           0

/* プロトタイプ */
int get_storage_usage(void);


#endif /* AGENTBOX_STORAGE_H */
