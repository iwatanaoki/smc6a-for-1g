/**
 * @file    ntp.h
 * @brief   時刻同期とその手法決定、サービス起動に関するユーティリティ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_NTP_H
#define AGENTBOX_NTP_H


/** @name set_ntpserver戻り値 */
/* @{ */
#define NTPSERVER_SAVE_SUCCEEDED  0 //!< 正常終了
#define NTPSERVER_SAVE_FAILED    -1 //!< 引数が範囲外
#define NTPSERVER_SAVE_INVALID   -2 //!< FQDN不正のためHTTP参照に変更保存した
/* @} */

/* プロトタイプ */
int set_ntpserver(int, const char*);
int run_timesync(void);

#endif /* AGENTBOX_NTP_H */
