/**
 * @file    abox_config.h
 * @brief   プロジェクト設定
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_ABOX_CONFIG_H
#define AGENTBOX_ABOX_CONFIG_H


/** 非ANSI C strdup(*const char)を使う */
#ifndef __cplusplus__strings__
#define __cplusplus__strings__
#endif


/** シンプルなトンネル生存チェックを行う */
#define AGENTBOX_CFG_TUNNEL_SHARROWTEST  0

/** netstatを使ったトンネル生存チェックを行う */
#define AGENTBOX_CFG_TUNNEL_DEEPTEST     1

/** エージェントボックスに関連付けられる監視カメラ最大数 */
#define AGENTBOX_CFG_MAX_CAMERA_NUM      16

/** エージェントボックスに関連付けられるレコーダ最大数 */
#define AGENTBOX_CFG_MAX_RECORDER_NUM    8

/** エージェントボックスに関連付けられるRTSPカメラ最大数 */
#define AGENTBOX_CFG_MAX_RTSPCAMERA_NUM  16

/** 保存期間の割り付けられる最大数 */
#define SAVESPAN_MAX_NUM                  5

/** ファイル転送・死活確認SSHポート */
#define AGENTBOX_CFG_SSH_PORT            9876

/** RSA秘密鍵再取得までの最大試行失敗回数 */
#define AGENTBOX_CFG_REVOKE_KEY_LIMIT    5

/** Samba共有のルートディレクトリ */
#define AGENTBOX_CFG_SMB_SHARE_ROOTDIR   "/mtd/coolrevo/tmp"

/** A312のネットワーク設定ファイルが置かれるパス */
#define AGENTBOX_CFG_YX_NETWORK_INI_PATH "/root/yx_config_network.ini"

/** A312のNTP設定ファイルが置かれるパス */
#define AGENTBOX_CFG_YX_NTP_INI_PATH     "/root/yx_config_ntp.ini"

/** 共有メモリのファクトリーデフォルト値を保存するパス */
#define AGENTBOX_CFG_SHM_DEFAULT_PATH    "/wondergate/cr/etc/shm.default.ini"

/** 共有メモリの内容をテキストファイルとしてダンプするパス */
#define AGENTBOX_CFG_SHM_STORED_PATH     "/mtd/coolrevo/etc/shm.ini"

/** エージェントボックスのファームウェアバージョンを保存するパス */
#define AGENTBOX_CFG_FIRMVER_PATH        "/mtd/coolrevo/etc/smc6afirmversion"

/** カーネルバージョン文字列を取得する */
#define AGENTBOX_CFG_CMD_KERN_VER        "cat /etc/version|sed -e 's/^Version \\([0-9\\.]\\+\\) \\(.*\\)/\\1/'"

/** 現在のIPアドレスをipで取得する */
//#define AGENTBOX_CFG_CMD_IP              "/sbin/ip -f inet -4 -o addr show eth0|sed -e 's/^.*\\?inet \\([0-9\\.\\/]\\+\\)\\/\\([0-9]\\+\\) brd \\([0-9\\.\\/]\\+\\) \\?scope\\(.*\\)$/\\1 \\2 \\3/g'"
#define AGENTBOX_CFG_CMD_IP              "ifconfig eth0 | grep 'inet addr' | sed 's/^[ ]*inet addr:\\([0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*\\)[ |\\t]*Bcast:\\([0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*\\)[ |\\t]*Mask:\\([0-9]*\\.[0-9]*\\.[0-9]*\\.[0-9]*\\)/\\1 \\3 \\2/'"

/** 現在のルーティングをrouteで取得する */
#define AGENTBOX_CFG_CMD_ROUTE           "route -n|grep 0.0.0.0|grep UG|awk '{print $2}'"

/** ネームサーバをresolv.confから取得する */
#define AGENTBOX_CFG_CMD_RESOLVCONF      "cat /etc/resolv.conf|grep -E 'nameserver (.*)'|awk '{print $2}'|xargs|head -2|sed -e 's/ /,/g'"

/** IP-HWアドレス一覧をarp-scanで取得する */
#define AGENTBOX_CFG_CMD_ARPSCAN         "/mtd/coolrevo/bin/arp-scan -glNqx -Ieth0|grep -E '%s'|awk '{print $2 \",\" $1}'"

/** SMC-3APlus をシャットダウンする */
#define AGENTBOX_CFG_CMD_SHUTDOWN        "/mtd/coolrevo/usr/sbin/aboxshutdown"

/** エージェントボックスを安全に再起動する */
#define AGENTBOX_CFG_CMD_REBOOT          AGENTBOX_CFG_CMD_SHUTDOWN " -r"

/** エージェントボックスを高速再起動する */
#define AGENTBOX_CFG_CMD_FAST_REBOOT     AGENTBOX_CFG_CMD_SHUTDOWN " -f"


/** crontabにジョブを追加する */
#define AGENTBOX_CFG_CMD_ADD_CRONTAB     "/wondergate/cr/usr/bin/tcedit add '%s'"

/** crontabからジョブを削除する */
#define AGENTBOX_CFG_CMD_DEL_CRONTAB     "/wondergate/cr/usr/bin/tcedit del"

/** ホスト名 <-> IPv4アドレスを変換する */
#define AGENTBOX_CFG_CMD_HOST2ADDR       "/mtd/coolrevo/usr/bin/host2addr %s"

/** @name 外部起動アプリケーション・システムファイルの定義 */
/* @{ */
/** @name 外部起動アプリケーション・システムファイルの定義 */
/* @{ */
#define AGENTBOX_CFG_TRANSFER_BIN_PATH   "/mtd/coolrevo/usr/sbin/transfer"    //!< SDカードに録画・保存されたを検索し、クラウドストレージへSCP転送するプログラム
#define AGENTBOX_CFG_TRANSFER_CFG_PATH   "/mtd/coolrevo/etc/transfer.conf.sh" //!< transferが動作するためのパス・アカウント等記述された設定ファイル
#define AGENTBOX_CFG_TRANSFER_PID_PATH   "/mtd/coolrevo/var/run/transfer.pid"    //!< transfer動作時に 自身のPIDを保存するファイル
#define AGENTBOX_CFG_TRANSFER_EXECUTABLE AGENTBOX_CFG_TRANSFER_BIN_PATH " &"   //!< transfer起動用コマンドライン
#define AGENTBOX_CFG_POORNTPD_BIN_PATH   "/mtd/coolrevo/usr/sbin/poorntpd"    //!< HTTPプロトコルで時刻同期を可能にするデーモンプログラム
/* @} */

/** @name APIの定義 */
/* @{ */
#define AGENTBOX_CFG_API_ACTIVATE        "api/cameras/activate"           //!< SMC-3A Plusをアクティブ化し、同期可能とするAPI
#define AGENTBOX_CFG_API_DEACTIVATE      "api/cameras/deactivate"         //!< SMC-3A Plusを非アクティブ化・停止状態にし、データ消去して新規関連付けを可能するAPI
#define AGENTBOX_CFG_API_REACTIVATE      "api/cameras/reactivate"         //!< 取得済み鍵を廃棄・新規取得し、再度同期可能とするAPI
#define AGENTBOX_CFG_API_GETALL          "api/cameras/getall"             //!< 会員情報・カメラとの関連付け・利用可能状態など、Agentbox動作に必要なデータを同期するAPI
#define AGENTBOX_CFG_API_CAMERA_SETIP    "api/cameras/setip"           //!< HWアドレスでネットワーク内検索されたカメラのIPアドレスを変更保存するAPI
#define AGENTBOX_CFG_API_CAMERA_HEALTH   "api/cameras/propsave"        //!< カメラのステータス等、属性値を変更保存するAPI

#define AGENTBOX_CFG_AUTH_TUNNEL_KEY  "api/authtunnel.php" //!< create ssh tunnel cgi
#define AGENTBOX_CFG_CHECK_TUNNEL  "api/checktunnel.php"   //!< check ssh tunnel cgi


#define AGENTBOX_CFG_API_POORNTP_MEPOCH  "api/ntp/microepoch"          //!< 時刻同期用API。マイクロ秒単位までのUNIX時刻を得る

/** aboxd ハートビートファイル */
#define AGENTBOX_HEARTBEAT        "/mtd/coolrevo/tmp/aboxd_heartbeat.txt"
/* @} */

/** @name API関連 */
/* @{ */
#define AGENTBOX_CFG_APIRES_MSG_NOTACTIVATED "Specified SMC-3A Plus cannot found."  //!< GETALLレスポンス非アクティベーション時メッセージ
/* @} */


#endif /* AGENTBOX_ABOX_CONFIG_H */
