/**
 * @file    ipaddr.h
 * @brief   IPアドレスに関するユーティリティのプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_IPADDR_H
#define AGENTBOX_IPADDR_H

#include "abox_typedefs.h"

/** IPアドレスバリデーション用パターン */
#ifndef REGEX_IPV4ADDR
#define REGEX_IPV4ADDR "^((^|\\.)(1[0-9]{2}|[1-9][0-9]|[0-9]|2[0-4][0-9]|25[0-5])){4}$"
#endif

/** ホスト名バリデーション用パターン */
#ifndef REGEX_HOSTNAME
#define REGEX_HOSTNAME "^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\\-]*[a-zA-Z0-9])\\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\\-]*[A-Za-z0-9])$"
#endif

/* プロトタイプ */
#ifdef AGENTBOX_IPADDR_C
int get_smc3a_cgi_ipaddr(void);
int set_smc3a_cgi_ipaddr(void);
int get_abox_ipaddr(void);
int set_abox_ipaddr(void*);
int save_abox_ipaddr(void*);
int get_yx_config(void*);
int get_src_address(void*);
void set_abox_nwsettings(void);
int parse_ifconfig(void*);
char* calc_network(const char*, const char*);
char* calc_broadcast(const char*, const char*);
int cidr2addr(char*, int);
int host2addr(char*, const char*);
int addr2oct(const char*, unsigned int[]);
int bmp2dec(const int[], int[]);
int parse_yx_config(void*, const char*, const char*, const char*);
int print_yx_config(yx_config_t*);
void zerowrite_yx_config(void*);
int match_ipaddr(const char*);
int match_hostaddr(const char*);
void safe_addrcpy(char*, char*, int);
int push_abox_ipaddr(void);
#else
extern int get_smc3a_cgi_ipaddr(void);
extern int set_smc3a_cgi_ipaddr(void);
extern int push_abox_ipaddr(void);
#endif // AGENTBOX_IPADDR_C


#endif /* AGENTBOX_IPADDR_H */
