/**
 * @file    auth.h
 * @brief   agentboxアカウント取得・認証関係のプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_AUTH_H
#define AGENTBOX_AUTH_H


#include "fetch.h"


/** HTTP通信時に名乗るユーザエージェント文字列のテンプレート */
//#define FETCH_USER_AGENT "Aboxd/%s (Linux; armv7l-linux-gnueabi 3.10.0; %s) Agentbox/%s libcurl/" LIBCURL_VERSION
#define FETCH_USER_AGENT "Aboxd/%s (Linux; arm-hisiv500-linux-uclibcgnueabi 3.18.20; %s) SMC-3A_Plus/%s libcurl/" LIBCURL_VERSION

/** 秘密鍵正当性チェック失敗数記録ファイル */
#define ACTIVATION_VERIFY_FAILURES "/tmp/failure_activate"


/* プロトタイプ */
#ifdef AGENTBOX_SSHTUNNEL_C
int sha256_digest(char*, const char*);
int get_hw_addr(char*);
char* get_hw_str(void);
int get_uniquekey(char*);
int activate_abox(const char*);
int force_reactivate_abox(const char*);
int deactivate_abox(void);
int remove_activatedkey(const char*);
void verify_activation(const char*);
int get_api_response(char*, size_t, const char*, const char*);
int get_api_response_code(char*, size_t, const char*, const char*);
int get_api_response_otherserver(char*, size_t, const char*, const char*, const char*);
#else
extern int sha256_digest(char*, const char*);
extern int get_hw_addr(char*);
extern char* get_hw_str(void);
extern int get_uniquekey(char*);
extern int activate_abox(const char*);
extern int force_reactivate_abox(const char*);
extern int deactivate_abox(void);
extern int remove_activatedkey(const char*);
extern void verify_activation(const char*);
extern int get_api_response(char*, size_t, const char*, const char*);
extern int get_api_response_code(char*, size_t, const char*, const char*);
extern int get_api_response_otherserver(char*, size_t, const char*, const char*, const char*);
#endif // AGENTBOX_SSHTUNNEL_C


#endif /* AGENTBOX_AUTH_H */
