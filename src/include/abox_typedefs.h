/**
 * @file    abox_typedefs.h
 * @brief   プロジェクト共通のタイプ定義
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_ABOX_TYPEDEFS_H
#define AGENTBOX_ABOX_TYPEDEFS_H


#include "abox_defines.h"


/**
 * @struct config_st
 * configによって作成される実行パラメタ
 */
struct config_st {
  unsigned long interval;       //!< メイン処理のループ間隔
  const char* pidfile;          //!< PIDファイルのパス
  const char* shmfile;          //!< 共有メモリIDファイルのパス
  const char* logfile;          //!< ログファイルのパス
  const char* cacert;           //!< アクティベーション後取得するSSH鍵ファイルのパス
  const char* server;           //!< 管理システムURL
  const char* kconffile;        //!< SimpleVMS設定ファイルパス
  const char* cnvdir;           //!< HLSファイル変換ディレクトリパス
  const char* csurl;            //!< 配信システムURL
  const char* tunnelurl;        //!< SSH Tunnel ServerURL
  unsigned long schoffset;      //!< ファイル取得スケジュールオフセット(秒)
  unsigned long diskotherthr;   //!< エクスポートパーティション閾値(%)
};
/**
 * @typedef config_t
 * config_stのタイプ定義
 *
 * @sa config_st
 */
typedef struct config_st config_t;

/**
 * @struct tc_st
 * @brief 帯域制御ルールを登録する構造体
 */
struct tc_st {
  int  sort;                    //!< ソート順
  char begin[18];               //!< 開始時刻 00:00:00
  char end[18];                 //!< 終了時刻
  float threshold;              //!< 閾値
};
/**
 * @typedef tc_t
 * tc_stのタイプ定義
 *
 * @sa tc_st
 */
typedef struct tc_st tc_t;

/**
 * @struct ipaddr_st
 * @brief IPアドレスのセットを登録する構造体
 */
struct ipaddr_st {
  int  dhcp;                    //!< DHCPモード
  char ipaddr[BUF_IPADDR];      //!< IPアドレス
  char netmask[BUF_IPADDR];     //!< サブネットマスク
  char broadcast[BUF_IPADDR];   //!< ブロードキャスト
  char gateway[BUF_IPADDR];     //!< デフォルトゲートウェイ
  char dns1[BUF_IPADDR];        //!< ネームサーバ1
  char dns2[BUF_IPADDR];        //!< ネームサーバ2
};
/**
 * @typedef ipaddr_t
 * ipaddr_stのタイプ定義
 *
 * @sa ipaddr_st
 */
typedef struct ipaddr_st ipaddr_t;

/**
 * @struct csserver_st
 * @brief 配信サーバのセットを登録する構造体
 */
struct csserver_st {
  char host[BUF_IPADDR];        //!< IPアドレス
  char prefix[BUF_IPADDR];      //!< 内部区別のためのホスト名
};
/**
 * @typedef csserver_t
 *
 * csserver_stのタイプ定義
 * @sa csserver_st
 */
typedef struct csserver_st csserver_t;

/**
 * @struct camera_st
 * @brief 監視カメラの登録情報を保存する構造体
 *
 * 構造と命名はテーブルカラムに基本準拠する。
 * PTZ用各URIのみフィールド名変えている。
 * エージェントボックス内で利用することのないvarchar,
 * TEXT型のカラム manufacturer, model, remarks
 * および created は更新カラムのブラックリスト扱い
 */
struct camera_st {
  int    id;                     //!< カメラID
  int    member_id;              //!< 会員ID
  int    group_id;               //!< 拠点ID
  int    user_id;                //!< サービサID
  int    wondergate_id;          //!< エージェントボックスID
  char   uniquekey[BUF_IDENT];   //!< カメラ固有キー
  char   name[BUF_200];          //!< カメラ名称
  ipaddr_t addr;                 //!< IPアドレス
  char   hwaddr[BUF_HWADDR];     //!< MACアドレス
  int    bitrate;                //!< 契約ビットレート
  int    preserve;               //!< 契約保存期間
  int    type;                   //!< カメラタイプ
  int    disabled;               //!< サーバ処理一時停止フラグ
  int    nocharged;              //!< 請求対象外フラグ
  int    discover;               //!< ネットワーク探索のタイプ
  int    status;                 //!< 稼働状態
  int    surveil;                //!< 監視対象フラグ
  int    promotion;              //!< プロモーションの対象有無
  int    recording;              //!< 録画対応フラグ
  char   placed[18];             //!< カメラ設置日
  int    live;                   //!< ライブ対応フラグ
  int    port;                   //!< フォワーディングポート番号
  int    motion;                 //!< 動体検知使用フラグ
  int    ptz;                    //!< PTZ対応フラグ
  char   live_url[BUF_200];      //!< ライブ視聴URL
  char   ptz_tu[BUF_200];        //!< PTZコマンドURL(Tilt Up)
  char   ptz_td[BUF_200];        //!< PTZコマンドURL(Tilt Down)
  char   ptz_pl[BUF_200];        //!< PTZコマンドURL(Pan Left)
  char   ptz_pr[BUF_200];        //!< PTZコマンドURL(Pan Right)
  char   ptz_zi[BUF_200];        //!< PTZコマンドURL(Zoom In)
  char   ptz_zo[BUF_200];        //!< PTZコマンドURL(Zoom Out)
  char   extension[8];           //!< 保存ファイル拡張子
  char   search_path[BUF_200];   //!< 保存ファイル検索パス

  int    ovf_type;               //!< ONVIF 種別
  char   ovf_uri[BUF_200];       //!< ONVIF カメラ接続URI
  char   edge_uri[BUF_200];      //!< kunai RTSP repeater 接続URI

  //録画保存設定（クラウド設定）
  int    sch_savingtype;         //!< 録画保存設定
  int    sch_periodtype;         //!< 録画保存期間
  char   sch_weekday[BUF_32];    //!< 録画保存曜日(月～日)(0,0,0,0,0,0,0)
  char   sch_from[BUF_32];       //!< 録画保存開始日時(YYY-MM-DD HH:MM)
  char   sch_to[BUF_32];         //!< 録画保存終了日時(YYY-MM-DD HH:MM)
  //録画保存設定（前回クラウド設定）
  int    sch_pre_savingtype;     //!< 前回設定録画保存設定
  int    sch_pre_periodtype;     //!< 前回設定録画保存期間
  char   sch_pre_weekday[BUF_32];//!< 前回設定録画保存曜日
  char   sch_pre_from[BUF_32];   //!< 前回設定録画保存開始日時
  char   sch_pre_to[BUF_32];     //!< 前回設定録画保存終了日時
  //録画保存設定（実行中）
  time_t sch_rt_from;            //!< 録画開始スケジュール
  time_t sch_rt_to;              //!< 録画終了スケジュール
};
/**
 * @typedef camera_t
 * camera_stのタイプ定義
 *
 * @sa camera_st
 */
typedef struct camera_st camera_t;

/**
 * @struct recorder_st
 * @brief レコーダの登録情報を保存する構造体
 *
 * 構造と命名はテーブルカラムに基本準拠する。
 * エージェントボックス内で利用することのないvarchar,
 * TEXT型のカラム remarks
 * および created は更新カラムのブラックリスト扱い
 */
struct recorder_st {
  int    id;                     //!< カメラID
  int    member_id;              //!< 会員ID
  int    group_id;               //!< 拠点ID
  int    user_id;                //!< サービサID
  int    wondergate_id;          //!< エージェントボックスID
  char   name[BUF_200];          //!< カメラ名称
  ipaddr_t addr;                 //!< IPアドレス
  char   hwaddr[BUF_HWADDR];     //!< MACアドレス
  char   manufacturer[BUF_IDENT];//!< メーカー名
  char   model[BUF_IDENT];       //!< 型番
  int    disabled;               //!< サーバ処理一時停止フラグ
  int    status;                 //!< 稼働状態
};
/**
 * @typedef recorder_t
 * recorder_stのタイプ定義
 *
 * @sa recorder_st
 */
typedef struct recorder_st recorder_t;

/**
 * @struct cameratype_st
 * @brief 対応カメラタイプIDと検索パスパターン
 */
struct cameratype_st {
  int  type;                     //!< タイプID
  char name[BUF_200];            //!< タイプ名
  char path[BUF_200];            //!< 検索パスパターン
  char suffix[8];                //!< 検索拡張子
};
/**
 * @typedef cameratype_t
 * cameratype_stのタイプ定義
 *
 * @sa cameratype_st
 */
typedef struct cameratype_st cameratype_t;

/**
 * @struct msq_st
 * @brief System V IPCメッセージングの構造体
 */
struct msq_st {
  long int type;                 //!< メッセージタイプ
  char msg[BUF_4096];            //!< メッセージ本文
};
/**
 * @typedef msq_t
 * msq_stのタイプ定義
 *
 * @sa msq_st
 */
typedef struct msq_st msq_t;

/**
 * @struct yx_config_st
 * @brief A312ネットワーク設定ファイルの内容を保存する構造体
 */
struct yx_config_st {
  char device_name[BUF_IDENT];      //!< インタフェイス名
  int  network_type;                //!< アドレス付与ルール 0:固定 1:DHCP
  char mac_address[BUF_HWADDR];     //!< HWアドレス
  char ip_address[BUF_IPADDR];      //!< IPアドレス
  char netmask_address[BUF_IPADDR]; //!< サブネットマスク
  char gate_address[BUF_IPADDR];    //!< デフォルトゲートウェイ
  char dns1[BUF_IPADDR];            //!< ネームサーバ1
  char dns2[BUF_IPADDR];            //!< ネームサーバ2
  int  primary;                     //!< IPアドレス
  char ver[18];                     //!< ファイルバージョン？
  int  dhcp_timeout;                //!< DHCPタイムアウト
  char service_name[BUF_128];       //!< PPPoE/PPTPサービス名？
  char username[BUF_IDENT];         //!< サービスユーザ名？
  char passwd[BUF_IDENT];           //!< サービスパスワード？
};
/**
 * @typedef yx_config_t
 * yx_config_stのタイプ定義
 *
 * @sa yx_config_st
 */
typedef struct yx_config_st yx_config_t;

/**
 * @struct sshtunnel_st
 * @brief sshtunnel structure
 */
struct sshtunnel_st {
  char    server_vip[BUF_128];     //!< 新方式のSSHTUNNEL接続サーバーVIP
  int     server_port;             //!< 新方式のSSHTUNNELサーバアサインポート
};
/**
 * @typedef sshtunnel_t
 * sshtunnel_stのタイプ定義
 *
 * @sa sshtunnel_st
 */
typedef struct sshtunnel_st sshtunnel_t;

#endif /* AGENTBOX_ABOX_TYPEDEFS_H */
