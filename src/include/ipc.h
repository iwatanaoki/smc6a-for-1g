/**
 * @file    ipc.h
 * @brief   プロセス間メッセージング管理のプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_IPC_H
#define AGENTBOX_IPC_H


#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>


/** System V IPCメッセージ長 */
#define ABOX_MSGQ_BUF 4096

/** System V IPCメッセージング用キュー */
#define ABOX_MSGQ ((key_t) 2236)

/** メッセージタイプ(msq_stのtypeに指定する) */
/* @{*/
#define ABOX_QTYPE_MSG 892301     //!< コマンド実行用メッセージID
#define ABOX_QTYPE_LOG 892303     //!< ログ保存用メッセージID
/* @} */

/* プロトタイプ */
int msqueue_open(void);
int msqueue_close(void);
int msqueue_send(const char*);
int msqueue_read(void);


#endif /* AGENTBOX_IPC_H */
