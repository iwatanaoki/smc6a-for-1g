/**
 * @file    rtsp.h
 * @brief   ffmpeg によるRTSPカメラへの接続・解除・再接続のための
 * マクロ及びプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_RTSP_H
#define AGENTBOX_RTSP_H

/**
 * ffmpeg->nginx 接続時のsecureToken で使用するシークレットキー
 */
#define RTSP_SECURETOKEN_SECRETKEY "cR-rTsP_key"


/**
 * RTSP接続コマンドテンプレート(RTSPリレー)
 * @par フォーマット指定子
 *      - %%s カメラへのRTSP接続URI
 *      - %%s カメラキー
 *      - %%d securetoken number
 *      - %%s securetoken key
 */
#define RTSP_RELAY_CMD "ffmpeg -y \
 -stimeout 5000000 \
 -fflags +genpts \
 -i %s \
 -c:v copy \
 -map 0:0 \
 -c:a copy \
 -map 0:1 \
 -metadata service_provider='cool.revo' \
 -metadata service_name='smc-cloud.jp LIVE' \
 -f flv rtmp://127.0.0.1:1935/%s/livestream?e=%d&st=%s"

/**
 * RTSP接続コマンドテンプレート(ローカルファイル保存)
 * @par フォーマット指定子
 *      - %%s カメラへのRTSP接続URI
 *      - %%s 保存先ベースでぃれくとり
 *      - %%s ログイン
 *      - %%s カメラキー
 *      - %%s カメラキー
 *      - %%s カメラキー
 */
#define RTSP_RECORDING_CMD "ffmpeg -y \
 -stimeout 5000000 \
 -fflags +genpts \
 -i %s \
 -c:v copy \
 -map 0:0 \
 -c:a copy \
 -map 0:1 \
 -metadata service_provider='cool.revo' \
 -metadata service_name='smc-cloud.jp VOD' \
 -f segment \
 -use_localtime 1 \
 -reset_timestamps 1 \
 -strftime 1 \
 -segment_time 300 \
  \"%s/rtsp/%s/%s_%%Y%%m%%d_%%H%%M%%S.ts\""

/**
 * RTSP接続(ffmpegコマンド）生存チェックコマンドテンプレート(ローカル)
 * @par フォーマット指定子
 *      - %%s カメラきー
 */
#define RTSP_PID_GREP "ps -aef | grep ffmpeg | grep %s | awk '{print $1}'"


//#define SMC3APLUS_LOCAL_RTSP_URL "rtsp://127.0.0.1/"
//#define SMC3APLUS_LOCAL_RTSP_URL "rtsp://admin:admin@127.0.0.1/"
#define SMC3APLUS_LOCAL_RTSP_URL "rtsp://CoolDss:ALTzmngX@127.0.0.1/"
#define LOCAL_RECORDING_TOPDIR "/www/avi"


#ifdef AGENTBOX_RTSP_C
pthread_t  g_pthrid_bkup_rtsp = NULL;
int g_pthrrslt_bkup_rtsp = -1;
int g_is_alive_pthr_bkup_rtsp = FALSE;

/* プロトタイプ */
int open_needed_rtsp(void);
int open_backup_recording_rtsp(void);
int kill_other_camera_rtsp(void);
int killall_recording_rtsp(void);
int killall_rtsp(void);
int getSecureTokenSec(void);
int getSecureTokenKey(int, char*, char*);
int changeOvfUriIfDiscoverByMac(int);
void *bkup_rtsp_execute_check(void*);
#else
extern pthread_t  g_pthrid_bkup_rtsp;
extern int g_is_alive_pthr_bkup_rtsp;
extern int g_pthrrslt_bkup_rtsp;

extern int open_needed_rtsp(void);
extern int open_backup_recording_rtsp(void);
extern int kill_other_camera_rtsp(void);
extern int killall_recording_rtsp(void);
extern int killall_rtsp(void);
extern int getSecureTokenSec(void);
extern int getSecureTokenKey(int, char*, char*);
extern int changeOvfUriIfDiscoverByMac(int);
extern void *bkup_rtsp_execute_check(void*);
#endif // AGENTBOX_RTSP_C


#endif /* AGENTBOX_RTSP_H */
