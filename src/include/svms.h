/**
 * @file    svms.h
 * @brief   SimpleVMSラッパー関数
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_SVMS_H
#define AGENTBOX_SVMS_H


#include "abox_defines.h"
#include "fetch.h"

//クナイAPI URL
#define SVMS_URL_API                    "http://127.0.0.1:80"       //!< API URI
#define SVMS_RESOLVE_API                "localhost:80:127.0.0.1"    //!< API URI for resolved.conf


/** @name 外部起動アプリケーション・システムファイルの定義 */
/* @{ */
#define SVMS_CFG_BIN_PATH               "/wondergate/kn/RequestBOOT.sh"         //!< SimpleVMSプログラム（クナイソフト）
#define SVMS_CFG_PID_PATH               "/wondergate/var/run/svms.pid"          //!< SimpleVMS動作時に自身のPIDを保存するファイル
#define SVMS_CFG_EXECUTABLE              SVMS_CFG_BIN_PATH " &"                 //!< SimpleVMS起動用コマンドライン
#define SVMS_CFG_ST_PATH_BOOTING        "/tmp/simpleVMS/BOOTING"                //!< SimpleVMS起動ステータス:after RequestBOOT and before enabled WebAPI
#define SVMS_CFG_ST_PATH_WORKING        "/tmp/simpleVMS/WORKING"                //!< SimpleVMS起動ステータス:after enabled WebAPI
#define SVMS_CFG_ST_PATH_STOPPING       "/tmp/simpleVMS/STOPPING"               //!< SimpleVMS起動ステータス:after RequestSHUTDOWN and disabled WebAPI
#define SVMS_CFG_ST_PATH_STOPPED        "/tmp/simpleVMS/STOPPED"                //!< SimpleVMS起動ステータス:process Stopped
#define SVMS_CFG_INTERVAL_SVMS          60                                      //!< SimpleVMS監視ファイル更新インターバル（秒）
#define SVMS_CFG_INTERVAL_HEALTHCHECK   10                                      //!< SimpleVMS監視ループインターバル（秒）
#define SVMS_CFG_SVMS_TRIGGER_SVMSCHECK      30                                 //!< SimpleVMS監視ループSVMSステータス待ち周期(秒)
#define SVMS_CFG_SVMS_TRIGGER_HEALTHCHECK    60                                 //!< SimpleVMS監視ループヘルスチェック周期(秒)
#define SVMS_CFG_INTERVAL_SCH           (1000*1000)                             //!< SimpleVMS監視ループインターバル（マイクロ秒）/* @} */
/* @} */

/** @name SVMS状態 */
/* @{ */
#define SVMS_ST_NONE                    0                                       //!< SVMS起動ステータス:起動時、ステータスファイルなし
#define SVMS_ST_MOUNTING                1                                       //!< SVMS起動ステータス:マウント未完了
#define SVMS_ST_BOOTING                 2                                       //!< SVMS起動ステータス:起動処理中
#define SVMS_ST_WORKING                 3                                       //!< SVMS起動ステータス:起動中
#define SVMS_ST_STOPPING                4                                       //!< SVMS起動ステータス:停止処理中
#define SVMS_ST_STOPPED                 5                                       //!< SVMS起動ステータス:停止中
/* @} */

/** @name APIの定義 */
/* @{ */
#define SVMS_CFG_API_BOOT               SVMS_CFG_BIN_PATH                           //!< 起動要求
#define SVMS_CFG_API_SHUTDOWN           "/wondergate/kn/RequestSHUTDOWN.sh"         //!< 終了要求
#define SVMS_CFG_API_FORCESTOP          "/wondergate/kn/ForceSTOP.sh"               //!< 強制終了方法
#define SVMS_CFG_API_GET_CAM_STATUS     "vms/cameras/GetCameraStatus"               //!< 録画状態取得
#define SVMS_CFG_API_EXPRECD_START      "vms/record/RequestSTART_EXPORT_RECORDED"   //!< 録画取得
#define SVMS_CFG_API_RECORDED_REMOVE    "vms/record/RequestREMOVE_RECORDED"         //!< 録画削除
#define SVMS_CFG_API_REC_START          "vms/record/RequestSTART_RECORD"            //!< 録画開始
#define SVMS_CFG_API_REC_STOP           "vms/record/RequestSTOP_RECORD"             //!< 録画停止
#define SVMS_CFG_API_SET_CLD_STATUS     "vms/system/SetCloudStatus"                 //!< 接続状態通知
#define SVMS_CFG_API_DSP_ERR_STATUS     "vms/system/DisplayErrorStatus"             //!< 異常状態表示
/* @} */

/** @name RequestSTART_RECORD */
/* @{ */
#define SVMS_API_RSTART_OK              "OK"                        //!< OK
#define SVMS_API_RSTART_NG              "NG"                        //!< NG
/* @} */

/** @name RequestSTOP_RECORD */
/* @{ */
#define SVMS_API_RSTOP_OK               "OK"                        //!< OK
#define SVMS_API_RSTOP_NG               "NG"                        //!< NG
/* @} */

/** @name GetCameraStatus */
/* @{ */
#define SVMS_API_GCS_RESP_OK            "ok"                        //!< 応答ステータスOK
#define SVMS_API_GCS_RESP_NG            "ng"                        //!< 応答ステータスNG
#define SVMS_API_GCS_STOP               "STOP"                      //!< 停止
#define SVMS_API_GCS_CONNECTING         "CONNECTING"                //!< 接続中
#define SVMS_API_GCS_RECORDING          "RECORDING"                 //!< 録画中
#define SVMS_API_GCS_STOPPING           "STOPPING"                  //!< 停止中
#define SVMS_API_GCS_DISCONNECT         "DISCONNECT"                //!< 切断
/* @} */

/** @name SetCloudStatus */
/* @{ */
#define SVMS_API_SCS_ST_DISCONNECT      "DISCONNECT"                //!< 切断中
#define SVMS_API_SCS_ST_CONNECT         "CONNECT"                   //!< 接続中
#define SVMS_API_SCS_ST_PAUSE           "PAUSE"                     //!< 一時停止
/* @} */

/** @name DisplayErrorStatus */
/* @{ */
#define SVMS_API_DES_ST_QUIET              "quiet"                     //!< 正常／通知消去
#define SVMS_API_DES_ST_ERROR              "error"                     //!< 異常
#define SVMS_API_DES_ST_VERBOSE            "verbose"                   //!< 詳細
#define SVMS_API_DES_ST_INFO               "info"                      //!< 通知
#define SVMS_API_DES_MSG_ACTIVATED         "WonderGate Activated"      //!< アクティベート
#define SVMS_API_DES_MSG_NOTACTIVATED      "WonderGate Not Activated"  //!< 非アクティベート
#define SVMS_API_DES_MSG_ENABLED           "WonderGate Enabled"        //!< 有効
#define SVMS_API_DES_MSG_DISABLED          "WonderGate Disabled"       //!< 一時停止
/* @} */

/** @name 録画保存期間(periodtype) */
/* @{ */
#define SVMS_CLD_PERIOD_DATE            0                           //!< 日時
#define SVMS_CLD_PERIOD_WEEKDAY         1                           //!< 曜日
#define SVMS_CLD_PERIOD_MONTH           2                           //!< 月間
#define SVMS_CLD_PERIOD_YEAR            3                           //!< 年間
/* @} */

/** @name 曜日 */
/* @{ */
#define SVMS_CLD_WEEKDAY_SUN            0                           //!< 日
#define SVMS_CLD_WEEKDAY_MON            1                           //!< 月
#define SVMS_CLD_WEEKDAY_TUE            2                           //!< 火
#define SVMS_CLD_WEEKDAY_WED            3                           //!< 水
#define SVMS_CLD_WEEKDAY_THU            4                           //!< 木
#define SVMS_CLD_WEEKDAY_FRI            5                           //!< 金
#define SVMS_CLD_WEEKDAY_SAT            6                           //!< 土

#define SVMS_CLD_WEEKDAY_ON             1                           //!< 対象
#define SVMS_CLD_WEEKDAY_OFF            0                           //!< 対象外
/* @} */

/** @name svms_get_api_response コンテントタイプ */
/* @{ */
#define SVMS_HDR_CT_DEF                 0                          //!< デフォルト
#define SVMS_HDR_CT_JSON                1                          //!< JSON
/* @} */

/** @name svms_get_camera_status GetCameraStatusの応答カメラ数 */
/** 1要求あたりの最大応答カメラ数 */
/* @{ */
#define SVMS_GCS_CAMERA_NUM             1
/* @} */

/** スケジュールオフセット */
/** To時刻到達時に即API実行すると末尾のデータが取れないため、 */
/** To時刻＋オフセット経過したのちにAPIを実行する */
/* @{ */
#define SVMS_OFFSET_SCH                 30                         //!< 秒
/* @} */
/** 時間範囲を超えた後、末尾のデータの取得が終わってから */
/** スケジュールを終了（又は次の日へ変更）する必要があるため、 */
/** To時刻＋オフセット経過したのちに設定する（SVMS_OFFSET_SCH～5分の範囲） */
/* @{ */
#define SVMS_OFFSET_NEXTSCH             90                         //!< 秒(SVMS_OFFSET_SCH+60)
/* @} */

/** unixtime計算用 */
/* @{ */
#define SVMS_SEC_1_YEAR                 31536000                   //!< 1年(60*60*24*365秒)
#define SVMS_SEC_1_DAY                  86400                      //!< 1日(60*60*24秒)
#define SVMS_SEC_5_MIN                  300                        //!< 5分(60*5秒)
/* @} */

/** transfer出力ファイル */
/* @{ */
#define SVMS_TRNS_LIST_PATH             "/wondergate/cr/var/tmp"                              //!< transfer出力ディレクトリ
#define SVMS_TRNS_LIST_TRNSFERED        SVMS_TRNS_LIST_PATH "/trnsfered_forwg.list"           //!< 転送済みファイルリスト
#define SVMS_TRNS_LIST_TRNSFEREDTMP     SVMS_TRNS_LIST_PATH "/trnsfered_forwg.list.tmp"       //!< 転送済みファイルリスト
#define SVMS_TRNS_LIST_NOTTRANS         SVMS_TRNS_LIST_PATH "/del_nottrnsfer_forwg.list"      //!< 非転送削除ファイルリスト
#define SVMS_TRNS_LIST_NOTTRANSTMP      SVMS_TRNS_LIST_PATH "/del_nottrnsfer_forwg.list.tmp"  //!< 非転送削除ファイルリスト
#define SVMS_TRNS_WAPP_TRNSFERED        SVMS_TRNS_LIST_PATH "/trnsfered_forwg.wapp"       //!< appファイル出力許可
#define SVMS_TRNS_WTRN_TRNSFERED        SVMS_TRNS_LIST_PATH "/trnsfered_forwg.wtrn"       //!< transferファイル出力許可
#define SVMS_TRNS_WAPP_NOTTRANS         SVMS_TRNS_LIST_PATH "/del_nottrnsfer_forwg.wapp"  //!< appファイル出力許可
#define SVMS_TRNS_WTRN_NOTTRANS         SVMS_TRNS_LIST_PATH "/del_nottrnsfer_forwg.wtrn"  //!< transferファイル出力許可
#define SVMS_TRNS_WAIT_WRITE            (500*1000)                                        //!< 書き込み予約監視周期（マイクロ秒）
#define SVMS_TRNS_WAIT_THRESH           120                                               //!< 書き込み予約監視閾値（SVMS_TRNS_WAIT_WRITE回数）
/* @} */

/* SimpleVMS EXPORT時のカメラタイプ毎の出力ディレクトリ */
/* <exportPath>/<uniquekey>/<uniquekey>_YYYYMMDD_HHmmss.ts */
/* <exportPath> = <cnvdir>/<本定義ディレクトリ> */
/* @{ */
#define SVMS_TRANS_DIR_STARVEDIA        "starvedia"                //!< Starvedia
#define SVMS_TRANS_DIR_AXIS             "axis"                     //!< AXIS
#define SVMS_TRANS_DIR_SMC              "smc"                      //!< SMC
#define SVMS_TRANS_DIR_RTSP             "rtsp"                     //!< RTSP
/* @} */

/** transferリスト識別 */
/* @{ */
/* starvedia */
//IPCamRecordFiles/Recording/N@<uniquekey>-<uniquekey>/YYYYMMDD/HHmmSS.crf
#define SVMS_TRNS_STAR_SEARCH       "IPCamRecordFiles/Recording/N@"
/* AXIS */
//axis-<uniquekey>/YYYYMMDD/oo/yyyymmdd_hhnnss_pppp_<uniquekey>/YYYYMMDD_HH/YYYYMMDD_HHmmSS_qqqq.mkv
#define SVMS_TRNS_AXIS_SEARCH       "axis-"
/* SMC */
//IPCamRecordFiles/Recording/<uniquekey>/YYYYMMDD/HHmmss.avi
#define SVMS_TRNS_SMC_SEARCH        "IPCamRecordFiles/Recording/"
/* RTSP */
//rtsp/<uniquekey>/<uniquekey>_YYYYMMDD_HHmmSS.ts
#define SVMS_TRNS_RTSP_SEARCH       "rtsp/"
/* @} */

/** デバイス関連 */
/* @{ */
#define SVMS_DEV_ISMOUNTFLG         "/wondergate/cr/tmp/ismounted_device.flg"  //!< デバイスマウント準備完了フラグ
/* @} */

#define SVMS_WAIT_CAM_LOOP              1000                       //!< カメラ全台ループの軽減(マイクロ秒)
#define SVMS_LEN_YYYYMMDDHHMMSS         19                         //!< strlen(YYYY-MM-DD hh:mm:ss)


/**
 * @struct svms_camera_stat_st
 * @brief 録画状態を保存する構造体
 *
 *　SVMSから取得する録画状態。
 */
struct svms_camera_stat_st {
  int    id;                     //!< カメラID
  char   name[BUF_200];          //!< カメラ名称
  char   uri[BUF_200];           //!< URL
  char   uniquekey[BUF_IDENT];   //!< ユニークキー
  char   status[BUF_64];         //!< カメラステータス
  char   edge_status[BUF_64];    //!< RTSPリピータステータス
  char   export_status[BUF_64];  //!< エクスポート関数ステータス
  char   cameras_err[BUF_64];    //!< カメラエラーステータス
  char   record_err[BUF_64];     //!< 録画エラーステータス
  char   export_err[BUF_64];     //!< エクスポートエラーステータス
};

/**
 * @typedef svms_camera_stat_t
 * svms_camera_stat_stのタイプ定義
 *
 * @sa svms_camera_stat_st
 */
typedef struct svms_camera_stat_st svms_camera_stat_t;

/**
 * @struct svms_resp_camera_stat_st
 * @brief 録画状態取得CGIレスポンス構造体
 *
 *　録画状態取得CGIレスポンス
 */
struct svms_resp_camera_stat_st {
  char status[BUF_32];                                //!< ステータス
  svms_camera_stat_t cam_stat[SVMS_GCS_CAMERA_NUM];   //!< カメラステータス
};

/**
 * @typedef svms_resp_camera_stat_st
 * svms_resp_camera_stat_stのタイプ定義
 *
 * @sa svms_resp_camera_stat_st
 */
typedef struct svms_resp_camera_stat_st svms_resp_camera_stat_t;

/** プロトタイプ */
#ifdef AGENTBOX_SVMS_C
int svms_boot_thread(void);
int svms_boot(void);
int svms_exit(void);
int svms_shutdown(void);
int svms_force_stop(void);
int svms_get_camera_status(char*, svms_resp_camera_stat_t*);
int svms_set_cloud_status(int, int);
int svms_dsp_error_status(char*, char*);
int svms_all_rec_stop(void);
int svms_rec_start(char*);
int svms_rec_stop(char*);
void* svms_execute_healthcheck(void*);
void svms_shutdown_execute_healthcheck(void);
void* svms_check_sch(void*);
int svms_check_trnsfered_forwg_list(int, char*);
int svms_check_del_nottrnsfer_forwg_list(int, char*, int);
int svms_remove_recorded_savingtype(int, char*, time_t);
int svms_remove_recorded(char*, time_t);
int svms_export_recorded_savingtype(int, char*, int, time_t);
int svms_export_recorded(char*, int, time_t, time_t);
void svms_shutdown_check_sch(void);
int svms_get_api_response(char*, size_t, const char*, const char*, int);
int get_sch_range_unixtime(int, time_t, char*, char*, time_t*, time_t*);
int get_sch_rt_range_unixtime(time_t, char*, char*, time_t*, time_t*);
int get_range_base_all(time_t, time_t*, time_t*);
int get_range_base_part(time_t, time_t*, time_t*);
int get_range_base_date(int, time_t, char*, char*, time_t*, time_t*);
int get_range(int, time_t, char*, char*, char*, time_t*, time_t*);
int is_multi_days(int, int, int, int);
int is_weekday_transfer(time_t, char*);
int is_cld_sch_change(void);
void set_cld_sch_change(camera_t*, int, int, char*, char*, char*, int, int, char*, char*, char*);
int IsLeapYear(int);
int GetLastDay(int, int);
int get_unixtime_ymdhms(char*, char*, time_t*);
int chk_expdir_exists(char*);
char *get_strtime(time_t);
#else
extern int svms_boot_thread(void);
extern int svms_boot(void);
extern int svms_exit(void);
extern int svms_shutdown(void);
extern int svms_force_stop(void);
extern int svms_get_camera_status(char*, svms_resp_camera_stat_t*);
extern int svms_set_cloud_status(int, int);
extern int svms_dsp_error_status(char*, char*);
extern int svms_all_rec_stop(void);
extern int svms_rec_start(char*);
extern int svms_rec_stop(char*);
extern void* svms_execute_healthcheck(void*);
extern void svms_shutdown_execute_healthcheck(void);
extern void* svms_check_sch(void*);
extern int svms_check_trnsfered_forwg_list(int, char*);
extern int svms_check_del_nottrnsfer_forwg_list(int, char*, int);
extern int svms_remove_recorded_savingtype(int, char*, time_t);
extern int svms_remove_recorded(char*, time_t);
extern int svms_export_recorded_savingtype(int, char*, int, time_t);
extern int svms_export_recorded(char*, int, time_t, time_t);
extern void svms_shutdown_check_sch(void);
extern int svms_get_api_response(char*, size_t, const char*, const char*, int);
extern int get_sch_range_unixtime(int, time_t, char*, char*, time_t*, time_t*);
extern int get_sch_rt_range_unixtime(time_t, char*, char*, time_t*, time_t*);
extern int get_range_base_all(time_t, time_t*, time_t*);
extern int get_range_base_part(time_t, time_t*, time_t*);
extern int get_range_base_date(int, time_t, char*, char*, time_t*, time_t*);
extern int get_range(int, time_t, char*, char*, char*, time_t*, time_t*);
extern int is_multi_days(int, int, int, int);
extern int is_weekday_transfer(time_t, char*);
extern int is_cld_sch_change(void);
extern void set_cld_sch_change(camera_t*, int, int, char*, char*, char*, int, int, char*, char*, char*);
extern int IsLeapYear(int);
extern int GetLastDay(int, int);
extern int get_unixtime_ymdhms(char*, char*, time_t*);
extern int chk_expdir_exists(char*);
extern char *get_strtime(time_t);
#endif // AGENTBOX_SVMS_C


#endif /* AGENTBOX_SVMS_H */
