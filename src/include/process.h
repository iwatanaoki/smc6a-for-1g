/**
 * @file    process.h
 * @brief   外部コマンド実行とステータス回収
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_PROCESS_H
#define AGENTBOX_PROCESS_H


/* プロトタイプ */
int check_run_process(const char*, const char*);
int check_process(const char*);
int run_system(const char*);
int kill_process(const char*);


#endif /* AGENTBOX_PROCESS_H */
