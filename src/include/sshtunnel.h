/**
 * @file    sshtunnel.h
 * @brief   SSHトンネル接続・解除・再接続のためのマクロ及びプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_SSHTUNNEL_H
#define AGENTBOX_SSHTUNNEL_H


/** トンネル用SSH秘密鍵のパス */
#define SSH_TUNNELLING_KEY "/mtd/coolrevo/etc/tunnel.key"
#define SSH_TUNNELLING_KEY_CERT_PUB "/mtd/coolrevo/etc/tunnel.key-cert.pub"
#define SSH_TUNNELLING_KEY_PUB "/mtd/coolrevo/etc/tunnel.key.pub"

/** トンネル用SSHユーザ */
//#define SSH_TUNNELLING_USR "tunnel"
#define SSH_TUNNELLING_USR "rmtods"

/** トンネル用SSHポート */
#define SSH_TUNNELLING_PORT AGENTBOX_CFG_SSH_PORT
/** **/
//#define SSH_TUNNELLING_MAPPING_PORT  81
#define SSH_TUNNELLING_MAPPING_PORT  80
#define SSH_TUNNELLING_TERMINAL_MAPPING_PORT 10022 

/** トンネル試行失敗数記録ファイル */
#define SSH_TUNNELLING_FAILURES "/tmp/failure_sshtunnel"

/**
 * @name sshトンネル維持のためのテンポラリファイル群
 * @par リスト内容
 *      - 監視カメラのローカルアドレスにフォワードされる用ポート番号
 *      - 監視カメラのローカルIPv4アドレス
 *      - 監視カメラの配信HTTPポート(現状80で決め打ち)
 *      - 配信サーバIPv4アドレス
 * 上記項目が空白で連結されたものが1行になる
 */
/* @{ */
#define TUNNELLING_NEEDED_LIST "/tmp/fwrds.list" //!< プロセス維持必要なカメラ。共有メモリのcamera構造体配列から作成される
#define TUNNELLING_NEEDED_PREV "/tmp/fwrds.prev" //!< 一つ前のチェック時に作成されたfwrds.list
#define TUNNELLING_NEEDED_DIFF "/tmp/fwrds.diff" //!< fwrds.list と fwrds.prev の差分を保存する
/* @} */

/**
 * @name トンネル生存チェックにnetstatを使うかどうかのスイッチ
 */
/* @{ */
#define USE_NETSTAT   1           //!< netstatを使う
#define NOUSE_NETSTAT 0           //!< netstatを使わない
/* @} */

/**
 * SSHトンネル開始コマンドテンプレート
 * @par フォーマット指定子
 *      - %%d SSHポート
 *      - %%s SSH秘密鍵のパス
 *      - %%d フォワーディングポート
 *      - %%s カメラIPアドレス
 *      - %%d カメラポート
 *      - %%s SSHユーザ
 *      - %%s 配信サーバアドレス
 */
#define SSH_TUNNELLING_CMD "ssh -fgnNT -p%d -oLogLevel=quiet -oServerAliveInterval=15 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oPreferredAuthentications=publickey -oIdentitiesOnly=yes -i%s -R %d:*:%d %s@%s"
// test
//#define SSH_TUNNELLING_CMD "ssh -fgnNT -p%d -oLogLevel=quiet -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oPreferredAuthentications=publickey -oIdentitiesOnly=yes -i%s -R %d:*:%d %s@%s"

/**
 * SSHトンネル生存チェックコマンドテンプレート(ローカル)
 * @par フォーマット指定子
 *      - %%d SSHポート
 *      - %%d フォワーディングポート
 *      - %%s カメラIPアドレス
 *      - %%d カメラポート
 *      - %%s SSHユーザ
 *      - %%s 配信サーバアドレス
 */
#define SSH_TUNNELLING_PGREP "pgrep -f \"^ssh -fgnNT -p%d .*R %d:\\*:%d %s@%s\""

/**
 * SSHトンネル生存チェックコマンドテンプレート(リモート)
 * @par フォーマット指定子
 *      - %%d SSHポート
 *      - %%s SSH秘密鍵のパス
 *      - %%s SSHユーザ
 *      - %%s 配信サーバアドレス
 *      - %%d フォワーディングポート
 */
#define SSH_TUNNELLING_CHK "ssh -p%d -oLogLevel=quiet -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oPreferredAuthentications=publickey -oIdentitiesOnly=yes -i%s %s@%s \"netstat -an -A inet|grep -E 'tcp.*:%d.*LISTEN'|grep -v grep\" >/dev/null 2>&1"


#ifdef AGENTBOX_SSHTUNNEL_C
/* プロトタイプ */
int get_tunnelkey(int, int);
int delete_tunnelkey(void);
int store_ssh_tunnels(int);
int open_needed_tunnels(int);
int kill_duplicated_tunnels(void);
int kill_unneeded_tunnels(int);
int killall_tunnels(void);
int check_status_tunnel(int);
#else
extern int get_tunnelkey(int, int);
extern int delete_tunnelkey(void);
extern int store_ssh_tunnels(int);
extern int open_needed_tunnels(int);
extern int kill_duplicated_tunnels(void);
extern int kill_unneeded_tunnels(int);
extern int killall_tunnels(void);
extern int check_status_tunnel(int);
#endif // AGENTBOX_SSHTUNNEL_C


#endif /* AGENTBOX_SSHTUNNEL_H */
