/**
 * @file    cgicommon.h
 * @brief   エージェントボックスWebUIで利用するマクロ定義とプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_CGICOMMON_H
#define AGENTBOX_CGICOMMON_H


#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/msg.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ccgi.h>
#include <ctemplate.h>
#include "abox_config.h"
#include "shm.h"
#include "util.h"
#include "ipaddr.h"
#include "ipc.h"
#include "aboxd.h"

/** message flash用マークアップ */
#define flashfmt "<div class=\"alert alert-%s\">%s</div>"

/* プロトタイプ */
int shmopen(void);
int shmclose(void);
int parse_handler(void*, const char*, const char*, const char*);
int session_write(const char*, const char*);
int session_exists(const char*);
int session_expire(const char*);


#endif /* AGENTBOX_CGICOMMON_H */
