/**
 * @file    fetch.h
 * @brief   libcurlを用いたリモートコンテンツ取得関数のプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_FETCH_H
#define AGENTBOX_FETCH_H


#include <curl/curl.h>
#include "abox_defines.h"


/** @name コンテンツ取得用サイズの定義 */
/* @{ */
#define FETCH_SIZE_LONG_LONG (BUF_131072 * 2)
#define FETCH_SIZE_8xLONG     BUF_131072
#define FETCH_SIZE_4xLONG     BUF_65536
#define FETCH_SIZE_2xLONG     BUF_32768
#define FETCH_SIZE_LONG       BUF_16384
#define FETCH_SIZE_MIDDLE     BUF_8192
#define FETCH_SIZE_SHORT      BUF_4096
#define FETCH_SIZE            BUF_1024
#define FETCH_SIZE_HALF       BUF_512
#define FETCH_SIZE_TINY       BUF_200
#define FETCH_SIZE_MICRO      BUF_128
/* @} */

/**
 * @struct MEMFILE
 * curlによるコンテンツ取得で利用する構造体
 */
typedef struct {
  char*  data;                //!< コンテンツbody
  size_t size;                //!< コンテンツ長
} MEMFILE;

/** プロトタイプ */
MEMFILE* memfopen(void);
void memfclose(MEMFILE*);
size_t memfwrite(char*, size_t, size_t, void*);
char* memfstrdup(MEMFILE*);


#endif /* AGENTBOX_FETCH_H */
