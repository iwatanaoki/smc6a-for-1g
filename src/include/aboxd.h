/**
 * @file    aboxd.h
 * @brief   agentbox メインスレッド マクロ定義及びプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_ABOXD_H
#define AGENTBOX_ABOXD_H


/** @name プログラム名 */
#define ABOXD_PROG_NAME         "aboxd"        //!< このプログラムの名前

/** @name デーモン化無効フラグの定義 */
#define ABOXD_FLAG_NODAEMONIZE  0              //!< デーモン化を無効にし、最前面で起動

/** @name デーモン化有効フラグの定義 */
#define ABOXD_FLAG_DAEMONIZE    1              //!< デーモン化し、プロセスは背面で動作する

/** @name デーモン終了フラグの定義 */
#define ABOXD_FLAG_KILL         2              //!< デーモン終了

/** @name アクティベーション失敗最大カウント */
#define ABOXD_ACTIV_FAIL_MAX    2              //!< アクティベーション失敗最大カウントを超えたらプロセス終了

/** @name メインスレッドのループ間隔に設定可能な最小値 */
#define ABOXD_MIN_LOOP_INTERVAL 60             //!< この値より小さなループ間隔には設定させない

/** @name メインスレッドで使用されるイベントトリガ周期 */
/* @{ */
#define ABOXD_EVT_TRIGGER_WORLD   3600         //!< 最大周期 1時間
#define ABOXD_EVT_TRIGGER_LONG     600         //!< 大周期 10分
#define ABOXD_EVT_TRIGGER_MIDDLE   300         //!< 中周期 5分
#define ABOXD_EVT_TRIGGER_SHORT     60         //!< 小周期 1分
#define ABOXD_EVT_TRIGGER_MICRO     30         //!< 最小周期 30秒
#define ABOXD_EVT_TRIGGER_NANO       5         //!< 最小周期  5秒
/* @} */

/** @name プログラム終了ステータス定義 */
/* @{ */
#define ABOXD_EXIT_NOERR                     0 //!< 正常終了
#define ABOXD_EXIT_DAEMON_NOT_STARTED       -1 //!< -k(--kill)オプションを与えられたが、pidが存在しない（起動していない）
#define ABOXD_EXIT_DAEMON_ALREADY_STARTED   -2 //!< 既にpidが存在している（デーモン起動済）
#define ABOXD_EXIT_ERR_DAEMONIZE            -3 //!< daemonize(1)に失敗
#define ABOXD_EXIT_ERR_SHM_OPEN            -10 //!< 共有メモリの開始に失敗（shmget/shmatの何れかに失敗）
#define ABOXD_EXIT_ERR_PID_OPEN            -11 //!< pidファイルの生成に失敗
#define ABOXD_EXIT_ERR_MSQ_OPEN            -12 //!< メッセージキューの開始に失敗（shmget/shmatの何れかに失敗）
#define ABOXD_EXIT_ERR_MALLOC              -13 //!< malloc 失敗
#define ABOXD_EXIT_ERR_EMPTY_ARGS         -100 //!< 必須パラメタが不足・または空の値が渡された
#define ABOXD_EXIT_ERR_EMPTY_PIDFILE_PATH -101 //!< pidファイルの保存パスに空の値が渡された
#define ABOXD_EXIT_ERR_EMPTY_SHMFILE_PATH -102 //!< 共有メモリidファイルshmの保存パスに空の値が渡された
#define ABOXD_EXIT_ERR_DETACH_SHM         -110 //!< 共有メモリの解放に失敗
#define ABOXD_EXIT_ERR_MSQ_CLOSE          -111 //!< メッセージキューの削除に失敗
#define ABOXD_EXIT_ERR_COULDNT_ACTIVATED  -200 //!< 最大カウントを超えてアクティベーションに失敗
/* @} */

/** @name デフォルトオプション */
/* @{ */
#define ABOXD_DEFAULT_CFG_PATH    "/mtd/coolrevo/etc/aboxd.conf"
#define ABOXD_DEFAULT_PID_PATH    "/mtd/coolrevo/var/run/aboxd.pid"
#define ABOXD_DEFAULT_SHM_PATH    "/mtd/coolrevo/var/run/aboxd.shm"
#define ABOXD_DEFAULT_LOG_PATH    "/mtd/coolrevo/var/log/aboxd.log"
#define ABOXD_DEFAULT_CACERT_PATH "/mtd/coolrevo/root/activated.key"
#define ABOXD_DEFAULT_SERVER_URI  "https://smc-cloud.jp"
#define ABOXD_DEFAULT_CS_URI      "https://cs.smc-cloud.jp"
#define ABOXD_DEFAULT_TUNNEL_URI  "https://smctunnel.treasure-tv.jp"

#define ABOXD_AUTOCONNECT_STATUS "/mtd/coolrevo/tmp/autoConnect.stat"
#define IS_SETSD_FLG             "/mtd/coolrevo/tmp/is_setsd.stat"
#define IS_SD_THR_OVER_FLG       "/mtd/coolrevo/tmp/is_sd_thr_over.stat"
#define IS_SAVESDCARD_FLG        "/mtd/coolrevo/tmp/is_save_sdcard.stat"
#define IS_DISCONNECT_RTSPSERVER_FLG "/mtd/coolrevo/tmp/is_disconnect_rtspserver.stat"
/* @} */

/* プロトタイプ */
#ifndef AGENTBOX_CGICOMMON_H
void sigcatch(int);
int parse_handler(void*, const char*, const char*, const char*);
#endif

#endif /* AGENTBOX_ABOXD_H */
