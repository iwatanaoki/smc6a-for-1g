/**
 * @file    camera.h
 * @brief   監視カメラ 構造体定義とプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_CAMERA_H
#define AGENTBOX_CAMERA_H


#include "abox_typedefs.h"

/** Starvedia製カメラから新規にアップロードされたファイルを検索するためのコマンドテンプレート */
#define FIND_NEWRECORDED_STARVEDIA "find %s/%s/IPCamRecordFiles/Recording/N@%s-%s -name '*.crf' -a -mmin -%d|wc -l"
/** AXIS製カメラから新規にアップロードされたファイルを検索するためのコマンドテンプレート */
#define FIND_NEWRECORDED_AXIS "find %s/%s/axis-%s -name '*.mkv' -a -mmin -%d|wc -l"
/** Hunt製カメラから新規にアップロードされたファイルを検索するためのコマンドテンプレート */
#define FIND_NEWRECORDED_SMC "find %s/%s/IPCamRecordFiles/Recording/%s \\( -name '*.avi' -o -name '*_tmp' \\) -a -mmin -%d|wc -l"
/** RTSPカメラから新規にアップロードされたファイルを検索するためのコマンドテンプレート */
#define FIND_NEWRECORDED_RTSP "find %s/%s/rtsp/%s -name '*.ts' -a -mmin -%d|wc -l"

/* プロトタイプ */
int find_camera_neigh(void);
int set_camera_ipaddr(ipaddr_t*, int);
int push_camera_ipaddr(ipaddr_t*, int);
int execute_camera_healthcheck(void);


#endif /* AGENTBOX_CAMERA_H */
