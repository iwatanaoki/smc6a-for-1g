/**
 * @file    reboot.h
 * @brief   再起動・システム停止他ハード制御
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_REBOOT_H
#define AGENTBOX_REBOOT_H


#ifdef AGENTBOX_REBOOT_C
/* プロトタイプ */
void safe_reboot(void);
#else
extern void safe_reboot(void);
#endif // AGENTBOX_REBOOT_C


#endif //AGENTBOX_REBOOT_H
