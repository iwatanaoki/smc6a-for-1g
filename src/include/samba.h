/**
 * @file    samba.h
 * @brief   smbユーザ処理とconf編集ユーティリティのプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_SAMBA_H
#define AGENTBOX_SAMBA_H


#include "abox_config.h"


/** Samba共有パス生成用 */
#define SMB_SHARED_FULLPATH AGENTBOX_CFG_SMB_SHARE_ROOTDIR "/%s"

/** Sambaアカウント生成時エラーステータス */
/* @{ */
#define SMB_CREATE_SMBD_ACCOUNT_ERR 101  //!< Sambaユーザの作成(pdbedit)に失敗
#define SMB_CREATE_UNIX_ACCOUNT_ERR 102  //!< Linuxユーザの作成(adduser)に失敗
#define SMB_CREATE_BOTH_ACCOUNT_ERR 103  //!< Sambaユーザ・Linuxユーザの作成両方(pdbedit/adduser)に失敗
/* @} */


/** @name Samba共有先 */
/* @{ */
/* @} */

/* プロトタイプ */
int prepare_transfer(void);

#endif /* AGENTBOX_SAMBA_H */

