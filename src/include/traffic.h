/**
 * @file    traffic.h
 * @brief   smbユーザ処理とconf編集ユーティリティのプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_TRAFFIC_H
#define AGENTBOX_TRAFFIC_H


/* プロトタイプ */
int enable_tc_cronjobs(void);
int disable_tc_cronjobs(void);
int enable_tc(double, double, double);
int disable_tc(void);


#endif /* AGENTBOX_TRAFFIC_H */
