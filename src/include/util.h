/**
 * @file    util.h
 * @brief   ごった煮ユーティリティ関数群のプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_UTIL_H
#define AGENTBOX_UTIL_H


#include <time.h>


/* プロトタイプ */
extern int md5_digest_bin(unsigned char*, int*, const char*);
extern int md5_digest(char*, const char*);
extern int atoi_safe(const char*);
extern float atof_safe(const char*);
extern char* strcpy_safe(char*, const char*);
extern char* date_rfc2822(time_t);
extern char* get_abox_version(void);
extern char* get_abox_version_long(void);
extern void str_replace(const char*, const char*, char*);
extern int verify_pem(const char*);
extern int validate_pem(const char*, const char*);
extern int add_failcounter(const char*);


#endif /* AGENTBOX_UTIL_H */
