/**
 * @file    shmcpy.h
 * @brief   共有メモリ入出力ユーティリティのプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_SHMCPY_H
#define AGENTBOX_SHMCPY_H


/* プロトタイプ */
int load_shm(void);
int parse_shm_ini(void*, const char*, const char*, const char*);
int print_shm(const char*);
int print_kunai_json(const char*);


#endif /* AGENTBOX_SHMCPY_H */
