/**
 * @file    json.h
 * @brief   libjanssonを使用した共有メモリユーティリティのプロトタイプ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_JSON_H
#define AGENTBOX_JSON_H


#include <jansson.h>
#include "svms.h"


/* プロトタイプ */
int json2shm(const char*);
int json_simple_val(char*, const char*, const char*);
int json2camstat(const char*, svms_resp_camera_stat_t*);

#endif /* AGENTBOX_JSON_H */
