/**
 * @file    ipaddr_p.h
 * @brief   IPアドレスユーティリティのプライベートヘッダ
 * @author  cool-revo inc.
 */
#ifndef AGENTBOX_IPADDR_P_H
#define AGENTBOX_IPADDR_P_H

#include <sys/socket.h>
#include <netinet/in.h>


int getipv4addr(char*, const char*);
int setipv4addr(const char*, const char*);
int getsubnetmask(char*, const char*);
int setsubnetmask(const char*, const char*);
int getbroadcast(char*, const char*);
int setbroadcast(const char*, const char*);
int setroute(const char*, const char*, const char*);
int setgateway(const char*, const char*);
int emptyroutes(const char*);
void getipv6addr(char*, const char*);

#endif /* AGENTBOX_IPADDR_P_H */
