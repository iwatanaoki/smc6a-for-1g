#!/bin/bash
##
## files sync for wondergate new firmfile.
## Please execute this script as root.
##

#TRGUNITSSHKEY=/home/vms/8b18.key
TRGUNITSSHKEY=/home/rmtods/SMC-3A_172A.key
#TRGUNITSSH="root@10.1.11.109"
TRGUNITSSH="root@10.1.11.128"
SSHPORT="-p 10022"
SCPPORT="-P 10022"
SSHOPT="-oServerAliveInterval=15 -oStrictHostKeyChecking=no -oUserKnownHostsFile=/dev/null -oPreferredAuthentications=publickey -oIdentitiesOnly=yes"

FROMTOPDIR=/home/rmtods/hunt_crosscompiles/src
TOFIRMTOPDIR=/firminstruct
TOPACKAGETOPDIR=/home/rmtods/hunt_crosscompiles/target
TOUNITTOPDIR=

CPUNIT=0
NODUMMY=0
HELP=0
for i in $*
do
  case "${i}" in
    "-u")
      CPUNIT=1 ;;
    "-nd")
      NODUMMY=1 ;;
    -h*)
      HELP=1 ;;
    *)
      ;;
  esac
done

if [ ${HELP} -eq 1 ]; then
  echo "Usage: `basename $0` [-u -nd -help]"
  echo "   -help: this messsage."
  echo "   -u   : transfer to wondergate01."
  echo "   -nd  : without dummy modules."
  exit 1
fi

CNT=1
FROMFILES[${CNT}]="${FROMTOPDIR}/aboxd/aboxd"
TOFIRMFILES[${CNT}]="${TOPFIRMTOPDIR}/coolrevo/usr/sbin/aboxd"
TOPACKAGEFILES[${CNT}]="${TOPPACKAGETOPDIR}/usr/sbin/aboxd"
TOUNITFILES[${CNT}]="${TOPUNITTOPDIR}/coolrevo/usr/sbin/aboxd"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/getshm/getshm"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/getshm"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/getshm"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/getshm"
ISDUMMY[${CNT}]=0

#CNT=`expr ${CNT} + 1`
#FROMFILES[${CNT}]="${FROMTOPDIR}/nohup/nohup"
#TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/nohup"
#TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/nohup"
#TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/nohup"
#ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/bin/run-parts"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/run-parts"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/run-parts"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/run-parts"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/cr_nslookup/cr_nslookup"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/cr_nslookup"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/cr_nslookup"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/cr_nslookup"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/ntpd/poorntpd"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/poorntpd"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/poorntpd"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/poorntpd"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/ffcheck/ffcheck"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/ffcheck"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/ffcheck"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/ffcheck"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/sendevent/sendevent"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/sendevent_org"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/sendevent_org"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/sendevent_org"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/respawnd.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/respawnd.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/respawnd.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/respawnd.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/autortspstatus_check.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/autortspstatus_check.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/autortspstatus_check.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/autortspstatus_check.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/cragent_update.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/cragent_update.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/cragent_update.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/cragent_update.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/aboxdhbcheck.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/aboxdhbcheck.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/aboxdhbcheck.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/aboxdhbcheck.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/fwupdate_check"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/fwupdate_check"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/fwupdate_check"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/fwupdate_check"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/transfer"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/transfer"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/transfer"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/transfer"
ISDUMMY[${CNT}]=0

#CNT=`expr ${CNT} + 1`
#FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/rtspserver_alivecheck"
#TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/rtspserver_alivecheck"
#TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/rtspserver_alivecheck"
#TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/rtspserver_alivecheck"
#ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/aboxshutdown"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/aboxshutdown"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/aboxshutdown"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/aboxshutdown"
ISDUMMY[${CNT}]=0


CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/cachefree.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/cachefree.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/cachefree.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/cachefree.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/reloadstunnelcf.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/reloadstunnelcf.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/reloadstunnelcf.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/reloadstunnelcf.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/regularyremove.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/regularyremove.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/regularyremove.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/regularyremove.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/sbin/loguploader.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/sbin/loguploader.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/sbin/loguploader.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/sbin/loguploader.sh"
ISDUMMY[${CNT}]=0


CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/bin/lsof"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/lsof"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/lsof"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/lsof"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/bin/autoConnect.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/autoConnect.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/autoConnect.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/autoConnect.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/bin/autoConnectStatus.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/autoConnectStatus.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/autoConnectStatus.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/autoConnectStatus.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/bin/saveSDcard.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/saveSDcard.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/saveSDcard.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/saveSDcard.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/bin/saveSDcardStatus.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/saveSDcardStatus.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/saveSDcardStatus.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/saveSDcardStatus.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/executable/bin/sendevent"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/usr/bin/sendevent"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/usr/bin/sendevent"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/usr/bin/sendevent"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/stunnel.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/stunnel/stunnel.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/stunnel/stunnel.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/stunnel/stunnel.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/stunnel.pem"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/stunnel/stunnel.pem"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/stunnel/stunnel.pem"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/stunnel/stunnel.pem"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/sshd_config"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/sshd_config"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/sshd_config"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/sshd_config"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/aboxd.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/aboxd.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/aboxd.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/aboxd.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/rc.local"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/rc.local"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/rc.local"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/rc.local"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/pre_exec.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/pre_exec.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/pre_exec.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/pre_exec.sh"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/autoConnect.stat"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/tmp/autoConnect.stat"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/tmp/autoConnect.stat"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/tmp/autoConnect.stat"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.d/aboxd.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.d/aboxd.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.d/aboxd.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.d/aboxd.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.d/application.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.d/application.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.conf.d/application.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.d/application.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.d/transfer.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.d/transfer.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.conf.d/transfer.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.d/transfer.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.d/sendevent.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.d/sendevent.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.conf.d/sendevent.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.d/sendevent.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.d/reboot.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.d/reboot.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.conf.d/reboot.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.d/reboot.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.d/sshd.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.d/sshd.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.conf.d/sshd.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.d/sshd.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.d/stunnel.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.d/stunnel.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.conf.d/stunnel.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.d/stunnel.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/logrotate.d/nginx.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/logrotate.d/nginx.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/logrorate.conf.d/nginx.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/logrotate.d/nginx.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/nginx.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/conf/nginx.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/conf/nginx.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/conf/nginx.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/conf.d/localhost.conf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/conf/conf.d/localhost.conf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/conf/conf.d/localhost.conf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/conf/conf.d/localhost.conf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/root"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/var/spool/cron/root"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/coolrevo/var/spool/cron/root"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/var/spool/cron/root"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/cron.minutely/logrotate"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/cron.minutely/logrotate"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/cron.minutely/logrotate"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/cron.minutely/logrotate"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/cron.minutely/respawnd"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/cron.minutely/respawnd"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/cron.minutely/respawnd"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/cron.minutely/respawnd"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/cron.minutely/aboxdhbcheck"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/cron.minutely/aboxdhbcheck"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/cron.minutely/aboxdhbcheck"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/cron.minutely/aboxdhbcheck"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/cron.hourly/cachefree"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/cron.hourly/cachefree"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/cron.hourly/cachefree"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/cron.hourly/cachefree"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/cron.hourly/regularyremove"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/cron.hourly/regularyremove"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/cron.hourly/regularyremove"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/cron.hourly/regularyremove"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/cron.daily/reloadstunnelcf"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/cron.daily/reloadstunnelcf"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/cron.daily/reloadstunnelcf"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/cron.daily/reloadstunnelcf"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/.profile"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/root/.profile"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/.profile"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/root/.profile"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/.profile"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/etc/.profile"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/etc/.profile"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/etc/.profile"
ISDUMMY[${CNT}]=0

CNT=`expr ${CNT} + 1`
FROMFILES[${CNT}]="${FROMTOPDIR}/file/env.sh"
TOFIRMFILES[${CNT}]="${TOFIRMTOPDIR}/coolrevo/env.sh"
TOPACKAGEFILES[${CNT}]="${TOPACKAGETOPDIR}/env.sh"
TOUNITFILES[${CNT}]="${TOUNITTOPDIR}/coolrevo/env.sh"
ISDUMMY[${CNT}]=0

LEN=${CNT}

exec_filesync() {
  FROM=$1
  TO1=$2
  TO2=$3
  TOUNIT=$4
  ISDUMMY=$5

  if [ "${FROM}" = "" -o "${TO1}" = "" -o "${TO2}" = "" ]; then
    echo "parametar not set, failed."
    return;
  elif [ ! -e ${FROM} -o ! -f ${FROM} ]; then
    echo "${FROM} is not found or not file."
    return;
  fi

  TO1DIR=`dirname ${TO1}`
  TO2DIR=`dirname ${TO2}`
  TOUNITDIR=`dirname ${TOUNIT}`

  if [ ! -d ${TO1DIR} ]; then
    echo "${TO1DIR} is not found, mkdir."
    mkdir -p ${TO1DIR}
  fi

  if [ ! -d ${TO2DIR} ]; then
    echo "${TO2DIR} is not found, mkdir."
    mkdir -p ${TO2DIR}
  fi

  if [ ${CPUNIT} -eq 1 ]; then
    if [ "${TOUNIT}" != "" ]; then
      if [ -f ${TRGUNITSSHKEY} -a "${TRGUNITSSH}" != "" ]; then
        ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} test -d ${TOUNITDIR}
        STAT=$?
        if [ ${STAT} -ne 0 ]; then
          echo "${TOUNITDIR} is not found, mkdir."
          ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} mkdir -p ${TOUNITDIR}
        fi
      fi
    fi
  fi

  FROMMD5=`md5sum ${FROM} | awk '{print $1}'`
  TO1MD5=""
  TO2MD5=""
  TOUNITMD5=""
  FROMTS=`stat -c %Z ${FROM}`
  TO1TS=""
  TO2TS=""
  TOUNITTS=""
  TO1STAT=""
  TO2STAT=""
  TOUNITSTAT=""
  if [ -f ${TO1} ]; then
    TO1MD5=`md5sum ${TO1} | awk '{print $1}'`
    TO1TS=`stat -c %Z ${TO1}`
    if [ "${FROMMD5}" != "${TO1MD5}" ]; then
      TO1STAT="DIFF"
    fi
    if [ ${FROMTS} -gt ${TO1TS} ]; then
      TO1STAT="OLD"
    else
      if [ "${TO1STAT}" = "" ]; then
        TO1STAT=""
      fi
    fi
  else
      TO1STAT="NONE"
  fi
  if [ -f ${TO2} ]; then
    TO2MD5=`md5sum ${TO2} | awk '{print $1}'`
    TO2TS=`stat -c %Z ${TO2}`
    if [ "${FROMMD5}" != "${TO2MD5}" ]; then
      TO2STAT="DIFF"
    fi
    if [ ${FROMTS} -gt ${TO2TS} ]; then
      TO2STAT="OLD"
    else
      if [ "${TO2STAT}" = "" ]; then
        TO2STAT=""
      fi
    fi
  else
      TO2STAT="NONE"
  fi
  if [ ${CPUNIT} -eq 1 ]; then
    if [ "${TOUNIT}" != "" ]; then
      if [ -f ${TRGUNITSSHKEY} -a "${TRGUNITSSH}" != "" ]; then
        ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} test -f ${TOUNIT}
        STAT=$?
        if [ ${STAT} -eq 0 ]; then
          TOUNITMD5=`ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} md5sum ${TOUNIT} | awk '{print $1}'`
          TOUNITTS=`ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} stat -c %Z ${TOUNIT}`
          if [ "${FROMMD5}" != "${TOUNITMD5}" ]; then
            TOUNITSTAT="DIFF"
          fi
          if [ ${FROMTS} -gt ${TOUNITTS} ]; then
            TOUNITSTAT="OLD"
          else
            TOUNITSTAT=""
          fi
        else
            TOUNITSTAT="NONE"
        fi
      fi
    fi
  fi
  echo "ISDUMMY:${ISDUMMY}, NODUMMY:${NODUMMY}"
  echo "FROM:${FROMMD5} ${FROMTS}"
  echo "TO1 :${TO1MD5} ${TO1TS} ${TO1STAT}"
  echo "TO2 :${TO2MD5} ${TO2TS} ${TO2STAT}"
  echo "TOUN:${TOUNITMD5} ${TOUNITTS} ${TOUNITSTAT}"

  if [ "${TO1STAT}" != "" -a \( ${ISDUMMY} -eq 0 -o \( ${ISDUMMY} -eq 1 -a ${NODUMMY} -ne 1 \) \) ]; then
    echo "cp -p ${FROM} ${TO1}"
    cp -p ${FROM} ${TO1}
    chown root:root ${TO1}
  fi
  if [ "${TO2STAT}" != "" -a \( ${ISDUMMY} -eq 0 -o \( ${ISDUMMY} -eq 1 -a ${NODUMMY} -ne 1 \) \) ]; then
    echo "cp -p ${FROM} ${TO2}"
    cp -p ${FROM} ${TO2}
    chown root:root ${TO2}
  fi
  if [ "${TOUNITSTAT}" != "" -a \( ${ISDUMMY} -eq 0 -o \( ${ISDUMMY} -eq 1 -a ${NODUMMY} -ne 1 \) \) ]; then
    if [ ${CPUNIT} -eq 1 ]; then
      if [ -f ${TRGUNITSSHKEY} -a "${TRGUNITSSH}" != "" ]; then
        echo "scp ${SCPPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${FROM} ${TRGUNITSSH}:${TOUNIT}"
        scp ${SCPPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${FROM} ${TRGUNITSSH}:${TOUNIT}
        STAT=$?
        if [ ${STAT} -ne 0 ]; then
          PRGNAME=`basename ${TOUNIT}`
          echo "ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} killall -KILL ${PRGNAME}"
          ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} killall -KILL ${PRGNAME}
          scp ${SCPPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${FROM} ${TRGUNITSSH}:${TOUNIT}
        else
          ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} chown root:root ${TOUNIT}
        fi
      fi
    fi
  fi

  if [ ${ISDUMMY} -eq 1 -a ${NODUMMY} -eq 1 ]; then
    if [ -e ${TO1} ]; then
      echo "rm -f ${TO1}"
      rm -f ${TO1}
    fi
    if [ -e ${TO2} ]; then
      echo "rm -f ${TO2}"
      rm -f ${TO2}
    fi
    if [ ${CPUNIT} -eq 1 ]; then
      if [ -f ${TRGUNITSSHKEY} -a "${TRGUNITSSH}" != "" ]; then
        echo "ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} test -f ${TOUNIT}"
        ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} test -f ${TOUNIT}
        STAT=$?
        if [ ${STAT} -eq 0 ]; then
          echo "ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} rm -f ${TOUNIT}"
          ssh ${SSHPORT} ${SSHOPT} -i ${TRGUNITSSHKEY} ${TRGUNITSSH} rm -f ${TOUNIT}
        fi
      fi
    fi
  fi
}


CNT=1
while :
do
  echo "${CNT}: ${FROMFILES[${CNT}]}"
  
  exec_filesync ${FROMFILES[${CNT}]} ${TOFIRMFILES[${CNT}]} ${TOPACKAGEFILES[${CNT}]} ${TOUNITFILES[${CNT}]} ${ISDUMMY[${CNT}]}
  CNT=`expr ${CNT} + 1`
  if [ ${CNT} -gt ${LEN} ]; then
    break;
  fi
done

exit 0
